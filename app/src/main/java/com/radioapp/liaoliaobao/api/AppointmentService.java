package com.radioapp.liaoliaobao.api;

import com.jaydenxiao.common.basebean.BaseRespose;
import com.radioapp.liaoliaobao.bean.address.AddressJsonBean;
import com.radioapp.liaoliaobao.bean.appointment.CareerBean;
import com.radioapp.liaoliaobao.bean.appointment.DatingHopesBean;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * 功能： 数据类的接口
 * 描述
 * Created by yue on 2019-07-17
 */
public interface AppointmentService {


    /**
     * 约会期望列表
     *
     * @return
     */
    @GET("appointment/datingHopes")
    Observable<BaseRespose<List<DatingHopesBean>>> getDatingHopesList();

    /**
     * 职业列表
     *
     * @return
     */
    @GET("appointment/careers")
    Observable<BaseRespose<List<CareerBean>>> getCareerList();


    /**
     * 城市列表
     */
    @GET("appointment/regions")
    Observable<BaseRespose<List<AddressJsonBean>>> getRegionsList();
}
