package com.radioapp.liaoliaobao.api;

import android.support.v4.util.ArrayMap;

import com.jaydenxiao.common.basebean.BaseRespose;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.radio.CommentBean;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;


import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-27
 */
public interface RadioService {

    /**
     * 发布广播（电台）
     *
     * @param params  参数列表
     * @param fileMap 文件列表
     * @return
     */
    @Multipart
    @POST("Broadcasts/release")
    Observable<BaseRespose<String>> radioPublish(@QueryMap ArrayMap<String, Object> params,
                                                 @PartMap ArrayMap<String, RequestBody> fileMap);

    /**
     * 广播列表
     *
     * @param gender
     * @param city_id
     * @param province_id
     * @param page
     * @return
     */
    @GET("Broadcasts/broadcastsList")
    Observable<BaseRespose<PageBean<RadioListBean>>> getBroadcastsList(@Query("gender") Integer gender, @Query("city_id") Integer city_id,
                                                                       @Query("province_id") Integer province_id, @Query("page") Integer page);


    /**
     * 广播详情
     *
     * @param broadcastId
     * @return
     */
    @GET("Broadcasts/broadcastInfo/{broadcast_id}")
    Observable<BaseRespose<RadioListBean>> getBroadcastInfo(@Path("broadcast_id") Integer broadcastId);


    /**
     * 删除广播
     *
     * @return
     */
    @POST("Broadcasts/delBroadcast")
    Observable<BaseRespose<String>> delBroadcast();

    /**
     * 点赞
     *
     * @param broadcastsId
     * @return
     */
    @GET("Broadcasts/like/{broadcasts_id}")
    Observable<BaseRespose<String>> broadcastsLike(@Path("broadcasts_id") Integer broadcastsId);

    /**
     * 阅读申请相册
     */
    @POST("Broadcasts/readImg/{dating_applies_id}")
    Observable<BaseRespose<String>> broadcastsReadImg(@Path("dating_applies_id") Integer datingAppliesId);

    /**
     * 约会申请
     *
     * @param broadCaseId 广播id
     * @return
     */
    @Multipart
    @POST("Broadcasts/datingApply/{broadcast_id}")
    Observable<BaseRespose<String>> broadcastsDatingApply(@Path("broadcast_id") Integer broadCaseId, @Part MultipartBody.Part file);

    /**
     * 申请通过
     *
     * @param friendId
     * @return
     */
    @POST("Broadcasts/pass/{friend_id}")
    Observable<BaseRespose<String>> brodcastsPass(@Path("friend_id") Integer friendId);


    /**
     * 广播评论
     *
     * @param broadCaseid
     * @param text
     * @return
     */
    @POST("Comments/addBroadcast/{broadcast_id}/0")
    Observable<BaseRespose<String>> addBroadcaseComment(@Path("broadcast_id") Integer broadCaseid, @Query("text") String text);


    /**
     * 阅后即焚
     *
     * @param UserId
     * @param mediaId
     * @return
     */
    @GET("users/mediaViewe/{user_id}/{media_id}")
    Observable<BaseRespose<String>> mediaViewe(@Path("user_id") Integer UserId, @Path("media_id") String mediaId);

    /**
     * 红包支付
     *
     * @param mediaId
     * @param payMethod
     * @return
     */
    @POST("Orders/redalbumPay/{media_id}")
    Observable<BaseRespose<PayBean>> redalbumPay(@Path("media_id") Integer mediaId, @Query("pay_method") Integer payMethod);


    /**
     * 获取自己的广播
     *
     * @return
     */
    @GET("Broadcasts/broadcastInfo")
    Observable<BaseRespose<RadioListBean>> getUserBroadcastInfo();

    /**
     * 广播评论列表
     *
     * @param broadcast_id
     * @return
     */
    @GET("Comments/broadcastCommentList/{broadcast_id}")
    Observable<BaseRespose<PageBean<CommentBean>>> getbroadcaseCommentList(@Path("broadcast_id") Integer broadcast_id);

    /**
     * 删除评论
     *
     * @param comment_id
     * @return
     */
    @POST("Comments/delBroadcast/{comment_id}")
    Observable<BaseRespose<String>> delComment(@Path("comment_id") Integer comment_id);

    /**
     * 付款后发布
     * @param payMethod
     * @return
     */
    @POST("Orders/broadcastPay")
    Observable<BaseRespose<PayBean>> broadcastPay( @Query("pay_method") Integer payMethod);

}
