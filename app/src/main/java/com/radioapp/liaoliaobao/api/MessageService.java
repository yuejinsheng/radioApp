package com.radioapp.liaoliaobao.api;


import com.jaydenxiao.common.basebean.BaseRespose;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.message.MessageListBean;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * 功能：消息
 * 描述
 * Created by yue on 2019-09-09
 */
public interface MessageService {

    /**
     * 获取系统消息
     *
     * @param page
     * @return
     */
    @GET("Notifications/systemMsg")
    Observable<BaseRespose<PageBean<MessageListBean>>> getSystemMsg(@Query("page") Integer page);

    /**
     * 消息列表
     *
     * @param type 消息列表 1付费查看好友联系方式,2付费查看好友相册,3好友申请查看详情页,4通过查看详情页, 5广播报名约会,6广播约会通过,7付费查看红包照片,8广播违规被删除,9申请提现到账, 10老用户邀请码申请成功,11图片违规被删除,12认证通过,13用户邀请码申请成功
     * @param page
     * @return
     */
    @GET("Notifications/notificationsList")
    Observable<BaseRespose<PageBean<MessageListBean>>> getMessageList(@Query("type") String type, @Query("page") Integer page);

    /**
     * 只曲一个数据
     * @param type
     * @return
     */
    @GET("Notifications/notificationsList?page=1&per_page=1")
    Observable<BaseRespose<PageBean<MessageListBean>>> getMessageList(@Query("type") String type);


    @GET("users/passApplyMyInfo/{from_user_id}/{n_id}")
    Observable<BaseRespose<String>> passAppLy(@Path("from_user_id") String from_user_id, @Path("n_id") String n_id);

}
