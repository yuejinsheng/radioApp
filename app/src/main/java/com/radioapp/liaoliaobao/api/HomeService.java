package com.radioapp.liaoliaobao.api;


import android.support.v4.util.ArrayMap;

import com.jaydenxiao.common.basebean.BaseRespose;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.home.CheckNumberOfViewBean;
import com.radioapp.liaoliaobao.bean.home.FriendContactBean;
import com.radioapp.liaoliaobao.bean.home.GenderBean;
import com.radioapp.liaoliaobao.bean.home.UserAlbumBean;


import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-06
 */
public interface HomeService {


    /**
     * 用户列表（首页数据）
     * @param params
     * @return
     */
    @GET("users/usersList")
    Observable<BaseRespose<PageBean<GenderBean>>> getUsersLit(@QueryMap ArrayMap<String,Object> params);


    /**
     * 用户点赞
     * @param favoriteable_id
     * @param status
     * @return
     */
    @GET("Favorites/follow")
    Observable<BaseRespose<String>> follow(@Query("favoriteable_id") Integer favoriteable_id,
                                           @Query("status") Integer status);


    /**
     * 查看联系方式
     * @param userId
     * @return
     */
    @GET("users/friendContact/{user_id}")
    Observable<BaseRespose<FriendContactBean>> getFriendContact(@Path("user_id") Integer userId);


    /**
     * 查看次数
     * @return
     */
    @GET("users/numberOfViews")
    Observable<BaseRespose<CheckNumberOfViewBean>> numberOfViews();

    /**
     * 付费查看联系方式
     * @param userId
     * @return
     */
    @POST("Orders/contactPay/{user_id}")
    Observable<BaseRespose<PayBean>> contactPay(@Path("user_id") int userId,@Query("pay_method") String payMethod);

    /**
     * 付费查看相册
     * @param userId
     * @param payMethod
     * @return
     */
    @POST("Orders/albumPay/{user_id}")
    Observable<BaseRespose<PayBean>> albumPay(@Path("user_id") int userId,@Query("pay_method")String payMethod);


    /**
     * 申请查看相册
     *
     * @param broadCaseId 广播id
     * @return
     */
    @Multipart
    @POST("users/applyViewUserInfo/{user_id}")
    Observable<BaseRespose<String>> applyViewUserInfo(@Path("user_id") Integer broadCaseId,@Part MultipartBody.Part file);

    /**
     * 会员使用一次机会免费查看付费相册
     * @param userId
     * @return
     */
    @GET("users/userMedias/{userId}")
    Observable<BaseRespose<UserAlbumBean>>  userMedias(@Path("userId")String userId);

}
