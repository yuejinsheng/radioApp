package com.radioapp.liaoliaobao.api;

import android.support.v4.util.ArrayMap;

import com.jaydenxiao.common.basebean.BaseRespose;
import com.radioapp.liaoliaobao.bean.CommonBean;
import com.radioapp.liaoliaobao.bean.KeyValueBean;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.home.GenderBean;
import com.radioapp.liaoliaobao.bean.user.BlackListBean;
import com.radioapp.liaoliaobao.bean.user.BroadcaseBean;
import com.radioapp.liaoliaobao.bean.user.CodeBean;
import com.radioapp.liaoliaobao.bean.user.LoginBean;
import com.radioapp.liaoliaobao.bean.user.RadioRegisterListBean;
import com.radioapp.liaoliaobao.bean.user.UpdateMediaBean;
import com.radioapp.liaoliaobao.bean.user.UserBean;
import com.radioapp.liaoliaobao.bean.user.UserInfoBean;
import com.radioapp.liaoliaobao.bean.user.VipBean;
import com.radioapp.liaoliaobao.bean.user.WalletBean;


import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * 功能： 用户的接口
 * 描述
 * Created by yue on 2019-07-16
 */
public interface UserService {

    /**
     * 登录
     *
     * @param mobile
     * @param password
     * @param deviceName 设备名称
     * @param version    设备版本
     * @return
     */
    @POST("login/login")
    @FormUrlEncoded
    Observable<BaseRespose<LoginBean>> login(@Field("mobile") String mobile, @Field("password") String password,
                                             @Field("lng") String lng, @Field("lat") String lat,
                                             @Field("device_no") String device_no,
                                             @Field("device_name") String deviceName, @Field("version") String version);


    /**
     * 发送验证码
     *
     * @param url:register/sendMobileSms,users/sendSmsUpdatePassword
     * @param mobile
     * @return
     */
    @POST("{url}")
    @FormUrlEncoded
    Observable<BaseRespose<Object>> sendMobileSms(@Path("url") String url, @Field("mobile") String mobile);

    /**
     * 注册
     *
     * @param mobile     手机号
     * @param password   密码
     * @param code       短信验证码
     * @param from_where 注册来源
     * @param lng        经度
     * @param lat        纬度
     * @param device_no  设备码
     * @param deviceName 设备名称
     * @param version    设备版本
     * @return
     */
    @POST("register/register")
    @FormUrlEncoded
    Observable<BaseRespose<LoginBean>> register(@Field("mobile") String mobile, @Field("password") String password,
                                                @Field("code") String code, @Field("from_where") String from_where,
                                                @Field("lng") double lng, @Field("lat") double lat,
                                                @Field("device_no") String device_no,
                                                @Field("device_name") String deviceName, @Field("version") String version);


    /**
     * 上传头像
     *
     * @param file
     * @return
     * @PartMap
     */
    @Multipart
    @POST("users/uploadAvatar")
    Observable<BaseRespose<CommonBean>> uploadAvatal(@Part MultipartBody.Part file);


    /**
     * 上传资源相册/视频
     *
     * @param file
     * @return
     * @Query 资源类型1红包，2阅后，3普通
     */
    @Multipart
    @POST("users/uploadResource")
    Observable<BaseRespose<CommonBean>> uploadResource(@Part MultipartBody.Part file, @Part("resource_type") int type);


    /**
     * 认证
     *
     * @param file            图片
     * @param alipay_account  帐号
     * @param alipay_realname 别人
     * @return
     */
    @Multipart
    @POST("users/verify")
    Observable<BaseRespose<String>> verify(@Part MultipartBody.Part file, @Part("alipay_account") String alipay_account,
                                           @Part("alipay_realname") String alipay_realname);


    /**
     *认证
     * @param requestBodyMap
     * @return
     */
    @Multipart
    @POST("users/verify")
    Observable<BaseRespose<String>> verify(@Part List<MultipartBody.Part> requestBodyMap);

    /**
     * 用户钱包信息
     *
     * @return
     */
    @GET("users/account")
    Observable<BaseRespose<WalletBean>> account();

    /**
     * 提现
     *
     * @param cash
     * @return
     */
    @POST("CashApplies/apply")
    Observable<BaseRespose<String>> apply(@Query("cash") String cash);

    /**
     * 更新用户相册状态
     *
     * @param mediaId
     * @return
     */
    @GET("users/userMedias/{media_id}")
    Observable<BaseRespose<List<UpdateMediaBean>>> updateMedia(@Path("media_id") Integer mediaId);

    /**
     * 更改密码
     *
     * @param mobile   手机号码
     * @param code     短信验证码
     * @param password 密码
     * @return
     */
    @POST("users/updatePassword")
    @FormUrlEncoded
    Observable<BaseRespose<String>> updatePassword(@Field("mobile") String mobile, @Field("code") int code, @Field("password") String password);

    /**
     * 隐私设置
     * <p>
     * 隐私状态 0:公开 1:付费查看相册 2:通过验证后查看 3:隐身 允许值: 0, 1, 2, 3
     * 是否隐藏地理位置：1是；0否
     * 隐藏社交帐号 1:是 0:否
     * 查看相册费用(单位分) 与隐私状态1相对应 允许值: {0，100-10000}
     *
     * @return
     */
    @POST("users/setPrivacy")
    @FormUrlEncoded
    Observable<BaseRespose<String>> setPrivacy(@FieldMap ArrayMap<String, Object> params);

    /**
     * 更改用户信息
     *
     * @param map 参数放map传
     * @return
     */
    @FormUrlEncoded
    @POST("users/updateUserInfo")
    Observable<BaseRespose<String>> updateUserInfo(@FieldMap ArrayMap<String, Object> map);

    @GET("users/userInfo")
    Observable<BaseRespose<UserBean>> getUserInfo();


    /**
     * 获取好友信息
     *
     * @param friendId 好友id
     * @return
     */
    @GET("users/friendInfo/{friend_id}")
    Observable<BaseRespose<UserInfoBean>> getFriendInfo(@Path("friend_id") Integer friendId);


    /**
     * 拉黑用户
     *
     * @param friendId 朋友id
     * @param status   1加入黑名单，0取消黑名单
     * @return
     */
    @GET("UserFriends/forbidden")
    Observable<BaseRespose<String>> friendForBidden(@Query("friend_id") Integer friendId, @Query("status") Integer status);

    /**
     * 老用户邀请码列表
     *
     * @param page 当前页
     * @return
     */
    @GET("users/inviteCodeList")
    Observable<BaseRespose<PageBean<CodeBean>>> inviteCodeList(@Query("page") Integer page);

    /**
     * 获取邀请码
     * @return
     */

    @GET("users/getInviteCode")
    Observable<BaseRespose<CommonBean>> getInviteCode();


    /**
     * vip说明
     *
     * @return
     */
    @GET("SystemConfigs/vipExplain")
    Observable<BaseRespose<List<VipBean>>> getvipExplain();


    /**
     * vip等级(价格)
     *
     * @return
     */
    @GET("SystemConfigs/vipMenuPrice")
    Observable<BaseRespose<List<VipBean>>> getVipMenuPrice();

    /**
     * vip支付
     *
     * @param pay_method 支付方法(1支付宝，2微信)
     * @param vip_type   vip类型(值为 15 30 90 180)
     * @return
     */

    @POST("Orders/vipPay")
    @FormUrlEncoded
    Observable<BaseRespose<PayBean>> vipPay(@Field("pay_method") Integer pay_method, @Field("vip_type") Integer vip_type);


    /**
     * 退出登录
     *
     * @return
     */
    @GET("login/outLogin")
    Observable<BaseRespose<String>> outLogin();


    /**
     * 我喜欢的
     *
     * @param params
     * @return
     */
    @GET("Favorites/list")
    Observable<BaseRespose<PageBean<GenderBean>>> getFavorites(@QueryMap ArrayMap<String, Object> params);


    /**
     * 填入邀请码
     *
     * @param code
     * @return
     */
    @POST("users/activateUser")
    @FormUrlEncoded
    Observable<BaseRespose<String>> setCode(@Field("invite_code") String code);


    /**
     * 客服信息
     *
     * @return
     */
    @GET("SystemConfigs/kf")
    Observable<BaseRespose<List<KeyValueBean>>> getKF();

    /**
     * 获取黑名单列表
     *
     * @param page
     * @param lng
     * @param lat
     * @return
     */
    @GET("UserFriends/forbiddenList")
    Observable<BaseRespose<PageBean<BlackListBean>>> getblackList(@Query("page") Integer page, @Query("lng") String lng, @Query("lat") String lat);


    /**
     * 解除黑名单
     *
     * @return
     */
    @GET("UserFriends/forbidden")
    Observable<BaseRespose<String>> forbidder(@Query("friend_id") String friendId, @Query("status") Integer status);

    /**
     * 我的广播列表
     *
     * @param broadCaseId
     * @param page
     * @return
     */
    @GET("Comments/broadcastCommentList/{broadcast_id}")
    Observable<BaseRespose<PageBean<BroadcaseBean>>> getMyBroadCaseList(@Path("broadcast_id") Integer broadCaseId, @Query("page") int page);


    /**
     * 帮朋友申请邀请码
     *
     * @return
     */
    @GET("users/generateCode")
    Observable<BaseRespose<String>> generateCode();


    /**
     * 删除发布的图片
     *
     * @param mediaId
     * @return
     */
    @POST("users/delMedia/{media_id}")
    Observable<BaseRespose<String>> delMedia(@Path("media_id") Integer mediaId);

    /**
     * 获取约会报名名单
     *
     * @return
     */
    @GET("Broadcasts/applies")
    Observable<BaseRespose<PageBean<RadioRegisterListBean>>> getRadioRegisterList();

    /**
     * 通过约会报名名单
     *
     * @param id
     * @return
     */
    @POST("Broadcasts/pass/{id}")
    Observable<BaseRespose<String>> passRadioRegisterList(@Path("id") Integer id);


    /**
     * 更新定位
     * @param lng
     * @param lat
     * @return
     */
    @POST("users/updateLngAndLat")
    @FormUrlEncoded
    Observable<BaseRespose<String>> updateLngAndLat(@Field("lng") String lng, @Field("lat") String lat);


}
