package com.radioapp.liaoliaobao.constant;

import com.google.common.reflect.TypeToken;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.MediaTypeBean;
import com.radioapp.liaoliaobao.bean.appointment.DatingHopesBean;
import com.radioapp.liaoliaobao.bean.radio.RadioPublishBean;
import com.radioapp.liaoliaobao.utils.GsonUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能： 常量类
 * 描述
 * Created by yue on 2019-07-22
 */
public class Constants {

     public static final String  LATITUDE="latitude"; //纬度
     public static final String LONGITUDE="longitude"; //经度
    public static final String  ISSPLASH="isSplash";

    public static final String PRIVACY_STATUS = "privacy_status";//隐私状态 0:公开 1:付费查看相册 2:通过验证后查看 3:隐身
    public static final String HIDE_LOCATION = "hide_location"; //是否隐藏地理位置：1是；0否
    public static final String HIDE_SOCIAL_ACCOUNT = "hide_social_account"; //隐藏社交帐号 1:是 0:否
    public static final String VIEW_ALBUM_PRICE = "view_album_price";  //查看相册费用(单位分) 与隐私状态1相对应


    //消息模块
    public static String Notice_System ="Notice_System";
    public static String Notice_APPLY_LOOK ="Notice_APPLY_LOOK";
    public static String Notice_RADIO ="Notice_RADIO";
    public static String Notice_DONGTAI ="Notice_DONGTAI";
    public static String Notice_RETURN ="Notice_RETURN";
    public static String REGISTER_MEETING ="REGISTER_MEETING";


    /**
     * 约会节目
     *
     * @return
     */
    public static List<DatingHopesBean> getShowList() {
        String json = " [\n" +
                "        {\n" +
                "            \"id\": 1,\n" +
                "                \"name\": \"健康运动\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 2,\n" +
                "                \"name\": \"夜蒲聚会\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 3,\n" +
                "                \"name\": \"我是吃货\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 4,\n" +
                "                \"name\": \"看电影\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 5,\n" +
                "                \"name\": \"玩游戏\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 6,\n" +
                "                \"name\": \"结伴旅行\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 7,\n" +
                "                \"name\": \"连麦聊天\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 8,\n" +
                "                \"name\": \"其他\"\n" +
                "        },\n" +
                "    ]\n";
        return GsonUtil.newInstance().fromJson(json.trim(), new TypeToken<List<DatingHopesBean>>() {
        }.getType());
    }


    public static List<RadioPublishBean> getPublishBroadCaseList() {
        List<RadioPublishBean> list = new ArrayList<>();
        RadioPublishBean bean = new RadioPublishBean(1, "玩游戏", R.mipmap.icon_broadcase1);
        RadioPublishBean bean1 = new RadioPublishBean(2, "旅行", R.mipmap.icon_broadcase2);
        RadioPublishBean bean2 = new RadioPublishBean(3, "看电影", R.mipmap.icon_broadcase3);
        RadioPublishBean bean3 = new RadioPublishBean(4, "运动", R.mipmap.icon_broadcase4);
        RadioPublishBean bean4 = new RadioPublishBean(5, "聚餐", R.mipmap.icon_broadcase5);
        RadioPublishBean bean5 = new RadioPublishBean(6, "下午茶", R.mipmap.icon_broadcase6);
        RadioPublishBean bean6 = new RadioPublishBean(7, "去唱歌", R.mipmap.icon_broadcase7);
        RadioPublishBean bean7 = new RadioPublishBean(8, "其他", R.mipmap.icon_broadcase8);
        list.add(bean);
        list.add(bean1);
        list.add(bean2);
        list.add(bean3);
        list.add(bean4);
        list.add(bean5);
        list.add(bean6);
        list.add(bean7);
        return list;
    }


    /**
     * 上传资源文件的参数
     *
     * @param gender
     * @return
     */
    public static List<MediaTypeBean> getMediaTypes(int gender) {
        List<MediaTypeBean> list = new ArrayList<>();
        MediaTypeBean bean = new MediaTypeBean(1, "红包照片");
        MediaTypeBean bean1 = new MediaTypeBean(2, "阅后即焚");
        MediaTypeBean bean2 = new MediaTypeBean(3, "普通照片");
        if (gender == 2) {
            list.add(bean);
        }
       // list.add(bean1);
        list.add(bean2);
        return list;
    }
}
