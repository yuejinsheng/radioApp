package com.radioapp.liaoliaobao.constant;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.jaydenxiao.common.sharepref.SharePref;
import com.radioapp.liaoliaobao.bean.user.LoginBean;
import com.radioapp.liaoliaobao.bean.user.UserBean;

/**
 * 功能： 保存信息
 * 描述
 * Created by yue on 2019-09-26
 */
public class Global {

    /**
     * 登录信息
     */
    public static final String LOGIN_BEAN = "login_bean";
    /**
     * 用户信息
     **/
    public static final String USER_INFO = "userInfo";

    /**
     * 获取用户信息
     **/
    public static UserBean getUserInfo() {
        String json = SharePref.getString(USER_INFO, "");
        if (!TextUtils.isEmpty(json))
            return new Gson().fromJson(json, UserBean.class);
        return new UserBean();
    }

    /**
     * 保存用户信息
     *
     * @param bean
     */
    public static void saveUserInfo(UserBean bean) {
        if (bean == null) return;
        SharePref.saveString(USER_INFO, new Gson().toJson(bean));
    }

    /**
     * 获取登录信息
     **/
    public static LoginBean getLoginBean() {
        String json = SharePref.getString(LOGIN_BEAN, "");
        if (!TextUtils.isEmpty(json))
            return new Gson().fromJson(json, LoginBean.class);
        return new LoginBean();
    }

    /**
     * 保存登录信息
     *
     * @param bean
     */
    public static void saveLoginBean(LoginBean bean) {
        if (bean == null) return;
        SharePref.saveString(LOGIN_BEAN, new Gson().toJson(bean));
    }
}
