package com.radioapp.liaoliaobao.constant;

/**
 * 功能： 跳转数据类
 * 描述
 * Created by yue on 2019-07-22
 */
public class IntentConstant {

    public static final String GENDER = "gender";//男或者女
    public static final String RADIOPUBLISHBEAN = "radioPublishBean";//发布电台的数据
    public static final String TITLE = "title";
    public static final String URL = "url";
    public static final String USERID = "userId";
    public static final String FRIENDID="friendId";
    public static final String VIP_ENDED_AT = "vip_ended_at";
    public static final String STATUS = "status";
    public static final String TYPE="type";

    //home
    public static final String HOME_GENDER = "home_gender";
    public static final String HOME_VERIFIED = "home_verified";
    public static final String HOME_NEWS = "home_verified";
    public static final String HOME_NICKNAME = "home_verified";
    public static final String HOME_CAREER_ID = "home_career_id";
    public static final String HOME_CITY_ID = "home_city_id";
    public static final String HOME_IS_VIP = "home_is_vip";
    public static final String HOME_LNG = "home_lng";
    public static final String HOME_LAT = "home_lat";


    public static final String BROADCASEID="broadCaseId";



}
