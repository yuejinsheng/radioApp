package com.radioapp.liaoliaobao.module.user.customer;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.KeyValueBean;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能： 客服
 * 描述
 * Created by yue on 2019-09-29
 */
public class CustomerFragment extends BaseRiggerFragment<CustomerView, CustomerPresenter> implements CustomerView {
    @BindView(R.id.tv_qq)
    TextView tvQq;
    @BindView(R.id.tv_copy_qq)
    TextView tvCopyQq;
    @BindView(R.id.tv_wechat)
    TextView tvWechat;
    @BindView(R.id.tv_copy_wechat)
    TextView tvCopyWechat;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_customer;
    }

    @Override
    protected void initView() {
        initToolbar("客服", true, false, null);

        mPresenter.getKeFu();
    }

    @Override
    public void tokenInvalidRefresh() {
        mPresenter.getKeFu();
    }


    @OnClick({R.id.tv_copy_qq, R.id.tv_copy_wechat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_copy_qq:
                if(TextUtils.isEmpty(tvQq.getText())){
                    ToastUitl.showLong("无内容可以复制");
                    return;
                }
                //复制
                ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("", tvQq.getText());
                clipboard.setPrimaryClip(clip);
                tvCopyQq.setText("已复制");
                tvCopyQq.setTextColor(Color.parseColor("#e7e7e7"));
                break;
            case R.id.tv_copy_wechat:
                if(TextUtils.isEmpty(tvCopyWechat.getText())){
                    ToastUitl.showLong("无内容可以复制");
                    return;
                }
                ClipboardManager clipboard1= (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip1 = ClipData.newPlainText("", tvQq.getText());
                clipboard1.setPrimaryClip(clip1);
                tvCopyWechat.setText("已复制");
                tvCopyWechat.setTextColor(Color.parseColor("#e7e7e7"));
                break;
        }
    }

    @Override
    public void getkf(List<KeyValueBean> list) {
        if (list != null && list.size() > 0) {
            tvQq.setText(list.get(0).value);
            tvWechat.setText(list.get(1).value);
        }

    }
}
