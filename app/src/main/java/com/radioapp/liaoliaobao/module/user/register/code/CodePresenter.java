package com.radioapp.liaoliaobao.module.user.register.code;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.CommonBean;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-26
 */
public class CodePresenter extends BasePresenter<CodeView> {


    public void  setCode(String code){
        ServiceManager.create(UserService.class)
                .setCode(code)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                       mView.success();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }

    public void getInviteCode(){
        ServiceManager.create(UserService.class)
                .getInviteCode()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<CommonBean>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(CommonBean commonBean) {
                       mView.setInviteCode(commonBean.getInvite_code());
                    }

                    @Override
                    protected void _TokenInvalid() {

                    }
                });
    }
}
