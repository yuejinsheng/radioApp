package com.radioapp.liaoliaobao.module.index;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import com.jaydenxiao.common.base.BaseActivity;
import com.jkb.fragment.rigger.rigger.Rigger;

/**
 * main
 */
public class MainActivity extends BaseActivity<MainView, MainPresenter> implements MainView {


    //  MainBarLifecycleObserver mainBarLifecycleObserver;
    //  @BindView(R.id.tabLayout)
    // CommonTabLayout tabLayout;

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public void initView() {
        Log.e("android", "dfsfs ");
        Rigger.getRigger(this).startFragment(new SplashFragment());
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    //防止重叠
    @SuppressLint("MissingSuperCall")
    @Override
    public void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
        // super.onSaveInstanceState(outState);
    }
}
