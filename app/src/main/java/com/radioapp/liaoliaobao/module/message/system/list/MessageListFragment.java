package com.radioapp.liaoliaobao.module.message.system.list;

import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.manager.ServiceManager;
import com.jaydenxiao.common.url.BaseConstant;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.previewlibrary.GPreviewBuilder;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.api.MessageService;
import com.radioapp.liaoliaobao.base.BaseRiggerRecyclerFragment;
import com.radioapp.liaoliaobao.bean.ImageBean;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.enumbean.MessageTitleEnum;
import com.radioapp.liaoliaobao.bean.lite.ChatUser;
import com.radioapp.liaoliaobao.bean.message.MessageListBean;
import com.radioapp.liaoliaobao.bean.user.PhotoImageBean;
import com.radioapp.liaoliaobao.bean.user.UpdateMediaBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.module.home.list.GenderListPresenter;
import com.radioapp.liaoliaobao.module.photoImage.PhotoImageFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.trello.rxlifecycle2.LifecycleTransformer;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * 功能： url
 * 描述
 * Created by yue on 2019-08-05
 */
public class MessageListFragment extends BaseRiggerRecyclerFragment<MessageListBean> implements MessageListView {
    private String types;
    public String title;
    private MessageListAdapter messageListAdapter;
    private MessageListPresenter presenter;

    public static MessageListFragment newInstance(String type, String title) {
        MessageListFragment messageListFragment = new MessageListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(IntentConstant.TYPE, type);
        bundle.putString(IntentConstant.TITLE, title);
        messageListFragment.setArguments(bundle);
        return messageListFragment;
    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        types = bundle.getString(IntentConstant.TYPE, "1");
        title = bundle.getString(IntentConstant.TITLE, MessageTitleEnum.MESSAGE.getCode());
    }

    @Override
    protected void initData() {
        initPresenter();
        messageListAdapter = new MessageListAdapter(title);
        messageListAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                MessageListBean messageListBean = (MessageListBean) adapter.getData().get(position);
                switch (view.getId()) {
                    case R.id.iv_message_avatar:
                        UiHelper.showUserInfoDetailFragment(mActivity, messageListBean.getData().getFrom_user_id());
                        break;
                    case R.id.tv_message_status:
                        if (title.equals(MessageTitleEnum.APPLY.getCode())) {
                            //查看申请
                            if (messageListBean.getData().getIs_agree() != 1) {
                                presenter.passApply(String.valueOf(messageListBean.getData().getFrom_user_id()), messageListBean.getN_id(), position);
                            }

                        } else if (title.equals(MessageTitleEnum.RADIO.getCode())) {
                            String name = "";
                            if (!TextUtils.isEmpty(messageListBean.getData().getContent())) {
                                String[] names = messageListBean.getData().getContent().split("，");
                                if (names.length > 0) {
                                    name = names[0];
                                }
                            }
                            //电台消息
                            ChatUser chatUser = new ChatUser(messageListBean.getData().getFrom_user_id(), name, BaseConstant.IMAGEURL + messageListBean.getData().getFrom_user_avatar());
                            UiHelper.showChatActivity(mActivity, String.valueOf(messageListBean.getData().getFrom_user_id()));
                        }
                        break;
                    case R.id.iv_message_image:
                        ImageBean imageBean=new ImageBean(BaseConstant.IMAGEURL + messageListBean.getData().getMeta().getImage());
                        Rect bounds = new Rect();
                        imageBean.setBounds(bounds);
                        GPreviewBuilder.from(mActivity)
                                .setSingleData(imageBean)
                                .setFullscreen(false)
                                .setCurrentIndex(0)
                                .setType(GPreviewBuilder.IndicatorType.Dot)
                                .start();//启动
                        break;
                }
            }
        });
    }

    @Override
    protected BaseQuickAdapter<MessageListBean, BaseViewHolder> getAdapter() {
        return messageListAdapter;
    }

    @Override
    protected Observable<PageBean<MessageListBean>> getUrl(int pageNo) {
        return ServiceManager.create(MessageService.class)
                .getMessageList(types, pageNo)
                .compose(RxHelper.handleFlatMap());
    }


    /**
     * 初始化presenter
     */
    public synchronized BasePresenter initPresenter() {
        if (presenter == null) {
            presenter = new MessageListPresenter();
            presenter.attachView(this);
            presenter.mContext = mActivity;
        }
        return presenter;
    }

    @Override
    public <T> LifecycleTransformer<T> bindToLife() {
        return bindToLifecycle();
    }

    @Override
    public void tokenInvalid() {
        tokenRefresh();
    }

    @Override
    public void passApplySuccess(Integer position) {
        if (messageListAdapter != null) {
            MessageListBean messageListBean = messageListAdapter.getData().get(position);
            messageListBean.getData().setFlag(1);
            messageListAdapter.notifyDataSetChanged();
        }
    }
}
