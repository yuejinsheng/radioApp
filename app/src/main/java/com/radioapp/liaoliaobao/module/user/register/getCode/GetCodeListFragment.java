package com.radioapp.liaoliaobao.module.user.register.getCode;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.base.BaseRiggerRecyclerFragment;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.user.CodeBean;

import io.reactivex.Observable;

/**
 * 功能： 获取验证码列表
 * 描述
 * Created by yue on 2019-08-23
 */
public class GetCodeListFragment extends BaseRiggerRecyclerFragment<CodeBean> {
    @Override
    protected void initData() {

    }

    @Override
    protected BaseQuickAdapter<CodeBean, BaseViewHolder> getAdapter() {
        return new GetCodeListAdapter();
    }

    @Override
    protected Observable<PageBean<CodeBean>> getUrl(int pageNo) {
        return ServiceManager.create(UserService.class)
                .inviteCodeList(page)
                .compose(RxHelper.handleFlatMap());
    }
}
