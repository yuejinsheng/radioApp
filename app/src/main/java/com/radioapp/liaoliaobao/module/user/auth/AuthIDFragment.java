package com.radioapp.liaoliaobao.module.user.auth;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.view.span.AndroidSpan;
import com.radioapp.liaoliaobao.view.span.SpanOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 功能：身份认证
 * 描述
 * Created by yue on 2019-12-11
 */
public class AuthIDFragment extends BaseRiggerFragment {
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.rt_auth_next)
    RelativeLayout rtNext;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_auth_id;
    }

    @Override
    protected void initView() {
        initToolbar("身份认证", true, false, null);

        SpanOptions spanOptions = new SpanOptions()
                .addForegroundColor(ContextCompat.getColor(mActivity, R.color.color_ff4));
        AndroidSpan androidSpan = new AndroidSpan()
                .drawCommonSpan("\t  照片认证：\n")
                .drawCommonSpan("1.取一张纸，写上右边显示的文字\t  ")
                .drawWithOptions("Wr0AoYqL\n", spanOptions)
                .drawCommonSpan("2.拿着纸与你个人合照一张（如上图所示）");
        tvContent.setText(androidSpan.getSpanText());
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    @OnClick(R.id.rt_auth_next)
    public void onViewClicked() {
        Rigger.getRigger(mActivity).startFragment(new AuthApplyFragment());
        Rigger.getRigger(this).close();
    }
}
