package com.radioapp.liaoliaobao.module.user.setting.update_pwd;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-08
 */
public class UpdatePwdPresenter extends BasePresenter<UpdatePwdView> {

    /**
     * 修改密码
     */
    public void update(String mobile, int code, String pwd) {
        ServiceManager.create(UserService.class)
                .updatePassword(mobile, code, pwd)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.success();
                    }

                    @Override
                    protected void _TokenInvalid() {
                      mView.tokenInvalid();
                    }
                });

    }
}
