package com.radioapp.liaoliaobao.module.find;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.radioapp.liaoliaobao.R;

/**
 * 功能：发现
 * 描述
 * Created by yue on 2019-07-15
 */
public class FindFragment extends BaseRiggerFragment {
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_find;
    }

    @Override
    protected void initView() {

    }

    @Override
    public void tokenInvalidRefresh() {

    }
}
