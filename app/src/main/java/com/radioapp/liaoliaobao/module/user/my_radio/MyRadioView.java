package com.radioapp.liaoliaobao.module.user.my_radio;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.radio.CommentBean;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-12
 */
public interface MyRadioView extends BaseView {


    void getRadioBean(RadioListBean bean);

    void getCommentList(List<CommentBean> commentBeans);

    void delRadioSuccess();

    void delCommentSuccess( int position);

    void notDate();
}
