package com.radioapp.liaoliaobao.module.user.my_radio.registerRadio;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.user.RadioRegisterListBean;

import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-11-04
 */
public class MyRadioRegisterListPresenter extends BasePresenter<MyRadioRegisterListView> {


    public void getRadioRegisterList(){
        ServiceManager.create(UserService.class)
                .getRadioRegisterList()
                .compose(RxHelper.handleFlatMap())
                .compose(mView.bindToLife())
                .subscribe(new RxSubscriber<PageBean<RadioRegisterListBean>>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(PageBean<RadioRegisterListBean> radioRegisterListBeans) {
                      mView.list(radioRegisterListBeans.getData());
                    }

                    @Override
                    protected void _TokenInvalid() {
                       mView.tokenInvalid();
                    }
                });
    }

    public void passRadioRegisterList(Integer id,int position){
        ServiceManager.create(UserService.class)
                .passRadioRegisterList(id)
                .compose(RxHelper.handleFlatMap())
                .compose(mView.bindToLife())
                .subscribe(new RxSubscriber<String>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                       mView.passSuccess(position);
                    }

                    @Override
                    protected void _TokenInvalid() {
                     mView.tokenInvalid();
                    }
                });
    }
}
