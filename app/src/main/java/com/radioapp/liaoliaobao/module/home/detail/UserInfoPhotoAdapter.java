package com.radioapp.liaoliaobao.module.home.detail;

import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.url.BaseConstant;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideApp;
import com.radioapp.liaoliaobao.app.GlideBlurTransformer;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.user.UserInfoBean;

import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-20
 */
public class UserInfoPhotoAdapter extends BaseQuickAdapter<UserInfoBean.UserMedias, BaseViewHolder> {
    public UserInfoPhotoAdapter() {
        super(R.layout.item_user_photo);
    }

    @Override
    protected void convert(BaseViewHolder helper, UserInfoBean.UserMedias item) {
        LogUtils.e("item--->" + item.getResource_url());
/*
        if (item.getIs_once() == 1) {

            //判断图片是查看了还是没有查看
            if (TextUtils.isEmpty(item.getUser_media_id())) {
                //红包图片
                GlideApp.with(mContext).setDefaultRequestOptions(new RequestOptions()
                        .apply(RequestOptions.bitmapTransform(new GlideBlurTransformer(mContext, 15, 3))))
                        .load(BaseConstant.IMAGEURL + item.getResource_url())
                        .into((ImageView) helper.getView(R.id.iv_item_user_photo_avatar));
                //没有查看
                helper.setText(R.id.item_user_details_photo_bg, "阅后即焚");
            } else {
                GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getResource_url(),
                        helper.getView(R.id.iv_item_user_photo_avatar));
                //查看了
                helper.setText(R.id.item_user_details_photo_bg, "已焚毁");
            }
            helper.setVisible(R.id.item_user_details_photo_bg, true);
        } else */if (item.getPrice() != 0) {

            //判断图片是查看了还是没有查看
            if (TextUtils.isEmpty(item.getUser_media_id())) {
                //没有查看
                helper.setText(R.id.item_user_details_photo_bg, "红包照片");
                //红包图片
                GlideApp.with(mContext).setDefaultRequestOptions(new RequestOptions()
                        .apply(RequestOptions.bitmapTransform(new GlideBlurTransformer(mContext, 15, 3))))
                        .load(BaseConstant.IMAGEURL + item.getResource_url())
                        .into((ImageView) helper.getView(R.id.iv_item_user_photo_avatar));
            } else {
                //查看了
                helper.setText(R.id.item_user_details_photo_bg, "已解锁");
                GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getResource_url(),
                        helper.getView(R.id.iv_item_user_photo_avatar));
            }
            helper.setVisible(R.id.item_user_details_photo_bg, true);
        } else {
            helper.setVisible(R.id.item_user_details_photo_bg, false);
            GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getResource_url(),
                    helper.getView(R.id.iv_item_user_photo_avatar));
        }
    }
}
