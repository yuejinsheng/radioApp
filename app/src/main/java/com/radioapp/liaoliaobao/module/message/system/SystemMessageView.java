package com.radioapp.liaoliaobao.module.message.system;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.message.MessageListBean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2020-02-08
 *
 * @author yue
 */
public interface SystemMessageView extends BaseView {


    void getList(List<MessageListBean> messageListBeans,Integer position);
}
