package com.radioapp.liaoliaobao.module.radio.list;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaeger.ninegridimageview.ItemImageClickListener;
import com.jaeger.ninegridimageview.NineGridImageView;
import com.jaeger.ninegridimageview.NineGridImageViewAdapter;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.url.BaseConstant;
import com.previewlibrary.GPreviewBuilder;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.utils.AssetsUtils;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-01
 */
public class RadioListAdapter extends BaseQuickAdapter<RadioListBean, BaseViewHolder> {


    public RadioListAdapter() {
        super(R.layout.item_radio_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, RadioListBean item) {

        helper.setText(R.id.tv_name, item.getNickname())
                .setText(R.id.tv_date, "发布于  " + item.getCreated_at())
                .setText(R.id.tv_time, "时间:" + item.getDating_time_str())
                .setText(R.id.tv_content, item.getText())
                .setText(R.id.tv_like, item.getLike_count() + "");
        GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getAvatar(), helper.getView(R.id.iv_radio_avatar));
        helper.setImageResource(R.id.iv_gender,item.getGender()==1?R.drawable.icon_man:R.drawable.icon_woman);

        //约会节目
        if (!TextUtils.isEmpty(item.getDating_types())) {
            StringBuffer expect = new StringBuffer();
            String[] sqlit = item.getDating_types().trim().split(",");
            if (sqlit.length > 0) {
                for (String s : sqlit) {
                    if (!TextUtils.isEmpty(s)) {
                        String name = (String) AssetsUtils.newInterface().getProgramBeansName(Integer.parseInt(s.trim())).get("name");
                        expect.append(name).append(",");
                    }
                }
                if (!TextUtils.isEmpty(expect.toString())) {
                    String expectNames = expect.toString().substring(0, expect.lastIndexOf(","));
                    helper.setText(R.id.tv_show, "约会节目:" + expectNames);
                }

            }

        }
        //约会期望
        if (!TextUtils.isEmpty(item.getDating_hopes())) {
            StringBuffer hope = new StringBuffer();
            String[] hopes = item.getDating_hopes().trim().split(",");
            if (hopes.length > 0) {
                for (String s : hopes) {
                    if (!TextUtils.isEmpty(s)) {
                        String name = (String) AssetsUtils.newInterface().getExpectName(Integer.parseInt(s.trim())).get("name");
                        hope.append(name).append(",");
                    }
                }
                if (!TextUtils.isEmpty(hope.toString())) {
                    String expectNames = hope.toString().substring(0, hope.lastIndexOf(","));
                    helper.setText(R.id.tv_dating_hopes, "约会期望:" + expectNames);
                }

            }
        }


        NineGridImageView mNglContent = (NineGridImageView<RadioListBean.BroadcaseResources>) helper.getView(R.id.ngl_images);

        mNglContent.setAdapter(new NineGridImageViewAdapter<RadioListBean.BroadcaseResources>() {
            @Override
            protected void onDisplayImage(Context context, ImageView imageView, RadioListBean.BroadcaseResources broadcaseResources) {
                GlideImageLoader.displayImage(context, broadcaseResources.getUrl(), imageView);
            }

        });
        mNglContent.setImagesData(item.getBroadcastResources());
        mNglContent.setItemImageClickListener(new ItemImageClickListener<RadioListBean.BroadcaseResources>() {
            @Override
            public void onItemImageClick(Context context, ImageView imageView, int index, List<RadioListBean.BroadcaseResources> list) {
                Log.d("onItemImageClick", list.get(index).getUrl());
                computeBoundsBackward(list, mNglContent);//组成数据
                GPreviewBuilder.from((Activity) context)
                        .setData(list)
                        .setCurrentIndex(index)
                        .setFullscreen(false)
                        .setType(GPreviewBuilder.IndicatorType.Dot)
                        .start();//启动
            }
        });


        //报名
        if (item.getGender() == Global.getUserInfo().getGender()) {
            helper.setVisible(R.id.ll_sign, false);
        } else {
            helper.setVisible(R.id.ll_sign, true);
            if (!TextUtils.isEmpty(item.getIs_apply())) {
                helper.setText(R.id.tv_sign, "您已参与");
            } else {
                helper.setText(R.id.tv_sign, "我要报名");
            }
        }
        //报名添加点击
        helper.addOnClickListener(R.id.ll_sign);


        //点赞
        helper.setImageResource(R.id.iv_like, TextUtils.isEmpty(item.getIs_like()) ? R.mipmap.icon_like : R.mipmap.icon_like_selector);
        //点击
        helper.addOnClickListener(R.id.ll_like);

       // helper.setVisible(R.id.ll_del, item.getUser_id() == Global.getUserInfo().getId());
      //  helper.addOnClickListener(R.id.ll_del);
        helper.setVisible(R.id.ll_comment,true);

        if (item.getOpen_comment()==1) {
            if (item.getComments() != null && item.getComments().size() > 0) {
                helper.setText(R.id.tv_comment, "评论(" + item.comments.size() + ")");

            } else {
                helper.setText(R.id.tv_comment, "评论");
            }
        }else {
            helper.setText(R.id.tv_comment, "禁止评论");

        }

       /* if (Global.getUserInfo().getGender() == 1) {
            if (IsVipUtil.isVip(Global.getUserInfo().getVip_ended_at())) {
                helper.setVisible(R.id.ll_comment,true);
            }
        }else if(Global.getUserInfo().getGender() == 2){
            if(!TextUtils.isEmpty(Global.getUserInfo().getVerified_at())){
                helper.setVisible(R.id.ll_comment,true);
            }
        }else{
            helper.setVisible(R.id.ll_comment,false);
        }*/
        //评论点击
        helper.addOnClickListener(R.id.ll_comment).addOnClickListener(R.id.iv_radio_avatar);

    }

    /**
     * 查找信息
     *
     * @param list 图片集合
     */
    private void computeBoundsBackward(List<RadioListBean.BroadcaseResources> list, NineGridImageView mNglContent) {
        for (int i = 0; i < mNglContent.getChildCount(); i++) {
            View itemView = mNglContent.getChildAt(i);
            Rect bounds = new Rect();
            if (itemView != null) {
                ImageView thumbView = (ImageView) itemView;
                thumbView.getGlobalVisibleRect(bounds);
            }
            list.get(i).setmBounds(bounds);
            // list.get(i).setResource_url(list.get(i).getUrl());
        }

    }

}
