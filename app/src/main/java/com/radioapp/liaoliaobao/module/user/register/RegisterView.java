package com.radioapp.liaoliaobao.module.user.register;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.user.LoginBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-18
 */
public interface RegisterView extends BaseView {

    void register(LoginBean loginBean);
}
