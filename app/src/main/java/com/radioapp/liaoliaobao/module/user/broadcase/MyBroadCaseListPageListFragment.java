package com.radioapp.liaoliaobao.module.user.broadcase;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.base.BaseRiggerRecyclerFragment;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.user.BroadcaseBean;
import com.radioapp.liaoliaobao.constant.IntentConstant;

import io.reactivex.Observable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-29
 */
public class MyBroadCaseListPageListFragment extends BaseRiggerRecyclerFragment<BroadcaseBean> {

    private int broadCaseId;

    public static Fragment myBroadCaseNewInterface(int broadCaseId) {
        MyBroadCaseListPageListFragment fragment = new MyBroadCaseListPageListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(IntentConstant.BROADCASEID, broadCaseId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        broadCaseId = bundle.getInt(IntentConstant.BROADCASEID);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected BaseQuickAdapter<BroadcaseBean, BaseViewHolder> getAdapter() {
        return null;
    }

    @Override
    protected Observable<PageBean<BroadcaseBean>> getUrl(int pageNo) {
        return ServiceManager.create(UserService.class)
                .getMyBroadCaseList(broadCaseId, pageNo)
                .compose(RxHelper.handleFlatMap());
    }
}
