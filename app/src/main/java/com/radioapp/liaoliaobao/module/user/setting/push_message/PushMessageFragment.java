package com.radioapp.liaoliaobao.module.user.setting.push_message;

import android.support.v4.widget.NestedScrollView;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.radioapp.liaoliaobao.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import butterknife.BindView;
import butterknife.OnCheckedChanged;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-08
 */
public class PushMessageFragment extends BaseRiggerFragment {
    @BindView(R.id.cbx_chat)
    CheckBox cbxChat;
    @BindView(R.id.cbx_broadcast)
    CheckBox cbxBroadcast;
    @BindView(R.id.cbx_dynamic)
    CheckBox cbxDynamic;
    @BindView(R.id.cbx_stealth)
    CheckBox cbxStealth;
    @BindView(R.id.cbx_request)
    CheckBox cbxRequest;
    @BindView(R.id.cbx_code)
    CheckBox cbxCode;
    @BindView(R.id.scrollview_detail)
    NestedScrollView scrollviewDetail;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_push_message;
    }

    @Override
    protected void initView() {
        initToolbar(getString(R.string.push_message_title), true, false, null);
    }

    @Override
    public void tokenInvalidRefresh() {

    }

    @OnCheckedChanged({R.id.cbx_chat, R.id.cbx_broadcast, R.id.cbx_dynamic, R.id.cbx_stealth,
            R.id.cbx_request, R.id.cbx_code})
    void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {
            case R.id.cbx_chat: //私聊消息
                ToastUitl.showLong(isChecked + "");
                break;
            case R.id.cbx_broadcast: //广播通知
                ToastUitl.showLong(isChecked + "");

                break;
            case R.id.cbx_dynamic: //动态通知
                ToastUitl.showLong(isChecked + "");

                break;
            case R.id.cbx_stealth: //隐身
                ToastUitl.showLong(isChecked + "");

                break;
            case R.id.cbx_request: //请求
                ToastUitl.showLong(isChecked + "");

                break;
            case R.id.cbx_code: //邀请码申请
                ToastUitl.showLong(isChecked + "");

                break;
        }
    }
}
