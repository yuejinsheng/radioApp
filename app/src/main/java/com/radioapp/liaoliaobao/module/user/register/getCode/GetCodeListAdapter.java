package com.radioapp.liaoliaobao.module.user.register.getCode;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.user.CodeBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-23
 */
public class GetCodeListAdapter extends  BaseQuickAdapter<CodeBean,BaseViewHolder>{
    public GetCodeListAdapter() {
        super(R.layout.item_get_code);
    }

    @Override
    protected void convert(BaseViewHolder helper, CodeBean item) {
           helper.setText(R.id.tv_time,item.getCreated_at())
                   .setText(R.id.tv_account,item.getValue())
                   .setText(R.id.tv_code,item.getValue());
    }
}
