package com.radioapp.liaoliaobao.module.radio.detail;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.module.radio.list.RadioListAdapter;
import com.radioapp.liaoliaobao.module.radio.list.RadioListPresenter;
import com.radioapp.liaoliaobao.module.radio.list.RadioListView;
import com.radioapp.liaoliaobao.module.user.auth.AuthApplyFragment;
import com.radioapp.liaoliaobao.module.user.auth.AuthIDFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.IsVipUtil;
import com.radioapp.liaoliaobao.utils.dialog.AlertDialogutils;
import com.radioapp.liaoliaobao.utils.dialog.DialogMeetingComment;
import com.radioapp.liaoliaobao.utils.timePicker.SelectImageUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 功能：电台详情
 * 描述
 * Created by yue on 2019-10-11
 */
public class RadioDetailFragment extends BaseRiggerFragment<RadioListView, RadioListPresenter> implements
        RadioListView, BaseQuickAdapter.OnItemChildClickListener {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    RadioListAdapter adapter;
    private int broadcaseId;
    private List<LocalMedia> selectList = new ArrayList<>();
    private int id;
    private int position;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_radio_detail;
    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        broadcaseId = bundle.getInt(IntentConstant.BROADCASEID, 0);
    }

    @Override
    protected void initView() {
        initToolbar("约会电台", true, false, null);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        adapter = new RadioListAdapter();
        recyclerView.setAdapter(adapter);
        mPresenter.getBroadcastInfo(broadcaseId);
        adapter.setOnItemChildClickListener(this);

    }

    @Override
    public void tokenInvalidRefresh() {

    }

    @Override
    public void likeSuccess(int position) {
        RadioListBean radioListBean = adapter.getData().get(position);
        radioListBean.setLike_count(radioListBean.getLike_count() + 1);
        radioListBean.setIs_like("已点赞");
        adapter.notifyItemChanged(position);
    }

    @Override
    public void datingApplySuccess() {
        selectList.clear();
        RadioListBean radioListBean = adapter.getData().get(position);
        radioListBean.setIs_apply("已报名");
        adapter.notifyItemChanged(position);
    }

    @Override
    public void commentSuccess() {
        ToastUitl.showLong("评论成功");
        adapter.notifyDataSetChanged();
    }

    @Override
    public void radioInfo(List<RadioListBean> radioListBeans) {
        adapter.setNewData(radioListBeans);
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        RadioListBean radioListBean = (RadioListBean) adapter.getData().get(position);
        switch (view.getId()) {
            case R.id.ll_sign: //报名
                if (Global.getUserInfo().getGender() == 1) {
                    if (!IsVipUtil.isVip(Global.getUserInfo().getVip_ended_at())) {
                        AlertDialogutils.createDialog(mActivity, "报名约会需要开通会员\n\n是否开通会员？", "开通会员",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        UiHelper.showMemberFragment(mActivity, Global.getUserInfo().getVip_ended_at());
                                    }
                                });
                        return;
                    }

                } else if (Global.getUserInfo().getGender() == 2) {
                    if (TextUtils.isEmpty(Global.getUserInfo().getVerified_at())) {
                        AlertDialogutils.createDialog(mActivity, "报名约会需要先通过身份认证\n\n是否去认证真实？", "去认证",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                       // UiHelper.showMemberFragment(mActivity, Global.getUserInfo().getVip_ended_at());
                                        UiHelper.showAuthIDFragment(mActivity);
                                    }
                                });
                        return;
                    }
                }

                this.position = position;
                this.id = radioListBean.getId();
                SelectImageUtils.showALlImage(RadioDetailFragment.this, selectList, 1, PictureConfig.CHOOSE_REQUEST);

                break;
            case R.id.ll_like: //点赞
                mPresenter.like(radioListBean.getId(), position);
                break;
            case R.id.ll_del: //删除广播
                AlertDialogutils.createDialog(mActivity, "确定删除该条约会电台吗？", "确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ToastUitl.showLong("接口删除没有广播id");
                    }
                });
                break;
            case R.id.ll_comment: //评论
                if (Global.getUserInfo().getGender() == 1) {
                    if (!IsVipUtil.isVip(Global.getUserInfo().getVip_ended_at())) {
                        ToastUitl.showLong("只有会员才可以评论");
                    }
                    return;
                } else if (Global.getUserInfo().getGender() == 2) {
                    if (TextUtils.isEmpty(Global.getUserInfo().getVerified_at())) {
                        ToastUitl.showLong("只有通过认证的用户才可以评论");
                    }
                    return;
                }
                new DialogMeetingComment(mActivity, new DialogMeetingComment.SendListener() {
                    @Override
                    public void send(String s) {
                        mPresenter.addBroadcaseComment(radioListBean.getId(),s);
                    }
                });

                break;
        }
    }
}
