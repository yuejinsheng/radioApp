package com.radioapp.liaoliaobao.module.user.wallet;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.user.WalletBean;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-08
 */
public class WalletPresenter extends BasePresenter<WalletView> {

    public void account() {
        ServiceManager.create(UserService.class)
                .account()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<WalletBean>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);

                    }

                    @Override
                    protected void _onNext(WalletBean walletBean) {
                        mView.data(walletBean);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 提现
     */
    public void apply(String cash){
        ServiceManager.create(UserService.class)
                .apply(cash)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                      mView.success();
                    }

                    @Override
                    protected void _TokenInvalid() {
                     mView.tokenInvalid();
                    }
                });
    }
}
