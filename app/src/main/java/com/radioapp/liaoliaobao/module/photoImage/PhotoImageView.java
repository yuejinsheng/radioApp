package com.radioapp.liaoliaobao.module.photoImage;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.PayBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-10
 */
public interface PhotoImageView extends BaseView {

    /**
     * 阅后即焚
     * @param position
     */
    void mediaVieweSuccess(int position);

    /**
     * 红包图片
     * @param payBean
     */
    void redPay(PayBean payBean,int position );

    void delSuccess(int position);
}
