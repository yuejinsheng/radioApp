package com.radioapp.liaoliaobao.module.user.auth;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.radioapp.liaoliaobao.R;

/**
 * 功能：认证提交页面
 * 描述
 * Created by yue on 2019-10-08
 */
public class AuthApplySuccessFragment extends BaseRiggerFragment {
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_auth_apply_success;
    }

    @Override
    protected void initView() {
        initToolbar("认证提交", true, false, null);
    }

    @Override
    public void tokenInvalidRefresh() {

    }
}
