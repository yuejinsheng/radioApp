package com.radioapp.liaoliaobao.module.radio.list;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.manager.ServiceManager;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.api.RadioService;
import com.radioapp.liaoliaobao.base.BaseRiggerRecyclerFragment;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.user.auth.AuthApplyFragment;
import com.radioapp.liaoliaobao.module.user.auth.AuthIDFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.IsVipUtil;
import com.radioapp.liaoliaobao.utils.dialog.AlertDialogutils;
import com.radioapp.liaoliaobao.utils.dialog.DialogMeetingComment;
import com.radioapp.liaoliaobao.utils.timePicker.SelectImageUtils;
import com.trello.rxlifecycle2.LifecycleTransformer;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-01
 */
public class RadioListFragment extends BaseRiggerRecyclerFragment<RadioListBean>
        implements BaseQuickAdapter.OnItemChildClickListener, RadioListView {

    RadioListAdapter adapter;
    private RadioListPresenter presenter;
    private List<LocalMedia> selectList = new ArrayList<>();
    private int id;
    private int position;
    private  Integer gender;
    private  Integer cityId;
    private Integer provinceId;

    @Override
    protected void initData() {
        initPreserter();
        adapter = new RadioListAdapter();
        adapter.setOnItemChildClickListener(this);
    }


    /**
     * 选择性别
     */
    public void  setGender(Integer gender){
        this.gender=gender;
        setRefreshParams();

    }
    public void setCity(Integer cityId,Integer provinceId){
        this.cityId=cityId;
        this.provinceId =provinceId;
        setRefreshParams();
    }


    @Override
    protected BaseQuickAdapter<RadioListBean, BaseViewHolder> getAdapter() {
        return adapter;
    }

    @Override
    protected Observable<PageBean<RadioListBean>> getUrl(int pageNo) {
        return ServiceManager.create(RadioService.class)
                .getBroadcastsList(gender, cityId, provinceId, pageNo)
                .compose(RxHelper.handleFlatMap());
    }


    /**
     * 初始化presenter
     */
    public synchronized BasePresenter initPreserter() {
        if (presenter == null) {
            presenter = new RadioListPresenter();
            presenter.attachView(this);
            presenter.mContext = mActivity;
        }
        return presenter;
    }

    @Override
    public <T> LifecycleTransformer<T> bindToLife() {
        return bindToLifecycle();
    }

    @Override
    public void tokenInvalid() {
        tokenRefresh();
    }


    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        RadioListBean radioListBean = (RadioListBean) adapter.getData().get(position);
        switch (view.getId()) {
            case R.id.iv_radio_avatar:
                UiHelper.showUserInfoDetailFragment(mActivity, radioListBean.getUser_id());
                break;
            case R.id.ll_sign: //报名
                if(TextUtils.isEmpty(radioListBean.getIs_apply())) {
                    if (Global.getUserInfo().getGender() == 1) {
                        if (!IsVipUtil.isVip(Global.getUserInfo().getVip_ended_at())) {
                            AlertDialogutils.createDialog(mActivity, "报名约会需要开通会员\n\n是否开通会员？", "开通会员",
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            UiHelper.showMemberFragment(mActivity, Global.getUserInfo().getVip_ended_at());
                                        }
                                    });
                            return;
                        }

                    } else if (Global.getUserInfo().getGender() == 2) {
                        if (TextUtils.isEmpty(Global.getUserInfo().getVerified_at())) {
                            AlertDialogutils.createDialog(mActivity, "报名约会需要先通过身份认证\n\n是否去认证真实？", "去认证",
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //UiHelper.showMemberFragment(mActivity, Global.getUserInfo().getVip_ended_at());
                                            UiHelper.showAuthIDFragment(mActivity);
                                        }
                                    });
                            return;
                        }
                    }

                    this.position = position;
                    this.id = radioListBean.getId();
                    AlertDialogutils.createDialog(mContext, "申请查看需要给对方发送一张照片\n\n确定申请嘛？", "确定", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SelectImageUtils.showALlImage(RadioListFragment.this, selectList, 1, PictureConfig.CHOOSE_REQUEST);
                        }
                    });
                }

                break;
            case R.id.ll_like: //点赞
                presenter.like(radioListBean.getId(), position);
                break;
            case R.id.ll_del: //删除广播
                AlertDialogutils.createDialog(mActivity, "确定删除该条约会电台吗？", "确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ToastUitl.showLong("接口删除没有广播id");
                    }
                });
                break;
            case R.id.ll_comment: //评论
                if (radioListBean.getOpen_comment()==0) {
                    ToastUitl.showLong("禁止评论");
                    return;
                }
                if (Global.getUserInfo().getGender() == 1) {
                    if (!IsVipUtil.isVip(Global.getUserInfo().getVip_ended_at())) {
                        ToastUitl.showLong("只有会员才可以评论");
                        return;
                    }

                } else if (Global.getUserInfo().getGender() == 2) {
                    if (TextUtils.isEmpty(Global.getUserInfo().getVerified_at())) {
                        ToastUitl.showLong("只有通过认证的用户才可以评论");
                        return;
                    }
                }
               new  DialogMeetingComment(mActivity, new DialogMeetingComment.SendListener() {
                   @Override
                   public void send(String s) {
                     presenter.addBroadcaseComment(radioListBean.getId(),s);
                   }
               }).show();

                break;
        }
    }

    @Override
    public void likeSuccess(int position) {
        RadioListBean radioListBean = adapter.getData().get(position);
        radioListBean.setLike_count(radioListBean.getLike_count() + 1);
        radioListBean.setIs_like("已点赞");
        adapter.notifyItemChanged(position);
    }

    @Override
    public void datingApplySuccess() {
        selectList.clear();
        RadioListBean radioListBean = adapter.getData().get(position);
        radioListBean.setIs_apply("已报名");
        adapter.notifyItemChanged(position);
    }

    @Override
    public void commentSuccess() {
        ToastUitl.showLong("评论成功");
        adapter.notifyDataSetChanged();
    }

    @Override
    public void radioInfo(List<RadioListBean> radioListBeans) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择
                    selectList = PictureSelector.obtainMultipleResult(data);
                    if (selectList.get(0) != null) {
                        String path = getpathImage(selectList.get(0));
                        presenter.datingApply(id, path);
                    }
                    break;
            }
        }
    }

    /**
     * 获取图片，
     *
     * @param model
     * @return
     */
    public String getpathImage(LocalMedia model) {
        String path = "";
        if (model.isCut() && !model.isCompressed()) {
            // 裁剪过
            path = model.getCutPath();
        } else if (model.isCompressed() || (model.isCut() && model.isCompressed())) {
            // 压缩过,或者裁剪同时压缩过,以最终压缩过图片为准
            path = model.getCompressPath();
        } else {
            // 原图
            path = model.getPath();
        }
        return path;
    }
}
