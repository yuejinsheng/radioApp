package com.radioapp.liaoliaobao.module.user;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.user.UpdateMediaBean;
import com.radioapp.liaoliaobao.bean.user.UserBean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-23
 */
public interface UserView extends BaseView {

    void getUserInfo(UserBean userBean);
    void loginOutSuccess();

    void generateCodeSuccess();

    void  avatarUrl(String avatar_url);

    void updateMedia(List<UpdateMediaBean> updateMediaBeans);
}
