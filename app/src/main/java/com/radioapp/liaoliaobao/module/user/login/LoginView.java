package com.radioapp.liaoliaobao.module.user.login;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.user.LoginBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-16
 */
public interface LoginView extends BaseView {

    void success(LoginBean loginBean);
}
