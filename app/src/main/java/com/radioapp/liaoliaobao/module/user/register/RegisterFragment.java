package com.radioapp.liaoliaobao.module.user.register;

import android.Manifest;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.token.TokenUtil;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.enumbean.SendSmsUrlEnum;
import com.radioapp.liaoliaobao.bean.user.LoginBean;
import com.radioapp.liaoliaobao.module.lifecyclerObserver.RxPermissionsFragmentObserver;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.BaiDuMapUtil;
import com.radioapp.liaoliaobao.utils.EaseUIUtils;
import com.radioapp.liaoliaobao.utils.RegexUtil;
import com.radioapp.liaoliaobao.view.sendcode.SendCodeTypeView;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能： 注册
 * 描述
 * Created by yue on 2019-07-15
 */
public class RegisterFragment extends BaseRiggerFragment<RegisterView, RegisterPresenter> implements RegisterView,
        RxPermissionsFragmentObserver.PermissionCallBack {
    @BindView(R.id.root_toolbar)
    FrameLayout toolbar;
    @BindView(R.id.toolbar_title_tv)
    TextView toolbarTitleTv;
    @BindView(R.id.et_register_mobile)
    EditText etRegisterMobile;
    @BindView(R.id.et_register_code)
    EditText etRegisterCode;
    @BindView(R.id.tv_sendCode)
    SendCodeTypeView tvSendCode;
    @BindView(R.id.et_register_pwd)
    EditText etRegisterPwd;
    @BindView(R.id.et_register_againPwd)
    EditText etRegisterAgainPwd;
    @BindView(R.id.et_register_from)
    EditText etRegisterFrom;
    @BindView(R.id.cb_agree)
    CheckBox cbAgree;
    @BindView(R.id.register_document)
    TextView registerDocument;
    @BindView(R.id.tv_register_next)
    TextView tvRegisterNext;
    BaiDuMapUtil gaoDeMapUtil;
    BDLocation mapLocation;

    RxPermissionsFragmentObserver rxPermissionsObserver;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    public void initView() {
        toolbar.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.color_5E9));
        initToolbar("手机注册", true, false, null);
        tvSendCode.setEtPhone(etRegisterMobile);
        //设置url地址
        tvSendCode.setUrl(SendSmsUrlEnum.REGISTERSMSURL.getCode());

        //权限申请
        rxPermissionsObserver = new RxPermissionsFragmentObserver(this);
        getLifecycle().addObserver(rxPermissionsObserver);
        rxPermissionsObserver.setCallBack(this);
        rxPermissionsObserver.requestPermissions(0, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA);

    }

    @Override
    public void tokenInvalidRefresh() {

    }


    /**
     * 高德地图定位
     */
    private void initGaode() {
        if (gaoDeMapUtil == null) {
            gaoDeMapUtil = BaiDuMapUtil.getInstance();

            gaoDeMapUtil.setContext(mActivity).setListener(new BaiDuMapUtil.GaoDeAMapLocationListener() {
                @Override
                public void onLocationChanged(BDLocation aMapLocation) {
                    mapLocation = aMapLocation;
                    LogUtils.e("获取纬度-->" + aMapLocation.getLatitude() + "经度" + aMapLocation.getLongitude());
                }
            });
            getLifecycle().addObserver(gaoDeMapUtil);
        }
    }


    @OnClick({R.id.register_document,R.id.register_document1, R.id.tv_register_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.register_document:  //注册协议
                UiHelper.showProtocolFragment(mActivity,"file:///android_asset/user_agree.html");
                break;
            case R.id.register_document1:  //隐私协议
                UiHelper.showProtocolFragment(mActivity,"file:///android_asset/user_agree1.html");
                break;
            case R.id.tv_register_next:  //下一步
                if (checkPhone(etRegisterMobile.getText().toString(), etRegisterPwd.getText().toString(),
                        etRegisterAgainPwd.getText().toString(),
                        etRegisterCode.getText().toString(), etRegisterFrom.getText().toString()))
                    mPresenter.register(etRegisterMobile.getText().toString(), etRegisterPwd.getText().toString(),
                            etRegisterCode.getText().toString(), etRegisterFrom.getText().toString(),
                            mapLocation == null ? -1 : mapLocation.getLongitude(),
                            mapLocation == null ? -1 : mapLocation.getLatitude());

                break;
        }
    }


    /**
     * 检查手机号.
     **/
    private boolean checkPhone(String phone, String pwd, String againPwd, String code, String from) {
        if (TextUtils.isEmpty(phone)) {
            ToastUitl.showLong(getString(R.string.login_hint_account));
            return false;
        }
        if (TextUtils.isEmpty(pwd)) {
            ToastUitl.showLong(getString(R.string.login_hint_pwd));
            return false;
        }
        if (TextUtils.isEmpty(code)) {
            ToastUitl.showLong(getString(R.string.register_code_input_hint));
            return false;
        }

        if (TextUtils.isEmpty(from)) {
            ToastUitl.showLong("请输入你从哪里了解到撩撩宝");
            return false;
        }

        if (!RegexUtil.isMobile(phone)) {
            ToastUitl.showLong(getString(R.string.code_correct_phone_hint));
            return false;
        }
        if (!RegexUtil.isPassword(pwd)) {
            ToastUitl.showLong(getString(R.string.login_pwd_correct));
            return false;
        }

        if (!pwd.equals(againPwd)) {
            ToastUitl.showLong(getString(R.string.register_again_equast));
            return false;
        }
        if (!cbAgree.isChecked()) {
            ToastUitl.showLong(R.string.register_again_cbx);
            return false;
        }


        return true;
    }

    @Override
    public void register(LoginBean loginBean) {
        //注册成功，
        //保存token
        TokenUtil.putTokenType(loginBean.getToken_type());
        TokenUtil.putAccessToken(loginBean.getAccess_token());
        EaseUIUtils.login(String.valueOf(loginBean.getEasemob_id()), loginBean.getEasemob_password());
        //选择性别
        UiHelper.showSelectGenderFragment(mActivity);
    }


    @Override
    public void granted(int type) {
        initGaode();
    }

    @Override
    public void shouldShowRequestPermissionRationale() {

    }

    @Override
    public void other() {

    }


}
