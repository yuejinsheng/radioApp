package com.radioapp.liaoliaobao.module.user.register.code;

import com.jaydenxiao.common.base.BaseView;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-26
 */
public interface CodeView extends BaseView {
     void success();
     void setInviteCode(String inviteCode);
}
