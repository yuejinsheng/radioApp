package com.radioapp.liaoliaobao.module.index;

import android.app.VoiceInteractor;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.support.v4.app.Fragment;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.baidu.platform.comapi.map.E;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.TabEntity;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.find.FindFragment;
import com.radioapp.liaoliaobao.module.home.HomeFragment;
import com.radioapp.liaoliaobao.module.message.MessageFragment;
import com.radioapp.liaoliaobao.module.radio.RadioFragment;
import com.radioapp.liaoliaobao.module.user.UserFragment;
import com.radioapp.liaoliaobao.module.user.login.LoginFragment;

import java.util.ArrayList;
import java.util.List;

import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能
 * 描述
 * Created by yue on 2019-07-03
 */
public class MainBarLifecycleObserver implements LifecycleObserver {
    private CommonTabLayout mTabLayout;

    private String[] mTitles = {"首页", "电台", "消息", "我的"};
    private int[] mIconUnselectIds = {
            R.mipmap.tab_home_pressed, R.mipmap.tab_radio_pressed,
           R.mipmap.tab_message_pressed, R.mipmap.tab_user_pressed};


    private int[] mIconSelectIds = {
            R.mipmap.tab_home_pressed, R.mipmap.tab_radio_pressed,
            R.mipmap.tab_message_pressed, R.mipmap.tab_user_pressed};

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();


    private HomeFragment homeFragment = new HomeFragment();
    private RadioFragment radioFragment = new RadioFragment();
   // private FindFragment findFragment = new FindFragment();
    private MessageFragment messageFragment = new MessageFragment();
    private UserFragment userFragment = new UserFragment();

    private MainActivity activity;


    public MainBarLifecycleObserver(CommonTabLayout mTabLayout, MainActivity activity) {
        Log.e("android,", "注入");
        this.mTabLayout = mTabLayout;
        this.activity = activity;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void onCreate() {
        Log.e("android,", "onCreate");
        initNavigationBar();


    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {
        setTabCountTotal();
      //  setTabCountTotal();
    }

    private void initNavigationBar() {
        mFragments.add(homeFragment);
        mFragments.add(radioFragment);
      //  mFragments.add(findFragment);
        mFragments.add(messageFragment);
        mFragments.add(userFragment);

        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }

        mTabLayout.setTabData(mTabEntities, activity, R.id.fragment_container, mFragments);


        setSelectVis(0);
        mTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                LogUtils.e("onTabSelect:"+position);
                setSelectVis(position);
            }

            @Override
            public void onTabReselect(int position) {
                LogUtils.e("onTabReselect:"+position);
            }
        });



    }


    public void setTabCountTotal() {
        if (mTabLayout != null) {
            //显示小红点
            //  tabLayout.showDot(1);
//显示消息数
            if (SharePref.getInt(Constants.Notice_APPLY_LOOK,0)==1||
                    SharePref.getInt(Constants.Notice_DONGTAI,0)==1||
                    SharePref.getInt(Constants.Notice_RADIO,0)==1||
                    SharePref.getInt(Constants.Notice_RETURN,0)==1||
                    SharePref.getInt(Constants.Notice_System,0)==1){
                mTabLayout.showDot(2);
            }else {
                mTabLayout.hideMsg(2);
            }


            if(getUnreadMsgCountTotal()==0){
                mTabLayout.hideMsg(2);
                // tabLayout.showMsg(3, getUnreadMsgCountTotal());
            }else{
                mTabLayout.showMsg(2, getUnreadMsgCountTotal());
            }

        }
    }

    /**
     * get unread message count
     *
     * @return
     */
    public int getUnreadMsgCountTotal() {
        return EMClient.getInstance().chatManager().getUnreadMessageCount();
    }



















    private void  setSelectVis(Integer position){
        for (int i = 0; i < mTitles.length; i++) {
            ImageView imageView = mTabLayout.getIconView(i);
            TextView textView = mTabLayout.getTitleView(i);
            if(position==null){
                textView.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
            }else{
                if(position==i){
                    textView.setVisibility(View.VISIBLE);
                    imageView.setVisibility(View.GONE);
                }else{
                    textView.setVisibility(View.GONE);
                    imageView.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
