package com.radioapp.liaoliaobao.module.user.wallet;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.user.WalletBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.user.auth.AuthApplyFragment;
import com.radioapp.liaoliaobao.module.user.auth.AuthIDFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.MoneyExchange;
import com.radioapp.liaoliaobao.utils.dialog.AlertDialogutils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能：钱包
 * 描述
 * Created by yue on 2019-10-08
 */
public class WalletFragment extends BaseRiggerFragment<WalletView, WalletPresenter> implements WalletView {


    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.tv_frozen_amount)
    TextView tvFrozenAmount;
    @BindView(R.id.tv_withdraw_amount)
    TextView tvWithdrawAmount;
    @BindView(R.id.tv_alipay_account)
    TextView tvAlipayAccount;
    @BindView(R.id.tv_withdraw_amount1)
    TextView tvWithdraw_amount1;
    WalletBean walletBean;

    private int canWithdraw;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_wallet;
    }

    @Override
    protected void initView() {
        initToolbar("钱包", true, false, null);

    }

    @Override
    public void tokenInvalidRefresh() {

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.account();
    }

    @Override
    public void data(WalletBean walletBean) {
        this.walletBean = walletBean;
        tvBalance.setText(MoneyExchange.exchange(walletBean.getCash()));
        tvFrozenAmount.setText(MoneyExchange.exchange(walletBean.getFrozen()));
        tvWithdrawAmount.setText(MoneyExchange.exchange(walletBean.getCash() - walletBean.getWithdrawal()));
        canWithdraw = walletBean.getCash() - walletBean.getWithdrawal();
        tvWithdraw_amount1.setText(MoneyExchange.exchange(canWithdraw));
        tvAlipayAccount.setText(Global.getUserInfo().getAlipay_account());
    }


    @OnClick(R.id.tv_wallet_commit)
    public void onViewClicked() {
        if (walletBean.getCash() == 0) {
            ToastUitl.showLong("可提现金额为0");
            return;
        }
        if (TextUtils.isEmpty(Global.getUserInfo().getVerified_at())) {
            AlertDialogutils.createDialog(mActivity, "只有认证用户才能申请提现,去认证?", "去认证", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UiHelper.showAuthIDFragment(mActivity);
                }
            });
            return;
        }

        if (walletBean.getWithdrawal() > 0) {
            ToastUitl.showLong("你有一笔金额正在提现中，\n暂时无法发起新的提现请求");
            return;
        }

        if ((walletBean.getCash() - walletBean.getWithdrawal()) > 0 && !TextUtils.isEmpty(tvAlipayAccount.getText().toString())) {
            mPresenter.apply(String.valueOf(canWithdraw / 100 * 100));
        }
    }


    @Override
    public void success() {
        ToastUitl.showLong("提现成功");
        Rigger.getRigger(this).close();
    }

}
