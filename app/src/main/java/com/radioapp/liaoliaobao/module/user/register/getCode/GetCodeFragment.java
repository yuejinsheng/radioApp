package com.radioapp.liaoliaobao.module.user.register.getCode;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.utils.FragmentUtil;

/**
 * 功能： 收到的验证码
 * 描述
 * Created by yue on 2019-08-23
 */
public class GetCodeFragment extends BaseRiggerFragment {
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_get_code;
    }

    @Override
    protected void initView() {
         initToolbar("收到邀请码",true,false,null);
        FragmentUtil.replaceFragment(getChildFragmentManager(), R.id.code_fragment_container, new GetCodeListFragment());
    }

    @Override
    public void tokenInvalidRefresh() {

    }
}
