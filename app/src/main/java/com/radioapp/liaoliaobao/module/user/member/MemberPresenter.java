package com.radioapp.liaoliaobao.module.user.member;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.user.VipBean;

import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-26
 */
public class MemberPresenter extends BasePresenter<MemberView> {


    /**
     * vip说明
     */
    public void getVipExplain() {
        ServiceManager.create(UserService.class)
                .getvipExplain()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<List<VipBean>>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(List<VipBean> vipBeans) {
                        mView.vipExplain(vipBeans);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });

    }

    /**
     * vip等级(价格)
     */
    public void getVipMenuPrice() {
        ServiceManager.create(UserService.class)
                .getVipMenuPrice()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<List<VipBean>>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(List<VipBean> vipBeans) {
                        mView.VipMenuPrice(vipBeans);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 支付
     *
     * @param payMethod
     * @param vipType
     */
    public void vipPay(int payMethod, int vipType) {
        ServiceManager.create(UserService.class)
                .vipPay(payMethod, vipType)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<PayBean>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(PayBean payBean) {
                        mView.vipPay(payBean);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }
}
