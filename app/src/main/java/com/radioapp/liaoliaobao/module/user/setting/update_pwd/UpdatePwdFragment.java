package com.radioapp.liaoliaobao.module.user.setting.update_pwd;

import android.text.TextUtils;
import android.widget.EditText;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.enumbean.SendSmsUrlEnum;
import com.radioapp.liaoliaobao.utils.RegexUtil;
import com.radioapp.liaoliaobao.view.sendcode.SendCodeTypeView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能： 修改密码
 * 描述
 * Created by yue on 2019-08-08
 */
public class UpdatePwdFragment extends BaseRiggerFragment<UpdatePwdView, UpdatePwdPresenter> implements UpdatePwdView {
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.tv_code)
    SendCodeTypeView tvCode;
    @BindView(R.id.et_pwd)
    EditText etPwd;
    @BindView(R.id.et_again_pwd)
    EditText etAgainPwd;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_update_pwd;
    }

    @Override
    protected void initView() {
        initToolbar(getString(R.string.update_pwd_title), true, false, null);
        tvCode.setEtPhone(etMobile);
        //设置url地址
        tvCode.setUrl(SendSmsUrlEnum.UPDATEPWDSMSURL.getCode());
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    @OnClick(R.id.tv_commit)
    public void onViewClicked() {
        if (checkPhone(etMobile.getText().toString(), etPwd.getText().toString(), etAgainPwd.getText().toString(), etCode.getText().toString())) {
            //请求数据，修改密码
            mPresenter.update(etMobile.getText().toString(), Integer.parseInt(etCode.getText().toString()), etPwd.getText().toString());
        }
    }

    /**
     * 校验
     **/
    private boolean checkPhone(String phone, String pwd, String againPwd, String code) {
        if (TextUtils.isEmpty(phone)) {
            ToastUitl.showLong(getString(R.string.update_pwd_mobile_hint));
            return false;
        }
        if (!RegexUtil.isMobile(phone)) {
            ToastUitl.showLong(getString(R.string.code_correct_phone_hint));
            return false;
        }

        if (TextUtils.isEmpty(code)) {
            ToastUitl.showLong(getString(R.string.register_code_input_hint));
            return false;
        }
        if (TextUtils.isEmpty(pwd)) {
            ToastUitl.showLong(getString(R.string.login_hint_pwd));
            return false;
        }

        if (!RegexUtil.isPassword(pwd)) {
            ToastUitl.showLong(getString(R.string.login_pwd_correct));
            return false;
        }

        if (!pwd.equals(againPwd)) {
            ToastUitl.showLong(getString(R.string.register_again_equast));
            return false;
        }

        return true;
    }

    @Override
    public void success() {
        //销毁当前页面
        Rigger.getRigger(this).close();
    }
}



