package com.radioapp.liaoliaobao.module.user.blacklist;

import com.jaydenxiao.common.base.BaseView;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-29
 */
public interface BlackListPageView extends BaseView {

    void forbidderBlackSuccess(int position);
}
