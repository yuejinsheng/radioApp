package com.radioapp.liaoliaobao.module.user.material;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.appointment.DatingHopesBean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-22
 */
public interface MaterialView extends BaseView {


    void  success();
    void  avatarUrl(String avatar_url);

    void getDatingHopes(List<DatingHopesBean> datingHopesBeans);
}
