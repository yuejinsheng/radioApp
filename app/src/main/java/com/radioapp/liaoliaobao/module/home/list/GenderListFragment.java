package com.radioapp.liaoliaobao.module.home.list;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.api.HomeService;
import com.radioapp.liaoliaobao.base.BaseRiggerRecyclerFragment;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.home.GenderBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.trello.rxlifecycle2.LifecycleTransformer;

import io.reactivex.Observable;
import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：首页中 性别的所有列表
 * 描述
 * Created by yue on 2019-09-06
 */
public class GenderListFragment extends BaseRiggerRecyclerFragment<GenderBean>
        implements GenderListView, BaseQuickAdapter.OnItemChildClickListener, BaseQuickAdapter.OnItemClickListener {

    private Integer gender;
    private Integer verified;
    private Integer news;
    private String nickname;
    private Integer career_id;
    private Integer city_id;
    private Integer is_vip;
    private String lng;
    private String lat;
    //参数集合
    private ArrayMap<String, Object> params;
    private GenderListPresenter presenter;
    private GenderListAdapter adapter;


    /**
     * 创建framgnet
     *
     * @param gender    性别:1男性，2女性
     * @param verified  是否认证 1认证
     * @param news      新用户 1新用户
     * @param nickname  昵称
     * @param career_id 职业
     * @param city_id   城市
     * @param is_vip    vip会员 传1
     * @return
     */
    public static Fragment newInterface(Integer gender, Integer verified, Integer news,
                                        Integer career_id, Integer city_id, Integer is_vip, String nickname, String lng, String lat) {
        GenderListFragment genderListFragment = new GenderListFragment();
        genderListFragment.gender=gender;
        genderListFragment.verified=verified;
        genderListFragment.news=news;
        genderListFragment.career_id=career_id;
        genderListFragment.city_id=city_id;
        genderListFragment.is_vip=is_vip;
        genderListFragment.nickname=nickname;
        genderListFragment.lng=lng;
        genderListFragment.lat=lat;
        /*Bundle bundle = new Bundle();
        bundle.putInt(IntentConstant.HOME_GENDER, gender);
        bundle.putInt(IntentConstant.HOME_VERIFIED, verified);
        bundle.putInt(IntentConstant.HOME_NEWS, news);
        bundle.putInt(IntentConstant.HOME_CAREER_ID, career_id);
        bundle.putInt(IntentConstant.HOME_CITY_ID, city_id);
        bundle.putInt(IntentConstant.HOME_IS_VIP, is_vip);
        bundle.putString(IntentConstant.HOME_NICKNAME, nickname);
        bundle.putString(IntentConstant.HOME_LNG, lng);
        bundle.putString(IntentConstant.HOME_LAT, lat);

        genderListFragment.setArguments(bundle);*/
        return genderListFragment;
    }


    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
       /* gender = bundle.getInt(IntentConstant.HOME_GENDER, -1);
        verified = bundle.getInt(IntentConstant.HOME_VERIFIED, -1);
        news = bundle.getInt(IntentConstant.HOME_NEWS, -1);
        career_id = bundle.getInt(IntentConstant.HOME_CAREER_ID, -1);
        city_id = bundle.getInt(IntentConstant.HOME_CITY_ID, -1);
        is_vip = bundle.getInt(IntentConstant.HOME_IS_VIP, -1);
        nickname = bundle.getString(IntentConstant.HOME_NICKNAME, "");
        lng = bundle.getString(IntentConstant.HOME_LNG, "");
        lat = bundle.getString(IntentConstant.HOME_LAT, "");*/
       // params = getParams();
        ArrayMap<String, Object> params = new ArrayMap<>();
        if (gender != -1) params.put("gender", gender);
        if (verified != -1) params.put("verified", verified);
        if (news != -1) params.put("news", news);
        if (career_id != -1) params.put("career_id", career_id);
        if (city_id != -1) params.put("city_id", city_id);
        if (is_vip != -1) params.put("is_vip", is_vip);
        if (!TextUtils.isEmpty(nickname)) params.put("nickname", nickname);
        if (!TextUtils.isEmpty(lat)) params.put("lat", lat);
        if (!TextUtils.isEmpty(lat)) params.put("lng", lng);
      //  this.params
    }


    /**
     * 参数
     */
    private ArrayMap<String, Object> getParams() {
        LogUtils.e("gender/"+gender+"verified/"+verified+"news/"+news);
        ArrayMap<String, Object> params = new ArrayMap<>();
        if (gender != -1) params.put("gender", gender);
        if (verified != -1) params.put("verified", verified);
        if (news != -1) params.put("news", news);
        if (career_id != -1) params.put("career_id", career_id);
        if (city_id != -1) params.put("city_id", city_id);
        if (is_vip != -1) params.put("is_vip", is_vip);
        if (!TextUtils.isEmpty(nickname)) params.put("nickname", nickname);
        if (!TextUtils.isEmpty(lat)) params.put("lat", lat);
        if (!TextUtils.isEmpty(lat)) params.put("lng", lng);
        return params;
    }


    @Override
    protected void initData() {
       this.params= getParams();
        initPreserter();
        adapter = new GenderListAdapter();
        adapter.setOnItemChildClickListener(this);
        adapter.setOnItemClickListener(this);
    }

    @Override
    protected BaseQuickAdapter<GenderBean, BaseViewHolder> getAdapter() {
        return adapter;
    }

    @Override
    protected Observable<PageBean<GenderBean>> getUrl(int pageNo) {
        if (params != null) {
            params.put("page", pageNo);
        }
        return ServiceManager.create(HomeService.class)
                .getUsersLit(params)
                .compose(RxHelper.handleFlatMap());
    }


    /**
     * 初始化presenter
     */
    public synchronized BasePresenter initPreserter() {
        if (presenter == null) {
            presenter = new GenderListPresenter();
            presenter.attachView(this);
            presenter.mContext = mActivity;
        }
        return presenter;
    }

    @Override
    public <T> LifecycleTransformer<T> bindToLife() {
        return bindToLifecycle();
    }

    @Override
    public void tokenInvalid() {
        tokenRefresh();
    }



    //子item点击
    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
       GenderBean genderBean=(GenderBean) adapter.getData().get(position);
        switch (view.getId()) {
            case R.id.tv_follow:
                //收藏
                if(presenter!=null)
                    presenter.follow(genderBean.getId(),genderBean.getFavorite() == 0?1:0,position);
                break;
        }
    }

    //item点击
    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        GenderBean genderBean=(GenderBean) adapter.getData().get(position);
        if(genderBean.getGender()== Global.getUserInfo().getGender()){
            ToastUitl.showLong("同性之间不能查看信息");
            return;
        }
        UiHelper.showUserInfoDetailFragment(mActivity,genderBean.getId());
    }

    //收藏或取消
    @Override
    public void followSuccess(Integer position) {
      GenderBean genderBean=  adapter.getData().get(position);
        if (genderBean.favorite == 0) {
            genderBean.favorite = 1;
        } else {
            genderBean.favorite = 0;
        }
        adapter.notifyItemChanged(position);
    }
}
