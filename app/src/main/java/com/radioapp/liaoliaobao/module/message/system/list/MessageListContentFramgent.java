package com.radioapp.liaoliaobao.module.message.system.list;

import android.os.Bundle;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.enumbean.MessageTitleEnum;
import com.radioapp.liaoliaobao.constant.IntentConstant;

/**
 * 功能：x消息集合类（动态，电台，收入）
 * 描述
 * Created by yue on 2019-08-05
 */
public class MessageListContentFramgent extends BaseRiggerFragment {
    private String title;
    private String type;



    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        title = bundle.getString(IntentConstant.TITLE);
        type=bundle.getString(IntentConstant.TYPE);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_app_message_list;
    }

    @Override
    public void initView() {
        initToolbar(MessageTitleEnum.getMsgByCode(title), true, false, null);
        Rigger.getRigger(this).showFragment(MessageListFragment.newInstance(type,title), R.id.fl_content_layout,true);
        //FragmentUtil.replaceFragment(getChildFragmentManager(), R.id.fl_content_layout, MessageListFragment.newInstance(type));

    }

    @Override
    public void tokenInvalidRefresh() {

    }
}
