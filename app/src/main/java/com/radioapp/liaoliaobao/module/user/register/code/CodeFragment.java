package com.radioapp.liaoliaobao.module.user.register.code;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.user.customer.CustomerFragment;
import com.radioapp.liaoliaobao.module.user.register.getCode.GetCodeFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能：邀请码
 * 描述
 * Created by yue on 2019-07-1
 */
public class CodeFragment extends BaseRiggerFragment<CodeView, CodePresenter> implements CodeView {
    @BindView(R.id.tv_see_code)
    TextView tvSeeCode;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.tv_carryOut)
    TextView tvCarryOut;
    @BindView(R.id.tv_customer_service)
    TextView tvCustomerService;
    @BindView(R.id.tv_jump)
    TextView tvJump;
    @BindView(R.id.tv_code_getCode)
    TextView tvCodeGetCode;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_code;
    }

    @Override
    public void initView() {
        initToolbar("邀请码", true, false, null);
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    @OnClick({R.id.tv_see_code, R.id.tv_carryOut, R.id.tv_customer_service, R.id.tv_jump,
            R.id.tv_code_getCode,R.id.tv_on_jump})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_see_code:
                Rigger.getRigger(mActivity).startFragment(new GetCodeFragment());
                break;
            case R.id.tv_carryOut:  //完成
                if (TextUtils.isEmpty(etCode.getText())) {
                    ToastUitl.showLong("请输入邀请码");
                    return;
                }
                mPresenter.setCode(etCode.getText().toString());
                break;
            case R.id.tv_customer_service: //客服
                //TODO 客服页面
                UiHelper.showCustomerFragment(mActivity);
                break;
            case R.id.tv_jump:  //加入会员
                UiHelper.showMemberFragment(mActivity, Global.getLoginBean().getVip_ended_at());
                break;
            case R.id.tv_code_getCode: //获取邀请码
                mPresenter.getInviteCode();
                break;
            case R.id.tv_on_jump:
                UiHelper.showMainFragment(mActivity);
                Rigger.getRigger(this).close();
                break;

        }
    }

    @Override
    public void success() {
        UiHelper.showMainFragment(mActivity);
        Rigger.getRigger(this).close();
    }

    @Override
    public void setInviteCode(String inviteCode) {
        if (TextUtils.isEmpty(inviteCode)) {
            ToastUitl.showLong("暂时没有收到邀请码");

        } else {
            etCode.setText(inviteCode);
        }
}
}
