package com.radioapp.liaoliaobao.module.user.blacklist;

import android.Manifest;

import com.baidu.location.BDLocation;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.module.lifecyclerObserver.RxPermissionsFragmentObserver;
import com.radioapp.liaoliaobao.utils.BaiDuMapUtil;
import com.radioapp.liaoliaobao.utils.FragmentUtil;

import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：黑名单
 * 描述
 * Created by yue on 2019-09-29
 */
public class BlackListFragment extends BaseRiggerFragment implements RxPermissionsFragmentObserver.PermissionCallBack {
    BaiDuMapUtil gaoDeMapUtil;

    BDLocation mapLocation;
    private RxPermissionsFragmentObserver rxPermissionsObserver;


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_black_list;
    }

    @Override
    protected void initView() {
        initToolbar("黑名单", true, false, null);
        //权限申请
        rxPermissionsObserver = new RxPermissionsFragmentObserver(this);
        getLifecycle().addObserver(rxPermissionsObserver);
        rxPermissionsObserver.setCallBack(this);
        rxPermissionsObserver.requestPermissions(0, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA);


    }

    @Override
    public void tokenInvalidRefresh() {

    }

    /**
     * 高德地图定位
     */
    public void initGaode() {
        if (gaoDeMapUtil == null) {
            LogUtils.e("定位---》");
            gaoDeMapUtil = BaiDuMapUtil.getInstance();
            gaoDeMapUtil.setContext(mActivity).setListener(new BaiDuMapUtil.GaoDeAMapLocationListener() {
                @Override
                public void onLocationChanged(BDLocation aMapLocation) {
                    mapLocation = aMapLocation;
                    LogUtils.e("获取纬度-->" + aMapLocation.getLatitude() + "经度" + aMapLocation.getLongitude());
                    FragmentUtil.replaceFragment(getChildFragmentManager(),R.id.fragment_black_list,BlackListPageFragment.blackListNewInterface(mapLocation.getLongitude() + "", mapLocation.getLatitude() + ""));
                   // Rigger.getRigger(this).showFragment(BlackListPageFragment.blackListNewInterface(mapLocation.getLongitude() + "", mapLocation.getLatitude() + ""), R.id.fragment_black_list);

                }

            });
            getLifecycle().addObserver(gaoDeMapUtil);
        }
    }

    @Override
    public void granted(int type) {
        initGaode();
    }

    @Override
    public void shouldShowRequestPermissionRationale() {
        FragmentUtil.replaceFragment(getChildFragmentManager(),R.id.fragment_black_list,BlackListPageFragment.blackListNewInterface("0",  "0"));
        //Rigger.getRigger(this).showFragment(BlackListPageFragment.blackListNewInterface("0", "0"), R.id.fragment_black_list, true);

    }

    @Override
    public void other() {
       // Rigger.getRigger(this).showFragment(BlackListPageFragment.blackListNewInterface("0", "0"), R.id.fragment_black_list, true);
        FragmentUtil.replaceFragment(getChildFragmentManager(),R.id.fragment_black_list,BlackListPageFragment.blackListNewInterface( "0", "0"));
    }

}
