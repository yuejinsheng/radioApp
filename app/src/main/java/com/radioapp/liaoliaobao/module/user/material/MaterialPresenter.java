package com.radioapp.liaoliaobao.module.user.material;

import android.support.v4.util.ArrayMap;
import android.text.TextUtils;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.api.AppointmentService;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.CommonBean;
import com.radioapp.liaoliaobao.bean.appointment.DatingHopesBean;

import java.io.File;
import java.util.List;

import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-22
 */

public class MaterialPresenter extends BasePresenter<MaterialView> {


    /**
     * 参数 列表
     *
     * @param gender       性别0未知，1男，2女
     * @param nickName     名称
     * @param birth_at     出生日期(YYYY-MM-DD)
     * @param career_id    职业id
     * @param dating_types 约会节目
     * @param dating_hopes 约会期望
     * @param user_regions 用户地区
     * @param wechat       微信号
     * @param qq           qq号
     * @param height       身高cm 取值范围: 30-300
     * @param weight       体重kg 取值范围: 10-400
     * @param intro        个人介绍
     */
    public ArrayMap setParams(int gender, String nickName, String birth_at, int career_id, String dating_types, String dating_hopes,
                              String user_regions, Integer province_id, Integer city_id,
                              String wechat, String qq, String height, String weight, String intro) {

        ArrayMap<String, Object> params = new ArrayMap<>();
        params.put("gender", String.valueOf(gender));
        params.put("nickname", nickName);
        params.put("birth_at", birth_at);
        params.put("career_id", career_id);
        if (!TextUtils.isEmpty(wechat))
            params.put("wechat", wechat);
        if (!TextUtils.isEmpty(qq))
            params.put("qq", qq);
        if (!TextUtils.isEmpty(dating_types))
            params.put("dating_types", dating_types);
        if (!TextUtils.isEmpty(dating_hopes))
            params.put("dating_hopes", dating_hopes);
        if (!TextUtils.isEmpty(user_regions)) {
            params.put("user_regions", user_regions);
        }
        params.put("province_id", province_id);
        params.put("city_id", city_id);
        if (!TextUtils.isEmpty(height))
            params.put("height", height);
        if (!TextUtils.isEmpty(weight))
            params.put("weight", weight);
        if (!TextUtils.isEmpty(intro))
            params.put("intro", intro);
        //  params.put("bwh_str", bwh_str);
        return params;
    }


    public boolean isFlag(String nickName, int career_id, String birth_at, String dating_types, String dating_hopes,
                          String user_regions, Integer province_id, Integer city_id,
                          String wechat, String qq) {
        if (TextUtils.isEmpty(nickName)) {
            ToastUitl.showLong(mContext.getString(R.string.material_nickName_hint));
            return false;
        }
        if (career_id == -1) {
            ToastUitl.showLong(mContext.getString(R.string.material_job_hint));
            return false;
        }
        if (TextUtils.isEmpty(birth_at)) {
            ToastUitl.showLong(mContext.getString(R.string.material_age_hint));
            return false;
        }
        if (TextUtils.isEmpty(dating_types)) {
            ToastUitl.showLong("请选择约会节目");
            return false;
        }
        if (TextUtils.isEmpty(dating_hopes)) {
            ToastUitl.showLong("请选择约会期望");
            return false;
        }
        if (TextUtils.isEmpty(user_regions)) {
            ToastUitl.showLong("请选择约会范围");
            return false;
        }
        if (province_id == null || city_id == null||province_id==0||city_id==0) {
            ToastUitl.showLong("请选择地区");
            return false;
        }
        if (TextUtils.isEmpty(wechat) && TextUtils.isEmpty(qq)) {
            ToastUitl.showLong("请填写微信或者qq");
            return false;
        }
        return true;
    }


    /**
     * 上传用户信息
     *
     * @param params
     */
    public void updateInfo(ArrayMap<String, Object> params) {
        ServiceManager.create(UserService.class)
                .updateUserInfo(params)
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.success();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });

    }

    /**
     * 上传头像
     *
     * @param file
     */
    public void uploadAcatar(File file) {
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("image/png"), file);
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        ServiceManager.create(UserService.class)
                .uploadAvatal(body)
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<CommonBean>(mContext) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(CommonBean commonBean) {
                        mView.avatarUrl(commonBean.getAvatar_url());
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 获取约会期望列表
     */
    public void getDatingHopes() {
        ServiceManager.create(AppointmentService.class)
                .getDatingHopesList()
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<List<DatingHopesBean>>(mContext) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(List<DatingHopesBean> datingHopesBeans) {
                        mView.getDatingHopes(datingHopesBeans);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });

    }


}
