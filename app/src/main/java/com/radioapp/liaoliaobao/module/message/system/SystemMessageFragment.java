package com.radioapp.liaoliaobao.module.message.system;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.sharepref.SharePref;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.enumbean.MessageTitleEnum;
import com.radioapp.liaoliaobao.bean.message.MessageListBean;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.module.message.system.list.MessageListContentFramgent;
import com.radioapp.liaoliaobao.uihelper.UiHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 功能：系统消息
 * 描述
 * Created by yue on 2019-08-05
 */
public class SystemMessageFragment extends BaseRiggerFragment<SystemMessageView, SystemMessagePresenter> implements SystemMessageView {
    @BindView(R.id.tv_message_count)
    TextView tvMessageCount;
    @BindView(R.id.tv_dynamic_count)
    TextView tvDynamicCount;
    @BindView(R.id.tv_radio_count)
    TextView tvRadioCount;
    @BindView(R.id.tv_apply_count)
    TextView tvApplyCount;
    @BindView(R.id.tv_earnings_count)
    TextView tvEarningsCount;
    @BindView(R.id.tv_sys_message_content)
    TextView tvSysMessageContent;
    @BindView(R.id.tv_message_radio_content)
    TextView tvMessageRadioContent;
    @BindView(R.id.tv_message_apply_content)
    TextView tvMessageApplyContent;
    @BindView(R.id.tv_message_earnings_content)
    TextView tvMessageEarningsContent;


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_system_message;
    }

    @Override
    protected void initView() {
        mPresenter.getMessageList("8,9,10,11,12,13", 1);
        mPresenter.getMessageList("3,4", 2);
        mPresenter.getMessageList("6", 3);

        //系统消息
        if (SharePref.getInt(Constants.Notice_System, 0) == 1) {
            tvMessageCount.setVisibility(View.VISIBLE);
            tvMessageCount.setText("1");
        } else {
            tvMessageCount.setVisibility(View.GONE);
        }
        //查看申请
        if (SharePref.getInt(Constants.Notice_APPLY_LOOK, 0) == 1) {
            tvApplyCount.setText("1");
            tvApplyCount.setVisibility(View.VISIBLE);
        } else {
            tvApplyCount.setVisibility(View.GONE);
        }

        //广播,电台
        if (SharePref.getInt(Constants.Notice_RADIO, 0) == 1) {
            tvRadioCount.setText("1");
            tvRadioCount.setVisibility(View.VISIBLE);
        } else {
            tvRadioCount.setVisibility(View.GONE);
        }
        //收益提醒
        if (SharePref.getInt(Constants.Notice_RETURN, 0) == 1) {
            tvEarningsCount.setText("1");
            tvEarningsCount.setVisibility(View.VISIBLE);
        } else {
            tvEarningsCount.setVisibility(View.GONE);
        }


    }

    @Override
    public void tokenInvalidRefresh() {

    }


    @OnClick({R.id.rl_message, R.id.rl_dynamic, R.id.rl_radio, R.id.rl_apply, R.id.rl_earnings})
    public void onViewClicked(View view) {
        MessageListContentFramgent fragment = new MessageListContentFramgent();
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.rl_message: //唯TA消息
                SharePref.saveInt(Constants.Notice_System, 0);
                bundle.putString(IntentConstant.TITLE, MessageTitleEnum.MESSAGE.getCode());
                bundle.putString(IntentConstant.TYPE, "8,9,10,11,12,13");
                UiHelper.showMessageListContentFragment(mActivity, bundle);
                //   Rigger.getRigger(mActivity).startFragment(new AppMessageListFragment());
                break;
            case R.id.rl_dynamic: //动态消息
                bundle.putString(IntentConstant.TITLE, MessageTitleEnum.DYNAMIC.getCode());
                bundle.putString(IntentConstant.TYPE, "123");
                UiHelper.showMessageListContentFragment(mActivity, bundle);
                break;
            case R.id.rl_radio: //电台消息
                SharePref.saveInt(Constants.Notice_RADIO, 0);
                bundle.putString(IntentConstant.TITLE, MessageTitleEnum.RADIO.getCode());
                bundle.putString(IntentConstant.TYPE, "6");
                UiHelper.showMessageListContentFragment(mActivity, bundle);
                break;
            case R.id.rl_apply:  //查看申请
                SharePref.saveInt(Constants.Notice_APPLY_LOOK, 0);
                bundle.putString(IntentConstant.TITLE, MessageTitleEnum.APPLY.getCode());
                bundle.putString(IntentConstant.TYPE, "3,4");
                UiHelper.showMessageListContentFragment(mActivity, bundle);
                break;
            case R.id.rl_earnings: //收入提醒
                SharePref.saveInt(Constants.Notice_RETURN, 0);
                bundle.putString(IntentConstant.TITLE, MessageTitleEnum.INCOME.getCode());
                bundle.putString(IntentConstant.TYPE, "1,2,7");
                UiHelper.showMessageListContentFragment(mActivity, bundle);
                break;
        }
    }

    //获取数据列表
    @Override
    public void getList(List<MessageListBean> messageListBeans, Integer position) {
        if (position == 1) {
            //系统消息
            if (messageListBeans != null && messageListBeans.size() > 0) {
                tvSysMessageContent.setText(messageListBeans.get(0).getData().getBody());
            }

        } else if (position == 2) {
            //查看申请
            if (messageListBeans != null && messageListBeans.size() > 0) {
                tvMessageApplyContent.setText(messageListBeans.get(0).getData().getContent());
            }
            //
        } else if (position == 3) {
            //电台消息
            if (messageListBeans != null && messageListBeans.size() > 0) {
                tvMessageRadioContent.setText(messageListBeans.get(0).getData().getContent());
            }

        }
    }


}
