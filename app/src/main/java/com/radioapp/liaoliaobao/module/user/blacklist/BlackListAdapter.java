package com.radioapp.liaoliaobao.module.user.blacklist;

import android.graphics.Color;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.url.BaseConstant;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.user.BlackListBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.utils.AssetsUtils;
import com.radioapp.liaoliaobao.utils.GetAgeUtils;
import com.radioapp.liaoliaobao.utils.IsVipUtil;
import com.radioapp.liaoliaobao.utils.TimeFormatExchangeUtils;

import java.text.ParseException;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-29
 */
public class BlackListAdapter extends BaseQuickAdapter<BlackListBean, BaseViewHolder> {
    public BlackListAdapter() {
        super(R.layout.item_black_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, BlackListBean item) {
        helper.setText(R.id.item_black_list_name, item.getNickname());
        GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getAvatar(), helper.getView(R.id.item_black_list_avatar));
        if (Global.getUserInfo().getGender() == 1) {
            if (TextUtils.isEmpty(item.getVerified_at())) {
                helper.setImageResource(R.id.item_black_list_avater_flag, R.mipmap.icon_follow_select);
            } else {
                helper.setImageResource(R.id.item_black_list_avater_flag, R.mipmap.icon_follow_normal);
            }

        } else if (Global.getUserInfo().getGender() == 2) {
            // TODO 其他的显示,图标不确定， 要改
            if (IsVipUtil.isVip(item.getVerified_at())) {
                helper.setImageResource(R.id.item_black_list_avater_flag, R.mipmap.icon_follow_select);
            } else {
                helper.setImageResource(R.id.item_black_list_avater_flag, R.mipmap.icon_follow_normal);
            }
        }


        String time = "在线";

        //距离和在线时间
//            if (data.hide_location == 1) {

        time = TimeFormatExchangeUtils.calculateTime(item.getUpdated_at());
        if (time.equals("在线")) {
            helper.setTextColor(R.id.item_black_list_distance_time, Color.parseColor("#5940B8"));
        } else {
            helper.setTextColor(R.id.item_black_list_distance_time, Color.parseColor("#333333"));
        }
        helper.setText(R.id.item_black_list_distance_time, "保密" + " · " + time);
//            } else {
        time = TimeFormatExchangeUtils.calculateTime(item.getUpdated_at());
        int distance = item.getDistance();
        if (time.equals("在线")) {
            helper.setTextColor(R.id.item_black_list_distance_time, Color.parseColor("#5940B8"));
        } else {
            helper.setTextColor(R.id.item_black_list_distance_time, Color.parseColor("#333333"));
        }
        if (distance / 1000 > 0) {
            if (distance % 1000 / 100 == 0) {
                helper.setText(R.id.item_black_list_distance_time, distance / 1000 + "km · " + time);
            } else {
                helper.setText(R.id.item_black_list_distance_time, distance / 1000 + "." + distance % 1000 / 100 + "km · " + time);
            }
        } else {
            helper.setText(R.id.item_black_list_distance_time, distance + "m · " + time);
        }


        //获取职业的name
        String careerName = (String) AssetsUtils.newInterface().getCareersId(item.getCareer_id()).get("careerName");
        careerName = TextUtils.isEmpty(careerName) ? "" : careerName;
        int ageByBirth = 100;
        if (!TextUtils.isEmpty(item.getBirth_at())) {
            try {
                ageByBirth = GetAgeUtils.getAgeByBirth(item.getBirth_at());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (item.getCity_id() != 0) {
            //获取城市的name
            String cityName = (String) AssetsUtils.newInterface().getAddress(-1, item.getCity_id(), -1).get("cityName");
            helper.setText(R.id.item_black_list_city_age_occupation, cityName + "\t\t" + ageByBirth + "岁\t\t" + careerName);
        } else {
            helper.setText(R.id.item_black_list_city_age_occupation, ageByBirth + "岁\t\t" + careerName);

        }

        //解除黑名单
        helper.addOnClickListener(R.id.item_black_list_remove_black);
    }
}
