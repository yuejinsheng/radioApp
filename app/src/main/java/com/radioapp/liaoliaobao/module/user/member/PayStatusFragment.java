package com.radioapp.liaoliaobao.module.user.member;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.constant.IntentConstant;

import java.util.Stack;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能： 支付状态页面
 * 描述
 * Created by yue on 2019-08-26
 */
public class PayStatusFragment extends BaseRiggerFragment {
    @BindView(R.id.iv_pay_status)
    ImageView ivPayStatus;
    @BindView(R.id.tv_pay_status)
    TextView tvPayStatus;
    @BindView(R.id.tv_pay_commit)
    TextView tvPayCommit;

    private boolean status;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_pay_status;
    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        status = bundle.getBoolean(IntentConstant.STATUS, false);
    }

    @Override
    protected void initView() {
        initToolbar("会员中心", true, false, null);

        ivPayStatus.setSelected(status);
        tvPayStatus.setText(status ? getString(R.string.member_pay_success_txt) : getString(R.string.member_pay_fail_txt));
        tvPayCommit.setText(status ? getString(R.string.member_pay_goHome_txt) : getString(R.string.member_pay_next_txt));

    }

    @Override
    public void tokenInvalidRefresh() {
    }


    @OnClick(R.id.tv_pay_commit)
    public void onViewClicked() {
        try {
            if (status) {
                Rigger.getRigger(this).close();
                Stack<String> strings = Rigger.getRigger(mActivity).getFragmentStack();
                for (int i = 0; i < strings.size(); i++) {
                    if (i != 0) {
                        Fragment fragment = Rigger.getRigger(mActivity).findFragmentByTag(strings.get(i));
                        Rigger.getRigger(fragment).close();
                    }
                }

            } else {
                Rigger.getRigger(this).close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
