package com.radioapp.liaoliaobao.module.user.member;

import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.user.VipBean;
import com.radioapp.liaoliaobao.utils.MoneyExchange;
import com.radioapp.liaoliaobao.utils.RegexUtil;

import java.time.temporal.ValueRange;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-26
 */
public class MemberAdapter extends BaseQuickAdapter<VipBean, BaseViewHolder> {
    private int selectItem = 0;
    public MemberAdapter() {
        super(R.layout.item_member);
    }

    public void setSelectItem(int position) {
        this.selectItem = position;
    }


    @Override
    protected void convert(BaseViewHolder helper, VipBean item) {
        try {
            helper.setText(R.id.tv_member_key,RegexUtil.numberInString(RegexUtil.getNumbers(item.getKey())))
                    .setText(R.id.tv_member_value, MoneyExchange.exchange(Integer.parseInt(item.getValue()))+ "元");
            LinearLayout baseView = helper.getView(R.id.ll_base_view);
            baseView.setSelected(selectItem == helper.getAdapterPosition());
        }catch (Exception e){
            e.printStackTrace();
        }


    }



}
