package com.radioapp.liaoliaobao.module.photoImage;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.url.BaseConstant;
import com.luck.picture.lib.photoview.PhotoView;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideApp;
import com.radioapp.liaoliaobao.app.GlideBlurTransformer;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.user.PhotoImageBean;
import com.radioapp.liaoliaobao.utils.pay.PayUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能：红包和图片查看
 * 描述
 * Created by yue on 2019-10-10
 */
public class PhotoImageFragment extends BaseRiggerFragment<PhotoImageView, PhotoImagePresenter>
        implements PhotoImageView, PayUtil.PayCallback {
    @BindView(R.id.photo_viewPager)
    ViewPager photoViewPager;
    @BindView(R.id.tv_right)
    TextView tvRight;

    private RequestOptions requestOptions;

    private ArrayList<PhotoImageBean> list = new ArrayList<>(); //集合
    private int position;//下标
    private boolean isDel;//是否有删除
    private PhotoAdapter photoAdapter;
    private int pageIndex;
    private int redIndex;

    /**
     * 创建当前页面
     *
     * @param list
     * @param position
     * @param isDel
     * @return
     */
    public static PhotoImageFragment newInterface(ArrayList<PhotoImageBean> list, int position, boolean isDel) {
        PhotoImageFragment fragment = new PhotoImageFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("photoImage", list);
        bundle.putInt("position", position);
        bundle.putBoolean("isDel", isDel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        this.list = bundle.getParcelableArrayList("photoImage");
        this.position = bundle.getInt("position");
        this.isDel = bundle.getBoolean("isDel");
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_photo_image;
    }

    @SuppressLint("CheckResult")
    @Override
    protected void initView() {
        initToolbar(position + 1 + "/" + list.size(), true, false, null);
        tvRight.setText(isDel ? "删除" : "");
        requestOptions = new RequestOptions();
        requestOptions.centerInside().apply(RequestOptions.bitmapTransform(new GlideBlurTransformer(mActivity, 25, 10)));
        photoAdapter = new PhotoAdapter();
        photoViewPager.setAdapter(photoAdapter);
        photoViewPager.setOffscreenPageLimit(list.size());
        photoViewPager.setCurrentItem(position);
        photoViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                toolbar_header.setText(i + 1 + "/" + list.size());
                pageIndex = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    public void tokenInvalidRefresh() {

    }


    //删除
    @OnClick(R.id.tv_right)
    public void onViewClicked() {
        if (TextUtils.isEmpty(tvRight.getText().toString())) {
            return;
        }
        mPresenter.delMedia(list.get(pageIndex).getId(), pageIndex);


    }

    @Override
    public void mediaVieweSuccess(int position) {
        list.get(position).setUser_media_id("1");
        photoAdapter.notifyDataSetChanged();
    }

    @Override
    public void redPay(PayBean payBean, int position) {
        this.redIndex = position;
        PayUtil.getInstance().Pay(mActivity, 2,payBean.getAppid(), new Gson().toJson(payBean), this);
    }

    @Override
    public void delSuccess(int position) {
        ToastUitl.showLong("删除成功");
        list.remove(position);
        toolbar_header.setText((position==0?1:position-1) + "/" + list.size());
        photoAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess() {
        list.get(position).setPrice(0);
        photoAdapter.notifyDataSetChanged();
    }

    @Override
    public void onError() {

    }

    @Override
    public void onCancel() {

    }


    public class PhotoAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_user_photo_preview, container, false);
            PhotoView photoView = view.findViewById(R.id.iv_photoView);
            LinearLayout llImageBg = view.findViewById(R.id.ll_image_bg);
            TextView tvPhotoName = view.findViewById(R.id.tv_photo_name);
            TextView tvPhotoContent = view.findViewById(R.id.tv_photo_content);
            TextView tv_see = view.findViewById(R.id.tv_see);
            tv_see.setVisibility(isDel ? View.INVISIBLE : View.VISIBLE);
            /*if (list.get(position).getIs_once() == 1) {
                tvPhotoName.setText("阅后即焚");
                llImageBg.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(list.get(position).getUser_media_id()) && !isDel) {
                    GlideApp.with(mContext)
                            .load(BaseConstant.IMAGEURL + list.get(position).getResource_url())
                            .apply(requestOptions)
                            .into(photoView);
                } else {
                    GlideApp.with(mContext)
                            .load(BaseConstant.IMAGEURL + list.get(position).getResource_url())
                            // .apply(requestOptions)
                            .into(photoView);
                }
            } else*/ if (list.get(position).getPrice() != 0) {
                tvPhotoName.setText("红包图片");
                tvPhotoContent.setText("需要付费" + list.get(position).getPrice() + "元");
                if (TextUtils.isEmpty(list.get(position).getUser_media_id()) && !isDel) {

                    GlideApp.with(mContext)
                            .load(BaseConstant.IMAGEURL + list.get(position).getResource_url())
                            .apply(requestOptions)
                            .into(photoView);
                } else {
                    GlideApp.with(mContext)
                            .load(BaseConstant.IMAGEURL + list.get(position).getResource_url())
                            // .apply(requestOptions)
                            .into(photoView);
                }
            } else {
                llImageBg.setVisibility(View.GONE);
                GlideApp.with(mContext)
                        .load(BaseConstant.IMAGEURL + list.get(position).getResource_url())
                        // .apply(requestOptions)
                        .into(photoView);
            }

            //长按查看图片
            llImageBg.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (isDel) {
                        return false;
                    }
                    if (list.get(position).getIs_once() == 1 && TextUtils.isEmpty(list.get(position).getUser_media_id())) {
                        //阅后即焚
                        mPresenter.mediaViewe(list.get(position).getUser_id(), list.get(position).getUser_media_id(), position);
                    } else if (list.get(position).getPrice() != 0) {
                        //红包图片
                        mPresenter.redalbumPay(list.get(position).getMedia_id(), position);
                    }

                    return true;
                }
            });

            container.addView(view);
            return view;
        }
    }

}
