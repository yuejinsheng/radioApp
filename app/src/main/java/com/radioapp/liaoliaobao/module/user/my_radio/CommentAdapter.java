package com.radioapp.liaoliaobao.module.user.my_radio;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.radio.CommentBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-12
 */
public class CommentAdapter extends BaseQuickAdapter<CommentBean, BaseViewHolder> {


    public CommentAdapter() {
        super(R.layout.item_comment);
    }

    @Override
    protected void convert(BaseViewHolder helper, CommentBean item) {
          helper.setText(R.id.item_comment_name,item.getNickname()+": ")
                  .setText(R.id.item_comment_content,item.getText());
          helper.addOnClickListener(R.id.item_comment_del);
    }
}
