package com.radioapp.liaoliaobao.module.user.mylike;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.base.BaseRiggerRecyclerFragment;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.home.GenderBean;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.trello.rxlifecycle2.LifecycleTransformer;

import io.reactivex.Observable;

/**
 * 功能：首页中 性别的所有列表
 * 描述
 * Created by yue on 2019-09-06
 */
public class MyLikeListFragment extends BaseRiggerRecyclerFragment<GenderBean>
        implements MyLikeListView, BaseQuickAdapter.OnItemChildClickListener, BaseQuickAdapter.OnItemClickListener {


    private String lng;
    private String lat;
    //参数集合
    private ArrayMap<String, Object> params;
    private MyLikeListPresenter presenter;
    private myLikeListAdapter adapter;


    /**
     * 创建framgnet
     * @return
     */
    public static Fragment newInterface( String lng, String lat) {
        MyLikeListFragment genderListFragment = new MyLikeListFragment();
        Bundle bundle = new Bundle();

        bundle.putString(IntentConstant.HOME_LNG, lng);
        bundle.putString(IntentConstant.HOME_LAT, lat);

        genderListFragment.setArguments(bundle);
        return genderListFragment;
    }


    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        lng = bundle.getString(IntentConstant.HOME_LNG, "");
        lat = bundle.getString(IntentConstant.HOME_LAT, "");
        params = getParams();
    }


    /**
     * 参数
     */
    private ArrayMap<String, Object> getParams() {
        ArrayMap<String, Object> params = new ArrayMap<>();
        if (!TextUtils.isEmpty(lat)) params.put("lat", lat);
        if (!TextUtils.isEmpty(lat)) params.put("lng", lng);
        return params;
    }


    @Override
    protected void initData() {
        initPreserter();
        adapter = new myLikeListAdapter();
        adapter.setOnItemChildClickListener(this);
        adapter.setOnItemClickListener(this);
    }

    @Override
    protected BaseQuickAdapter<GenderBean, BaseViewHolder> getAdapter() {
        return adapter;
    }

    @Override
    protected Observable<PageBean<GenderBean>> getUrl(int pageNo) {
        if (params != null) {
            params.put("page", pageNo);
        }
        return ServiceManager.create(UserService.class)
                .getFavorites(params)
                .compose(RxHelper.handleFlatMap());
    }


    /**
     * 初始化presenter
     */
    public synchronized BasePresenter initPreserter() {
        if (presenter == null) {
            presenter = new MyLikeListPresenter();
            presenter.attachView(this);
            presenter.mContext = mActivity;
        }
        return presenter;
    }

    @Override
    public <T> LifecycleTransformer<T> bindToLife() {
        return bindToLifecycle();
    }

    @Override
    public void tokenInvalid() {
        tokenRefresh();
    }

    //子item点击
    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
       GenderBean genderBean=(GenderBean) adapter.getData().get(position);
        switch (view.getId()) {
            case R.id.tv_follow:
                //收藏
                if(presenter!=null)
                    presenter.follow(genderBean.getFavoriteable_id(),0,position);
                break;
        }
    }

    //item点击
    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        GenderBean genderBean=(GenderBean) adapter.getData().get(position);
        UiHelper.showUserInfoDetailFragment(mActivity,genderBean.getFavoriteable_id());
    }

    //收藏或取消
    @Override
    public void followSuccess(Integer position) {
      GenderBean genderBean=  adapter.getData().get(position);
      adapter.remove(position);
    }
}
