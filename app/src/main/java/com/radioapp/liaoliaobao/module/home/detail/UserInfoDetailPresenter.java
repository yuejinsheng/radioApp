package com.radioapp.liaoliaobao.module.home.detail;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.ApiException;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.HomeService;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.home.CheckNumberOfViewBean;
import com.radioapp.liaoliaobao.bean.home.FriendContactBean;
import com.radioapp.liaoliaobao.bean.home.UserAlbumBean;
import com.radioapp.liaoliaobao.bean.user.UserInfoBean;
import com.radioapp.liaoliaobao.module.home.HomeFragment;

import java.io.File;

import javax.crypto.Mac;

import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-20
 */
public class UserInfoDetailPresenter extends BasePresenter<UserInfoDetailView> {


    /**
     * 获取用户信息
     *
     * @param friendId
     */
    public void getFriendInfo(Integer friendId) {
        ServiceManager.create(UserService.class)
                .getFriendInfo(friendId)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<UserInfoBean>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(UserInfoBean userInfoBean) {
                        mView.getFriendInfo(userInfoBean);
                    }

                    @Override
                    protected void _onError(ApiException api) {
                        super._onError(api);
                       mView.error(api);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }

    /**
     * 拉黑用户
     *
     * @param friendId
     * @param status
     */
    public void friendForBidden(Integer friendId, Integer status) {
        ServiceManager.create(UserService.class)
                .friendForBidden(friendId, status)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.biddenSuccess();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 收藏
     *
     * @param id
     * @param status
     */
    public void follow(Integer id, Integer status) {
        ServiceManager.create(HomeService.class)
                .follow(id, status)
                .compose(RxHelper.handleFlatMap())
                .compose(mView.bindToLife())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.followSuccess();
                    }

                    @Override
                    protected void _TokenInvalid() {

                    }
                });
    }


    /**
     * 获取联系方式
     * @param id
     */
    public void getFriendContact(Integer id){
        ServiceManager.create(HomeService.class)
                .getFriendContact(id)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<FriendContactBean>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }


                    @Override
                    protected void _onNext(FriendContactBean friendContactBean) {
                      mView.setFriendContact(friendContactBean);
                    }

                    @Override
                    protected void _onError(ApiException api) {
                        super._onError(api);
                        if(api.getDisplayMessage().contains("付费后")){
                            //TODO 付费
                            mView.error(api);
                        }
                    }

                   /* @Override
                    protected void _onError(String message) {
                        super._onError(Api);
                        if(message.contains("付费后")){
                            //TODO 付费
                            mView.onContactError();
                        }
                    }*/

                    @Override
                    protected void _TokenInvalid() {
                      mView.tokenInvalid();
                    }
                });

    }


    /**
     * 会员使用一次机会免费查看付费相册
     * @param userId
     */
    public  void freeUnlockAlbum(String userId){
        ServiceManager.create(HomeService.class)
                .userMedias(userId)
                .compose(RxHelper.handleFlatMap())
                .compose(mView.bindToLife())
                .subscribe(new RxSubscriber<UserAlbumBean>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(UserAlbumBean userAlbumBean) {
                        getFriendInfo(Integer.parseInt(userId));
                    }

                    @Override
                    protected void _onError(ApiException api) {
                        super._onError(api);
                        getFriendInfo(Integer.parseInt(userId));
                    }


                    @Override
                    protected void _TokenInvalid() {

                    }
                });
    }

    /**
     * 男角色进去需要的请求次数
     */
    public void numberOfViews(){
        ServiceManager.create(HomeService.class)
                .numberOfViews()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<CheckNumberOfViewBean>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(CheckNumberOfViewBean checkNumberOfViewBean) {
                      mView.getCheckNumberOfViewBean(checkNumberOfViewBean);
                    }

                    @Override
                    protected void _TokenInvalid() {
                   mView.tokenInvalid();
                    }
                });
    }


    /**
     * 请求购买联系方式微信订单
     * @param userId
     * @param paymethod
     */
    public void contactPay(int userId,String paymethod){
        ServiceManager.create(HomeService.class)
                .contactPay(userId, paymethod)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<PayBean>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(PayBean payBean) {
                       mView.pay(payBean);
                    }

                    @Override
                    protected void _TokenInvalid() {
                     mView.tokenInvalid();
                    }
                });
    }

    /**
     * 请求查看相册
     */
    public void  albumPay(int userId,String payMethod){
        ServiceManager.create(HomeService.class)
                .albumPay(userId,payMethod)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<PayBean>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(PayBean payBean) {
                        mView.pay(payBean);
                    }

                    @Override
                    protected void _TokenInvalid() {

                    }
                });
    }

    /**
     *申请查看相册
     *
     * @param id
     * @param path
     */
    public void applyViewUserInfo(int id, String path) {
        //转换图片
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("image/png"), new File(path));
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", new File(path).getName(), requestFile);
        ServiceManager.create(HomeService.class)
                .applyViewUserInfo(id, body)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String commonBean) {
                        mView.applyViewUserInfo();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }
}
