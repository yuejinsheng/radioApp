package com.radioapp.liaoliaobao.module.index;

import android.Manifest;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseActivity;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.baseapp.AppManager;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.App;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.lifecyclerObserver.RxPermissionsFragmentObserver;
import com.radioapp.liaoliaobao.module.lifecyclerObserver.RxPermissionsObserver;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.dialog.AlertDialogutils;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;
import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：引导页
 * 描述
 * Created by yue on 2019-12-09
 */
public class SplashFragment extends BaseRiggerFragment implements RxPermissionsFragmentObserver.PermissionCallBack {

    @BindView(R.id.iv_splash_image)
    ImageView ivSplashImage;
    Disposable mDisposable;
    TextView tvCountDown;
    RxPermissionsFragmentObserver rxPermissionsObserver;

    private Boolean isClick=false;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_splash;
    }

    @Override
    public void initView() {

        //权限申请
        rxPermissionsObserver = new RxPermissionsFragmentObserver(this);
        getLifecycle().addObserver(rxPermissionsObserver);
        rxPermissionsObserver.setCallBack(this);
        rxPermissionsObserver.requestPermissions(0, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_PHONE_STATE);
        setImageAlphaAnimation();

        SharePref.saveBoolean(Constants.ISSPLASH, true);
    }


    @Override
    public void tokenInvalidRefresh() {

    }


    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
        if (!SharePref.getBoolean("first_app", false)&&isClick) {
            AlertDialogutils.createProtocolDialog(mActivity, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharePref.saveBoolean("first_app", true);
                    App.startLocation();
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppManager.getAppManager().AppExit(mActivity, true);
                }
            });
        }
    }

    /**
     * 后面进入的app
     * s设置imageView的显示的动画
     */
    private void setImageAlphaAnimation() {
        //  Glide.with(mContext).load(R.drawable.splash_icon).asBitmap().centerCrop().into(mIvSplashImage) ;
        //渐变展示启动屏
        AlphaAnimation aa = new AlphaAnimation(0.3f, 1.0f);
        aa.setDuration(1000);
        ivSplashImage.startAnimation(aa);
        aa.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                isClick=true;
                if (!SharePref.getBoolean("first_app", false)) {
                    AlertDialogutils.createProtocolDialog(mActivity, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharePref.saveBoolean("first_app", true);
                            App.startLocation();
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppManager.getAppManager().AppExit(mActivity, true);
                        }
                    });
                } else {
                    App.startLocation();
                }



              /*  if(!TextUtils.isEmpty(Global.getLoginBean().getAccess_token())){
                    UiHelper.showMainFragment(mActivity);
                    Rigger.getRigger(SplashFragment.this).close();
                }else{
                    UiHelper.showLoginFragment(mActivity);
                    Rigger.getRigger(SplashFragment.this).close();
                }*/

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {

            }

        });
    }

    @Override
    public void granted(int type) {
        setImageAlphaAnimation();

    }

    @Override
    public void shouldShowRequestPermissionRationale() {

    }

    @Override
    public void other() {

    }
}
