package com.radioapp.liaoliaobao.module.user;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.url.BaseConstant;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.user.UpdateMediaBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-29
 */
public class UserMediaAdapter extends BaseQuickAdapter<UpdateMediaBean, BaseViewHolder> {
    public UserMediaAdapter() {
        super(R.layout.item_user_media);
    }

    @Override
    protected void convert(BaseViewHolder helper, UpdateMediaBean item) {
        /*if (item.getIs_once() == 1) {
            GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getResource_url(),
                    helper.getView(R.id.iv_item_user_photo_avatar));
            helper.setText(R.id.item_user_details_photo_bg, "阅后即焚");
            helper.setVisible(R.id.item_user_details_photo_bg, true);
        } else*/ if (item.getPrice() != 0) {
           /* //红包图片
            GlideApp.with(mContext).setDefaultRequestOptions(new RequestOptions()
                    .apply(RequestOptions.bitmapTransform(new GlideBlurTransformer(mContext, 15, 3))))
                    .load(BaseConstant.IMAGEURL + item.getResource_url())
                    .into((ImageView) helper.getView(R.id.iv_item_user_photo_avatar));*/
            GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getResource_url(),
                    helper.getView(R.id.iv_item_user_photo_avatar));
            //没有查看
            helper.setText(R.id.item_user_details_photo_bg, "红包照片");

            helper.setVisible(R.id.item_user_details_photo_bg, true);
        } else {
            helper.setVisible(R.id.item_user_details_photo_bg, false);
            GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getResource_url(),
                    helper.getView(R.id.iv_item_user_photo_avatar));
        }

    }
}
