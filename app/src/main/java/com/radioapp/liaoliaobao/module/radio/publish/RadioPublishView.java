package com.radioapp.liaoliaobao.module.radio.publish;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.appointment.DatingHopesBean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-27
 */
public interface RadioPublishView extends BaseView {
    void getDatingHopes(List<DatingHopesBean> datingHopesBeans);

    void success();
    void pay(PayBean payBean);
}
