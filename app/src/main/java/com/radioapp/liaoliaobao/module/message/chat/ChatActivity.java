package com.radioapp.liaoliaobao.module.message.chat;

import android.content.Intent;

import com.gyf.barlibrary.ImmersionBar;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.hyphenate.util.EasyUtils;
import com.jaydenxiao.common.base.BaseActivity;
import com.jaydenxiao.common.baseapp.AppManager;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.module.index.MainActivity;
import com.radioapp.liaoliaobao.utils.FragmentUtil;

/**
 * 功能：聊天页面
 * 描述
 * Created by yue on 2019-08-05
 */
public class ChatActivity extends BaseActivity {
    String toChatUsername;
    private EaseChatFragment chatFragment;

    @Override
    public int getLayoutId() {
        return R.layout.activity_chat;
    }

    @Override
    public void handIntent() {
        super.handIntent();
        toChatUsername = getIntent().getExtras().getString(IntentConstant.USERID);
    }

    @Override
    public void initView() {
        ImmersionBar.with(this).fitsSystemWindows(true).statusBarColor(R.color.white).keyboardEnable(true).statusBarDarkFont(true,0.2f).init();
        AppManager.getAppManager().addActivity(this);
        //get user id or group id


        //use EaseChatFratFragment
        chatFragment = new ChatFragment();
        //pass parameters to chat fragment
        chatFragment.setArguments(getIntent().getExtras());
        FragmentUtil.replaceFragment(getSupportFragmentManager(), R.id.container, chatFragment);
    }

    @Override
    public void tokenInvalidRefresh() {

    }

    @Override
    public void onBackPressed() {
        chatFragment.onBackPressed();
        if (EasyUtils.isSingleActivity(this)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    public String getToChatUsername(){
        return toChatUsername;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getAppManager().finishActivity(this);
    }
}
