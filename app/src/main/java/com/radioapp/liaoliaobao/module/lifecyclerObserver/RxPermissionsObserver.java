package com.radioapp.liaoliaobao.module.lifecyclerObserver;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.jaydenxiao.common.commonutils.ToastUitl;
import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.functions.Consumer;

/**
 * 功能： 权限申请类
 * 描述
 * Created by yue on 2019-07-26
 */
public class RxPermissionsObserver implements LifecycleObserver {
    private FragmentActivity activity;

    private PermissionCallBack callBack;
    private RxPermissions rxPermission;

    public void setCallBack(PermissionCallBack callBack) {
        this.callBack = callBack;
    }

    public RxPermissionsObserver(FragmentActivity activity) {
        this.activity = activity;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void onCreate() {
        Log.e("android,", "onCreate");
        if (rxPermission == null)
            rxPermission = new RxPermissions(activity);
    }

    /**
     * 权限申请
     *
     * @param type        不同的数据进行初始化回调
     * @param permissions
     */
    public void requestPermissions(int type, String... permissions) {
        if (rxPermission == null) {
            rxPermission = new RxPermissions(activity);
        }
        rxPermission
                .requestEach(permissions)
                .subscribe(new Consumer<Permission>() {
                    @Override
                    public void accept(Permission permission) throws Exception {
                        if (permission.granted) {
                            // 用户已经同意该权限
                            Log.d("", permission.name + " is granted.");
                            if (callBack != null) {
                                callBack.granted(type);
                            }

                        } else if (permission.shouldShowRequestPermissionRationale) {
                            // 用户拒绝了该权限，没有选中『不再询问』（Never ask again）,那么下次再次启动时。还会提示请求权限的对话框
                            Log.d("", permission.name + " is denied. More info should be provided.");
                            if (callBack != null) {
                                callBack.shouldShowRequestPermissionRationale();
                            }
                            ToastUitl.showLong("由于您拒绝了权限，可能为你接下的操作造成影响,请前往设置开启权限");
                        } else {
                            // 用户拒绝了该权限，而且选中『不再询问』
                            Log.d("", permission.name + " is denied.");
                            if (callBack != null) {
                                callBack.other();
                            }
                            ToastUitl.showLong("由于您拒绝了权限，可能为你接下的操作造成影响,请前往设置开启权限");
                        }
                    }
                });

    }


    public interface PermissionCallBack {
        void granted(int type);

        void shouldShowRequestPermissionRationale();

        void other();
    }
}
