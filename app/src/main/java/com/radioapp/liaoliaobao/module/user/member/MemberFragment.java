package com.radioapp.liaoliaobao.module.user.member;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.TimeUtil;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.user.VipBean;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.utils.IsVipUtil;
import com.radioapp.liaoliaobao.utils.MoneyExchange;
import com.radioapp.liaoliaobao.utils.pay.PayUtil;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.DateUtil;
import com.radioapp.liaoliaobao.utils.RegexUtil;
import com.radioapp.liaoliaobao.view.span.AndroidSpan;
import com.radioapp.liaoliaobao.view.span.SpanOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能： 会员中心
 * 描述
 * Created by yue on 2019-08-26
 */
public class MemberFragment extends BaseRiggerFragment<MemberView, MemberPresenter> implements
        MemberView, BaseQuickAdapter.OnItemClickListener, PayUtil.PayCallback {
    @BindView(R.id.iv_member_top)
    ImageView ivMemberTop;
    @BindView(R.id.tv_member_top)
    TextView tvMemberTop;
    @BindView(R.id.tv_member_privilege)
    TextView tvMemberPrivilege; //会员特权
    @BindView(R.id.member_recyclerView)
    RecyclerView memberRecyclerView;
    @BindView(R.id.tv_pay_money)
    TextView tvPayMoney;
    @BindView(R.id.tv_member_commit)
    TextView tvMemberCommit;
    @BindView(R.id.ll_wx)
    LinearLayout llWx;
    @BindView(R.id.ll_alipay)
    LinearLayout llAlipay;
    @BindView(R.id.tv_member_wx)
    TextView tvMemberWx;
    @BindView(R.id.tv_member_alipay)
    TextView tvMemberAlipy;


    private MemberAdapter memberAdapter;
    private int payType=2; //1:支付宝。2微信
    private String vip_ended_at = "";
    private int vipType = 0;


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_member;
    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        vip_ended_at = bundle.getString(IntentConstant.VIP_ENDED_AT, "");
        if (!IsVipUtil.isVip(vip_ended_at)) {
            ivMemberTop.setSelected(false);
            tvMemberTop.setText(getString(R.string.member_normal_txt));
        } else {
            ivMemberTop.setSelected(true);
            int day = DateUtil.caculateTotalTime(TimeUtil.getCurrentDay(), vip_ended_at);
            SpanOptions spanOptions = new SpanOptions()
                    .addStyleSpan(Typeface.BOLD)
                    .addAbsoluteSizeSpan(20, true)
                    .addForegroundColor(ContextCompat.getColor(mActivity, R.color.color_66));
            AndroidSpan androidSpan = new AndroidSpan()
                    .drawCommonSpan("距离会员到期还有\t\t\t")
                    .drawWithOptions(day + "", spanOptions)
                    .drawCommonSpan("\t\t\t天");

            tvMemberTop.setText(androidSpan.getSpanText());
        }
    }

    @Override
    protected void initView() {
        initToolbar("会员中心", true, false, null);
        getData();
        setPayBg(true);
        tvPayMoney.setText(getString(R.string.member_pay_money_txt, "0"));


        memberAdapter = new MemberAdapter();
        memberRecyclerView.setLayoutManager(new GridLayoutManager(mActivity, 2));
        memberRecyclerView.setNestedScrollingEnabled(false);//禁止滑动
        memberRecyclerView.setAdapter(memberAdapter);
        memberAdapter.setOnItemClickListener(this);
    }

    protected void getData() {
        mPresenter.getVipExplain();
        mPresenter.getVipMenuPrice();
    }

    @Override
    public void tokenInvalidRefresh() {
        getData();
    }


    /**
     * @param isPay true 为微信，false 支付宝
     */
    public void setPayBg(Boolean isPay) {
        if (isPay) {
            tvMemberWx.setSelected(true);
            tvMemberAlipy.setSelected(false);
            tvMemberWx.setTextColor(ContextCompat.getColor(mActivity, R.color.color_22));
            tvMemberAlipy.setTextColor(ContextCompat.getColor(mActivity, R.color.color_99));
            payType = 2;
        } else {
            tvMemberWx.setSelected(false);
            tvMemberAlipy.setSelected(true);
            tvMemberAlipy.setTextColor(ContextCompat.getColor(mActivity, R.color.color_22));
            tvMemberWx.setTextColor(ContextCompat.getColor(mActivity, R.color.color_99));
            payType = 1;
        }

    }

    //提交
    @OnClick({R.id.ll_wx, R.id.ll_alipay, R.id.tv_member_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_wx:
                setPayBg(true);
                break;
            case R.id.ll_alipay:
                setPayBg(false);
                break;
            case R.id.tv_member_commit:
                if (vipType == 0) {
                    ToastUitl.showLong("参数异常");
                    return;
                }
                if(payType==1){
                    ToastUitl.showLong("支付宝支付暂未开通");
                    return;
                }
                mPresenter.vipPay(payType, vipType);
                break;
        }
    }

    @Override
    public void vipExplain(List<VipBean> beans) {
        StringBuffer stringBuffer = new StringBuffer();
        if (beans != null && beans.size() > 0) {
            for (VipBean bean : beans) {
                stringBuffer.append(bean.getValue()).append("\n");
            }

        }
        tvMemberPrivilege.setText(stringBuffer.toString());
    }

    @Override
    public void VipMenuPrice(List<VipBean> beans) {
        memberAdapter.setNewData(beans);
        if (beans != null && beans.size() > 0) {
            tvPayMoney.setText(getString(R.string.member_pay_money_txt, MoneyExchange.exchange(Integer.parseInt(beans.get(0).getValue()))));
            vipType = Integer.parseInt(RegexUtil.getNumbers(beans.get(0).getKey()));
        }
    }

    @Override
    public void vipPay(PayBean payBean) {
        if(payBean==null){
            ToastUitl.showLong("返回参数异常");
            return;
        }

        PayUtil.getInstance().Pay(mActivity,payType,payBean.getAppid(),new Gson().toJson(payBean),this);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        VipBean vipBean = (VipBean) adapter.getData().get(position);
        memberAdapter.setSelectItem(position);
        memberAdapter.notifyDataSetChanged();
        tvPayMoney.setText(getString(R.string.member_pay_money_txt, MoneyExchange.exchange(Integer.parseInt(vipBean.getValue()))));
        vipType = Integer.parseInt(RegexUtil.getNumbers(vipBean.getKey()));
    }


    //支付成功
    @Override
    public void onSuccess() {
        UiHelper.showPayStatusFragment(mActivity,true);
    }

    //支付失败
    @Override
    public void onError() {
        UiHelper.showPayStatusFragment(mActivity,false);
    }

    //支付取消
    @Override
    public void onCancel() {

    }
}
