package com.radioapp.liaoliaobao.module.message.system.list;

import android.widget.ImageView;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.url.BaseConstant;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideApp;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.enumbean.MessageTitleEnum;
import com.radioapp.liaoliaobao.bean.message.MessageListBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-05
 */
public class MessageListAdapter extends BaseQuickAdapter<MessageListBean, BaseViewHolder> {
    public String title = "1";

    public MessageListAdapter(String title) {
        super(R.layout.item_message_list);
        this.title = title;

    }

    @Override
    protected void convert(BaseViewHolder helper, MessageListBean data) {

        helper.setText(R.id.tv_content, title.equals("1") ? data.getData().getBody() : data.getData().getContent())
                .setText(R.id.tv_message_time, data.getCreated_at());
        GlideApp.with(mContext)
                .applyDefaultRequestOptions(new RequestOptions().bitmapTransform(new CircleCrop()))
                .load(BaseConstant.IMAGEURL + data.getData().getFrom_user_avatar())
                .into((ImageView) helper.getView(R.id.iv_message_avatar));

        helper.setGone(R.id.tv_message_status, title.equals(MessageTitleEnum.RADIO.getCode()) || title.equals(MessageTitleEnum.APPLY.getCode()));

        //查看申请
        if (title.equals(MessageTitleEnum.APPLY.getCode())) {
            if (data.getData().getFlag() == 3) {
                if (data.getData().getIs_agree() == 1) {
                    helper.setText(R.id.tv_message_status, "已同意");
                    helper.setBackgroundRes(R.id.tv_message_status, R.drawable.selector_msg_pass_gread);
                } else {
                    helper.setText(R.id.tv_message_status, "同意");
                    helper.setBackgroundRes(R.id.tv_message_status, R.drawable.selector_msg_pass);
                }
                if (data.getData().getMeta() != null) {
                    helper.setGone(R.id.iv_message_image, true);
                    GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + data.getData().getMeta().getImage(), helper.getView(R.id.iv_message_image));
                } else {
                    helper.setGone(R.id.iv_message_image, false);
                }
            }else{
                helper.setGone(R.id.iv_message_image, false);
                helper.setGone(R.id.tv_message_status,false);
            }
        } else {
            helper.setGone(R.id.iv_message_image, false);
        }

        //电台消息
        if (title.equals(MessageTitleEnum.RADIO.getCode())) {
            if (data.getData().getFlag() == 1) {
                helper.setText(R.id.tv_message_status, "联系Ta");
                helper.setBackgroundRes(R.id.tv_message_status, R.drawable.selector_msg_pass_gread);
            } else {
                helper.setText(R.id.tv_message_status, "联系Ta");
                helper.setBackgroundRes(R.id.tv_message_status, R.drawable.selector_msg_pass_red);
            }
        }

        helper.addOnClickListener(R.id.tv_message_status)
                .addOnClickListener(R.id.iv_message_avatar)
                .addOnClickListener(R.id.iv_message_image);
    }
}

