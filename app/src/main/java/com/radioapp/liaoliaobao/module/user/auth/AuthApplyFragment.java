package com.radioapp.liaoliaobao.module.user.auth;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.user.UserBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.utils.timePicker.SelectImageUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能：认证(支付宝)
 * 描述
 * Created by yue on 2019-10-08
 */
public class AuthApplyFragment extends BaseRiggerFragment<AuthApplyView, AuthApplyPresenter> implements AuthApplyView {
    @BindView(R.id.iv_auth_add)
    ImageView ivImage;
    @BindView(R.id.et_alipay_account)
    EditText etAlipayAccount;
    @BindView(R.id.et_alipay_name)
    EditText etAlipayName;
    @BindView(R.id.tv_auth_next)
    TextView tvAuthCommit;

    private List<LocalMedia> selectList = new ArrayList<>();

    String path = "";

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_auth_apply;
    }

    @Override
    protected void initView() {
        initToolbar("认证", true, false, null);


    }

    @Override
    public void tokenInvalidRefresh() {

    }

    @OnClick({R.id.iv_auth_add, R.id.tv_auth_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_auth_add: //上传图片
                SelectImageUtils.showALlImage(this, selectList, 1, PictureConfig.CHOOSE_REQUEST);
                break;
            case R.id.tv_auth_next: //提交
                if (TextUtils.isEmpty(path)) {
                    ToastUitl.showLong("请上传图片");
                    return;
                }
                if (TextUtils.isEmpty(etAlipayAccount.getText().toString())) {
                    ToastUitl.showLong("请输入支付宝帐号");
                    return;
                }
                if (TextUtils.isEmpty(etAlipayName.getText().toString())) {
                    ToastUitl.showLong("请输入支付宝姓名");
                    return;
                }
                mPresenter.verify(path, etAlipayAccount.getText().toString(), etAlipayName.getText().toString());
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择
                    selectList = PictureSelector.obtainMultipleResult(data);
                    if (selectList.get(0) != null) {
                        path = getpathImage(selectList.get(0));
                    }
                    GlideImageLoader.displayImage(mActivity, path, ivImage);
                    break;
            }
        }
    }

    /**
     * 获取图片，
     *
     * @param model
     * @return
     */
    public String getpathImage(LocalMedia model) {
        String path = "";
        if (model.isCut() && !model.isCompressed()) {
            // 裁剪过
            path = model.getCutPath();
        } else if (model.isCompressed() || (model.isCut() && model.isCompressed())) {
            // 压缩过,或者裁剪同时压缩过,以最终压缩过图片为准
            path = model.getCompressPath();
        } else {
            // 原图
            path = model.getPath();
        }
        return path;
    }

    @Override
    public void success() {
        Rigger.getRigger(mActivity).startFragment(new AuthApplySuccessFragment());
        UserBean userBean = Global.getUserInfo();
        userBean.setVerified_at("已认证");
        Global.saveUserInfo(userBean);
        Rigger.getRigger(this).close();
    }
}
