package com.radioapp.liaoliaobao.module.user.my_radio.registerRadio;

import android.app.Activity;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jaeger.ninegridimageview.NineGridImageView;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.url.BaseConstant;
import com.previewlibrary.GPreviewBuilder;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.lite.ChatUser;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;
import com.radioapp.liaoliaobao.bean.user.RadioRegisterListBean;
import com.radioapp.liaoliaobao.uihelper.UiHelper;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 功能：约会报名名单
 * 描述
 * Created by yue on 2019-10-12
 */
public class MyRadioRegisterListFragment extends BaseRiggerFragment<MyRadioRegisterListView, MyRadioRegisterListPresenter>
        implements MyRadioRegisterListView, BaseQuickAdapter.OnItemChildClickListener {
    @BindView(R.id.radioListRecyclerView)
    RecyclerView recyclerView;

    private MyRadioRegisterListAdapter adapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_radio_register_list;
    }

    @Override
    protected void initView() {
        initToolbar("约会报名名单", true, false, null);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        adapter=new MyRadioRegisterListAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setOnItemChildClickListener(this);
        mPresenter.getRadioRegisterList();
    }

    @Override
    public void tokenInvalidRefresh() {
        mPresenter.getRadioRegisterList();
    }

    @Override
    public void list(List<RadioRegisterListBean> radioRegisterListBeans) {
        if (radioRegisterListBeans != null && radioRegisterListBeans.size() > 0) {
            adapter.setNewData(radioRegisterListBeans);
        } else {
            ToastUitl.showLong("还没有人报名您的约会呢");
        }
    }

    @Override
    public void passSuccess(int position) {
        adapter.getData().get(position).setIs_chosen(1);
        adapter.notifyItemChanged(position);
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        RadioRegisterListBean registerListBean = (RadioRegisterListBean) adapter.getData().get(position);
        switch (view.getId()) {
            case R.id.iv_radio_avatar:  //头像
                UiHelper.showUserInfoDetailFragment(mActivity, registerListBean.getUser_id());
                break;
            case R.id.item_radio_register_list_image:
                Rect bounds = new Rect();
                registerListBean.setmBounds(bounds);
                GPreviewBuilder.from(mActivity)
                        .setSingleData(registerListBean)
                        .setCurrentIndex(0)
                        .setFullscreen(false)
                        .setType(GPreviewBuilder.IndicatorType.Dot)
                        .start();//启动
                break;
            case R.id.item_datio_notice_agree:
                if (registerListBean.getIs_chosen() != 0) {
                    //保存数据
                    ChatUser chatUser = new ChatUser(registerListBean.getId()
                            , registerListBean.getNickname(), BaseConstant.IMAGEURL + registerListBean.getAvatar());

                    UiHelper.showChatActivity(mActivity, registerListBean.getUser_id() + "");
                } else {
                    mPresenter.passRadioRegisterList(registerListBean.getUser_id(), position);
                }
                break;
        }
    }
}
