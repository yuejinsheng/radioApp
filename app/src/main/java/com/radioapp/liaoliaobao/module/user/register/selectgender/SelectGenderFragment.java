package com.radioapp.liaoliaobao.module.user.register.selectgender;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.uihelper.UiHelper;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能： 选择性别
 * 描述
 * Created by yue on 2019-07-16
 */
public class SelectGenderFragment extends BaseRiggerFragment {
    @BindView(R.id.iv_select_man)
    ImageView ivSelectMan;
    @BindView(R.id.iv_man_bg)
    View ivManBg;
    @BindView(R.id.iv_select_woman)
    ImageView ivSelectWoman;
    @BindView(R.id.iv_woman_bg)
    View ivWomanBg;
    @BindView(R.id.tv_register_next)
    TextView tvRegisterNext;

    private int gender=1;//1为男，2为女



    @Override
    protected int getLayoutResource() {
        return R.layout.activity_select_gender;
    }

    @Override
    public void initView() {
       initToolbar("选择性别",true,false,null);

        genderSelectMan();
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    /**
     * 选择男的
     */
    private void genderSelectMan(){
        ivManBg.setVisibility(View.VISIBLE);
        ivSelectMan.setVisibility(View.VISIBLE);
        ivWomanBg.setVisibility(View.INVISIBLE);
        ivSelectWoman.setVisibility(View.INVISIBLE);
        gender=1;
    }
    /**
     * 选择女 的
     */
    private void genderSelectWoMan(){
        ivWomanBg.setVisibility(View.VISIBLE);
        ivSelectWoman.setVisibility(View.VISIBLE);
        ivManBg.setVisibility(View.GONE);
        ivSelectMan.setVisibility(View.INVISIBLE);
        gender=2;
    }

    @OnClick({R.id.rl_man, R.id.rl_woman, R.id.tv_register_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_man:
                genderSelectMan();
                break;
            case R.id.rl_woman:
                genderSelectWoMan();
                break;
            case R.id.tv_register_next:
                //TODO 完善资料
                UiHelper.showMaterialActivity(mActivity,gender);
                break;
        }
    }
}
