package com.radioapp.liaoliaobao.module.user.member;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.user.VipBean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-26
 */
public interface MemberView extends BaseView {

    void vipExplain(List<VipBean> beans);

    void VipMenuPrice(List<VipBean> beans);

    void vipPay(PayBean payBean);

}
