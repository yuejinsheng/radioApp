package com.radioapp.liaoliaobao.module.home.list;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.HomeService;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-08
 */
public class GenderListPresenter extends BasePresenter<GenderListView> {


    public void follow(Integer id,Integer status,Integer position){
        ServiceManager.create(HomeService.class)
                .follow(id, status)
                .compose(RxHelper.handleFlatMap())
                .compose(mView.bindToLife())
                .subscribe(new RxSubscriber<String>(mContext,false) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                      mView.followSuccess(position);
                    }

                    @Override
                    protected void _TokenInvalid() {

                    }
                });
    }
}
