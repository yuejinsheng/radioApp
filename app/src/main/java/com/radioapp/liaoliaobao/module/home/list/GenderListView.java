package com.radioapp.liaoliaobao.module.home.list;

import com.jaydenxiao.common.base.BaseView;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-08
 */
public interface GenderListView extends BaseView {

     void followSuccess(Integer position);
}
