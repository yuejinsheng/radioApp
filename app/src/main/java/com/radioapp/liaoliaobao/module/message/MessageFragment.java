package com.radioapp.liaoliaobao.module.message;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;

import com.flyco.tablayout.SlidingTabLayout;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.module.index.MainFragment;
import com.radioapp.liaoliaobao.module.message.chat.ChatMessageFragment;
import com.radioapp.liaoliaobao.module.message.system.SystemMessageFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：消息
 * 描述
 * Created by yue on 2019-07-15
 */
public class MessageFragment extends BaseRiggerFragment {
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.message_tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.message_viewPager)
    ViewPager viewPager;

    String[] tabTitle = {"私聊消息", "系统消息"};
    private ChatMessageFragment chatMessageFragment = new ChatMessageFragment();
    private SystemMessageFragment systemMessageFragment = new SystemMessageFragment();
    private ArrayList<Fragment> fragments = new ArrayList<>();

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_message;
    }

    @Override
    protected void initView() {
        fragments.add(chatMessageFragment);
        fragments.add(systemMessageFragment);
        viewPager.setOffscreenPageLimit(2);
        if (tabLayout != null)
            tabLayout.setViewPager(viewPager, tabTitle, (FragmentActivity) mActivity, fragments);



    }


    /**
     * 更新环信的数据
     */
    public void refreshUIWithMessage() {
        if (chatMessageFragment != null) {
            chatMessageFragment.refresh();
        }

    }


    public Integer getTabIndex() {
        if (tabLayout != null) {
            return tabLayout.getCurrentTab();
        }
        return 0;
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    @OnClick(R.id.iv_search)
    public void onViewClicked() {
        // ToastUitl.showLong("搜索");
        // UiHelper.showChatActivity(mActivity,"69");

    }
}
