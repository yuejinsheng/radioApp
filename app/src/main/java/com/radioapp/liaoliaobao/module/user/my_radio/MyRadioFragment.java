package com.radioapp.liaoliaobao.module.user.my_radio;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jaeger.ninegridimageview.ItemImageClickListener;
import com.jaeger.ninegridimageview.NineGridImageView;
import com.jaeger.ninegridimageview.NineGridImageViewAdapter;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jaydenxiao.common.url.BaseConstant;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.previewlibrary.GPreviewBuilder;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.radio.CommentBean;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.module.user.my_radio.registerRadio.MyRadioRegisterListFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.AssetsUtils;
import com.radioapp.liaoliaobao.view.NiceImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.autosize.utils.LogUtils;
import razerdp.basepopup.QuickPopupBuilder;
import razerdp.basepopup.QuickPopupConfig;

/**
 * 功能： 我的电台
 * 描述
 * Created by yue on 2019-10-12
 */
public class MyRadioFragment extends BaseRiggerFragment<MyRadioView, MyRadioPresenter> implements
        MyRadioView, BaseQuickAdapter.OnItemChildClickListener {
    @BindView(R.id.iv_radio_avatar)
    NiceImageView ivRadioAvatar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.iv_hot_avatar)
    ImageView ivHotAvatar;
    @BindView(R.id.ll_name_layout)
    LinearLayout llNameLayout;
    @BindView(R.id.iv_gender)
    ImageView ivGender;
    @BindView(R.id.tv_age)
    TextView tvAge;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_show)
    TextView tvShow;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_dating_hopes)
    TextView tvDatingHopes;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.ngl_images)
    NineGridImageView nglImages;
    @BindView(R.id.iv_like)
    ImageView ivLike;
    @BindView(R.id.tv_like)
    TextView tvLike;
    @BindView(R.id.ll_like)
    LinearLayout llLike;
    @BindView(R.id.tv_sign)
    TextView tvSign;
    @BindView(R.id.ll_sign)
    LinearLayout llSign;
    @BindView(R.id.comment_recyclerView)
    RecyclerView commentRecyclerView;
    @BindView(R.id.ll_all)
    LinearLayout ll_all;
    @BindView(R.id.base_layout)
    LinearLayout baseLayout;

    private CommentAdapter commentAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_radio;
    }

    @Override
    protected void initView() {
        initToolbar("我的广播", true, false, null);
        SharePref.saveInt(Constants.REGISTER_MEETING, 0);
        commentAdapter = new CommentAdapter();
        commentRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        commentRecyclerView.setAdapter(commentAdapter);
        commentAdapter.setOnItemChildClickListener(this);
        mPresenter.getMyRadio();
    }

    @Override
    public void tokenInvalidRefresh() {

    }

    @OnClick({R.id.ll_sign, R.id.ll_all,R.id.ll_del})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_sign://查看当前约会名单
                UiHelper.showMyRadioRegisterListFragment(mActivity);
                break;
            case R.id.ll_all: //更多
                LogUtils.e("点击了");
                QuickPopupBuilder.with(mActivity)
                        .contentView(R.layout.del_report_popwindow)
                        .config(new QuickPopupConfig()
                                .gravity(Gravity.BOTTOM)
                                .withClick(R.id.tv_del, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mPresenter.delRadio();
                                    }
                                }, true))
                        .show(ll_all);
                break;
            case R.id.ll_del: //删除广播
                mPresenter.delRadio();
                break;
        }
    }


    @Override
    public void getRadioBean(RadioListBean bean) {
        tvName.setText(bean.getNickname());
        tvDate.setText("发布于  " + bean.getCreated_at());
        tvTime.setText("时间:" + bean.getDating_time_str());
        tvContent.setText(bean.getText());
        tvLike.setText(bean.getLike_count() + "");
        GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + bean.getAvatar(), ivRadioAvatar);
        ivGender.setImageResource(bean.getGender()==1?R.drawable.icon_man:R.drawable.icon_woman);

        //约会节目
        if (!TextUtils.isEmpty(bean.getDating_types())) {
            StringBuffer expect = new StringBuffer();
            String[] sqlit = bean.getDating_types().trim().split(",");
            if (sqlit.length > 0) {
                for (String s : sqlit) {
                    if (!TextUtils.isEmpty(s)) {
                        String name = (String) AssetsUtils.newInterface().getProgramBeansName(Integer.parseInt(s.trim())).get("name");
                        expect.append(name).append(",");
                    }
                }
                if (!TextUtils.isEmpty(expect.toString())) {
                    String expectNames = expect.toString().substring(0, expect.lastIndexOf(","));
                    tvShow.setText("约会节目:" + expectNames);
                }

            }

        }
        //约会期望
        if (!TextUtils.isEmpty(bean.getDating_hopes())) {
            StringBuffer hope = new StringBuffer();
            String[] hopes = bean.getDating_hopes().trim().split(",");
            if (hopes.length > 0) {
                for (String s : hopes) {
                    if (!TextUtils.isEmpty(s)) {
                        String name = (String) AssetsUtils.newInterface().getExpectName(Integer.parseInt(s.trim())).get("name");
                        hope.append(name).append(",");
                    }
                }
                if (!TextUtils.isEmpty(hope.toString())) {
                    String expectNames = hope.toString().substring(0, hope.lastIndexOf(","));
                    tvDatingHopes.setText("约会期望:" + expectNames);
                }

            }
        }


        nglImages.setAdapter(new NineGridImageViewAdapter<RadioListBean.BroadcaseResources>() {
            @Override
            protected void onDisplayImage(Context context, ImageView imageView, RadioListBean.BroadcaseResources broadcaseResources) {
                GlideImageLoader.displayImage(context, broadcaseResources.getUrl(), imageView);
            }

        });
        nglImages.setImagesData(bean.getBroadcastResources());
        nglImages.setItemImageClickListener(new ItemImageClickListener<RadioListBean.BroadcaseResources>() {
            @Override
            public void onItemImageClick(Context context, ImageView imageView, int index, List<RadioListBean.BroadcaseResources> list) {
                Log.d("onItemImageClick", list.get(index).getUrl());
                computeBoundsBackward(list, nglImages);//组成数据
                GPreviewBuilder.from((Activity) context)
                        .setData(list)
                        .setFullscreen(false)
                        .setCurrentIndex(index)
                        .setType(GPreviewBuilder.IndicatorType.Dot)
                        .start();//启动
            }
        });

    }


    /**
     * 查找信息
     *
     * @param list 图片集合
     */
    private void computeBoundsBackward(List<RadioListBean.BroadcaseResources> list, NineGridImageView mNglContent) {
        for (int i = 0; i < mNglContent.getChildCount(); i++) {
            View itemView = mNglContent.getChildAt(i);
            Rect bounds = new Rect();
            if (itemView != null) {
                ImageView thumbView = (ImageView) itemView;
                thumbView.getGlobalVisibleRect(bounds);
            }
            list.get(i).setmBounds(bounds);
            // list.get(i).setResource_url(list.get(i).getUrl());
        }

    }


    @Override
    public void getCommentList(List<CommentBean> commentBeans) {
        commentAdapter.setNewData(commentBeans);
    }

    @Override
    public void delRadioSuccess() {
        baseLayout.setVisibility(View.GONE);
    }

    @Override
    public void delCommentSuccess(int position) {
        commentAdapter.remove(position);

    }

    @Override
    public void notDate() {
        baseLayout.setVisibility(View.GONE);
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        CommentBean commentBean = (CommentBean) adapter.getData().get(position);
        if (view.getId() == R.id.item_comment_del) {
            mPresenter.delComment(commentBean.getId(), position);
        }
    }
}
