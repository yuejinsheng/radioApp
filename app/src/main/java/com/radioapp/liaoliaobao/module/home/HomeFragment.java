package com.radioapp.liaoliaobao.module.home;

import android.Manifest;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.flyco.tablayout.SlidingTabLayout;
import com.hyphenate.chat.EMClient;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.user.LoginBean;
import com.radioapp.liaoliaobao.bean.user.UserBean;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.home.list.GenderListFragment;
import com.radioapp.liaoliaobao.module.index.MainFragment;
import com.radioapp.liaoliaobao.module.lifecyclerObserver.RxPermissionsFragmentObserver;
import com.radioapp.liaoliaobao.module.user.login.LoginFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.BaiDuMapUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：首页
 * 描述
 * Created by yue on 2019-07-0
 */
public class HomeFragment extends BaseRiggerFragment implements RxPermissionsFragmentObserver.PermissionCallBack {

    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout1)
    SlidingTabLayout tabLayout1;
    @BindView(R.id.viewPager1)
    ViewPager viewPager1;

    String[] tabManTitle = {"热门男士", "付费会员"};
    String[] tabWoManTitle = {"热门", "新来", "认证"};
    @BindView(R.id.cb_gender)
    TextView cbGender;
    @BindView(R.id.ll_man)
    LinearLayout llMan;

    private int gender = 2;//性别:1男性，2女性


    private ArrayList<Fragment> manFragments = new ArrayList<>(); //男性列表
    private ArrayList<Fragment> woManFragments = new ArrayList<>();//女性列表


    private RxPermissionsFragmentObserver rxPermissionsObserver;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView() {
       if(Global.getUserInfo().getGender()!=0){
           if(Global.getUserInfo().getGender()==1){
               gender=2;
           }else {
               gender=1;
           }
       }

        //权限申请
        rxPermissionsObserver = new RxPermissionsFragmentObserver(this);
        getLifecycle().addObserver(rxPermissionsObserver);
        rxPermissionsObserver.setCallBack(this);
        rxPermissionsObserver.requestPermissions(0, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA);

        // selectGender();


    }

    @Override
    public void tokenInvalidRefresh() {
        try {

            LogUtils.e("token失效了");
            if(viewPager!=null) {
                if (viewPager.getVisibility() == View.GONE) {
                    gender = 2;
                    for (Fragment woManFragment : woManFragments) {
                        if (woManFragment instanceof GenderListFragment) {
                            ((GenderListFragment) woManFragment).tokenInvalid();
                        }
                    }
                } else {
                    gender = 1;
                    for (Fragment woManFragment : manFragments) {
                        if (woManFragment instanceof GenderListFragment) {
                            ((GenderListFragment) woManFragment).tokenInvalid();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @OnClick({R.id.iv_home_search, R.id.ll_man})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_home_search:
                UiHelper.showSearchFragment(mActivity,
                        gender, SharePref.getString(Constants.LONGITUDE,"0"),
                          SharePref.getString(Constants.LATITUDE,"0"));
                break;
            case R.id.ll_man:
                selectGender();
                break;
        }
    }


    /**
     * 选择性别
     */
    private void selectGender() {
        if (gender == 1) {
            cbGender.setSelected(true);
            cbGender.setText(getString(R.string.home_woman));
            gender = 2;
            manPage();
        } else if(gender==2) {
            cbGender.setSelected(false);
            cbGender.setText(getString(R.string.home_man));
            gender = 1;
            woManPage();
        }
    }


    /**
     * 男性的tab
     */
    private void manPage() {


        if (manFragments.size() > 0) {
            tabLayout.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.VISIBLE);
            tabLayout1.setVisibility(View.GONE);
            viewPager1.setVisibility(View.GONE);
            tabLayout.notifyDataSetChanged();
        } else {
            Fragment hotFragment = GenderListFragment.newInterface(1, -1, -1, -1, -1, -1, "",
                    SharePref.getString(Constants.LONGITUDE,"0"),
                    SharePref.getString(Constants.LATITUDE,"0"));
            Fragment vipFragment = GenderListFragment.newInterface(1, -1, -1, -1, -1, 1, "",
                    SharePref.getString(Constants.LONGITUDE,"0"),
                    SharePref.getString(Constants.LATITUDE,"0"));
            manFragments.add(hotFragment);
            manFragments.add(vipFragment);
            tabLayout.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.VISIBLE);
            tabLayout1.setVisibility(View.GONE);
            viewPager1.setVisibility(View.GONE);
            if (tabLayout != null) {
                tabLayout.setViewPager(viewPager, tabManTitle, (FragmentActivity) mActivity, manFragments);

            }

        }


    }

    /**
     * 女性的tab
     */
    private void woManPage() {


        if (woManFragments.size() > 0) {
            tabLayout1.setVisibility(View.VISIBLE);
            viewPager1.setVisibility(View.VISIBLE);
            tabLayout.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            tabLayout1.notifyDataSetChanged();
        } else {
            Fragment hotFragment = GenderListFragment.newInterface(2, -1, -1, -1, -1, -1, "",
                    SharePref.getString(Constants.LONGITUDE,"0"),
                    SharePref.getString(Constants.LATITUDE,"0"));
            Fragment newsFragment = GenderListFragment.newInterface(2, -1, 1, -1, -1, -1, "",
                    SharePref.getString(Constants.LONGITUDE,"0"),
                    SharePref.getString(Constants.LATITUDE,"0"));
            Fragment vipFragment = GenderListFragment.newInterface(2, 1, -1, -1, -1, -1, "",
                    SharePref.getString(Constants.LONGITUDE,"0"),
                    SharePref.getString(Constants.LATITUDE,"0"));
            tabLayout1.setVisibility(View.VISIBLE);
            viewPager1.setVisibility(View.VISIBLE);
            tabLayout.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            woManFragments.add(hotFragment);
            woManFragments.add(newsFragment);
            woManFragments.add(vipFragment);
            if (tabLayout1 != null)
                tabLayout1.setViewPager(viewPager1, tabWoManTitle, (FragmentActivity) mActivity, woManFragments);
        }

    }




    @Override
    public void granted(int type) {
        selectGender();
    }

    @Override
    public void shouldShowRequestPermissionRationale() {
        LogUtils.e("没有请求");
        selectGender();

    }

    @Override
    public void other() {
        LogUtils.e("没有请求");
        selectGender();
    }


}
