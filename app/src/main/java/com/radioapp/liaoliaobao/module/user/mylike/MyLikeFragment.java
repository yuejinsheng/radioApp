package com.radioapp.liaoliaobao.module.user.mylike;

import android.Manifest;

import com.baidu.location.BDLocation;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.module.lifecyclerObserver.RxPermissionsFragmentObserver;
import com.radioapp.liaoliaobao.utils.BaiDuMapUtil;
import com.radioapp.liaoliaobao.utils.FragmentUtil;

import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：我喜欢的
 * 描述
 * Created by yue on 2019-07-22
 */
public class MyLikeFragment extends BaseRiggerFragment implements RxPermissionsFragmentObserver.PermissionCallBack {
    private String lng;
    private String lat;

    BaiDuMapUtil gaoDeMapUtil;

    BDLocation mapLocation;
    private RxPermissionsFragmentObserver rxPermissionsObserver;


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mylike;
    }

    @Override
    public void initView() {
        initToolbar("我喜欢的",true,false,null);
        //权限申请
        rxPermissionsObserver = new RxPermissionsFragmentObserver(this);
        getLifecycle().addObserver(rxPermissionsObserver);
        rxPermissionsObserver.setCallBack(this);
        rxPermissionsObserver.requestPermissions(0, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA);

    }


    @Override
    public void tokenInvalidRefresh() {

    }

    /**
     * 高德地图定位
     */
    public void initGaode() {
        if (gaoDeMapUtil == null) {
            gaoDeMapUtil = BaiDuMapUtil.getInstance();
            gaoDeMapUtil.setContext(mActivity).setListener(new BaiDuMapUtil.GaoDeAMapLocationListener() {
                @Override
                public void onLocationChanged(BDLocation aMapLocation) {
                    mapLocation = aMapLocation;
                    LogUtils.e("获取纬度-->" + aMapLocation.getLatitude() + "经度" + aMapLocation.getLongitude());

                    FragmentUtil.replaceFragment(getChildFragmentManager(),R.id.myLike_frameLayout, MyLikeListFragment.newInterface(mapLocation.getLongitude() + "", mapLocation.getLatitude() + ""));
                   /* Rigger.getRigger(this).showFragment(MyLikeListFragment.newInterface(
                            mapLocation == null ? "" : String.valueOf(mapLocation.getLongitude()),
                            mapLocation == null ? "" : String.valueOf(mapLocation.getLatitude())),
                            R.id.myLike_frameLayout);*/
                }

            });
            getLifecycle().addObserver(gaoDeMapUtil);
        }
    }


    @Override
    public void granted(int type) {
        initGaode();
    }

    @Override
    public void shouldShowRequestPermissionRationale() {
        FragmentUtil.replaceFragment(getChildFragmentManager(),R.id.myLike_frameLayout,
                MyLikeListFragment.newInterface(mapLocation == null ? "" : String.valueOf(mapLocation.getLongitude()),
                        mapLocation == null ? "" : String.valueOf(mapLocation.getLatitude())));
       /* Rigger.getRigger(this).showFragment(MyLikeListFragment.newInterface(
                mapLocation == null ? "" : String.valueOf(mapLocation.getLongitude()),
                mapLocation == null ? "" : String.valueOf(mapLocation.getLatitude())),
                R.id.myLike_frameLayout);*/
    }

    @Override
    public void other() {
        FragmentUtil.replaceFragment(getChildFragmentManager(),R.id.myLike_frameLayout,
                MyLikeListFragment.newInterface(mapLocation == null ? "" : String.valueOf(mapLocation.getLongitude()),
                        mapLocation == null ? "" : String.valueOf(mapLocation.getLatitude())));
       /* Rigger.getRigger(this).showFragment(MyLikeListFragment.newInterface(
                mapLocation == null ? "" : String.valueOf(mapLocation.getLongitude()),
                mapLocation == null ? "" : String.valueOf(mapLocation.getLatitude())),
                R.id.myLike_frameLayout);*/
    }
}
