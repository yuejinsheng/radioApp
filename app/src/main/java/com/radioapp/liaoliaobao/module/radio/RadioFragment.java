package com.radioapp.liaoliaobao.module.radio;

import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.MediaTypeBean;
import com.radioapp.liaoliaobao.bean.address.AddressBean;
import com.radioapp.liaoliaobao.bean.radio.RadioPublishBean;
import com.radioapp.liaoliaobao.module.radio.list.RadioListFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.timePicker.TimePickerCityUtil;
import com.radioapp.liaoliaobao.utils.timePicker.TimePickerOneUtil;
import com.radioapp.liaoliaobao.view.dialog.publish_broadcast.PublishBroadCastDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 功能：电台
 * 描述
 * Created by yue on 2019-07-15
 */
public class RadioFragment extends BaseRiggerFragment implements PublishBroadCastDialog.OnItemClickListener {
    @BindView(R.id.tv_top)
    TextView tvTop;

    @BindView(R.id.tv_publish)
    TextView tvPublish;
    @BindView(R.id.fl_content_layout)
    FrameLayout flContentLayout;

    PublishBroadCastDialog publishBroadCastDialog;
    @BindView(R.id.tv_radio_gender)
    TextView tvRadioGender;
    @BindView(R.id.tv_radio_city)
    TextView tvRadioCity;

    List<String> genderList = new ArrayList<>(Arrays.asList("不限性别", "男", "女"));
    RadioListFragment radioListFragment;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_radio;
    }

    @Override
    protected void initView() {
        initToolbar("约会电台", false, false, null);
        publishBroadCastDialog = new PublishBroadCastDialog();
        publishBroadCastDialog.setOnItemClickListener(this);

    }

    @Override
    public void tokenInvalidRefresh() {

    }

    @Override
    public void onFragmentFirstVisible() {
        super.onFragmentFirstVisible();
        radioListFragment=new RadioListFragment();
        Rigger.getRigger(this).showFragment(radioListFragment, R.id.fl_content_layout, true);
    }

    @OnClick(R.id.tv_publish)
    public void onViewClicked() {
        publishBroadCastDialog.show(getFragmentManager(), "publishBroadCastDialog");
    }

    @Override
    public void onItemClick(RadioPublishBean bean) {
        //发布电台
        UiHelper.showRadioPublishFragment(mActivity, bean);
    }


    @OnClick({R.id.tv_radio_gender, R.id.tv_radio_city})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_radio_gender: //选择性别
                new TimePickerOneUtil<String>().showPickerView(mActivity, "选择性别", genderList, new TimePickerOneUtil.SelectListener<String>() {
                    @Override
                    public void onSelected(String s) {
                        if (s.contains("不限性别")) {
                            if(radioListFragment!=null){
                                radioListFragment.setGender(null);
                            }
                        } else if (s.contains("男")) {
                            if(radioListFragment!=null) {
                                radioListFragment.setGender(1);
                            }

                        } else if (s.contains("女")) {
                            if(radioListFragment!=null) {
                                radioListFragment.setGender(2);
                            }
                        }

                        tvRadioGender.setText(s);
                    }
                });
                break;
            case R.id.tv_radio_city: //选择城市
                TimePickerCityUtil.newInterface().showPickerViewAll(mActivity, 2, new TimePickerCityUtil.CitySelectAllListener() {
                    @Override
                    public void onSelected(AddressBean province, AddressBean.CitysBean citysBean, AddressBean.CitysBean.AreasBean areasBean) {
                        if(radioListFragment!=null) {
                            radioListFragment.setCity(citysBean.getId(),province.getId());
                        }
                        tvRadioCity.setText("约会范围:"+citysBean.getAreaName());
                    }

                    @Override
                    public void onCancel() {
                        if(radioListFragment!=null) {
                            radioListFragment.setCity(null,null);
                        }
                        tvRadioCity.setText("约会范围:全国");
                    }
                });
                break;
        }
    }
}
