package com.radioapp.liaoliaobao.module.home.detail;

import com.jaydenxiao.common.base.BaseView;
import com.jaydenxiao.common.baserx.ApiException;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.home.CheckNumberOfViewBean;
import com.radioapp.liaoliaobao.bean.home.FriendContactBean;
import com.radioapp.liaoliaobao.bean.user.UserInfoBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-20
 */
public interface UserInfoDetailView extends BaseView {

    void getFriendInfo(UserInfoBean userInfoBean);

    void biddenSuccess();

    void followSuccess();

    void setFriendContact(FriendContactBean friendContact);

    void getCheckNumberOfViewBean(CheckNumberOfViewBean checkNumberOfViewBean);


    void pay(PayBean payBean);

    void applyViewUserInfo();

    void error(ApiException api);

}
