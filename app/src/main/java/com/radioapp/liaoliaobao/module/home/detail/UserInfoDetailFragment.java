package com.radioapp.liaoliaobao.module.home.detail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.baserx.ApiException;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.url.BaseConstant;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.ImageBean;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.home.CheckNumberOfViewBean;
import com.radioapp.liaoliaobao.bean.home.FriendContactBean;
import com.radioapp.liaoliaobao.bean.lite.ChatUser;
import com.radioapp.liaoliaobao.bean.user.PhotoImageBean;
import com.radioapp.liaoliaobao.bean.user.UserInfoBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.module.photoImage.PhotoImageFragment;
import com.radioapp.liaoliaobao.utils.pay.PayUtil;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.AssetsUtils;
import com.radioapp.liaoliaobao.utils.GetAgeUtils;
import com.radioapp.liaoliaobao.utils.IsVipUtil;
import com.radioapp.liaoliaobao.utils.dialog.AlertDialogutils;
import com.radioapp.liaoliaobao.utils.timePicker.SelectImageUtils;
import com.radioapp.liaoliaobao.view.NiceImageView;

import org.litepal.LitePal;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.autosize.utils.LogUtils;
import razerdp.basepopup.QuickPopupBuilder;
import razerdp.basepopup.QuickPopupConfig;

/**
 * 功能：(个人详情)
 * 描述
 * Created by yue on 2019-07-30
 */
public class UserInfoDetailFragment extends BaseRiggerFragment<UserInfoDetailView, UserInfoDetailPresenter> implements
        UserInfoDetailView, BaseQuickAdapter.OnItemClickListener {


    @BindView(R.id.tv_back)
    LinearLayout tvBack;  //返回
    @BindView(R.id.tv_title)
    TextView tvTitle;  //标题
    @BindView(R.id.tv_follow)
    ImageView tvFollow; //关注
    @BindView(R.id.tv_more)
    ImageView tvMore;  //更多
    @BindView(R.id.iv_avatar)
    NiceImageView ivAvatar;  //头像
    @BindView(R.id.tv_auto)
    TextView tvAuto;  //认真
    @BindView(R.id.info_recyclerView)
    RecyclerView recyclerView;  //图片列表
    @BindView(R.id.tv_show)
    TextView tvShow;  //约会节目
    @BindView(R.id.tv_expect)
    TextView tvExpect;  //约会期望
    @BindView(R.id.tv_age)
    TextView tvAge;  //年龄
    @BindView(R.id.tv_career)
    TextView tvCareer;  //职业
    @BindView(R.id.tv_address)
    TextView tvAddress; //约会地址
    @BindView(R.id.tv_wx)
    TextView tvWx; //微信/qq
    @BindView(R.id.ll_wx)
    LinearLayout ll_wx;
    @BindView(R.id.tv_height)
    TextView tvHeight; //身高
    @BindView(R.id.tv_weight)
    TextView tvWeight; //体重
    @BindView(R.id.tv_self)
    TextView tvSelf; //自己简介
    @BindView(R.id.ll_chat)
    LinearLayout llChat; //私聊
    @BindView(R.id.ll_social)
    LinearLayout llSocial; //社交
    @BindView(R.id.ll_bottom)
    LinearLayout llBottom;
    @BindView(R.id.ll_info_weight)
    LinearLayout llInfoWeight;//体重 fu
    @BindView(R.id.ll_info_height)
    LinearLayout llInfoHeight; //身高fu
    @BindView(R.id.tv_photo_tips)
    TextView tvPhotoTips;//如果没有上传图片显示这个
    @BindView(R.id.tv_info_nickName)
    TextView tvInfoNickName;
    @BindView(R.id.rl_payAlbum)
    RelativeLayout rlPayAlbum; //相册布局
    @BindView(R.id.tv_photo_pay)
    TextView tvPhotoPay;//付费查看相册
    @BindView(R.id.userui_apply_look_ll)
    LinearLayout userui_apply_look_ll;//申请查看
    @BindView(R.id.ll_detail_home)
    LinearLayout ll_detail_home;

    private Integer friendId;
    //图片适配器
    private UserInfoPhotoAdapter userInfoPhotoAdapter;

    private AssetsUtils assetsUtils;
    private UserInfoBean userInfoBean;

    //图片集合放到浏览页面用的
    private List<ImageBean> images = new ArrayList<>();

    GridLayoutManager gridLayoutManager;

    CheckNumberOfViewBean checkNumberOfViewBean;

    private List<LocalMedia> selectList = new ArrayList<>();

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_radio_detail;
    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        friendId = bundle.getInt(IntentConstant.FRIENDID, 0);
    }

    @Override
    public void initView() {
        gridLayoutManager = new GridLayoutManager(mActivity, 4);
        recyclerView.setLayoutManager(gridLayoutManager);
        userInfoPhotoAdapter = new UserInfoPhotoAdapter();
        recyclerView.setAdapter(userInfoPhotoAdapter);
        userInfoPhotoAdapter.setOnItemClickListener(this);
        assetsUtils = AssetsUtils.newInterface();


        if (Global.getUserInfo().getGender() == 1) {
            mPresenter.numberOfViews();
        }

    }

    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
        mPresenter.getFriendInfo(friendId);
    }

    @Override
    public void tokenInvalidRefresh() {
        mPresenter.getFriendInfo(friendId);
    }


    /**
     * 获取用户数据
     *
     * @param userInfoBean
     */
    @Override
    public void getFriendInfo(UserInfoBean userInfoBean) {
        this.userInfoBean = userInfoBean;
        userui_apply_look_ll.setVisibility(View.GONE);
        llBottom.setVisibility(View.VISIBLE);
        ll_detail_home.setVisibility(View.VISIBLE);
        //昵称
        tvTitle.setText(userInfoBean.getNickname());
        tvInfoNickName.setText(userInfoBean.getNickname());
        //头像
        RequestOptions normalOptions = new RequestOptions();
        normalOptions.error(R.mipmap.image_error);
        normalOptions.placeholder(R.mipmap.image_loading);
        normalOptions.fallback(R.mipmap.image_shield);
        normalOptions.centerCrop();
        GlideImageLoader.displayImage(mActivity, BaseConstant.IMAGEURL + userInfoBean.getAvatar(), ivAvatar);

        //身高,体重
        llInfoHeight.setVisibility(!TextUtils.isEmpty(userInfoBean.getHeight()) ? View.VISIBLE : View.GONE);
        llInfoWeight.setVisibility(!TextUtils.isEmpty(userInfoBean.getWeight()) ? View.VISIBLE : View.GONE);
        tvHeight.setText(userInfoBean.getHeight());
        tvWeight.setText(userInfoBean.getWeight());
        //个人介绍
        tvSelf.setText(TextUtils.isEmpty(userInfoBean.getIntro()) ? "Ta比较懒，未填写个人介绍" : userInfoBean.getIntro());
        //判断图片
        Boolean mediasFlag = userInfoBean.getUser_medias() != null && userInfoBean.getUser_medias().size() > 0;
        tvPhotoTips.setVisibility(mediasFlag ? View.GONE : View.VISIBLE);
        recyclerView.setVisibility(mediasFlag ? View.VISIBLE : View.GONE);
        userInfoPhotoAdapter.setNewData(userInfoBean.getUser_medias());
        //添加图片
        if (mediasFlag) {
            for (UserInfoBean.UserMedias media : userInfoBean.getUser_medias()) {
                ImageBean imageBean = new ImageBean(BaseConstant.IMAGEURL + media.getResource_url());
                images.add(imageBean);
            }
        }

        //收藏
        tvFollow.setImageResource(userInfoBean.getMe_like() == 1 ? R.mipmap.icon_follow_select : R.mipmap.icon_follow_normal);

        //年龄
        if (!TextUtils.isEmpty(userInfoBean.getBirth_at())) {
            try {
                int ageByBirth = GetAgeUtils.getAgeByBirth(userInfoBean.getBirth_at());
                tvAge.setText(ageByBirth + "");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (userInfoBean.getGender() == 1) {
            ll_wx.setVisibility(View.GONE);
        }
        //微信
        //tvWx.setText(userInfoBean.getGeohash());
        //vip
        tvAuto.setText(IsVipUtil.isVip(userInfoBean.getVip_ended_at()) ? "会员" : "非会员");

        //职业
        String careerName = (String) AssetsUtils.newInterface().getCareersId(userInfoBean.getCareer_id()).get("careerName");
        tvCareer.setText(careerName);
        //地区
        if (userInfoBean.getRegions() != null && userInfoBean.getRegions().size() > 0) {
            StringBuffer regions = new StringBuffer();
            for (UserInfoBean.RegionsBean region : userInfoBean.getRegions()) {
                if (!TextUtils.isEmpty(assetsUtils.getAddress(-1, region.getCity_id(), -1).get("cityName") + "")) {
                    String cityName = (String) assetsUtils.getAddress(-1, region.getCity_id(), -1).get("cityName");
                    regions.append(cityName).append(",");
                }
            }
            if (!TextUtils.isEmpty(regions.toString())) {
                String regionsNames = regions.toString().substring(0, regions.lastIndexOf(","));
                tvAddress.setText(regionsNames);
            }
        } else {

            String cityName = (String) assetsUtils.getAddress(-1, userInfoBean.getCity_id(), -1).get("cityName");
            tvAddress.setText(cityName);
        }


        //约会期望
        if (userInfoBean.getUser_dating_hopes() != null && userInfoBean.getUser_dating_hopes().size() > 0) {
            StringBuffer expect = new StringBuffer();
            for (UserInfoBean.UserDatingHopesBean userDatingHope : userInfoBean.getUser_dating_hopes()) {
                if (!TextUtils.isEmpty(assetsUtils.getExpectName(userDatingHope.getDating_hope_id()).get("name") + "")) {
                    String name = assetsUtils.getExpectName(userDatingHope.getDating_hope_id()).get("name") + "";
                    expect.append(name).append(",");
                }

            }
            if (!TextUtils.isEmpty(expect.toString())) {
                String expectNames = expect.toString().substring(0, expect.lastIndexOf(","));
                tvExpect.setText(expectNames);
            }
        }


        //约会节目
        if (userInfoBean.getUser_dating_types() != null && userInfoBean.getUser_dating_types().size() > 0) {
            StringBuffer program = new StringBuffer();
            for (UserInfoBean.UserDatingTypesBean userDatingTypesBean : userInfoBean.getUser_dating_types()) {
                if (!TextUtils.isEmpty(assetsUtils.getProgramBeansName(userDatingTypesBean.getDating_type_id()).get("name") + "")) {
                    String name = assetsUtils.getProgramBeansName(userDatingTypesBean.getDating_type_id()).get("name") + "";
                    program.append(name).append(",");
                }

            }
            if (!TextUtils.isEmpty(program.toString())) {
                String programNames = program.toString().substring(0, program.lastIndexOf(","));
                tvShow.setText(programNames);
            }
        }


        //男的进去需要请求的
        if (Global.getUserInfo().getGender() == 1) {
          //  mPresenter.numberOfViews();
            llSocial.setVisibility(View.VISIBLE);

        }else if(Global.getUserInfo().getGender() ==2){
            llSocial.setVisibility(View.GONE);
        }



        //付费相册(女的)
        if (userInfoBean.getGender() == 2) {

            if (userInfoBean.getPrivacy_status() == 1) {
                if (userInfoBean.getIs_view_album() != 1) {
                    recyclerView.setVisibility(View.GONE);
                    tvPhotoTips.setVisibility(View.GONE);
                    tvPhotoPay.setVisibility(View.VISIBLE);
                    tvPhotoPay.setText("解锁她的相册(" + userInfoBean.getView_album_price() / 100 + " 元)，会员免费");
                }else{
                    tvPhotoPay.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            } else if (userInfoBean.getPrivacy_status() == 2) { //申请查看
                recyclerView.setVisibility(View.GONE);
                tvPhotoTips.setVisibility(View.GONE);
                tvPhotoPay.setVisibility(View.GONE);
                userui_apply_look_ll.setVisibility(View.VISIBLE);
            }
        }


        //距离
       /* if (userInfoBean.getHide_location() == 1) {
            mDistance.setText("保密");
        } else {
            double aDouble = PreferenceUtils.getDouble(ManUserUI.this, Constants.LONGITUDE);
            double aDouble1 = PreferenceUtils.getDouble(ManUserUI.this, Constants.LATITUDE);
            long distance = DistanceUtils.calculateDistance(Double.parseDouble(meta.lng),Double.parseDouble( meta.lat),aDouble , aDouble1);
            if (distance / 1000 > 0) {
                if (distance % 1000 / 100 == 0) {
                    mDistance.setText(distance / 1000 + "km");
                } else {
                    mDistance.setText(distance / 1000 + "." + distance % 1000 / 100 + "km");
                }
            } else {
                mDistance.setText(distance + "m");
            }

        }*/


    }


    //图片点击
    @SuppressLint("CheckResult")
    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        List<UserInfoBean.UserMedias> userMedias = (List<UserInfoBean.UserMedias>) adapter.getData();
        ArrayList<PhotoImageBean> photoImageBeans = new ArrayList<>();
        for (UserInfoBean.UserMedias userMedia : userMedias) {
            PhotoImageBean photoImageBean = new PhotoImageBean();
            photoImageBean.setUser_media_id(userMedia.getUser_media_id());
            photoImageBean.setIs_once(userMedia.getIs_once());
            photoImageBean.setPrice(userMedia.getPrice());
            photoImageBean.setFile_size(userMedia.getFile_size());
            photoImageBean.setMedia_id(userMedia.getMedia_id());
            photoImageBean.setType(userMedia.getType());
            photoImageBean.setResource_url(userMedia.getResource_url());
            photoImageBean.setMime_type(userMedia.getMime_type());
            photoImageBeans.add(photoImageBean);
        }
        //图片列表
        Rigger.getRigger(mActivity).startFragment(PhotoImageFragment.newInterface(photoImageBeans, position, false));


       /* computeBoundsBackward(gridLayoutManager.findFirstVisibleItemPosition());
        GPreviewBuilder.from(this)
                .setData(images)
                .setCurrentIndex(position)
                .setSingleFling(true)
                .setType(GPreviewBuilder.IndicatorType.Number)
                .start();*/

    }


    /**
     * 查找信息
     * 从第一个完整可见item逆序遍历，如果初始位置为0，则不执行方法内循环
     */
    private void computeBoundsBackward(int firstCompletelyVisiblePos) {
        for (int i = firstCompletelyVisiblePos; i < images.size(); i++) {
            View itemView = gridLayoutManager.findViewByPosition(i);
            Rect bounds = new Rect();
            if (itemView != null) {
                ImageView thumbView = (ImageView) itemView.findViewById(R.id.iv_item_user_photo_avatar);
                thumbView.getGlobalVisibleRect(bounds);
            }
            images.get(i).setBounds(bounds);
        }
    }

    /**
     * 拉黑成功
     */
    @Override
    public void biddenSuccess() {
        ToastUitl.showLong("拉黑成功");
    }

    /**
     * 收藏成功
     */
    @Override
    public void followSuccess() {
        if (userInfoBean.getMe_like() == 0) {
            userInfoBean.setMe_like(1);
        } else {
            userInfoBean.setMe_like(0);
        }
        //收藏
        tvFollow.setImageResource(userInfoBean.getMe_like() == 0 ? R.mipmap.icon_follow_select : R.mipmap.icon_follow_normal);
    }

    @Override
    public void setFriendContact(FriendContactBean friendContact) {
        if (friendContact.getHide_social_account() == 1) {
            ToastUitl.showLong("请通过私聊向我获取");
        } else {
            mPresenter.numberOfViews();
            ll_wx.setVisibility(View.VISIBLE);

            tvWx.setText("微信：" + friendContact.getWechat() + "/QQ:" + friendContact.getQq());
            AlertDialogutils.createDialog(mActivity, "微信：" + friendContact.getWechat() + "/QQ:" + friendContact.getQq(), "复制", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //复制
                    ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("", "微信：" + friendContact.getWechat() + "/QQ:" + friendContact.getQq());
                    clipboard.setPrimaryClip(clip);
                    ToastUitl.showLong("已复制到剪切板中");
                }
            });
        }
    }

    @Override
    public void getCheckNumberOfViewBean(CheckNumberOfViewBean checkNumberOfViewBean) {
        this.checkNumberOfViewBean = checkNumberOfViewBean;
        mPresenter.getFriendInfo(friendId);
    }


    @Override
    public void applyViewUserInfo() {
        ToastUitl.showLong("申请成功，请等待对方通过");
    }

    @Override
    public void error(ApiException api) {
        if (api.getCode() == 10071) {
            userui_apply_look_ll.setVisibility(View.VISIBLE);
            llBottom.setVisibility(View.GONE);
            ll_detail_home.setVisibility(View.GONE);
        } else if (api.getCode() == 10073) {
            AlertDialogutils.createDialog(mActivity, "非会员一天只能免费查看10位用户的详情\n\n会员无限制，是否开通会员?", "开通会员", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UiHelper.showMemberFragment(mActivity, Global.getUserInfo().getCreated_at());
                }
            });
        } else if (api.getCode() == 10066) {
            ToastUitl.showLong("您已被对方拉黑");
            Rigger.getRigger(this).close();
        } else if (api.getCode() == 10067) {
            ToastUitl.showLong("付费后才可以查看");
        } else if (api.getCode() == 10075) {
            ToastUitl.showLong("请通过私聊与我获取");
        } else if (api.getCode() == 10072) {
            AlertDialogutils.createDialog(mActivity, "今天免费解锁机会已经用完\n\n需要另外付费查看，是否付费?", "付费查看", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.contactPay(userInfoBean.getId(), "2");
                }
            });
        } else {
            ToastUitl.showLong("该用户已设置隐身");
            Rigger.getRigger(this).close();
        }
    }

    @Override
    public void pay(PayBean payBean) {
        PayUtil.getInstance().Pay(mActivity, 2,payBean.getAppid(), new Gson().toJson(payBean), new PayUtil.PayCallback() {
            @Override
            public void onSuccess() {
                ToastUitl.showLong("支付成功");
                mPresenter.getFriendInfo(friendId);
                mPresenter.numberOfViews();
            }

            @Override
            public void onError() {

            }

            @Override
            public void onCancel() {

            }
        });
    }


    @OnClick({R.id.tv_back, R.id.tv_follow, R.id.ll_more, R.id.tv_auto, R.id.rl_info_broadcast,
            R.id.rl_info_dynamic, R.id.ll_chat, R.id.ll_social, R.id.tv_photo_pay, R.id.userui_apply_look_ll})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_back: //退出
                Rigger.getRigger(this).close();
                break;
            case R.id.tv_follow: //收藏
                mPresenter.follow(friendId, userInfoBean.getMe_like() == 0 ? 1 : 0);
                break;
            case R.id.ll_more: //更多
                QuickPopupBuilder.with(mActivity)
                        .contentView(R.layout.black_report_popwindow)
                        .config(new QuickPopupConfig()
                                .gravity(Gravity.BOTTOM)
                                .withClick(R.id.add_black, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mPresenter.friendForBidden(friendId, 1);

                                    }
                                }, true))
                        .show(tvMore);
                break;
            case R.id.tv_auto: //认证
                UiHelper.showMemberFragment(mActivity, userInfoBean.getVip_ended_at());
                break;
            case R.id.rl_info_broadcast: //广播
                //TODO 用户广播列表
                UiHelper.showRadioDetailFragment(mActivity, userInfoBean.getHasBroadcast());
                break;

            case R.id.rl_info_dynamic: //动态
                //TODO 用户动态列表
                ToastUitl.showLong("暂未开通");
                break;
            case R.id.ll_chat: //私聊
                if (userInfoBean == null) {
                    ToastUitl.showLong("信息出错");
                    return;
                }

                if(Global.getUserInfo().getGender()==1){
                    if (userInfoBean.getIs_view_contact() == 1) {

                        ChatUser chatUser = new ChatUser(userInfoBean.getId()
                                , userInfoBean.getNickname(), BaseConstant.IMAGEURL + userInfoBean.getAvatar());
                        //聊天
                        UiHelper.showChatActivity(mActivity, friendId + "");
                    } else {
                        lookContactMethod();
                    }
                }else if(Global.getUserInfo().getGender()==2){
                    if(TextUtils.isEmpty(Global.getUserInfo().getVerified_at())){
                        ToastUitl.showLong("只有认证真实性后才能发起聊天哦");
                    }else{
                        ChatUser chatUser = new ChatUser(userInfoBean.getId()
                                , userInfoBean.getNickname(), BaseConstant.IMAGEURL + userInfoBean.getAvatar());
                        //聊天
                        UiHelper.showChatActivity(mActivity, friendId + "");
                    }
                }

                break;
            case R.id.ll_social: //社交
                if (userInfoBean == null) {
                    ToastUitl.showLong("信息出错");
                    return;
                }

                //TODO 社交
                if (userInfoBean.getIs_view_contact() == 1) {
                    //已解锁
                    mPresenter.getFriendContact(userInfoBean.getId());
                } else {
                    lookContactMethod();
                }
                break;
            case R.id.tv_photo_pay: //解锁付费红包
                lookPayALbum();
                break;
            case R.id.userui_apply_look_ll: //申请查看
                AlertDialogutils.createDialog(mActivity, "申请查看需要给对方发送一张照片\n\n确定申请嘛？", "确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SelectImageUtils.showALlImage(UserInfoDetailFragment.this, selectList, 1, PictureConfig.CHOOSE_REQUEST);
                    }
                });
                break;
        }
    }


    /**
     * 查看相册的接口
     */
    private void lookPayALbum() {
        if (IsVipUtil.isVip(Global.getUserInfo().getVip_ended_at())) {
            if (checkNumberOfViewBean.getContact_amount() >= 3) {
                AlertDialogutils.createDialog(mActivity, "今天免费解锁机会已经用完\n\n需要另外付费查看，是否付费?", "付费查看", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPresenter.albumPay(userInfoBean.getId(), "2");
                    }
                });
            } else {
                AlertDialogutils.createDialog(mActivity, "是否使用一次机会解锁Ta的联系方式", "确定",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mPresenter.freeUnlockAlbum(String.valueOf(userInfoBean.getId()));
                            }
                        });
            }
        } else {
            //非会员单次购买
            AlertDialogutils.createThreeDialog(mActivity, "查看" + userInfoBean.getNickname() + "的付费相册", "付费(" + userInfoBean.getView_album_price() / 100 + "元)查看", "成为会员,免费查看",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mPresenter.albumPay(userInfoBean.getId(), "2");
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            UiHelper.showMemberFragment(mActivity, Global.getUserInfo().getVip_ended_at());
                        }
                    });
        }
    }


    /**
     * 查看信息的判断
     */
    private void lookContactMethod() {
        if (IsVipUtil.isVip(Global.getUserInfo().getVip_ended_at())) {
            if (checkNumberOfViewBean.getContact_amount() >= 3) {
                AlertDialogutils.createDialog(mActivity, "今天免费解锁机会已经用完\n\n需要另外付费查看，是否付费?", "付费查看", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPresenter.contactPay(userInfoBean.getId(), "2");
                    }
                });
            } else {
                AlertDialogutils.createDialog(mActivity, "是否使用一次机会解锁Ta的联系方式", "确定",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mPresenter.getFriendContact(userInfoBean.getId());
                            }
                        });
            }
        } else {
            //非会员单次购买
            AlertDialogutils.createThreeDialog(mActivity, "查看" + userInfoBean.getNickname() + "的全部资料", "付费(10元)查看", "成为会员",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mPresenter.contactPay(userInfoBean.getId(), "2");
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            UiHelper.showMemberFragment(mActivity, Global.getUserInfo().getVip_ended_at());
                        }
                    });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    //申请查看相册
                    // 图片选择
                    selectList = PictureSelector.obtainMultipleResult(data);
                    if (selectList.get(0) != null) {
                        String path = getpathImage(selectList.get(0));
                        //presenter.datingApply(id, path);
                        mPresenter.applyViewUserInfo(friendId, path);
                    }
                    break;
            }
        }
    }


    /**
     * 获取图片，
     *
     * @param model
     * @return
     */
    public String getpathImage(LocalMedia model) {
        String path = "";
        if (model.isCut() && !model.isCompressed()) {
            // 裁剪过
            path = model.getCutPath();
        } else if (model.isCompressed() || (model.isCut() && model.isCompressed())) {
            // 压缩过,或者裁剪同时压缩过,以最终压缩过图片为准
            path = model.getCompressPath();
        } else {
            // 原图
            path = model.getPath();
        }
        return path;
    }
}
