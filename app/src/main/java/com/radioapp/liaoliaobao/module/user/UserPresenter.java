package com.radioapp.liaoliaobao.module.user;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.ApiException;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.CommonBean;
import com.radioapp.liaoliaobao.bean.user.UpdateMediaBean;
import com.radioapp.liaoliaobao.bean.user.UserBean;

import java.io.File;
import java.util.List;

import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.jaydenxiao.common.manager.ServiceManager.create;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-23
 */
public class UserPresenter extends BasePresenter<UserView> {

    private List<String> path;

    public void getUserInfo() {
        create(UserService.class)
                .getUserInfo()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<UserBean>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(UserBean userBean) {
                        mView.getUserInfo(userBean);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    public void loginOut() {
        create(UserService.class)
                .outLogin()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.loginOutSuccess();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }

    public void generateCode() {
        create(UserService.class)
                .generateCode()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.generateCodeSuccess();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 上传头像
     *
     * @param file
     */
    public void uploadAcatar(File file) {
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("image/png"), file);
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        create(UserService.class)
                .uploadAvatal(body)
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<CommonBean>(mContext) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(CommonBean commonBean) {
                        mView.avatarUrl(commonBean.getAvatar_url());
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 上传用户资源(合并2个请求)
     *
     * @param path
     */
    public void uploadResource(List<String> path, int type, int userId) {

        this.path = path;
        if (path == null && path.size() == 0) {
            ToastUitl.showLong("错误的图片");
            return;
        }
        //把所以图片装进一个集合一起请求
        for (int i = 0; i < path.size(); i++) {
          int   index = i;
            //转换图片
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("image/png"), new File(path.get(i)));
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("file", new File(path.get(i)).getName(), requestFile);
            ServiceManager.create(UserService.class).uploadResource(body, type)
                    .compose(mView.bindToLife())
                    .compose(RxHelper.handleFlatMap())
                    .subscribe(new RxSubscriber<CommonBean>(mContext, true) {
                        @Override
                        protected void subscribe(Disposable d) {
                            addSubscribe(d);
                        }

                        @Override
                        protected void _onNext(CommonBean commonBean) {
                           if(index==path.size()-1){
                               updateMediaList(userId);
                           }
                        }

                        @Override
                        protected void _onError(ApiException api) {
                            super._onError(api);
                            if(index==path.size()-1){
                                updateMediaList(userId);
                            }
                        }

                        /*@Override
                        protected void _onError(String message) {
                            super._onError(message);
                            if(index==path.size()-1){
                                updateMediaList(userId);
                            }
                        }*/

                        @Override
                        protected void _TokenInvalid() {
                            mView.tokenInvalid();
                        }
                    });
        }
    }

    /**
     * 单独请求相册数据
     *
     * @param userId
     */
    public void updateMediaList(int userId) {
        ServiceManager.create(UserService.class)
                .updateMedia(userId)
                .compose(RxHelper.handleFlatMap())
                .compose(mView.bindToLife())
                .subscribe(new RxSubscriber<List<UpdateMediaBean>>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(List<UpdateMediaBean> updateMediaBeans) {
                        mView.updateMedia(updateMediaBeans);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }

}
