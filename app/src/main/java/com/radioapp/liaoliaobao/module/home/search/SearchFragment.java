package com.radioapp.liaoliaobao.module.home.search;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.TypedValue;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.module.home.list.GenderListFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能：搜索
 * 描述
 * Created by yue on 2019-07-22
 */
public class SearchFragment extends BaseRiggerFragment {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.search_view)
    SearchView mSearchView;
    private Integer gender;
    private String lng;
    private String lat;



    /**
     * 创建framgnet
     *
     * @param gender    性别:1男性，2女性
     * @return
     */
    public static Fragment newInterface(Integer gender, String lng, String lat) {
        SearchFragment searchFragment = new SearchFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(IntentConstant.HOME_GENDER, gender);
        bundle.putString(IntentConstant.HOME_LNG, lng);
        bundle.putString(IntentConstant.HOME_LAT, lat);
        searchFragment.setArguments(bundle);
        return searchFragment;
    }


    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        gender = bundle.getInt(IntentConstant.HOME_GENDER,-1);
        lng=bundle.getString(IntentConstant.HOME_LNG,"");
        lat=bundle.getString(IntentConstant.HOME_LAT,"");
    }



    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search;
    }

    @Override
    public void initView() {
        initSearchView();
    }



    public void initSearchView() {
        mSearchView.onActionViewExpanded();
        mSearchView.setQueryHint(getString(R.string.home_search_hint));

        //不使用默认
        mSearchView.setIconifiedByDefault(false);

        mSearchView.setSubmitButtonEnabled(false);//去掉右边的提交按钮
        //让键盘的回车键设置成搜索
        mSearchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        //搜索框是否展开，false表示展开
        mSearchView.setIconified(false);
        //获取焦点
        mSearchView.setFocusable(true);
        mSearchView.requestFocusFromTouch();
        TextView txt_search = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//设置字体大小为14sp
        txt_search.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);//14sp

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {//去网络获取数据
                if (query.trim().isEmpty()) {
                    ToastUitl.showLong("请输入搜索的内容");
                    return false;
                }
                //添加数据，
                Rigger.getRigger(SearchFragment.this).showFragment(GenderListFragment.newInterface(gender==1?2:1,-1,-1,-1,-1,-1,query,lng,lat),R.id.search_frameLayout,true);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return false;
            }
        });
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        Rigger.getRigger(this).close();
    }
}
