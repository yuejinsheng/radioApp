package com.radioapp.liaoliaobao.module.user.privacy;

import com.jaydenxiao.common.base.BaseView;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-14
 */
public interface PrivacySettingView extends BaseView {
    void success(int type);
}
