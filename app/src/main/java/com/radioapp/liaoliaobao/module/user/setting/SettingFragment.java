package com.radioapp.liaoliaobao.module.user.setting;

import android.view.View;
import android.widget.TextView;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.module.user.setting.push_message.PushMessageFragment;
import com.radioapp.liaoliaobao.module.user.setting.update_pwd.UpdatePwdFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 功能： 设置的fragment
 * 描述
 * Created by yue on 2019-08-08
 */
public class SettingFragment extends BaseRiggerFragment {
    @BindView(R.id.tv_cache)
    TextView tvCache;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_setting;
    }

    @Override
    protected void initView() {
      initToolbar("设置",true,false,null);
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    @OnClick({R.id.rl_push_message, R.id.rl_update_pwd, R.id.rl_cache, R.id.rl_agreement,R.id.rl_agreement1})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_push_message:  //消息推送
                UiHelper.showPushMessageFragment(mActivity);
                break;
            case R.id.rl_update_pwd:  //更新密码
                UiHelper.showUpdatePwdFragment(mActivity);
                break;
            case R.id.rl_cache:  //清除缓存
                break;
            case R.id.rl_agreement: //协议
                UiHelper.showProtocolFragment(mActivity,"file:///android_asset/user_agree.html");
                break;
            case  R.id.rl_agreement1:
                UiHelper.showProtocolFragment(mActivity,"file:///android_asset/user_agree1.html");
                break;
        }
    }

}
