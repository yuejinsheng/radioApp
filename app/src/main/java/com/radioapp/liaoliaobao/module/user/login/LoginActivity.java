package com.radioapp.liaoliaobao.module.user.login;

import android.Manifest;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.jaydenxiao.common.base.BaseActivity;
import com.jaydenxiao.common.baseapp.BaseApplication;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jaydenxiao.common.token.TokenUtil;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.user.LoginBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.lifecyclerObserver.RxPermissionsObserver;
import com.radioapp.liaoliaobao.module.user.forgetPwd.ForgetPwdFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.utils.BaiDuMapUtil;
import com.radioapp.liaoliaobao.utils.EaseUIUtils;
import com.radioapp.liaoliaobao.utils.RegexUtil;

import butterknife.BindView;
import butterknife.OnClick;
import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能： 登录
 * 描述
 * Created by yue on 2019-07-15
 */
public class LoginActivity extends BaseActivity<LoginView, LoginPresenter> implements LoginView, RxPermissionsObserver.PermissionCallBack  {
    @BindView(R.id.et_login_account)
    EditText etLoginAccount;
    @BindView(R.id.et_login_pwd)
    EditText etLoginPwd;
    @BindView(R.id.tv_commit)
    TextView tvCommit;
    @BindView(R.id.tv_register)
    TextView tvRegister;
    @BindView(R.id.tv_forget)
    TextView tvForget;
    @BindView(R.id.iv_wx)
    ImageView ivWx;
    @BindView(R.id.iv_qq)
    ImageView ivQq;
    @BindView(R.id.iv_wb)
    ImageView ivWb;

    BaiDuMapUtil gaoDeMapUtil;

    BDLocation mapLocation;
    private RxPermissionsObserver rxPermissionsObserver;

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initView() {
        //环信退出
        EaseUIUtils.loginOut();
        //清除数据
        SharePref.clear();

        //权限申请
        rxPermissionsObserver=new RxPermissionsObserver(this);
        getLifecycle().addObserver(rxPermissionsObserver);
        rxPermissionsObserver.setCallBack(this);
        rxPermissionsObserver.requestPermissions(0,Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA);
    }

    @Override
    public void tokenInvalidRefresh() {

    }

    /**
     * 高德地图定位
     */
    public void initGaode() {
        if(gaoDeMapUtil==null) {
            gaoDeMapUtil = BaiDuMapUtil.getInstance();
            gaoDeMapUtil.setContext(this).setListener(new BaiDuMapUtil.GaoDeAMapLocationListener() {
                @Override
                public void onLocationChanged(BDLocation aMapLocation) {
                    mapLocation = aMapLocation;
                    LogUtils.e("login获取纬度-->" + aMapLocation.getLatitude() + "经度" + aMapLocation.getLongitude());
                }

            });
            getLifecycle().addObserver(gaoDeMapUtil);
        }
    }

    @OnClick({R.id.tv_commit, R.id.tv_register, R.id.tv_forget, R.id.iv_wx, R.id.iv_qq, R.id.iv_wb})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_commit: //登录
                if (checkPhone(etLoginAccount.getText().toString(), etLoginPwd.getText().toString())) {
                    mPresenter.login(etLoginAccount.getText().toString(), etLoginPwd.getText().toString(),
                            mapLocation==null?"0":String.valueOf(mapLocation.getLongitude()),
                            mapLocation==null?"0":String.valueOf(mapLocation.getLatitude()));
                }
                break;
            case R.id.tv_register: //注册
             //   UiHelper.shwRegisterActivity(mActivity);
                break;
            case R.id.tv_forget: //忘记密码
                Rigger.getRigger(new ForgetPwdFragment());
                break;
            case R.id.iv_wx:  //wx
                break;
            case R.id.iv_qq: //qq
                break;
            case R.id.iv_wb:  //wb
                break;
        }
    }


    /**
     * 检查手机号.
     **/
    private boolean checkPhone(String phone, String pwd) {
        if (TextUtils.isEmpty(phone)) {
            ToastUitl.showLong(getString(R.string.login_hint_account));
            return false;
        }
        if (TextUtils.isEmpty(pwd)) {
            ToastUitl.showLong(getString(R.string.login_hint_pwd));
            return false;
        }

        if (!RegexUtil.isMobile(phone)) {
            ToastUitl.showLong(getString(R.string.code_correct_phone_hint));
            return false;
        }
        if (!RegexUtil.isPassword(pwd)) {
            ToastUitl.showLong(getString(R.string.login_pwd_correct));
            return false;
        }


        return true;
    }

    @Override
    public void success(LoginBean loginBean) {
        //保存登录信息
        Global.saveLoginBean(loginBean);
        //保存token
        TokenUtil.putTokenType(loginBean.getToken_type());
        TokenUtil.putAccessToken(loginBean.getAccess_token());

        if (loginBean.getGender() == 0) {
            //选择性别
            UiHelper.showSelectGenderFragment(mActivity);
        } else if (loginBean.getGender() == 1) {
            //邀请码
            if (TextUtils.isEmpty(loginBean.getInvite_code()) && TextUtils.isEmpty(loginBean.getVip_ended_at())) {
                UiHelper.showCodeFragment(mActivity);
            }
        }

        EaseUIUtils.login(String.valueOf(loginBean.getEasemob_id()), loginBean.getEasemob_password());
        Rigger.getRigger(this).close();
    }

    @Override
    public void granted(int type) {
        initGaode();
    }

    @Override
    public void shouldShowRequestPermissionRationale() {

    }

    @Override
    public void other() {
    }



}
