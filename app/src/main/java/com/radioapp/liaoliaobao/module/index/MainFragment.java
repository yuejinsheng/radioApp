package com.radioapp.liaoliaobao.module.index;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.flyco.tablayout.CommonTabLayout;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.baseapp.AppManager;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.message.MessageFragment;
import com.radioapp.liaoliaobao.module.user.login.LoginFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;

import java.util.List;

import butterknife.BindView;
import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-09
 */
public class MainFragment extends BaseRiggerFragment {
    MainBarLifecycleObserver mainBarLifecycleObserver;
    @BindView(R.id.tabLayout)
    CommonTabLayout tabLayout;
    private long exitTime;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override

    protected void initView() {


        mainBarLifecycleObserver = new MainBarLifecycleObserver(tabLayout, (MainActivity) mActivity);
        getLifecycle().addObserver(mainBarLifecycleObserver);


    }





    /**
     * get unread message count
     *
     * @return
     */
    public int getUnreadMsgCountTotal() {
        return EMClient.getInstance().chatManager().getUnreadMessageCount();
    }


    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
    }

    @Override
    public void tokenInvalidRefresh() {

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    /**
     * 退出应用
     *
     * @return
     */
    public boolean onRiggerBackPressed() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(mActivity, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
            return true;
        } else {
            AppManager.getAppManager().AppExit(mActivity, true);
            return false;
        }


    }

}
