package com.radioapp.liaoliaobao.module.user.login;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.user.LoginBean;
import com.radioapp.liaoliaobao.utils.EquipmentUtil;

import io.reactivex.disposables.Disposable;


/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-16
 */
public class LoginPresenter extends BasePresenter<LoginView> {


    public void login(String mobile, String password, String lng, String lat) {
        ServiceManager.create(UserService.class)
                .login(mobile, password, lng, lat,
                        EquipmentUtil.getAndroidID(),
                        EquipmentUtil.getDeviceBrand() + EquipmentUtil.getSystemDevice(),
                        EquipmentUtil.getSystemVersion())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<LoginBean>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(LoginBean loginBean) {
                        mView.success(loginBean);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }
}
