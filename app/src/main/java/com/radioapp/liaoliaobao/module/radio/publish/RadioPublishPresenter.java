package com.radioapp.liaoliaobao.module.radio.publish;


import android.support.v4.util.ArrayMap;
import android.text.TextUtils;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.ApiException;
import com.jaydenxiao.common.baserx.CustomException;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.AppointmentService;
import com.radioapp.liaoliaobao.api.RadioService;
import com.radioapp.liaoliaobao.bean.PayBean;
import com.radioapp.liaoliaobao.bean.appointment.DatingHopesBean;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-27
 */
public class RadioPublishPresenter extends BasePresenter<RadioPublishView> {

    /**
     * 获取约会期望列表
     */
    public void getDatingHopes() {
        ServiceManager.create(AppointmentService.class)
                .getDatingHopesList()
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<List<DatingHopesBean>>(mContext) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(List<DatingHopesBean> datingHopesBeans) {
                        mView.getDatingHopes(datingHopesBeans);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });

    }



    private RequestBody toRequestBody(String value) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), value);
        return requestBody;
    }

    /**
     * 发布
     *
     * @param dating_types    约会类型 格式1,2,3
     * @param dating_hopes    约会期望 格式1,2,3
     * @param province_id     省
     * @param city_id         城市
     * @param lng             经度
     * @param lat             纬度
     * @param dating_time_str 约会时间
     * @param text            约会内容
     * @param show_same_sex   同姓可看 1可看，0否
     * @param open_comment    是否允许评价 1可评价，0否
     * @param paths           图片列表
     */
    public void radioPublish(String dating_types, String dating_hopes, int province_id, int city_id, String lng,
                             String lat, String dating_time_str, String text, int show_same_sex, int open_comment, List<String> paths) {
        //参数
        ArrayMap<String, Object> params = new ArrayMap<>();
        params.put("dating_types", dating_types);
        params.put("dating_hopes", dating_hopes);
        params.put("province_id", province_id);
        params.put("city_id", city_id);
        params.put("lng", lng);
        params.put("lat", lat);
        params.put("dating_time_str", dating_time_str);
        if(!TextUtils.isEmpty(text))
        params.put("text", text);
        params.put("show_same_sex", show_same_sex);
        params.put("open_comment", open_comment);


        //图片
        ArrayMap<String, RequestBody> bodys = new ArrayMap<>();
        if (paths != null && paths.size() > 0) {
            for (String path : paths) {
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("image/png"), new File(path));
                // MultipartBody.Part is used to send also the actual file name
               /* MultipartBody.Part body =
                        MultipartBody.Part.createFormData("file", new File(path).getName(), requestFile);*/
                bodys.put("file[]\"; filename=\"" + new File(path).getName(), requestFile);
            }
        }else{
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("image/png"),"");
            // MultipartBody.Part is used to send also the actual file name
               /* MultipartBody.Part body =
                        MultipartBody.Part.createFormData("file", new File(path).getName(), requestFile);*/
            bodys.put("file[]\"; filename=\"" + "", requestFile);
        }

        ServiceManager.create(RadioService.class)
                .radioPublish(params,bodys)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                       mView.success();
                    }


                    @Override
                    protected void _onError(ApiException api) {
                        super._onError(api);
                        if(api.getCode()== CustomException.PLEASE_PAY_FOR_THE_BROADCAST){
                            ToastUitl.showLong(api.getMessage());
                            broadcastPay();
                        }
                    }


                    @Override
                    protected void _TokenInvalid() {
                      mView.tokenInvalid();
                    }
                });


    }

    public  void broadcastPay(){
        ServiceManager.create(RadioService.class)
                .broadcastPay(2)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<PayBean>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(PayBean payBean) {
                          mView.pay(payBean);
                    }

                    @Override
                    protected void _TokenInvalid() {
                      mView.tokenInvalid();
                    }
                });

    }
}
