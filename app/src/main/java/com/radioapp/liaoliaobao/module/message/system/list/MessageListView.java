package com.radioapp.liaoliaobao.module.message.system.list;

import com.jaydenxiao.common.base.BaseView;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-12-17
 */
public interface MessageListView extends BaseView {
    void passApplySuccess(Integer position);
}
