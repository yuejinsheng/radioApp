package com.radioapp.liaoliaobao.module.user.wallet;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.user.WalletBean;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-08
 */
public interface WalletView extends BaseView {

    void  data(WalletBean walletBean);
    void success();
}
