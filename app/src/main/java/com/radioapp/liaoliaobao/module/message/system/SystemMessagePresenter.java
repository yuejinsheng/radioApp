package com.radioapp.liaoliaobao.module.message.system;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.MessageService;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.message.MessageListBean;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2020-02-08
 *
 * @author yue
 */
public class SystemMessagePresenter extends BasePresenter<SystemMessageView> {


     public void  getMessageList(String type,int position){
         ServiceManager.create(MessageService.class)
                 .getMessageList(type)
                 .compose(mView.bindToLife())
                 .compose(RxHelper.handleFlatMap())
                 .subscribe(new RxSubscriber<PageBean<MessageListBean>>(mContext,false) {
                     @Override
                     protected void subscribe(Disposable d) {
                         addSubscribe(d);
                     }

                     @Override
                     protected void _onNext(PageBean<MessageListBean> messageListBeanPageBean) {
                      mView.getList(messageListBeanPageBean.getData(),position);
                     }

                     @Override
                     protected void _TokenInvalid() {
                       mView.bindToLife();
                     }
                 });
     }
}
