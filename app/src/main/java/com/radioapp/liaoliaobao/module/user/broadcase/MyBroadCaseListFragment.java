package com.radioapp.liaoliaobao.module.user.broadcase;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.radioapp.liaoliaobao.R;

/**
 * 功能：我的广播
 * 描述
 * Created by yue on 2019-09-29
 */
public class MyBroadCaseListFragment  extends BaseRiggerFragment {
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_mybroadcase_list;
    }

    @Override
    protected void initView() {
        initToolbar("我的广播",true,false,null);

    }

    @Override
    public void tokenInvalidRefresh() {

    }
}
