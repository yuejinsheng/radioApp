package com.radioapp.liaoliaobao.module.user.customer;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.KeyValueBean;

import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-29
 */
public class CustomerPresenter extends BasePresenter<CustomerView> {

    public void getKeFu() {
        ServiceManager.create(UserService.class)
                .getKF()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<List<KeyValueBean>>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(List<KeyValueBean> list) {
                        mView.getkf(list);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.bindToLife();
                    }
                });
    }
}
