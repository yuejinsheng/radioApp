package com.radioapp.liaoliaobao.module.user.blacklist;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-29
 */
public class BlackListPagePresenter extends BasePresenter<BlackListPageView> {

     public void forBidderBlack(String friendId,Integer status,int position){
         ServiceManager.create(UserService.class)
                 .forbidder(friendId,status)
                 .compose(mView.bindToLife())
                 .compose(RxHelper.handleFlatMap())
                 .subscribe(new RxSubscriber<String>(mContext,true) {
                     @Override
                     protected void subscribe(Disposable d) {
                         addSubscribe(d);
                     }

                     @Override
                     protected void _onNext(String s) {
                        mView.forbidderBlackSuccess(position);
                     }

                     @Override
                     protected void _TokenInvalid() {
                         mView.tokenInvalid();
                     }
                 });
     }
}
