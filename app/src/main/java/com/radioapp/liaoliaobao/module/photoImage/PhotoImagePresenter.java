package com.radioapp.liaoliaobao.module.photoImage;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.RadioService;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.PayBean;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-10
 */
public class PhotoImagePresenter extends BasePresenter <PhotoImageView>{





    /**
     * 阅后即焚
     * @param userId
     * @param mediaId
     */
    public void mediaViewe(Integer userId,String mediaId,Integer positon){
        ServiceManager.create(RadioService.class)
                .mediaViewe(userId,mediaId)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                       mView.mediaVieweSuccess(positon);
                    }

                    @Override
                    protected void _TokenInvalid() {
                       mView.bindToLife();
                    }
                });
    }

    /**
     * 红包图片
     * @param mediaId
     */
    public void redalbumPay(Integer mediaId,Integer position){
        ServiceManager.create(RadioService.class)
                .redalbumPay(mediaId,2)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<PayBean>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(PayBean payBean) {
                        mView.redPay(payBean,position);
                    }

                    @Override
                    protected void _TokenInvalid() {
                      mView.tokenInvalid();
                    }
                });
    }

    /**
     * 删除成功
     * @param mediaId
     */
    public void delMedia(Integer mediaId,Integer position){
        ServiceManager.create(UserService.class)
                .delMedia(mediaId)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                      mView.delSuccess(position);
                    }

                    @Override
                    protected void _TokenInvalid() {
                    mView.tokenInvalid();
                    }
                });
    }
}
