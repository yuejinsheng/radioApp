package com.radioapp.liaoliaobao.module.user.blacklist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.base.BaseRiggerRecyclerFragment;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.user.BlackListBean;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.trello.rxlifecycle2.LifecycleTransformer;

import io.reactivex.Observable;

/**
 * 功能：黑名单
 * 描述
 * Created by yue on 2019-09-29
 */
public class BlackListPageFragment extends BaseRiggerRecyclerFragment<BlackListBean> implements BlackListPageView,
        BaseQuickAdapter.OnItemChildClickListener {


    private String lng;
    private String lat;

    private BlackListAdapter adapter;

    private BlackListPagePresenter presenter;


    public static Fragment blackListNewInterface(String lng, String lat) {

        BlackListPageFragment fragment = new BlackListPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(IntentConstant.HOME_LNG, lng);
        bundle.putString(IntentConstant.HOME_LAT, lat);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        lng = bundle.getString(IntentConstant.HOME_LNG);
        lat = bundle.getString(IntentConstant.HOME_LAT);

    }

    @Override
    protected void initData() {
        initPreserter();
        adapter = new BlackListAdapter();
        adapter.setOnItemChildClickListener(this);
    }

    @Override
    protected BaseQuickAdapter<BlackListBean, BaseViewHolder> getAdapter() {
        return adapter;
    }

    @Override
    protected Observable<PageBean<BlackListBean>> getUrl(int pageNo) {
        return ServiceManager.create(UserService.class)
                .getblackList(pageNo, lng, lat)
                .compose(RxHelper.handleFlatMap());
    }


    /**
     * 初始化presenter
     */
    public synchronized BasePresenter initPreserter() {
        if (presenter == null) {
            presenter = new BlackListPagePresenter();
            presenter.attachView(this);
            presenter.mContext = mActivity;
        }
        return presenter;
    }

    @Override
    public <T> LifecycleTransformer<T> bindToLife() {
        return bindToLifecycle();
    }

    @Override
    public void tokenInvalid() {
        tokenRefresh();
    }


    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        BlackListBean bean = (BlackListBean) adapter.getData().get(position);
        //解除黑名单
        if (view.getId() == R.id.item_black_list_remove_black) {
            if (presenter != null) {
                presenter.forBidderBlack(bean.getFriend_id(),0,position);
            }
        }
    }

    //解除成功
    @Override
    public void forbidderBlackSuccess(int position) {

        if(adapter!=null){
            adapter.getData().remove(position);
            adapter.notifyDataSetChanged();
        }
    }
}
