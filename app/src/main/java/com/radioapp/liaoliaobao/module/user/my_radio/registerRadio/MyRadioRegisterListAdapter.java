package com.radioapp.liaoliaobao.module.user.my_radio.registerRadio;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.url.BaseConstant;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.user.RadioRegisterListBean;
import com.radioapp.liaoliaobao.module.message.chat.ChatActivity;

import me.jessyan.autosize.utils.LogUtils;
import okhttp3.Call;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-11-04
 */
public class MyRadioRegisterListAdapter extends BaseQuickAdapter<RadioRegisterListBean, BaseViewHolder> {
    public MyRadioRegisterListAdapter() {
        super(R.layout.item_radio_register_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, RadioRegisterListBean item) {

        GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL+item.getAvatar(),helper.getView(R.id.iv_radio_avatar));
        GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL+item.getImg(),helper.getView(R.id.item_radio_register_list_image));
         helper.setText(R.id.item_radio_register_list_name,item.getNickname())
                 .setText(R.id.item_datio_notice_time,item.getCreated_at());
        if(item.getIs_chosen()!=0){
            helper.setText(R.id.item_datio_notice_agree,"联系Ta")
                    .setTextColor(R.id.item_datio_notice_agree,Color.parseColor("#e7e7e7"));
        }else{
            helper.setText(R.id.item_datio_notice_agree,"同意")
                    .setTextColor(R.id.item_datio_notice_agree,Color.parseColor("#5940B8"));
        }

        helper.addOnClickListener(R.id.iv_radio_avatar)
                .addOnClickListener(R.id.item_radio_register_list_image)
                .addOnClickListener(R.id.item_datio_notice_agree);

    }
}
