package com.radioapp.liaoliaobao.module.radio.list;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-12
 */
public interface RadioListView extends BaseView {
    void likeSuccess(int position);
    void datingApplySuccess();
    void commentSuccess();
    void radioInfo(List<RadioListBean> radioListBeans);
}
