package com.radioapp.liaoliaobao.module.user.privacy;


import android.support.v4.util.ArrayMap;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.UserService;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-14
 */
public class PrivacySettingPresenter extends BasePresenter<PrivacySettingView> {

    /**
     * 设置状态
     *
     * @param privacy_status
     * @param hideLocation
     * @param hide_social_account
     * @param view_album_price
     */
    public void setPrivacy(Integer privacy_status, Integer hideLocation,
                           Integer hide_social_account, Integer view_album_price) {
        ArrayMap<String, Object> params = new ArrayMap<>();
        if (privacy_status != null) {
            params.put("privacy_status", privacy_status);
        }
        if (hideLocation != null) {
            params.put("hideLocation", hideLocation);
        }
        if (hide_social_account != null) {
            params.put("hide_social_account", hide_social_account);
        }
        if (view_album_price != null) {
            params.put("view_album_price", view_album_price);
        }
        ServiceManager.create(UserService.class)
                .setPrivacy(params)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.success(privacy_status==null?0:privacy_status);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });

    }
}
