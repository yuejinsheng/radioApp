package com.radioapp.liaoliaobao.module.message.system.list;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.MessageService;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-12-17
 */
public class MessageListPresenter extends BasePresenter<MessageListView> {



    public void  passApply(String fromUserId,String nId,Integer position){
        ServiceManager.create(MessageService.class)
                .passAppLy(fromUserId, nId)
                .compose(RxHelper.handleFlatMap())
                .compose(mView.bindToLife())
                .subscribe(new RxSubscriber<String>(mContext,true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                       mView. passApplySuccess(position);
                    }

                    @Override
                    protected void _TokenInvalid() {

                    }
                });
    }
}
