package com.radioapp.liaoliaobao.module.user.privacy;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.jaydenxiao.common.sharepref.SharePref;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.utils.dialog.AlertDialogutils;

import butterknife.BindView;
import butterknife.OnCheckedChanged;

/**
 * 功能：隐私设置
 * 描述 :
 * Created by yue on 2019-08-13
 */
public class PrivacySettingFragment extends BaseRiggerFragment<PrivacySettingView, PrivacySettingPresenter> implements PrivacySettingView {
    @BindView(R.id.cbx_material)
    CheckBox cbxMaterial;
    @BindView(R.id.cbx_photo)
    CheckBox cbxPhoto;
    @BindView(R.id.cbx_code)
    CheckBox cbxCode;
    @BindView(R.id.cbx_stealth)
    CheckBox cbxStealth;
    @BindView(R.id.cbx_address)
    CheckBox cbxAddress; //地址
    @BindView(R.id.cbx_account)
    CheckBox cbxAccount;
    @BindView(R.id.rl_photo)
    RelativeLayout rlPhoto; //付费相册

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_privacy_setting;
    }

    @Override
    protected void initView() {
        initToolbar("隐私设置", true, false, null);
        if (SharePref.getInt(Constants.PRIVACY_STATUS, 0) == 0) {
            cbxMaterial.setChecked(true);
        }
        //设置初始值
        setPrivacy(SharePref.getInt(Constants.PRIVACY_STATUS, 0) == 0, SharePref.getInt(Constants.PRIVACY_STATUS, 0) == 1,
                SharePref.getInt(Constants.PRIVACY_STATUS, 0) == 2, SharePref.getInt(Constants.PRIVACY_STATUS, 0) == 3);
        cbxAddress.setChecked(SharePref.getBoolean(Constants.HIDE_LOCATION, false));
        cbxAccount.setChecked(SharePref.getBoolean(Constants.HIDE_SOCIAL_ACCOUNT, false));

         if(Global.getUserInfo().getGender()==1){
             rlPhoto.setVisibility(View.GONE);
         }
    }


    @Override
    public void tokenInvalidRefresh() {

    }


    /**
     * 设置数据
     *
     * @param isMaterial
     * @param isPhoto
     * @param isCode
     * @param isStealth
     */
    private void setPrivacy(boolean isMaterial, boolean isPhoto, boolean isCode, boolean isStealth) {
        cbxMaterial.setChecked(isMaterial);
        cbxPhoto.setChecked(isPhoto);
        cbxCode.setChecked(isCode);
        cbxStealth.setChecked(isStealth);
    }


    @OnCheckedChanged({R.id.cbx_material, R.id.cbx_photo, R.id.cbx_code, R.id.cbx_stealth,
            R.id.cbx_address, R.id.cbx_account})
    void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cbx_material: //公开资料（推荐）
                if (isChecked) {
                    setPrivacy(true, false, false, false);
                    mPresenter.setPrivacy(0, null, null, null);
                }
                break;
            case R.id.cbx_photo: //付费查看相册
                if (isChecked) {
                AlertDialogutils.createPriceDialog(mActivity, new AlertDialogutils.DialogPriceListener() {
                    @Override
                    public void setPrice(Double price) {
                        AlertDialogutils.createDialog(mActivity, "查看我的相册需要付费" + price / 100 + "元", "确定", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setPrivacy(false, true, false, false);
                                mPresenter.setPrivacy(1, null, null, price.intValue());
                            }
                        });
                    }

                });
                }


                break;
            case R.id.cbx_code: //通过我验证后查看
                if (isChecked) {
                    setPrivacy(false, false, true, false);
                    mPresenter.setPrivacy(2, null, null, null);
                }
                break;
            case R.id.cbx_stealth: //隐身
                if (isChecked) {
                    setPrivacy(false, false, false, true);
                    mPresenter.setPrivacy(3, null, null, null);
                }
                break;
            case R.id.cbx_address: //对他人隐藏我的距离
                mPresenter.setPrivacy(null, isChecked ? 1 : 0, null, null);
                break;
            case R.id.cbx_account: //对他人隐藏我的社交账号
                mPresenter.setPrivacy(null, null, isChecked ? 1 : 0, null);
                break;
        }
    }

    @Override
    public void success(int type) {
        if (type != 0) {
            SharePref.saveInt(Constants.PRIVACY_STATUS, type);
        }
        SharePref.saveBoolean(Constants.HIDE_LOCATION, cbxAddress.isChecked());
        SharePref.saveBoolean(Constants.HIDE_SOCIAL_ACCOUNT, cbxAccount.isChecked());

    }
}
