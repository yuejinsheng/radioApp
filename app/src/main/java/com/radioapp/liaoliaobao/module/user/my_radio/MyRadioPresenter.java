package com.radioapp.liaoliaobao.module.user.my_radio;

import android.nfc.tech.NfcB;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.ApiException;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.RadioService;
import com.radioapp.liaoliaobao.bean.PageBean;
import com.radioapp.liaoliaobao.bean.radio.CommentBean;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;

import io.reactivex.disposables.Disposable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-12
 */
public class MyRadioPresenter extends BasePresenter<MyRadioView> {


    /**
     * 合并广播和评论数据
     */
    public void getMyRadio() {
        ServiceManager.create(RadioService.class)
                .getUserBroadcastInfo()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<RadioListBean>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(RadioListBean bean) {
                        mView.getRadioBean(bean);
                        getCommentList(bean.getId());
                    }


                    @Override
                    protected void _onError(ApiException api) {
                        super._onError(api);
                        mView.notDate();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });

    }

    /**
     * 获取评论列表
     *
     * @param radioId
     */
    public void getCommentList(int radioId) {
        ServiceManager.create(RadioService.class)
                .getbroadcaseCommentList(radioId)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<PageBean<CommentBean>>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);

                    }

                    @Override
                    protected void _onNext(PageBean<CommentBean> commentBeans) {
                        mView.getCommentList(commentBeans.getData());
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 删除广播
     */
    public void delRadio() {
        ServiceManager.create(RadioService.class)
                .delBroadcast()
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.delRadioSuccess();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 删除评论
     *
     * @param commentId
     */
    public void delComment(Integer commentId, int position) {
        ServiceManager.create(RadioService.class)
                .delComment(commentId)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.delCommentSuccess(position);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }
}
