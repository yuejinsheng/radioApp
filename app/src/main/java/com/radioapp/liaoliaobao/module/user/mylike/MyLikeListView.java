package com.radioapp.liaoliaobao.module.user.mylike;

import com.jaydenxiao.common.base.BaseView;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-08
 */
public interface MyLikeListView extends BaseView {

     void followSuccess(Integer position);
}
