package com.radioapp.liaoliaobao.module.user.register.protocol;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jaydenxiao.common.base.BaseRiggerFragment;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.IntentConstant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 功能：注册协议
 * 描述
 * Created by yue on 2020-03-01
 *
 * @author yue
 */
public class ProtocolFragment extends BaseRiggerFragment {
    @BindView(R.id.webview)
    WebView webview;

    private String url;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_protocol;
    }

    @Override
    public void handIntent(Bundle bundle) {
        super.handIntent(bundle);
        url=bundle.getString(IntentConstant.URL,"");
    }

    @Override
    protected void initView() {
        initToolbar("",true,false,null);
        webview.loadUrl(url);
        webview.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                initToolbar(title,true,false,null);
            }
        });
//支持App内部javascript交互
        webview.getSettings().setJavaScriptEnabled(true);
//自适应屏幕
        webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webview.getSettings().setLoadWithOverviewMode(true);
//设置可以支持缩放
        webview.getSettings().setSupportZoom(true);
//扩大比例的缩放
        webview.getSettings().setUseWideViewPort(true);
//设置是否出现缩放工具
        webview.getSettings().setBuiltInZoomControls(true);


    }

    @Override
    public void tokenInvalidRefresh() {

    }


}
