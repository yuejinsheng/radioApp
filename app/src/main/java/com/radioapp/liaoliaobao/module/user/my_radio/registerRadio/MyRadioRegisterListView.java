package com.radioapp.liaoliaobao.module.user.my_radio.registerRadio;

import com.jaydenxiao.common.base.BaseView;
import com.radioapp.liaoliaobao.bean.user.RadioRegisterListBean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-11-04
 */
public interface MyRadioRegisterListView extends BaseView {

    void list(List<RadioRegisterListBean> radioRegisterListBeans);
    void passSuccess(int position);
}
