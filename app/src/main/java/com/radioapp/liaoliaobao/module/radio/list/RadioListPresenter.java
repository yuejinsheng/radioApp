package com.radioapp.liaoliaobao.module.radio.list;

import com.jaydenxiao.common.base.BasePresenter;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.radioapp.liaoliaobao.api.RadioService;
import com.radioapp.liaoliaobao.bean.radio.RadioListBean;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-12
 */
public class RadioListPresenter extends BasePresenter<RadioListView> {

    /**
     * 点赞
     *
     * @param id
     */
    public void like(int id, int position) {
        ServiceManager.create(RadioService.class)
                .broadcastsLike(id)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.likeSuccess(position);
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }

    /**
     * 我要报名
     *
     * @param id
     * @param path
     */
    public void datingApply(int id, String path) {
        //转换图片
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("image/png"), new File(path));
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", new File(path).getName(), requestFile);
        ServiceManager.create(RadioService.class)
                .broadcastsDatingApply(id, body)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String commonBean) {
                        mView.datingApplySuccess();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }

    /**
     * 评论
     *
     * @param id
     * @param text
     */
    public void addBroadcaseComment(int id, String text) {
        ServiceManager.create(RadioService.class)
                .addBroadcaseComment(id, text)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .subscribe(new RxSubscriber<String>(mContext, true) {
                    @Override
                    protected void subscribe(Disposable d) {
                        addSubscribe(d);
                    }

                    @Override
                    protected void _onNext(String s) {
                        mView.commentSuccess();
                    }

                    @Override
                    protected void _TokenInvalid() {
                        mView.tokenInvalid();
                    }
                });
    }


    /**
     * 广播详情
     */
    public void getBroadcastInfo(int id) {
        ServiceManager.create(RadioService.class)
                .getBroadcastInfo(id)
                .compose(mView.bindToLife())
                .compose(RxHelper.handleFlatMap())
                .map(new Function<RadioListBean, List<RadioListBean>>() {
                    @Override
                    public List<RadioListBean> apply(RadioListBean radioListBean) throws Exception {
                        return new ArrayList<>(Arrays.asList(radioListBean));
                    }
                }).subscribe(new RxSubscriber<List<RadioListBean>>(mContext, true) {
            @Override
            protected void subscribe(Disposable d) {
                addSubscribe(d);
            }

            @Override
            protected void _onNext(List<RadioListBean> radioListBeans) {
                mView.radioInfo(radioListBeans);
            }

            @Override
            protected void _TokenInvalid() {
                mView.tokenInvalid();
            }
        });
    }
}
