package com.radioapp.liaoliaobao.module.home.list;

import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.jaydenxiao.common.url.BaseConstant;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.GlideImageLoader;
import com.radioapp.liaoliaobao.bean.home.GenderBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.utils.AssetsUtils;
import com.radioapp.liaoliaobao.utils.GetAgeUtils;
import com.radioapp.liaoliaobao.utils.TimeFormatExchangeUtils;
import com.radioapp.liaoliaobao.view.NiceImageView;

import java.text.ParseException;

/**
 * 功能： 性别列表
 * 描述
 * Created by yue on 2019-07-22
 */
public class GenderListAdapter extends BaseQuickAdapter<GenderBean, BaseViewHolder> {
    public GenderListAdapter() {
        super(R.layout.item_hot_man);
    }

    @Override
    protected void convert(BaseViewHolder helper, GenderBean item) {
        NiceImageView ivAvatar = helper.getView(R.id.iv_avatar);
        //加载图片
        GlideImageLoader.displayImage(mContext, BaseConstant.IMAGEURL + item.getAvatar(), ivAvatar);

        helper.setText(R.id.tv_nickName, item.getNickname())
                .setVisible(R.id.iv_avatar_right, !TextUtils.isEmpty(item.getVerified_at()));

        if (item.getGender() != Global.getUserInfo().getGender()) {
            helper.setVisible(R.id.tv_follow, true);
            if (item.favorite == 0) {
                helper.setImageResource(R.id.tv_follow, R.mipmap.icon_follow_normal);
            } else {
                helper.setImageResource(R.id.tv_follow, R.mipmap.icon_follow_select);
            }
        } else {
            helper.setVisible(R.id.tv_follow, false);
        }
        //城市
        helper.setGone(R.id.tv_hot_city, item.city_id != 0);
        String cityName = (String) AssetsUtils.newInterface().getAddress(-1, item.getCity_id(), -1).get("cityName");
        helper.setText(R.id.tv_hot_city, cityName);

        //生日
        int ageByBirth = 0;
        if (!TextUtils.isEmpty(item.birth_at)) {
            try {
                ageByBirth = GetAgeUtils.getAgeByBirth(item.birth_at);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        helper.setGone(R.id.tv_hot_age, ageByBirth != 0);
        helper.setText(R.id.tv_hot_age, ageByBirth + "岁");


        //获取职业的name
        String careerName = (String) AssetsUtils.newInterface().getCareersId(item.getCareer_id()).get("careerName");
        helper.setGone(R.id.tv_type, !TextUtils.isEmpty(careerName));
        helper.setText(R.id.tv_type, careerName);


        //职业

        String time = "在线";
        //先隐藏掉
        helper.setGone(R.id.tv_content, false);
        //距离和在线时间
        if (item.hide_location == 1) {

            time = TimeFormatExchangeUtils.calculateTime(item.updated_at);

            helper.setText(R.id.tv_content, "保密" + "\t\t" + time);
        } else {
            time = TimeFormatExchangeUtils.calculateTime(item.updated_at);
            helper.setText(R.id.tv_content1, time);
            int distance = item.distance;

            if (distance / 1000 > 0) {
                if (distance % 1000 / 100 == 0) {
                    helper.setText(R.id.tv_content, item.distance / 1000 + "km");
                } else {
                    helper.setText(R.id.tv_content, item.distance / 1000 + "." + distance % 1000 / 100 + "km");
                }
            } else {
                helper.setText(R.id.tv_content, item.distance + "m");
            }
        }
        //收藏
        helper.addOnClickListener(R.id.tv_follow);

    }
}
