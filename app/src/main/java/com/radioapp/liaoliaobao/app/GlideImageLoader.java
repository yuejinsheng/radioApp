package com.radioapp.liaoliaobao.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.previewlibrary.loader.IZoomMediaLoader;
import com.previewlibrary.loader.MySimpleTarget;
import com.radioapp.liaoliaobao.R;

/**
 * 加载图片
 */

public class GlideImageLoader implements IZoomMediaLoader {


    /**
     * 加载图片
     *
     * @param context
     * @param path
     * @param imageView
     */
    public static void displayImage(Context context, @NonNull String path, ImageView imageView) {
        GlideApp.with(context)
                .setDefaultRequestOptions(new RequestOptions().error(R.mipmap.image_error)
                        .placeholder(R.mipmap.image_loading)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .asBitmap()
                .load(path)
                //  .placeholder(android.R.color.darker_gray)
                .fitCenter()
                .into(imageView);
    }

    @Override
    public void displayImage(@NonNull Fragment context, @NonNull String path, ImageView imageView, @NonNull MySimpleTarget simpleTarget) {
        GlideApp.with(context)
                .asBitmap()
                .load(path)
                .apply(new RequestOptions().centerInside().error(R.mipmap.image_error).placeholder(R.mipmap.image_loading).priority(Priority.HIGH))
                //  .placeholder(android.R.color.darker_gray)
                .centerInside()
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        simpleTarget.onLoadFailed(null);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        simpleTarget.onResourceReady();
                        return false;
                    }
                }).into(imageView);
    }

    @Override
    public void displayGifImage(@NonNull Fragment context, @NonNull String path, ImageView imageView, @NonNull MySimpleTarget simpleTarget) {
        GlideApp.with(context)
                .setDefaultRequestOptions(new RequestOptions().error(R.mipmap.image_error)
                        .placeholder(R.mipmap.image_loading).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                .asGif()
                .load(path)
                //可以解决gif比较几种时 ，加载过慢  //DiskCacheStrategy.NONE
                .dontAnimate() //去掉显示动画
                .listener(new RequestListener<GifDrawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                        simpleTarget.onResourceReady();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                        simpleTarget.onLoadFailed(null);
                        return false;
                    }
                })
                .into(imageView);
    }

    @Override
    public void onStop(@NonNull Fragment context) {
        GlideApp.with(context).onStop();
    }

    @Override
    public void clearMemory(@NonNull Context c) {
        Glide.get(c).clearMemory();

    }
}
