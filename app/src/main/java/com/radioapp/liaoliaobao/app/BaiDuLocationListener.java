package com.radioapp.liaoliaobao.app;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.hyphenate.chat.EMClient;
import com.jaydenxiao.common.baseapp.AppManager;
import com.jaydenxiao.common.baserx.RxHelper;
import com.jaydenxiao.common.baserx.RxSchedulersThread;
import com.jaydenxiao.common.baserx.RxSubscriber;
import com.jaydenxiao.common.manager.ServiceManager;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jaydenxiao.common.token.TokenUtil;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.api.UserService;
import com.radioapp.liaoliaobao.bean.user.LoginBean;
import com.radioapp.liaoliaobao.bean.user.UserBean;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.index.MainActivity;
import com.radioapp.liaoliaobao.module.index.SplashFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;

import io.reactivex.disposables.Disposable;
import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能：定位
 * 描述
 * Created by yue on 2019-12-13
 */
public class BaiDuLocationListener implements BDLocationListener {
    @Override
    public void onReceiveLocation(BDLocation location) {
        try {


        LogUtils.e("定位" + location.getLatitude() + "/" + location.getLongitude());
        SharePref.saveString(Constants.LATITUDE, location.getLatitude() + "");
        SharePref.saveString(Constants.LONGITUDE, location.getLongitude() + "");
        if (SharePref.getBoolean(Constants.ISSPLASH, true)) {
            SharePref.saveBoolean(Constants.ISSPLASH, false);
            //判断登录
            LoginBean loginBean = Global.getLoginBean();
            UserBean userBean = Global.getUserInfo();
            int gender = userBean.getGender() == 0 ? loginBean.getGender() : userBean.getGender();
            String vip = userBean.getVip_ended_at() == null ? loginBean.getVip_ended_at() : userBean.getVip_ended_at();
            String inviteCode = userBean.getInvite_code() == null ? loginBean.getInvite_code() : userBean.getInvite_code();
            LogUtils.e("gender--" + gender);
            LogUtils.e("token--" + TokenUtil.getAccessToken());
            Activity activity = AppManager.getAppManager().currentActivity();
            if (activity instanceof MainActivity) {
                if (!TextUtils.isEmpty(TokenUtil.getAccessToken())) {
                    if (gender == 0) {
                        /**判断性别，0是没有填，跳转到性别页面，1或2代表已经填过资料了
                         * 1是男用户
                         * 2是女用户，直接登录成功
                         */

                        UiHelper.showLoginFragment(activity);
                    } else if (gender == 1) {
                        /**
                         * 判断邀请码和vip是否为空
                         * 后台逻辑是填了邀请码就有邀请码，开通了会员vip有一个会员到期时间，
                         */
                        if (TextUtils.isEmpty(inviteCode) && TextUtils.isEmpty(vip)) {
                            UiHelper.showLoginFragment(activity);
                        } else {
                            EMClient.getInstance().chatManager().loadAllConversations();
                            UiHelper.showMainFragment(activity);
                        }
                    } else if (gender == 2) {
                        EMClient.getInstance().chatManager().loadAllConversations();
                        UiHelper.showMainFragment(activity);
                    } else {
                        UiHelper.showLoginFragment(activity);
                    }
                } else {
                    UiHelper.showLoginFragment(activity);
                }

                Fragment fragmentByTag = Rigger.getRigger(activity).findFragmentByTag(SplashFragment.class.getName());
                if (fragmentByTag != null) {
                    Rigger.getRigger(activity).close(fragmentByTag);
                }

            }
        }
            if (!TextUtils.isEmpty(TokenUtil.getAccessToken())) {
                ServiceManager.create(UserService.class)
                        .updateLngAndLat(SharePref.getString(Constants.LONGITUDE, "0"),
                                SharePref.getString(Constants.LATITUDE, "0"))
                        .compose(RxSchedulersThread.io_main())
                        .compose(RxHelper.handleFlatMap())
                        .subscribe(new RxSubscriber<String>(App.getInstance(), false) {
                            @Override
                            protected void subscribe(Disposable d) {

                            }

                            @Override
                            protected void _onNext(String s) {
                                LogUtils.e("定位更新");
                            }

                            @Override
                            protected void _TokenInvalid() {

                            }
                        });
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
