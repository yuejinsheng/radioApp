package com.radioapp.liaoliaobao.uihelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.jaydenxiao.common.url.BaseConstant;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.radio.RadioPublishBean;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.module.home.HomeFragment;
import com.radioapp.liaoliaobao.module.home.search.SearchFragment;
import com.radioapp.liaoliaobao.module.index.MainFragment;
import com.radioapp.liaoliaobao.module.index.SplashFragment;
import com.radioapp.liaoliaobao.module.message.chat.ChatActivity;
import com.radioapp.liaoliaobao.module.home.detail.UserInfoDetailFragment;
import com.radioapp.liaoliaobao.module.message.system.list.MessageListContentFramgent;
import com.radioapp.liaoliaobao.module.radio.detail.RadioDetailFragment;
import com.radioapp.liaoliaobao.module.radio.publish.RadioPublishFragment;
import com.radioapp.liaoliaobao.module.user.auth.AuthApplyFragment;
import com.radioapp.liaoliaobao.module.user.auth.AuthApplySuccessFragment;
import com.radioapp.liaoliaobao.module.user.auth.AuthIDFragment;
import com.radioapp.liaoliaobao.module.user.blacklist.BlackListFragment;
import com.radioapp.liaoliaobao.module.user.customer.CustomerFragment;
import com.radioapp.liaoliaobao.module.user.forgetPwd.ForgetPwdFragment;
import com.radioapp.liaoliaobao.module.user.login.LoginActivity;
import com.radioapp.liaoliaobao.module.user.login.LoginFragment;
import com.radioapp.liaoliaobao.module.user.material.MaterialFragment;
import com.radioapp.liaoliaobao.module.user.member.MemberFragment;
import com.radioapp.liaoliaobao.module.user.member.PayStatusFragment;
import com.radioapp.liaoliaobao.module.user.my_radio.MyRadioFragment;
import com.radioapp.liaoliaobao.module.user.my_radio.registerRadio.MyRadioRegisterListFragment;
import com.radioapp.liaoliaobao.module.user.mylike.MyLikeFragment;
import com.radioapp.liaoliaobao.module.user.privacy.PrivacySettingFragment;
import com.radioapp.liaoliaobao.module.user.register.RegisterFragment;
import com.radioapp.liaoliaobao.module.user.register.code.CodeFragment;
import com.radioapp.liaoliaobao.module.user.register.getCode.GetCodeFragment;
import com.radioapp.liaoliaobao.module.user.register.protocol.ProtocolFragment;
import com.radioapp.liaoliaobao.module.user.register.selectgender.SelectGenderFragment;
import com.radioapp.liaoliaobao.module.user.setting.SettingFragment;
import com.radioapp.liaoliaobao.module.user.setting.push_message.PushMessageFragment;
import com.radioapp.liaoliaobao.module.user.setting.update_pwd.UpdatePwdFragment;
import com.radioapp.liaoliaobao.module.user.wallet.WalletFragment;

import java.util.InputMismatchException;

/**
 * 功能： 跳转
 * 描述
 * Created by yue on 2019-07-16
 */
public class UiHelper {


    /**
     * 进入首页
     *
     * @param mActivity
     */
    public static void showMainFragment(Activity mActivity) {
        MainFragment mainFragment = new MainFragment();
        startFragment(mActivity, mainFragment, null);
    }


    /**
     * 引导页
     *
     * @param mActivity
     */
    public static void showSplashFragment(Activity mActivity) {
        SplashFragment splashFragment = new SplashFragment();
        startFragment(mActivity, splashFragment, null);
    }

    /**
     * 登录
     *
     * @param activity
     */
    public static void showLoginFragment(Activity activity) {
        LoginFragment fragment = new LoginFragment();
        startFragment(activity, fragment, null);

    }


    /**
     * 注册
     *
     * @param activity
     */
    public static void shwRegisterFrament(Activity activity) {
        RegisterFragment registerFragment = new RegisterFragment();
        startFragment(activity, registerFragment, null);
    }

    public static void showForgetPwdFragment(Activity activity){
        ForgetPwdFragment forgetPwdFragment=new ForgetPwdFragment();
        startFragment(activity,forgetPwdFragment,null);
    }


    /**
     * 选择性别
     */
    public static void showSelectGenderFragment(Activity activity) {
        SelectGenderFragment selectGenderFragment = new SelectGenderFragment();
        startFragment(activity, selectGenderFragment, null);
    }

    /**
     * 邀请码
     *
     * @param activity
     */
    public static void showCodeFragment(Activity activity) {
        CodeFragment codeFragment = new CodeFragment();
        startFragment(activity, codeFragment, null);
    }

    /**
     * 填写资料
     */
    public static void showMaterialActivity(Activity activity, int gender) {
        MaterialFragment materialFragment = new MaterialFragment();
        Bundle args = new Bundle();
        args.putInt(IntentConstant.GENDER, gender);
        startFragment(activity, materialFragment, args);

    }

    /**
     * 会员中心
     *
     * @param activity
     * @param vip_ended_at
     */
    public static void showMemberFragment(Activity activity, String vip_ended_at) {
        MemberFragment memberFragment = new MemberFragment();
        Bundle args = new Bundle();
        args.putString(IntentConstant.VIP_ENDED_AT, vip_ended_at);
        startFragment(activity, memberFragment, args);
    }


    /**
     * 支付状态页面
     *
     * @param activity
     * @param status
     */
    public static void showPayStatusFragment(Activity activity, boolean status) {
        PayStatusFragment statusFragment = new PayStatusFragment();
        Bundle args = new Bundle();
        args.putBoolean(IntentConstant.STATUS, status);
        startFragment(activity, statusFragment, args);
    }

    /**
     * 搜索
     * TODO 搜索要改就改下Bundle
     *
     * @param activity
     */
    public static void showSearchFragment(Activity activity, int gender, String lng, String lat) {
        Rigger.getRigger(activity).startFragment(SearchFragment.newInterface(gender, lng, lat));

    }

    /**
     * 聊天
     *
     * @param activity
     * @param userId
     */
    public static void showChatActivity(Activity activity, String userId) {
        Intent intent = new Intent(activity, ChatActivity.class);
        intent.putExtra(IntentConstant.USERID, userId);
        //自己的要传到对方的手机去
        intent.putExtra("nickName", Global.getUserInfo().getNickname());
        intent.putExtra("avatar", BaseConstant.IMAGEURL + Global.getUserInfo().getAvatar());
        activity.startActivity(intent);
    }


    /**
     * 发布电台
     *
     * @param activity
     * @param bean     当前类
     */
    public static void showRadioPublishFragment(Activity activity, RadioPublishBean bean) {
        RadioPublishFragment radioPublishFragment = new RadioPublishFragment();
        Bundle args = new Bundle();
        args.putParcelable(IntentConstant.RADIOPUBLISHBEAN, bean);
        startFragment(activity, radioPublishFragment, args);
    }

    /**
     * 用户的详情信息
     *
     * @param activity
     * @param friendId 朋友id
     */
    public static void showUserInfoDetailFragment(Activity activity, Integer friendId) {
        UserInfoDetailFragment userInfoDetailFragment = new UserInfoDetailFragment();
        Bundle args = new Bundle();
        args.putInt(IntentConstant.FRIENDID, friendId);
        startFragment(activity, userInfoDetailFragment, args);
    }


    /**
     * 电台详情
     *
     * @param activity
     * @param broadcaseId
     */
    public static void showRadioDetailFragment(Activity activity, Integer broadcaseId) {
        RadioDetailFragment radioDetailFragment = new RadioDetailFragment();
        Bundle args = new Bundle();
        args.putInt(IntentConstant.BROADCASEID, broadcaseId);
        startFragment(activity, radioDetailFragment, args);
    }

    /**
     * 跳转系统消息
     *
     * @param activity
     */
    public static void showMessageListContentFragment(Activity activity, Bundle bundle) {
        MessageListContentFramgent fragment = new MessageListContentFramgent();
        startFragment(activity, fragment, bundle);

    }


    /**
     * 认证身份证
     *
     * @param activity
     */
    public static void showAuthIDFragment(Activity activity) {
        AuthIDFragment authIDFragment = new AuthIDFragment();
        startFragment(activity, authIDFragment, null);
    }

    public static void showAuthApplyFragment(Activity activity) {
        AuthApplyFragment authApplyFragment = new AuthApplyFragment();
        startFragment(activity, authApplyFragment, null);
    }

    /**
     * 认证支付宝成功页面
     *
     * @param activity
     */
    public static void showAuthApplySuccessFragment(Activity activity) {
        AuthApplySuccessFragment authApplySuccessFragment = new AuthApplySuccessFragment();
        startFragment(activity, authApplySuccessFragment, null);
    }


    /**
     * 查看当前约会名单
     * @param activity
     */
    public static void showMyRadioRegisterListFragment(Activity activity){
        MyRadioRegisterListFragment myRadioRegisterListFragment=new MyRadioRegisterListFragment();
        startFragment(activity, myRadioRegisterListFragment, null);
    }


    /**
     *获取邀请码
     * @param activity
     */
    public static void showGetCodeFragment(Activity activity){
        GetCodeFragment getCodeFragment=new GetCodeFragment();
        startFragment(activity,getCodeFragment,null);
    }

    /**
     * 客服
     * @param activity
     */
    public static void showCustomerFragment(Activity activity){
        CustomerFragment customerFragment=new CustomerFragment();
        startFragment(activity,customerFragment,null);
    }


    /**
     * 消息推送设置
     * @param activity
     */
    public static void showPushMessageFragment(Activity activity){
        PushMessageFragment pushMessageFragment=new PushMessageFragment();
        startFragment(activity,pushMessageFragment,null);
    }


    /**
     * 修改密码
     * @param activity
     */
    public static void showUpdatePwdFragment(Activity activity){
        UpdatePwdFragment updatePwdFragment=new UpdatePwdFragment();
        startFragment(activity,updatePwdFragment,null);
    }


    /**
     * 钱包
     * @param activity
     */
    public static void showWalletFragment(Activity activity){
        WalletFragment walletFragment=new WalletFragment();
        startFragment(activity,walletFragment,null);
    }


    /**
     * 隐私设置
     * @param activity
     */
    public static void showPrivacySettingFragment(Activity activity){
        PrivacySettingFragment privacySettingFragment=new PrivacySettingFragment();
        startFragment(activity,privacySettingFragment,null);
    }

    /**
     * 我的广播
     * @param activity
     */
     public static void showMyRadioFragment(Activity activity){
         MyRadioFragment myRadioFragment=new MyRadioFragment();
         startFragment(activity,myRadioFragment,null);
     }

    /**
     * 我喜欢的
     * @param activity
     */
    public static void showMyLikeFragment(Activity activity){
         MyLikeFragment myLikeFragment=new MyLikeFragment();
         startFragment(activity,myLikeFragment,null);
     }


    /**
     * 黑名单
     * @param activity
     */
    public static void showBlackListFragment(Activity activity){
         BlackListFragment blackListFragment=new BlackListFragment();
         startFragment(activity,blackListFragment,null);
     }

     /**
      * 设置
      */
     public static void showSettingFragment(Activity activity){
         SettingFragment settingFragment=new SettingFragment();
         startFragment(activity,settingFragment,null);
     }

    /**
     * 注册协议
     * @param activity
     */
     public static void showProtocolFragment(Activity activity,String url){
         ProtocolFragment protocolFragment=new ProtocolFragment();
         Bundle args = new Bundle();
         args.putString(IntentConstant.URL, url);
         startFragment(activity,protocolFragment,args);
     }



    /**
     * 跳转fragment，如何添加到战区，就显示
     *
     * @param activity
     * @param fragment
     * @param bundle
     */
    public static void startFragment(Activity activity, Fragment fragment, Bundle bundle) {
        Fragment fragmentTag = Rigger.getRigger(activity).findFragmentByTag(fragment.getClass().getName());
        if (fragmentTag != null) {

            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            Rigger.getRigger(activity).showFragment(fragment.getClass().getName());
        } else {
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            Rigger.getRigger(activity).startFragment(fragment);
        }
    }


}
