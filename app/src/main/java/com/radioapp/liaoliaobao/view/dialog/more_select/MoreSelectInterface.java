package com.radioapp.liaoliaobao.view.dialog.more_select;

/**
 * 功能： 显示的数据必须实现这个接口
 * 描述
 * Created by yue on 2019-07-24
 */
public interface MoreSelectInterface {
    String getText();
}
