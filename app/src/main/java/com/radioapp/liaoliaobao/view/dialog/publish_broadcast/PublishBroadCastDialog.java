package com.radioapp.liaoliaobao.view.dialog.publish_broadcast;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.bean.radio.RadioPublishBean;
import com.radioapp.liaoliaobao.constant.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 功能：发布广播弹窗
 * 描述  发布广播的弹窗显示
 * Created by yue on 2019-08-01
 */
public class PublishBroadCastDialog extends DialogFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    BroadCaseAdapter adapter;
    private Activity activity;

    OnItemClickListener onItemClickListener;
    private Unbinder mUnbinder;


    public interface OnItemClickListener {
        void onItemClick(RadioPublishBean bean);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        //设置style
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomDialog);

    }


    @Override
    public void onStart() {
        super.onStart();
        //设置 dialog 的宽高
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置 dialog 的背景为 null
        getDialog().getWindow().setBackgroundDrawable(null);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //去除标题栏
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);


        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.CENTER; //底部
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        return createView(inflater, container);
    }


    //重写此方法，设置布局文件
    protected View createView(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.broadcase_dialog, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    /**
     * 初始化控件
     */
    private void initView() {
        recyclerView.setLayoutManager(new GridLayoutManager(activity, 4));
        adapter = new BroadCaseAdapter(Constants.getPublishBroadCaseList());
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                RadioPublishBean bean = (RadioPublishBean) adapter.getData().get(position);
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(bean);
                }
                dismiss();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null)
            mUnbinder.unbind();

    }

    @OnClick(R.id.iv_close)
    public void onViewClicked() {
        dismiss();
    }


    /**
     * 适配器
     */

    class BroadCaseAdapter extends BaseQuickAdapter<RadioPublishBean, BaseViewHolder> {


        public BroadCaseAdapter(@Nullable List<RadioPublishBean> data) {
            super(R.layout.item_broadcase, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, RadioPublishBean item) {
            helper.setText(R.id.tv_name, item.getTitle());
            helper.setImageResource(R.id.iv_img, item.getImg());
        }
    }
}
