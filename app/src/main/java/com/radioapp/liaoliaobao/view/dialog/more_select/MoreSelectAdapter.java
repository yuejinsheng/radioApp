package com.radioapp.liaoliaobao.view.dialog.more_select;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.radioapp.liaoliaobao.R;

import java.util.Arrays;
import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-24
 */
public class MoreSelectAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {
    private Boolean[] selectStates;

    public MoreSelectAdapter() {
        super(R.layout.item_more_list);
    }



    public Boolean[] getSelectStates() {
        return selectStates;
    }

    public void setSelectStates(Boolean[] selectStates) {
        this.selectStates = selectStates;
    }

    /**
     * 初始化当前的集合
     * @param list
     */
    public void initStates(List<MoreSelectInterface> list){
        selectStates=new Boolean[list.size()];
        Arrays.fill(selectStates,false);
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        if(item instanceof MoreSelectInterface){
            helper.setText(R.id.tv_name,((MoreSelectInterface) item).getText());
        }
        helper.addOnClickListener(R.id.root_view);
        helper.getView(R.id.tv_name).setSelected(selectStates[helper.getAdapterPosition()]);
    }
}
