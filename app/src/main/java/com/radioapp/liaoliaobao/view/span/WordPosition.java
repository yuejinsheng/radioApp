package com.radioapp.liaoliaobao.view.span;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-26
 */
public class WordPosition {
    int start;
    int end;

    public WordPosition(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "WordPosition{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
