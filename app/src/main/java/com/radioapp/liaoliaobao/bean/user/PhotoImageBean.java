package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：红包，阅后即焚，预览数据
 * 描述
 * Created by yue on 2019-10-10
 */
public class PhotoImageBean implements Parcelable {
    private int id;
    private int user_id;//     "user_id": 106,
    private int type;//     "type": 1,
    private String resource_url;//     "resource_url": "images\/2019_08\/07\/jwKP2phpaujaDj1565169627190807.png",
    private String mime_type;//     "mime_type": "image\/png",
    private int file_size;//     "file_size": 161088,
    private int price;//     "price": 0,
    private int is_once;//     "is_once": 1
    private String user_media_id;
    public int  media_id;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeInt(this.type);
        dest.writeString(this.resource_url);
        dest.writeString(this.mime_type);
        dest.writeInt(this.file_size);
        dest.writeInt(this.price);
        dest.writeInt(this.is_once);
        dest.writeString(this.user_media_id);
        dest.writeInt(this.media_id);
    }

    public PhotoImageBean() {
    }

    protected PhotoImageBean(Parcel in) {
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.type = in.readInt();
        this.resource_url = in.readString();
        this.mime_type = in.readString();
        this.file_size = in.readInt();
        this.price = in.readInt();
        this.is_once = in.readInt();
        this.user_media_id = in.readString();
        this.media_id=in.readInt();
    }

    public static final Parcelable.Creator<PhotoImageBean> CREATOR = new Parcelable.Creator<PhotoImageBean>() {
        @Override
        public PhotoImageBean createFromParcel(Parcel source) {
            return new PhotoImageBean(source);
        }

        @Override
        public PhotoImageBean[] newArray(int size) {
            return new PhotoImageBean[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getResource_url() {
        return resource_url;
    }

    public void setResource_url(String resource_url) {
        this.resource_url = resource_url;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public int getFile_size() {
        return file_size;
    }

    public void setFile_size(int file_size) {
        this.file_size = file_size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIs_once() {
        return is_once;
    }

    public void setIs_once(int is_once) {
        this.is_once = is_once;
    }

    public String getUser_media_id() {
        return user_media_id;
    }

    public void setUser_media_id(String user_media_id) {
        this.user_media_id = user_media_id;
    }

    public int getMedia_id() {
        return media_id;
    }

    public void setMedia_id(int media_id) {
        this.media_id = media_id;
    }
}
