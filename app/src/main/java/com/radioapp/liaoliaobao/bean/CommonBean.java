package com.radioapp.liaoliaobao.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能： 公共部分， 可能一个参数的
 * 描述
 * Created by yue on 2019-07-17
 */
public class CommonBean implements Parcelable {

    private String avatar_url;//头像
    private String invite_code;



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.avatar_url);
        dest.writeString(this.invite_code);
    }

    public CommonBean() {
    }

    protected CommonBean(Parcel in) {
        this.avatar_url = in.readString();
        this.invite_code=in.readString();
    }

    public static final Parcelable.Creator<CommonBean> CREATOR = new Parcelable.Creator<CommonBean>() {
        @Override
        public CommonBean createFromParcel(Parcel source) {
            return new CommonBean(source);
        }

        @Override
        public CommonBean[] newArray(int size) {
            return new CommonBean[size];
        }
    };

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getInvite_code() {
        return invite_code;
    }

    public void setInvite_code(String invite_code) {
        this.invite_code = invite_code;
    }
}
