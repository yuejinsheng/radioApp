package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-26
 */
public class VipBean implements Parcelable {

    private String value;
    private String key;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
        dest.writeString(this.key);
    }

    public VipBean() {
    }

    protected VipBean(Parcel in) {
        this.value = in.readString();
        this.key = in.readString();
    }

    public static final Parcelable.Creator<VipBean> CREATOR = new Parcelable.Creator<VipBean>() {
        @Override
        public VipBean createFromParcel(Parcel source) {
            return new VipBean(source);
        }

        @Override
        public VipBean[] newArray(int size) {
            return new VipBean[size];
        }
    };
}

