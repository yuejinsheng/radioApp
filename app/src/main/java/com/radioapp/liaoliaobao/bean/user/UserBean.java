package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-23
 */
public class UserBean implements Parcelable {

    /**
     * id : 69
     * mobile : 18022665753
     * cash : 0
     * invite_code : 6314EE23
     * gender : 1
     * avatar : images/2019_08/04/K2TZYphpFUooJK1564915296190804.JPEG
     * nickname : 大荣
     * career_id : 13
     * wechat : 123456
     * qq : 123456
     * height : 155
     * weight : 47
     * bwh_str :
     * intro : 哈比才
     * vip_ended_at : null
     * is_public : 0
     * is_visible : 1
     * hide_location : 0
     * hide_social_account : 0
     * view_album_price : 0
     * verified_at : null
     * lng : 0.000000
     * lat : 0.000000
     * latest_online_at : null
     * from_where : 1802266575
     * alipay_account :
     * alipay_realname :
     * cert_code : KdIE1Oje
     * login_ip : 1885365353
     * reg_ip : 1885365353
     * history_visitors : 0
     * banned_at : null
     * created_at : 2019-07-20 02:40:15
     * updated_at : 2019-08-23 17:35:06
     * birth_at : 2019-08-06 00:00:00
     * geohash : s00000000
     * privacy_status : 0
     * province_id : 0
     * city_id : 0
     * regions : [{"user_id":69,"province_id":140000,"city_id":140100}]
     * user_dating_hopes : [{"user_id":69,"dating_hope_id":5}]
     * user_dating_types : [{"user_id":69,"dating_type_id":5}]
     */
    public int in_certification;//1为女士认证中
    private int id;  //id
    private String mobile;   //手机号
    private int cash;    //现金，金额
    private String invite_code; //邀请码
    private int gender; //性别0未知，1男，2女
    private String avatar; //头像
    private String nickname; //昵称
    private int career_id;  //职业id
    private String wechat; //微信号
    private String qq;  //qq号
    private String height; //身高
    private String weight;  //体重
    private String bwh_str;
    private String intro;  // 个人介绍
    private String  vip_ended_at; //vip的时间
    private int is_public;  //公开
    private int is_visible;  //隐藏
    private int hide_location; //隐藏定位
    private int hide_social_account; //隐藏社交账号
    private int view_album_price;  //付费相册价格
    private String  verified_at;  //非空为已认证
    private String lng;  //定位坐标
    private String lat; //
    private String latest_online_at;
    private String from_where;
    private String alipay_account;
    private String alipay_realname;
    private String cert_code;
    private String  login_ip;
    private String reg_ip;
    private int history_visitors;  //历史别人看过自己的人数
    private String banned_at;
    private String created_at;
    private String updated_at;
    private String birth_at;
    private String geohash;
    private int privacy_status; // 0公开，1付费相册，2查看前需要通过我验证，3隐身
    private int province_id;  //出生省份
    private int city_id;  //出生城市
    private List<RegionsBean> regions;  //约会范围
    private List<UserDatingHopesBean> user_dating_hopes;  //约会期望
    private List<UserDatingTypesBean> user_dating_types; //约会节目

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public String getInvite_code() {
        return invite_code;
    }

    public void setInvite_code(String invite_code) {
        this.invite_code = invite_code;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getCareer_id() {
        return career_id;
    }

    public void setCareer_id(int career_id) {
        this.career_id = career_id;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBwh_str() {
        return bwh_str;
    }

    public void setBwh_str(String bwh_str) {
        this.bwh_str = bwh_str;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String  getVip_ended_at() {
        return vip_ended_at;
    }

    public void setVip_ended_at(String vip_ended_at) {
        this.vip_ended_at = vip_ended_at;
    }

    public int getIs_public() {
        return is_public;
    }

    public void setIs_public(int is_public) {
        this.is_public = is_public;
    }

    public int getIs_visible() {
        return is_visible;
    }

    public void setIs_visible(int is_visible) {
        this.is_visible = is_visible;
    }

    public int getHide_location() {
        return hide_location;
    }

    public void setHide_location(int hide_location) {
        this.hide_location = hide_location;
    }

    public int getHide_social_account() {
        return hide_social_account;
    }

    public void setHide_social_account(int hide_social_account) {
        this.hide_social_account = hide_social_account;
    }

    public int getView_album_price() {
        return view_album_price;
    }

    public void setView_album_price(int view_album_price) {
        this.view_album_price = view_album_price;
    }

    public String  getVerified_at() {
        return verified_at;
    }

    public void setVerified_at(String verified_at) {
        this.verified_at = verified_at;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLatest_online_at() {
        return latest_online_at;
    }

    public void setLatest_online_at(String latest_online_at) {
        this.latest_online_at = latest_online_at;
    }

    public String getFrom_where() {
        return from_where;
    }

    public void setFrom_where(String from_where) {
        this.from_where = from_where;
    }

    public String getAlipay_account() {
        return alipay_account;
    }

    public void setAlipay_account(String alipay_account) {
        this.alipay_account = alipay_account;
    }

    public String getAlipay_realname() {
        return alipay_realname;
    }

    public void setAlipay_realname(String alipay_realname) {
        this.alipay_realname = alipay_realname;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getLogin_ip() {
        return login_ip;
    }

    public void setLogin_ip( String login_ip) {
        this.login_ip = login_ip;
    }

    public String  getReg_ip() {
        return reg_ip;
    }

    public void setReg_ip(String reg_ip) {
        this.reg_ip = reg_ip;
    }

    public int getHistory_visitors() {
        return history_visitors;
    }

    public void setHistory_visitors(int history_visitors) {
        this.history_visitors = history_visitors;
    }

    public String getBanned_at() {
        return banned_at;
    }

    public void setBanned_at(String banned_at) {
        this.banned_at = banned_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBirth_at() {
        return birth_at;
    }

    public void setBirth_at(String birth_at) {
        this.birth_at = birth_at;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    public int getPrivacy_status() {
        return privacy_status;
    }

    public void setPrivacy_status(int privacy_status) {
        this.privacy_status = privacy_status;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public List<RegionsBean> getRegions() {
        return regions;
    }

    public void setRegions(List<RegionsBean> regions) {
        this.regions = regions;
    }

    public List<UserDatingHopesBean> getUser_dating_hopes() {
        return user_dating_hopes;
    }

    public void setUser_dating_hopes(List<UserDatingHopesBean> user_dating_hopes) {
        this.user_dating_hopes = user_dating_hopes;
    }

    public List<UserDatingTypesBean> getUser_dating_types() {
        return user_dating_types;
    }

    public void setUser_dating_types(List<UserDatingTypesBean> user_dating_types) {
        this.user_dating_types = user_dating_types;
    }


    public int getIn_certification() {
        return in_certification;
    }

    public void setIn_certification(int in_certification) {
        this.in_certification = in_certification;
    }

    public static class RegionsBean implements Parcelable {
        /**
         * user_id : 69
         * province_id : 140000
         * city_id : 140100
         */

        private int user_id;
        private int province_id;
        private int city_id;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getProvince_id() {
            return province_id;
        }

        public void setProvince_id(int province_id) {
            this.province_id = province_id;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.province_id);
            dest.writeInt(this.city_id);
        }

        public RegionsBean() {
        }

        protected RegionsBean(Parcel in) {
            this.user_id = in.readInt();
            this.province_id = in.readInt();
            this.city_id = in.readInt();
        }

        public static final Creator<RegionsBean> CREATOR = new Creator<RegionsBean>() {
            @Override
            public RegionsBean createFromParcel(Parcel source) {
                return new RegionsBean(source);
            }

            @Override
            public RegionsBean[] newArray(int size) {
                return new RegionsBean[size];
            }
        };
    }

    public static class UserDatingHopesBean implements Parcelable {
        /**
         * user_id : 69
         * dating_hope_id : 5
         */

        private int user_id;
        private int dating_hope_id;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getDating_hope_id() {
            return dating_hope_id;
        }

        public void setDating_hope_id(int dating_hope_id) {
            this.dating_hope_id = dating_hope_id;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.dating_hope_id);
        }

        public UserDatingHopesBean() {
        }

        protected UserDatingHopesBean(Parcel in) {
            this.user_id = in.readInt();
            this.dating_hope_id = in.readInt();
        }

        public static final Creator<UserDatingHopesBean> CREATOR = new Creator<UserDatingHopesBean>() {
            @Override
            public UserDatingHopesBean createFromParcel(Parcel source) {
                return new UserDatingHopesBean(source);
            }

            @Override
            public UserDatingHopesBean[] newArray(int size) {
                return new UserDatingHopesBean[size];
            }
        };
    }

    public static class UserDatingTypesBean implements Parcelable {
        /**
         * user_id : 69
         * dating_type_id : 5
         */

        private int user_id;
        private int dating_type_id;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getDating_type_id() {
            return dating_type_id;
        }

        public void setDating_type_id(int dating_type_id) {
            this.dating_type_id = dating_type_id;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.dating_type_id);
        }

        public UserDatingTypesBean() {
        }

        protected UserDatingTypesBean(Parcel in) {
            this.user_id = in.readInt();
            this.dating_type_id = in.readInt();
        }

        public static final Creator<UserDatingTypesBean> CREATOR = new Creator<UserDatingTypesBean>() {
            @Override
            public UserDatingTypesBean createFromParcel(Parcel source) {
                return new UserDatingTypesBean(source);
            }

            @Override
            public UserDatingTypesBean[] newArray(int size) {
                return new UserDatingTypesBean[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.in_certification);
        dest.writeInt(this.id);
        dest.writeString(this.mobile);
        dest.writeInt(this.cash);
        dest.writeString(this.invite_code);
        dest.writeInt(this.gender);
        dest.writeString(this.avatar);
        dest.writeString(this.nickname);
        dest.writeInt(this.career_id);
        dest.writeString(this.wechat);
        dest.writeString(this.qq);
        dest.writeString(this.height);
        dest.writeString(this.weight);
        dest.writeString(this.bwh_str);
        dest.writeString(this.intro);
        dest.writeString(this.vip_ended_at);
        dest.writeInt(this.is_public);
        dest.writeInt(this.is_visible);
        dest.writeInt(this.hide_location);
        dest.writeInt(this.hide_social_account);
        dest.writeInt(this.view_album_price);
        dest.writeString(this.verified_at);
        dest.writeString(this.lng);
        dest.writeString(this.lat);
        dest.writeString(this.latest_online_at);
        dest.writeString(this.from_where);
        dest.writeString(this.alipay_account);
        dest.writeString(this.alipay_realname);
        dest.writeString(this.cert_code);
        dest.writeString(this.login_ip);
        dest.writeString(this.reg_ip);
        dest.writeInt(this.history_visitors);
        dest.writeString(this.banned_at);
        dest.writeString(this.created_at);
        dest.writeString(this.updated_at);
        dest.writeString(this.birth_at);
        dest.writeString(this.geohash);
        dest.writeInt(this.privacy_status);
        dest.writeInt(this.province_id);
        dest.writeInt(this.city_id);
        dest.writeList(this.regions);
        dest.writeList(this.user_dating_hopes);
        dest.writeList(this.user_dating_types);
    }

    public UserBean() {
    }

    protected UserBean(Parcel in) {
        this.in_certification=in.readInt();
        this.id = in.readInt();
        this.mobile = in.readString();
        this.cash = in.readInt();
        this.invite_code = in.readString();
        this.gender = in.readInt();
        this.avatar = in.readString();
        this.nickname = in.readString();
        this.career_id = in.readInt();
        this.wechat = in.readString();
        this.qq = in.readString();
        this.height = in.readString();
        this.weight = in.readString();
        this.bwh_str = in.readString();
        this.intro = in.readString();
        this.vip_ended_at = in.readString();
        this.is_public = in.readInt();
        this.is_visible = in.readInt();
        this.hide_location = in.readInt();
        this.hide_social_account = in.readInt();
        this.view_album_price = in.readInt();
        this.verified_at = in.readString();
        this.lng = in.readString();
        this.lat = in.readString();
        this.latest_online_at = in.readString();
        this.from_where = in.readString();
        this.alipay_account = in.readString();
        this.alipay_realname = in.readString();
        this.cert_code = in.readString();
        this.login_ip = in.readString();
        this.reg_ip = in.readString();
        this.history_visitors = in.readInt();
        this.banned_at = in.readString();
        this.created_at = in.readString();
        this.updated_at = in.readString();
        this.birth_at = in.readString();
        this.geohash = in.readString();
        this.privacy_status = in.readInt();
        this.province_id = in.readInt();
        this.city_id = in.readInt();
        this.regions = new ArrayList<RegionsBean>();
        in.readList(this.regions, RegionsBean.class.getClassLoader());
        this.user_dating_hopes = new ArrayList<UserDatingHopesBean>();
        in.readList(this.user_dating_hopes, UserDatingHopesBean.class.getClassLoader());
        this.user_dating_types = new ArrayList<UserDatingTypesBean>();
        in.readList(this.user_dating_types, UserDatingTypesBean.class.getClassLoader());
    }

    public static final Creator<UserBean> CREATOR = new Creator<UserBean>() {
        @Override
        public UserBean createFromParcel(Parcel source) {
            return new UserBean(source);
        }

        @Override
        public UserBean[] newArray(int size) {
            return new UserBean[size];
        }
    };
}
