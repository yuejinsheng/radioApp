package com.radioapp.liaoliaobao.bean.radio;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能： 发布电台的数据
 * 描述
 * Created by yue on 2019-07-31
 */
public class RadioPublishBean implements Parcelable {
    private int id;  //id
    private String title; //标题
    private int img; //图片


    public RadioPublishBean(int id, String title, int img) {
        this.id = id;
        this.title = title;
        this.img = img;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeInt(this.img);
    }

    public RadioPublishBean() {
    }

    protected RadioPublishBean(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.img = in.readInt();
    }

    public static final Parcelable.Creator<RadioPublishBean> CREATOR = new Parcelable.Creator<RadioPublishBean>() {
        @Override
        public RadioPublishBean createFromParcel(Parcel source) {
            return new RadioPublishBean(source);
        }

        @Override
        public RadioPublishBean[] newArray(int size) {
            return new RadioPublishBean[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
