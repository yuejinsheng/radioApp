package com.radioapp.liaoliaobao.bean;

/**
 * @author yue
 */
public class EMCmdMessageBean {

        public String title;//"title": "\u7533\u8bf7\u67e5\u770b\u4e86\u60a8\u7684\u8be6\u60c5\u9875",
        public int flag;//"flag": 3,
        public String msg;//"msg": "\u597392\uff0c\u8bf7\u6c42\u67e5\u770b\u60a8\u7684\u8be6\u60c5\u9875!",
        public String image;//"image": "images\/2019_09\/01\/V0auCphpToeJlU1567346141190901.JPEG",
        public String friend_id;//"friend_id": 129,
        public String friend_avatar;//"friend_avatar": "images\/2019_08\/30\/5uFTdphp0NdOCr1567096444190830.jpg"
}
