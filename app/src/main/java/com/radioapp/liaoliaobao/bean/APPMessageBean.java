package com.radioapp.liaoliaobao.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-05
 */
public class APPMessageBean implements Parcelable {

    private String url;
    private String content;
    private String date;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeString(this.content);
        dest.writeString(this.date);
    }

    public APPMessageBean() {
    }

    protected APPMessageBean(Parcel in) {
        this.url = in.readString();
        this.content = in.readString();
        this.date = in.readString();
    }

    public static final Parcelable.Creator<APPMessageBean> CREATOR = new Parcelable.Creator<APPMessageBean>() {
        @Override
        public APPMessageBean createFromParcel(Parcel source) {
            return new APPMessageBean(source);
        }

        @Override
        public APPMessageBean[] newArray(int size) {
            return new APPMessageBean[size];
        }
    };
}
