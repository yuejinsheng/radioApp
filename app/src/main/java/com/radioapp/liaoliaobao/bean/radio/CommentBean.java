package com.radioapp.liaoliaobao.bean.radio;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-12
 */
public class CommentBean {

    private int  id;          ;//  "id": 4,
    private int   user_id         ;// "user_id": 129,
    private int    parent_id        ;// "parent_id": 0,
    private String   text         ;// "text": "真好看",
    private String  created_at          ;// "created_at": "2019-09-01 08:40:33",
    private String nickname           ;// "nickname": "女92",
    private String  avatar          ;// "avatar": "images\/2019_08\/30\/5uFTdphp0NdOCr1567096444190830.jpg",
    private int    gender        ;// "gender": 2

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }
}

