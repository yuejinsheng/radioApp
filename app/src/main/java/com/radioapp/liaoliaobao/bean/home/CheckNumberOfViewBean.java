package com.radioapp.liaoliaobao.bean.home;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-12
 */
public class CheckNumberOfViewBean {
    private   int contact_amount;//会员今天查看了几次联系方式
    private int user_info_amount;//非会员今天打开了几个人的详情页
    private int album_amount;//会员今天查看了几次付费相册

    public int getContact_amount() {
        return contact_amount;
    }

    public void setContact_amount(int contact_amount) {
        this.contact_amount = contact_amount;
    }

    public int getUser_info_amount() {
        return user_info_amount;
    }

    public void setUser_info_amount(int user_info_amount) {
        this.user_info_amount = user_info_amount;
    }

    public int getAlbum_amount() {
        return album_amount;
    }

    public void setAlbum_amount(int album_amount) {
        this.album_amount = album_amount;
    }
}
