package com.radioapp.liaoliaobao.bean.lite;

import com.jaydenxiao.common.url.BaseConstant;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;

import java.util.List;

/**
 * 功能：聊天的用户信息
 * 描述
 * Created by yue on 2019-10-11
 */
public class ChatUser extends LitePalSupport {

    private int userId;
    private String  nickName;
    private String avatar;


    public ChatUser() {
    }

    public ChatUser(int userId, String nickName, String avatar) {
        this.userId = userId;
        this.nickName = nickName;
        this.avatar = avatar;

        boolean isFlag = false;
        List<ChatUser> list = LitePal.findAll(ChatUser.class);
        if (list != null && list.size() > 0) {
            for (ChatUser user : list) {
                if (user.getUserId() == userId) {
                    this.update(user.getBaseObjId());
                    isFlag = true;

                }
            }
        }
        if (!isFlag){
            this.save();
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


}
