package com.radioapp.liaoliaobao.bean;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-31
 */
public class PageBean<T> {
    private int current_page; //当前页
    private int total; //
    private List<T> data; //集合数据



    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "current_page=" + current_page +
                ", data=" + data +
                '}';
    }
}
