package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：上传资源图片
 * 描述
 * Created by yue on 2019-09-29
 */
public class UpdateMediaBean implements Parcelable {
    private int id;
    private String resource_url;// "resource_url": "images\/2019_08\/02\/EwKoxphp0JM6Om1564683634190802.png",
    private int type;// "type": 1,
    private String mime_type;// "mime_type": "image\/png",
    private int file_size;// "file_size": 133745,
    private int price;// "price": 0,
    private int is_once;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.resource_url);
        dest.writeInt(this.type);
        dest.writeString(this.mime_type);
        dest.writeInt(this.file_size);
        dest.writeInt(this.price);
        dest.writeInt(this.is_once);
    }

    public UpdateMediaBean() {
    }

    protected UpdateMediaBean(Parcel in) {
        this.id = in.readInt();
        this.resource_url = in.readString();
        this.type = in.readInt();
        this.mime_type = in.readString();
        this.file_size = in.readInt();
        this.price = in.readInt();
        this.is_once = in.readInt();
    }

    public static final Parcelable.Creator<UpdateMediaBean> CREATOR = new Parcelable.Creator<UpdateMediaBean>() {
        @Override
        public UpdateMediaBean createFromParcel(Parcel source) {
            return new UpdateMediaBean(source);
        }

        @Override
        public UpdateMediaBean[] newArray(int size) {
            return new UpdateMediaBean[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResource_url() {
        return resource_url;
    }

    public void setResource_url(String resource_url) {
        this.resource_url = resource_url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public int getFile_size() {
        return file_size;
    }

    public void setFile_size(int file_size) {
        this.file_size = file_size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIs_once() {
        return is_once;
    }

    public void setIs_once(int is_once) {
        this.is_once = is_once;
    }
}
