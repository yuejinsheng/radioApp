package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：黑名单
 * 描述
 * Created by yue on 2019-09-29
 */
public class BlackListBean implements Parcelable {
    private String   friend_id       ;//      "friend_id": 106,
    private String   avatar       ;//      "avatar": "images\/2019_08\/03\/iiGl6phpYrHAvx1564773731190803.png",
    private String   nickname       ;//      "nickname": "34",
    private int   career_id       ;//      "career_id": 9,
    private String   birth_at       ;//      "birth_at": "2019-07-31 00:00:00",
    private String    lng      ;//      "lng": "113.308017",
    private String    lat      ;//      "lat": "23.224348",
    private String    latest_online_at      ;//      "latest_online_at": null,
    private String     updated_at     ;//      "updated_at": "2019-08-17 20:24:25",
    private int     distance     ;//      "distance": 113
    private String verified_at;
    private String vip_ended_at;
    private int province_id;
    private int city_id;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.friend_id);
        dest.writeString(this.avatar);
        dest.writeString(this.nickname);
        dest.writeInt(this.career_id);
        dest.writeString(this.birth_at);
        dest.writeString(this.lng);
        dest.writeString(this.lat);
        dest.writeString(this.latest_online_at);
        dest.writeString(this.updated_at);
        dest.writeInt(this.distance);
        dest.writeString(this.verified_at);
        dest.writeString(this.vip_ended_at);
        dest.writeInt(this.province_id);
        dest.writeInt(this.city_id);
    }

    public BlackListBean() {
    }

    protected BlackListBean(Parcel in) {
        this.friend_id = in.readString();
        this.avatar = in.readString();
        this.nickname = in.readString();
        this.career_id = in.readInt();
        this.birth_at = in.readString();
        this.lng = in.readString();
        this.lat = in.readString();
        this.latest_online_at = in.readString();
        this.updated_at = in.readString();
        this.distance = in.readInt();
        this.verified_at = in.readString();
        this.vip_ended_at = in.readString();
        this.province_id = in.readInt();
        this.city_id = in.readInt();
    }

    public static final Parcelable.Creator<BlackListBean> CREATOR = new Parcelable.Creator<BlackListBean>() {
        @Override
        public BlackListBean createFromParcel(Parcel source) {
            return new BlackListBean(source);
        }

        @Override
        public BlackListBean[] newArray(int size) {
            return new BlackListBean[size];
        }
    };

    public String getFriend_id() {
        return friend_id;
    }

    public void setFriend_id(String friend_id) {
        this.friend_id = friend_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getCareer_id() {
        return career_id;
    }

    public void setCareer_id(int career_id) {
        this.career_id = career_id;
    }

    public String getBirth_at() {
        return birth_at;
    }

    public void setBirth_at(String birth_at) {
        this.birth_at = birth_at;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLatest_online_at() {
        return latest_online_at;
    }

    public void setLatest_online_at(String latest_online_at) {
        this.latest_online_at = latest_online_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getVerified_at() {
        return verified_at;
    }

    public void setVerified_at(String verified_at) {
        this.verified_at = verified_at;
    }

    public String getVip_ended_at() {
        return vip_ended_at;
    }

    public void setVip_ended_at(String vip_ended_at) {
        this.vip_ended_at = vip_ended_at;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }
}
