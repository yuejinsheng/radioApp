package com.radioapp.liaoliaobao.bean.home;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-12
 */
public class FriendContactBean implements Parcelable {
    private String qq;
    private String wechat;
    private int hide_social_account;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.qq);
        dest.writeString(this.wechat);
        dest.writeInt(this.hide_social_account);
    }

    public FriendContactBean() {
    }

    protected FriendContactBean(Parcel in) {
        this.qq = in.readString();
        this.wechat = in.readString();
        this.hide_social_account = in.readInt();
    }

    public static final Parcelable.Creator<FriendContactBean> CREATOR = new Parcelable.Creator<FriendContactBean>() {
        @Override
        public FriendContactBean createFromParcel(Parcel source) {
            return new FriendContactBean(source);
        }

        @Override
        public FriendContactBean[] newArray(int size) {
            return new FriendContactBean[size];
        }
    };

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public int getHide_social_account() {
        return hide_social_account;
    }

    public void setHide_social_account(int hide_social_account) {
        this.hide_social_account = hide_social_account;
    }
}
