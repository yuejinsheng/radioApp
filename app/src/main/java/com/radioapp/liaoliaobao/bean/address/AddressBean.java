package com.radioapp.liaoliaobao.bean.address;

import com.contrarywind.interfaces.IPickerViewData;

import java.util.List;

/**
 * 省市区
 */
public class AddressBean extends AddressJsonBean implements IPickerViewData {
    private List<CitysBean> citys;

    @Override
    public String getPickerViewText() {
        return this.areaName;
    }

    public List<CitysBean> getCitys() {
        return citys;
    }

    public void setCitys(List<CitysBean> citys) {
        this.citys = citys;
    }

     //市
    public static class CitysBean extends AddressJsonBean implements IPickerViewData {

        private List<AreasBean> areas;

        @Override
        public String getPickerViewText() {
            return areaName;
        }

        public List<AreasBean> getAreas() {
            return areas;
        }

        public void setAreas(List<AreasBean> areas) {
            this.areas = areas;
        }


        //区
        public static class AreasBean extends AddressJsonBean implements IPickerViewData {

            @Override
            public String getPickerViewText() {
                return areaName;
            }
        }
    }


}
