package com.radioapp.liaoliaobao.bean.enumbean;

/**
 * 功能：消息标题
 * 描述
 * Created by yue on 2019-07-17
 */
public enum MessageTitleEnum {

    MESSAGE("1","系统消息"),
    DYNAMIC("2","动态消息"),
    RADIO("3","电台消息"),
    APPLY("4","查看申请"),
    INCOME("5","收入提醒");

    public final String code;
    public final String name;

    MessageTitleEnum(String code, String name)
    {
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public String getName()
    {
        return name;
    }


    /**
     * 根据value值获取key
     * @param msg
     * @return
     */
    public static String getCodeByMsg(String msg){
        for (MessageTitleEnum v : MessageTitleEnum.values()) {
            if (v.getName().equals(msg)) {
                return v.getCode();
            }
        }
        return "0";
    }

    /**
     * 根据key值获取value
     * @param code
     * @return
     */
    public static String getMsgByCode(String code){
        for (MessageTitleEnum v : MessageTitleEnum.values()) {
            if (v.getCode().equals(code)) {
                return v.getName();
            }
        }
        return "未知错误";
    }
}
