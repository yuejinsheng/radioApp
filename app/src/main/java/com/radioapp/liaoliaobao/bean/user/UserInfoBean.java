package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-20
 */
public class UserInfoBean implements Parcelable {


    private int province_id;
    private int city_id;
    private int id;//  "id": 102,
    private int cash;//  "cash": 0,
    private String invite_code;//  "invite_code": null,
    private int gender;//  "gender": 2,
    private String avatar;//  "avatar": "",
    private String nickname;//  "nickname": "30女",
    private int career_id;//  "career_id": 9,
    private String height;//  "height": "",
    private String weight;//  "weight": "",
    private String bwh_str;//  "bwh_str": "",
    private String intro;//  "intro": "",
    private String vip_ended_at;//  "vip_ended_at": null,
    private int is_private;//  "is_private": 0,
    private int is_visible;//  "is_visible": 1,
    private int hide_location;//  "hide_location": 0,
    private int hide_social_account;//  "hide_social_account": 0,
    private int view_album_price;//  "view_album_price": 0,
    private String verified_at;//  "verified_at": null,
    private String lng;//  "lng": "113.308815",
    private String lat;//  "lat": "23.223586",
    private String latest_online_at;//  "latest_online_at": null,
    private String from_where;//  "from_where": "女",
    private String alipay_account;//  "alipay_account": "",
    private String alipay_realname;//  "alipay_realname": "",
    private String cert_code;//  "cert_code": "Ps1Wqc4I",
    private String login_ip;//  "login_ip": 1885389716,
    private String reg_ip;//  "reg_ip": 1885389716,
    private int history_visitors;//  "history_visitors": 0,
    private String banned_at;//  "banned_at": "2019-07-30 15:46:44",
    private String created_at;//  "created_at": "2019-07-30 05:50:13",
    private String updated_at;//  "updated_at": "2019-07-30 22:39:10",
    private String birth_at;//  "birth_at": "2019-0   7-30 00:00:00",
    private String geohash;//  "geohash": "ws0s47szn",
    private int privacy_status;//  "privacy_status": 0,
    private int me_like;
    private int is_view_contact;
    private int is_view_album;
    private int hasBroadcast;
    private List<RegionsBean> regions;  //约会范围
    private List<UserDatingHopesBean> user_dating_hopes;  //约会期望
    private List<UserDatingTypesBean> user_dating_types; //约会节目
    private List<UserMedias> user_medias;

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public String getInvite_code() {
        return invite_code;
    }

    public void setInvite_code(String invite_code) {
        this.invite_code = invite_code;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getCareer_id() {
        return career_id;
    }

    public void setCareer_id(int career_id) {
        this.career_id = career_id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBwh_str() {
        return bwh_str;
    }

    public void setBwh_str(String bwh_str) {
        this.bwh_str = bwh_str;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getVip_ended_at() {
        return vip_ended_at;
    }

    public void setVip_ended_at(String vip_ended_at) {
        this.vip_ended_at = vip_ended_at;
    }

    public int getIs_private() {
        return is_private;
    }

    public void setIs_private(int is_private) {
        this.is_private = is_private;
    }

    public int getIs_visible() {
        return is_visible;
    }

    public void setIs_visible(int is_visible) {
        this.is_visible = is_visible;
    }

    public int getHide_location() {
        return hide_location;
    }

    public void setHide_location(int hide_location) {
        this.hide_location = hide_location;
    }

    public int getHide_social_account() {
        return hide_social_account;
    }

    public void setHide_social_account(int hide_social_account) {
        this.hide_social_account = hide_social_account;
    }

    public int getView_album_price() {
        return view_album_price;
    }

    public void setView_album_price(int view_album_price) {
        this.view_album_price = view_album_price;
    }

    public String getVerified_at() {
        return verified_at;
    }

    public void setVerified_at(String verified_at) {
        this.verified_at = verified_at;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLatest_online_at() {
        return latest_online_at;
    }

    public void setLatest_online_at(String latest_online_at) {
        this.latest_online_at = latest_online_at;
    }

    public String getFrom_where() {
        return from_where;
    }

    public void setFrom_where(String from_where) {
        this.from_where = from_where;
    }

    public String getAlipay_account() {
        return alipay_account;
    }

    public void setAlipay_account(String alipay_account) {
        this.alipay_account = alipay_account;
    }

    public String getAlipay_realname() {
        return alipay_realname;
    }

    public void setAlipay_realname(String alipay_realname) {
        this.alipay_realname = alipay_realname;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getLogin_ip() {
        return login_ip;
    }

    public void setLogin_ip(String login_ip) {
        this.login_ip = login_ip;
    }

    public String getReg_ip() {
        return reg_ip;
    }

    public void setReg_ip(String reg_ip) {
        this.reg_ip = reg_ip;
    }

    public int getHistory_visitors() {
        return history_visitors;
    }

    public void setHistory_visitors(int history_visitors) {
        this.history_visitors = history_visitors;
    }

    public String getBanned_at() {
        return banned_at;
    }

    public void setBanned_at(String banned_at) {
        this.banned_at = banned_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBirth_at() {
        return birth_at;
    }

    public void setBirth_at(String birth_at) {
        this.birth_at = birth_at;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    public int getPrivacy_status() {
        return privacy_status;
    }

    public void setPrivacy_status(int privacy_status) {
        this.privacy_status = privacy_status;
    }

    public int getMe_like() {
        return me_like;
    }

    public void setMe_like(int me_like) {
        this.me_like = me_like;
    }

    public int getIs_view_contact() {
        return is_view_contact;
    }

    public void setIs_view_contact(int is_view_contact) {
        this.is_view_contact = is_view_contact;
    }

    public int getIs_view_album() {
        return is_view_album;
    }

    public void setIs_view_album(int is_view_album) {
        this.is_view_album = is_view_album;
    }

    public List<RegionsBean> getRegions() {
        return regions;
    }

    public void setRegions(List<RegionsBean> regions) {
        this.regions = regions;
    }

    public List<UserDatingHopesBean> getUser_dating_hopes() {
        return user_dating_hopes;
    }

    public void setUser_dating_hopes(List<UserDatingHopesBean> user_dating_hopes) {
        this.user_dating_hopes = user_dating_hopes;
    }

    public List<UserDatingTypesBean> getUser_dating_types() {
        return user_dating_types;
    }

    public void setUser_dating_types(List<UserDatingTypesBean> user_dating_types) {
        this.user_dating_types = user_dating_types;
    }

    public int getHasBroadcast() {
        return hasBroadcast;
    }

    public void setHasBroadcast(int hasBroadcast) {
        this.hasBroadcast = hasBroadcast;
    }

    public List<UserMedias> getUser_medias() {
        return user_medias;
    }

    public void setUser_medias(List<UserMedias> user_medias) {
        this.user_medias = user_medias;
    }

    public static class UserMedias implements Serializable, Parcelable {
        private int user_id;//     "user_id": 106,
        private int type;//     "type": 1,
        private String resource_url;//     "resource_url": "images\/2019_08\/07\/jwKP2phpaujaDj1565169627190807.png",
        private String mime_type;//     "mime_type": "image\/png",
        private int file_size;//     "file_size": 161088,
        private int price;//     "price": 0,
        private int is_once;//     "is_once": 1
        private String user_media_id;
        private int media_id;//   "media_id": 81,


        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getResource_url() {
            return resource_url;
        }

        public void setResource_url(String resource_url) {
            this.resource_url = resource_url;
        }

        public String getMime_type() {
            return mime_type;
        }

        public void setMime_type(String mime_type) {
            this.mime_type = mime_type;
        }

        public int getFile_size() {
            return file_size;
        }

        public void setFile_size(int file_size) {
            this.file_size = file_size;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getIs_once() {
            return is_once;
        }

        public void setIs_once(int is_once) {
            this.is_once = is_once;
        }

        public String getUser_media_id() {
            return user_media_id;
        }

        public void setUser_media_id(String user_media_id) {
            this.user_media_id = user_media_id;
        }

        public int getMedia_id() {
            return media_id;
        }

        public void setMedia_id(int media_id) {
            this.media_id = media_id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.type);
            dest.writeString(this.resource_url);
            dest.writeString(this.mime_type);
            dest.writeInt(this.file_size);
            dest.writeInt(this.price);
            dest.writeInt(this.is_once);
            dest.writeString(this.user_media_id);
            dest.writeInt(this.media_id);
        }

        public UserMedias() {
        }

        protected UserMedias(Parcel in) {
            this.user_id = in.readInt();
            this.type = in.readInt();
            this.resource_url = in.readString();
            this.mime_type = in.readString();
            this.file_size = in.readInt();
            this.price = in.readInt();
            this.is_once = in.readInt();
            this.user_media_id = in.readString();
            this.media_id = in.readInt();
        }

        public static final Parcelable.Creator<UserMedias> CREATOR = new Parcelable.Creator<UserMedias>() {
            @Override
            public UserMedias createFromParcel(Parcel source) {
                return new UserMedias(source);
            }

            @Override
            public UserMedias[] newArray(int size) {
                return new UserMedias[size];
            }
        };
    }

    public static class RegionsBean implements Parcelable {
        /**
         * user_id : 69
         * province_id : 140000
         * city_id : 140100
         */

        private int user_id;
        private int province_id;
        private int city_id;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getProvince_id() {
            return province_id;
        }

        public void setProvince_id(int province_id) {
            this.province_id = province_id;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.province_id);
            dest.writeInt(this.city_id);
        }

        public RegionsBean() {
        }

        protected RegionsBean(Parcel in) {
            this.user_id = in.readInt();
            this.province_id = in.readInt();
            this.city_id = in.readInt();
        }

        public static final Creator<RegionsBean> CREATOR = new Creator<RegionsBean>() {
            @Override
            public RegionsBean createFromParcel(Parcel source) {
                return new RegionsBean(source);
            }

            @Override
            public RegionsBean[] newArray(int size) {
                return new RegionsBean[size];
            }
        };
    }

    public static class UserDatingHopesBean implements Parcelable {
        /**
         * user_id : 69
         * dating_hope_id : 5
         */

        private int user_id;
        private int dating_hope_id;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getDating_hope_id() {
            return dating_hope_id;
        }

        public void setDating_hope_id(int dating_hope_id) {
            this.dating_hope_id = dating_hope_id;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.dating_hope_id);
        }

        public UserDatingHopesBean() {
        }

        protected UserDatingHopesBean(Parcel in) {
            this.user_id = in.readInt();
            this.dating_hope_id = in.readInt();
        }

        public static final Creator<UserDatingHopesBean> CREATOR = new Creator<UserDatingHopesBean>() {
            @Override
            public UserDatingHopesBean createFromParcel(Parcel source) {
                return new UserDatingHopesBean(source);
            }

            @Override
            public UserDatingHopesBean[] newArray(int size) {
                return new UserDatingHopesBean[size];
            }
        };
    }

    public static class UserDatingTypesBean implements Parcelable {
        /**
         * user_id : 69
         * dating_type_id : 5
         */

        private int user_id;
        private int dating_type_id;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getDating_type_id() {
            return dating_type_id;
        }

        public void setDating_type_id(int dating_type_id) {
            this.dating_type_id = dating_type_id;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.dating_type_id);
        }

        public UserDatingTypesBean() {
        }

        protected UserDatingTypesBean(Parcel in) {
            this.user_id = in.readInt();
            this.dating_type_id = in.readInt();
        }

        public static final Creator<UserDatingTypesBean> CREATOR = new Creator<UserDatingTypesBean>() {
            @Override
            public UserDatingTypesBean createFromParcel(Parcel source) {
                return new UserDatingTypesBean(source);
            }

            @Override
            public UserDatingTypesBean[] newArray(int size) {
                return new UserDatingTypesBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.province_id);
        dest.writeInt(this.city_id);
        dest.writeInt(this.id);
        dest.writeInt(this.cash);
        dest.writeString(this.invite_code);
        dest.writeInt(this.gender);
        dest.writeString(this.avatar);
        dest.writeString(this.nickname);
        dest.writeInt(this.career_id);
        dest.writeString(this.height);
        dest.writeString(this.weight);
        dest.writeString(this.bwh_str);
        dest.writeString(this.intro);
        dest.writeString(this.vip_ended_at);
        dest.writeInt(this.is_private);
        dest.writeInt(this.is_visible);
        dest.writeInt(this.hide_location);
        dest.writeInt(this.hide_social_account);
        dest.writeInt(this.view_album_price);
        dest.writeString(this.verified_at);
        dest.writeString(this.lng);
        dest.writeString(this.lat);
        dest.writeString(this.latest_online_at);
        dest.writeString(this.from_where);
        dest.writeString(this.alipay_account);
        dest.writeString(this.alipay_realname);
        dest.writeString(this.cert_code);
        dest.writeString(this.login_ip);
        dest.writeString(this.reg_ip);
        dest.writeInt(this.history_visitors);
        dest.writeString(this.banned_at);
        dest.writeString(this.created_at);
        dest.writeString(this.updated_at);
        dest.writeString(this.birth_at);
        dest.writeString(this.geohash);
        dest.writeInt(this.privacy_status);
        dest.writeInt(this.me_like);
        dest.writeInt(this.is_view_contact);
        dest.writeInt(this.is_view_album);
        dest.writeInt(this.hasBroadcast);
        dest.writeTypedList(this.regions);
        dest.writeTypedList(this.user_dating_hopes);
        dest.writeTypedList(this.user_dating_types);
        dest.writeTypedList(this.user_medias);
    }

    public UserInfoBean() {
    }

    protected UserInfoBean(Parcel in) {
        this.province_id = in.readInt();
        this.city_id = in.readInt();
        this.id = in.readInt();
        this.cash = in.readInt();
        this.invite_code = in.readString();
        this.gender = in.readInt();
        this.avatar = in.readString();
        this.nickname = in.readString();
        this.career_id = in.readInt();
        this.height = in.readString();
        this.weight = in.readString();
        this.bwh_str = in.readString();
        this.intro = in.readString();
        this.vip_ended_at = in.readString();
        this.is_private = in.readInt();
        this.is_visible = in.readInt();
        this.hide_location = in.readInt();
        this.hide_social_account = in.readInt();
        this.view_album_price = in.readInt();
        this.verified_at = in.readString();
        this.lng = in.readString();
        this.lat = in.readString();
        this.latest_online_at = in.readString();
        this.from_where = in.readString();
        this.alipay_account = in.readString();
        this.alipay_realname = in.readString();
        this.cert_code = in.readString();
        this.login_ip = in.readString();
        this.reg_ip = in.readString();
        this.history_visitors = in.readInt();
        this.banned_at = in.readString();
        this.created_at = in.readString();
        this.updated_at = in.readString();
        this.birth_at = in.readString();
        this.geohash = in.readString();
        this.privacy_status = in.readInt();
        this.me_like = in.readInt();
        this.is_view_contact = in.readInt();
        this.is_view_album = in.readInt();
        this.hasBroadcast=in.readInt();
        this.regions = in.createTypedArrayList(RegionsBean.CREATOR);
        this.user_dating_hopes = in.createTypedArrayList(UserDatingHopesBean.CREATOR);
        this.user_dating_types = in.createTypedArrayList(UserDatingTypesBean.CREATOR);
        this.user_medias = in.createTypedArrayList(UserMedias.CREATOR);
    }

    public static final Parcelable.Creator<UserInfoBean> CREATOR = new Parcelable.Creator<UserInfoBean>() {
        @Override
        public UserInfoBean createFromParcel(Parcel source) {
            return new UserInfoBean(source);
        }

        @Override
        public UserInfoBean[] newArray(int size) {
            return new UserInfoBean[size];
        }
    };
}
