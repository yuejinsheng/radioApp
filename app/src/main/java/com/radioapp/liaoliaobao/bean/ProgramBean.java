package com.radioapp.liaoliaobao.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：约会节目
 * 描述
 * Created by yue on 2019-09-06
 */
public class ProgramBean implements Parcelable {


    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
    }

    public ProgramBean() {
    }

    protected ProgramBean(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
    }

    public static final Creator<ProgramBean> CREATOR = new Creator<ProgramBean>() {
        @Override
        public ProgramBean createFromParcel(Parcel source) {
            return new ProgramBean(source);
        }

        @Override
        public ProgramBean[] newArray(int size) {
            return new ProgramBean[size];
        }
    };
}
