package com.radioapp.liaoliaobao.bean.message;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-09
 */
public class MessageListBean {


    /**
     * n_id : e3a7a1e2-6a28-4dca-a6dd-f8bf72f24a6e
     * data : {"title":"收益提醒","content":"72，查看了您的红包照片，您的积分增加了3","meta":{"order_out_trade_no":"r15678442049537889","total_fee":"3"},"from_user_id":111,"from_user_avatar":"","flag":7}
     * read_at : null
     * created_at : 2019-09-07 16:16:52
     */

    private String n_id;
    private DataBean data;
    private Object read_at;
    private String created_at;


    public String getN_id() {
        return n_id;
    }

    public void setN_id(String n_id) {
        this.n_id = n_id;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public Object getRead_at() {
        return read_at;
    }

    public void setRead_at(Object read_at) {
        this.read_at = read_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }



    public static class DataBean {
        /**
         * title : 收益提醒
         * content : 72，查看了您的红包照片，您的积分增加了3
         * meta : {"order_out_trade_no":"r15678442049537889","total_fee":"3"}
         * from_user_id : 111
         * from_user_avatar :
         * flag : 7
         */

        private String title;
        private String content;
        private String body;
        private MetaBean meta;
        private int from_user_id;
        private String from_user_avatar;
        private int flag;
        private int is_agree;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public MetaBean getMeta() {
            return meta;
        }

        public void setMeta(MetaBean meta) {
            this.meta = meta;
        }

        public int getFrom_user_id() {
            return from_user_id;
        }

        public void setFrom_user_id(int from_user_id) {
            this.from_user_id = from_user_id;
        }

        public String getFrom_user_avatar() {
            return from_user_avatar;
        }

        public void setFrom_user_avatar(String from_user_avatar) {
            this.from_user_avatar = from_user_avatar;
        }

        public int getFlag() {
            return flag;
        }

        public void setFlag(int flag) {
            this.flag = flag;
        }
        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public int getIs_agree() {
            return is_agree;
        }

        public void setIs_agree(int is_agree) {
            this.is_agree = is_agree;
        }

        public static class MetaBean {
            /**
             * order_out_trade_no : r15678442049537889
             * total_fee : 3
             */

            private String order_out_trade_no;
            private String total_fee;
            public String image;

            public String getOrder_out_trade_no() {
                return order_out_trade_no;
            }

            public void setOrder_out_trade_no(String order_out_trade_no) {
                this.order_out_trade_no = order_out_trade_no;
            }

            public String getTotal_fee() {
                return total_fee;
            }

            public void setTotal_fee(String total_fee) {
                this.total_fee = total_fee;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }
}

