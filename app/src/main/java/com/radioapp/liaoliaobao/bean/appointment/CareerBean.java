package com.radioapp.liaoliaobao.bean.appointment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * 功能：职业列表
 * 描述
 * Created by yue on 2019-07-17
 */
public class CareerBean implements Parcelable {

    /**
     * id : 1
     * parent_id : 0
     * name : 信息技术
     * sort : 0
     */
    public int id;
    @SerializedName("parent_id")
    public int parentId;
    public String name;
    public int sort;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.parentId);
        dest.writeString(this.name);
        dest.writeInt(this.sort);
    }

    public CareerBean() {
    }

    protected CareerBean(Parcel in) {
        this.id = in.readInt();
        this.parentId = in.readInt();
        this.name = in.readString();
        this.sort = in.readInt();
    }

    public static final Parcelable.Creator<CareerBean> CREATOR = new Parcelable.Creator<CareerBean>() {
        @Override
        public CareerBean createFromParcel(Parcel source) {
            return new CareerBean(source);
        }

        @Override
        public CareerBean[] newArray(int size) {
            return new CareerBean[size];
        }
    };
}
