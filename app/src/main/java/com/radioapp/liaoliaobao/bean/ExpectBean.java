package com.radioapp.liaoliaobao.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.radioapp.liaoliaobao.view.dialog.more_select.MoreSelectInterface;

/**
 * 功能：约会期望
 * 描述
 * Created by yue on 2019-09-06
 */
public class ExpectBean implements Parcelable , MoreSelectInterface {
    /**
     * id : 1
     * name : 看脸
     */

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
    }

    public ExpectBean() {
    }

    protected ExpectBean(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<ExpectBean> CREATOR = new Parcelable.Creator<ExpectBean>() {
        @Override
        public ExpectBean createFromParcel(Parcel source) {
            return new ExpectBean(source);
        }

        @Override
        public ExpectBean[] newArray(int size) {
            return new ExpectBean[size];
        }
    };

    @Override
    public String getText() {
        return name;
    }
}
