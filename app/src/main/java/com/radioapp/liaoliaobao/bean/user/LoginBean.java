package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：登录
 * 描述
 * Created by yue on 2019-07-16
 */
public class LoginBean implements Parcelable {
    private  int easemob_id;
    private String token_type;               //Bearer
    private int expires_in;                  //31622400,
    private String access_token;            //eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImZkNTYzMjE3NmE4NWEwMWNjYTYyZmQwYzQ3NTFjZGNhNWQyYzU2MGJiMjY1ZDA3MzUyNTJjOGJkZTc4MDQwZjg3YWM3M2IxMzU1YmVlYTkzIn0.eyJhdWQiOiIyIiwianRpIjoiZmQ1NjMyMTc2YTg1YTAxY2NhNjJmZDBjNDc1MWNkY2E1ZDJjNTYwYmIyNjVkMDczNTI1MmM4YmRlNzgwNDBmODdhYzczYjEzNTViZWVhOTMiLCJpYXQiOjE1NjMxNzQzNTcsIm5iZiI6MTU2MzE3NDM1NywiZXhwIjoxNTk0Nzk2NzU3LCJzdWIiOiI1MCIsInNjb3BlcyI6WyIqIl19.sgUqtlE5B6WblaW7LAx1NDq-ViBgMurJ7Bh-iNlsRnBRS7eb0YMAKXY2UbcPYSs_IvmjrRSwEH_s-QklSCf29d3cNhI6vp6PDeyXNi4z9drFDCUhbrgjXaYyZsGpnxkeoiq4xIDl3zeGLMNbNDI_xROjtMocy5OJ8e-e9VO9cKOoAWK8yHI3EdpZq5n7DzEoejlj_S9i_o7SfcmfSU9mPlaUxw_809G4dUnBo0qNVr-hfEwTN2mDUZgXdA5mqo2o_oNxI4ZdpMYu84Vq0KHEthjVGRe2oPKqafkG-gS5MpTUaRx5tGKtCzczzYsK-Fwii9TuWVpv9zGZsNZZHxrlR5gbQFKBM-wNZit9KCPIM3Yu7Y3uUnYDg7aIZ_f2tFOZJZsJWtHIFnDB3z0lCDtt6gP-P_min-LEifrTFnZTh0VAhRXWvOo40BlHiw3iqq4-q93Ela3lquvgyAskjE6QeeYFfPSEXr6jZXqAAN-22rVGE70NjHBHBgEi8zpM-U1IlW8P7AR-euMgGJDM7iBsofocxg5j5C7m0Aa5dmF_4kEtIU2FDMxnZdwSoHshxwJGp7bXZKA_Ju-8_SJpaXiYtJYlezRHcMp3LQhAGVxNrtuINWfXpw-sbNT6QzrUAVvT1YcgJhICS_V5E0D6NBLJU270qzFOkb5oCM1-imFipxI",
    private String refresh_token;           //def5020071861dc3920af40e8e1f828c5642b95ae85944e9dde28893b674e6ef22f5148c8a81f7c2d1ca343b672a0f5cc2d816a09db93cf201359d62bf1dd8d1f8dc7f2b001772cb2f731c73e0e373088b8ffcda5e7e3cd79403eb057d0e185e87944904820dca26261c35918c1518a701ee7965e0472c7417fd26cb18aefd93651f4a434018d0398fd3f02ec7c7b056d385f8b3ec3148280ada4dd67b11e4b4877505fafcc5c32eec15e866acc46757e63f1210c3fa33eeb6fb4da15faf90cf4e811a43fb35afb7ad74958e13079e80d7dc5d507a121e35d944cd25978db539463ac3586660670e15a1c67bfd1a75cd8cf22b6111a792c68fec673ef70474ee8ed1c6e26cd3e33eed808dd629b35e8ce90a0723fb57e0e0ec2fb9f81c53554bc709d11813b70dc22f94366e23999051046ff5bfae91b9d755035c3a774fca91a6a83bac92340aa6a8d844049b6bb954931bc053938410738f9d9a24336befad97c340de19",
    private String easemob_password;        //af7a70c219cbe15b03aea59313d5fc4a"
    private int gender;          //        "gender": 1,
    private String invite_code;       //"invite_code": null,
    private String  vip_ended_at;      //"vip_ended_at": "1970-01-16 08:00:00"


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.easemob_id);
        dest.writeString(this.token_type);
        dest.writeInt(this.expires_in);
        dest.writeString(this.access_token);
        dest.writeString(this.refresh_token);
        dest.writeString(this.easemob_password);
        dest.writeInt(this.gender);
        dest.writeString(this.invite_code);
        dest.writeString(this.vip_ended_at);
    }

    public LoginBean() {
    }

    protected LoginBean(Parcel in) {
        this.easemob_id = in.readInt();
        this.token_type = in.readString();
        this.expires_in = in.readInt();
        this.access_token = in.readString();
        this.refresh_token = in.readString();
        this.easemob_password = in.readString();
        this.gender = in.readInt();
        this.invite_code = in.readString();
        this.vip_ended_at = in.readString();
    }

    public static final Creator<LoginBean> CREATOR = new Creator<LoginBean>() {
        @Override
        public LoginBean createFromParcel(Parcel source) {
            return new LoginBean(source);
        }

        @Override
        public LoginBean[] newArray(int size) {
            return new LoginBean[size];
        }
    };

    public int getEasemob_id() {
        return easemob_id;
    }

    public void setEasemob_id(int easemob_id) {
        this.easemob_id = easemob_id;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getEasemob_password() {
        return easemob_password;
    }

    public void setEasemob_password(String easemob_password) {
        this.easemob_password = easemob_password;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getInvite_code() {
        return invite_code;
    }

    public void setInvite_code(String invite_code) {
        this.invite_code = invite_code;
    }

    public String getVip_ended_at() {
        return vip_ended_at;
    }

    public void setVip_ended_at(String vip_ended_at) {
        this.vip_ended_at = vip_ended_at;
    }


}
