package com.radioapp.liaoliaobao.bean;

import com.contrarywind.interfaces.IPickerViewData;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-29
 */
public class MediaTypeBean implements IPickerViewData {
    @Override
    public String getPickerViewText() {
        return name;
    }

    public int id;
    public String name;

    public MediaTypeBean(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
