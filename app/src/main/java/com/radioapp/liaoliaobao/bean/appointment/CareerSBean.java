package com.radioapp.liaoliaobao.bean.appointment;

import com.contrarywind.interfaces.IPickerViewData;

import java.util.List;

/**
 * 功能： 职业列表
 * 描述
 * Created by yue on 2019-07-18
 */
public class CareerSBean extends CareerBean implements IPickerViewData {
    private List<CarBean> list;

    public List<CarBean> getList() {
        return list;
    }

    public void setList(List<CarBean> list) {
        this.list = list;
    }

    @Override
    public String getPickerViewText() {
        return name;
    }

    public static class CarBean extends CareerBean implements IPickerViewData {

        @Override
        public String getPickerViewText() {
            return name;
        }
    }
}
