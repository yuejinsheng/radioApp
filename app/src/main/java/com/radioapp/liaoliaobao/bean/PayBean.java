package com.radioapp.liaoliaobao.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * 功能： 支付参数
 * 描述
 * Created by yue on 2019-08-26
 */
public class PayBean implements Parcelable {
    private String appid; //应用APPID
    private String partnerid; //商户号
    private String prepayid; //预支付交易会话标识
    private String timestamp; //时间截
    private String noncestr; //随机字符串
    @SerializedName("package")
    private String payPackage; //package
    private String sign; //签名
    private String sn;//订单号


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.appid);
        dest.writeString(this.partnerid);
        dest.writeString(this.prepayid);
        dest.writeString(this.timestamp);
        dest.writeString(this.noncestr);
        dest.writeString(this.payPackage);
        dest.writeString(this.sign);
        dest.writeString(this.sn);
    }

    public PayBean() {
    }

    protected PayBean(Parcel in) {
        this.appid = in.readString();
        this.partnerid = in.readString();
        this.prepayid = in.readString();
        this.timestamp = in.readString();
        this.noncestr = in.readString();
        this.payPackage = in.readString();
        this.sign = in.readString();
        this.sn = in.readString();
    }

    public static final Parcelable.Creator<PayBean> CREATOR = new Parcelable.Creator<PayBean>() {
        @Override
        public PayBean createFromParcel(Parcel source) {
            return new PayBean(source);
        }

        @Override
        public PayBean[] newArray(int size) {
            return new PayBean[size];
        }
    };

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public String getPayPackage() {
        return payPackage;
    }

    public void setPayPackage(String payPackage) {
        this.payPackage = payPackage;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }
}
