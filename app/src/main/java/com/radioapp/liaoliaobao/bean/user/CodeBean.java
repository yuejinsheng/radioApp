package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-23
 */
public class CodeBean implements Parcelable {

    private String value;//邀请码
    private String expired_at; //失效时间
    private String created_at; //申请时间


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
        dest.writeString(this.expired_at);
        dest.writeString(this.created_at);
    }

    public CodeBean() {
    }

    protected CodeBean(Parcel in) {
        this.value = in.readString();
        this.expired_at = in.readString();
        this.created_at = in.readString();
    }

    public static final Parcelable.Creator<CodeBean> CREATOR = new Parcelable.Creator<CodeBean>() {
        @Override
        public CodeBean createFromParcel(Parcel source) {
            return new CodeBean(source);
        }

        @Override
        public CodeBean[] newArray(int size) {
            return new CodeBean[size];
        }
    };

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getExpired_at() {
        return expired_at;
    }

    public void setExpired_at(String expired_at) {
        this.expired_at = expired_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
