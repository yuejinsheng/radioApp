package com.radioapp.liaoliaobao.bean.enumbean;

/**
 * 功能： 发送短信的url
 * 描述
 * Created by yue on 2019-07-17
 */
public enum SendSmsUrlEnum {


    /**
     * 获取手机验证码(注册或修改密码时使用)
     */
    REGISTERSMSURL("register/sendMobileSms"),
    /**
     * 修改密码发送短信 url
     */
    UPDATEPWDSMSURL("users/sendSmsUpdatePassword?operation=update");

    private String code;

    SendSmsUrlEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
