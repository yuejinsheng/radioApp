package com.radioapp.liaoliaobao.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：keyand value
 * 描述
 * Created by yue on 2019-09-29
 */
public class KeyValueBean implements Parcelable {
    public String value;
    public String key;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
        dest.writeString(this.key);
    }

    public KeyValueBean() {
    }

    protected KeyValueBean(Parcel in) {
        this.value = in.readString();
        this.key = in.readString();
    }

    public static final Parcelable.Creator<KeyValueBean> CREATOR = new Parcelable.Creator<KeyValueBean>() {
        @Override
        public KeyValueBean createFromParcel(Parcel source) {
            return new KeyValueBean(source);
        }

        @Override
        public KeyValueBean[] newArray(int size) {
            return new KeyValueBean[size];
        }
    };

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
