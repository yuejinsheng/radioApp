package com.radioapp.liaoliaobao.bean.appointment;

import android.os.Parcel;
import android.os.Parcelable;

import com.radioapp.liaoliaobao.view.dialog.more_select.MoreSelectInterface;

/**
 * 功能：约会期望列表
 * 描述
 * Created by yue on 2019-07-17
 */
public class DatingHopesBean implements Parcelable, MoreSelectInterface {
    private int id;
    private String name;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
    }

    public DatingHopesBean() {
    }

    protected DatingHopesBean(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<DatingHopesBean> CREATOR = new Parcelable.Creator<DatingHopesBean>() {
        @Override
        public DatingHopesBean createFromParcel(Parcel source) {
            return new DatingHopesBean(source);
        }

        @Override
        public DatingHopesBean[] newArray(int size) {
            return new DatingHopesBean[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getText() {
        return name;
    }
}
