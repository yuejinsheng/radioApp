package com.radioapp.liaoliaobao.bean.user;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.previewlibrary.enitity.IThumbViewInfo;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-11-04
 */
public class RadioRegisterListBean implements Parcelable, IThumbViewInfo {
    private int  id;           //"id": 47,
    private int  user_id;      //"user_id": 3,
    private int  broadcast_id;    //"broadcast_id": 39,
    private int  is_chosen;    //"is_chosen": 0,
    private String  created_at;    //"created_at": "2019-08-22 10:06:49",
    private String  img;          //"img": "",
    private String  viewed_at;    //"viewed_at": null,
    private String  nickname;    //"nickname": "我是小明",
    private String  avatar;       //"avatar": "",
    private int  gender;       //"gender": 2
    private Rect mBounds; // 记录坐标
    private String videoUrl;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getBroadcast_id() {
        return broadcast_id;
    }

    public void setBroadcast_id(int broadcast_id) {
        this.broadcast_id = broadcast_id;
    }

    public int getIs_chosen() {
        return is_chosen;
    }

    public void setIs_chosen(int is_chosen) {
        this.is_chosen = is_chosen;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getViewed_at() {
        return viewed_at;
    }

    public void setViewed_at(String viewed_at) {
        this.viewed_at = viewed_at;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }


    public Rect getmBounds() {
        return mBounds;
    }

    public void setmBounds(Rect mBounds) {
        this.mBounds = mBounds;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public String getUrl() {
        return img;
    }

    @Override
    public Rect getBounds() {
        return mBounds;
    }

    @Nullable
    @Override
    public String getVideoUrl() {
        return videoUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeInt(this.broadcast_id);
        dest.writeInt(this.is_chosen);
        dest.writeString(this.created_at);
        dest.writeString(this.img);
        dest.writeString(this.viewed_at);
        dest.writeString(this.nickname);
        dest.writeString(this.avatar);
        dest.writeInt(this.gender);
        dest.writeParcelable(this.mBounds, flags);
        dest.writeString(this.videoUrl);
    }

    public RadioRegisterListBean() {
    }

    protected RadioRegisterListBean(Parcel in) {
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.broadcast_id = in.readInt();
        this.is_chosen = in.readInt();
        this.created_at = in.readString();
        this.img = in.readString();
        this.viewed_at = in.readString();
        this.nickname = in.readString();
        this.avatar = in.readString();
        this.gender = in.readInt();
        this.mBounds = in.readParcelable(Rect.class.getClassLoader());
        this.videoUrl=in.readString();
    }

    public static final Creator<RadioRegisterListBean> CREATOR = new Creator<RadioRegisterListBean>() {
        @Override
        public RadioRegisterListBean createFromParcel(Parcel source) {
            return new RadioRegisterListBean(source);
        }

        @Override
        public RadioRegisterListBean[] newArray(int size) {
            return new RadioRegisterListBean[size];
        }
    };
}
