package com.radioapp.liaoliaobao.bean.address;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-17
 */
public class AddressJsonBean implements Parcelable {
    /**
     * id : 130100
     * area_name : 石家庄市
     * parent_id : 130000
     * city_code : 0311
     * lng : 114.502461
     * lat : 38.045474
     * level : 2
     * sort : 5
     */

    protected int id;
    @SerializedName("area_name")
    protected String areaName;
    @SerializedName("parent_id")
    protected int parentId;
    @SerializedName("city_code")
    protected String cityCode;
    protected String lng;
    protected String lat;
    protected int level;
    protected int sort;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.areaName);
        dest.writeInt(this.parentId);
        dest.writeString(this.cityCode);
        dest.writeString(this.lng);
        dest.writeString(this.lat);
        dest.writeInt(this.level);
        dest.writeInt(this.sort);
    }

    public AddressJsonBean() {
    }

    protected AddressJsonBean(Parcel in) {
        this.id = in.readInt();
        this.areaName = in.readString();
        this.parentId = in.readInt();
        this.cityCode = in.readString();
        this.lng = in.readString();
        this.lat = in.readString();
        this.level = in.readInt();
        this.sort = in.readInt();
    }

    public static final Creator<AddressJsonBean> CREATOR = new Creator<AddressJsonBean>() {
        @Override
        public AddressJsonBean createFromParcel(Parcel source) {
            return new AddressJsonBean(source);
        }

        @Override
        public AddressJsonBean[] newArray(int size) {
            return new AddressJsonBean[size];
        }
    };

    @Override
    public String toString() {
        return "AddressJsonBean{" +
                "id=" + id +
                ", areaName='" + areaName + '\'' +
                ", parentId=" + parentId +
                ", cityCode='" + cityCode + '\'' +
                ", lng='" + lng + '\'' +
                ", lat='" + lat + '\'' +
                ", level=" + level +
                ", sort=" + sort +
                '}';
    }
}
