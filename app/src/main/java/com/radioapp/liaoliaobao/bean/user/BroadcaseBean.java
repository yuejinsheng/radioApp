package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能： 广播
 * 描述
 * Created by yue on 2019-09-29
 */
public class BroadcaseBean implements Parcelable {
    public int  id;          ;//  "id": 4,
    public int   user_id         ;// "user_id": 129,
    public int    parent_id        ;// "parent_id": 0,
    public String   text         ;// "text": "真好看",
    public String  created_at          ;// "created_at": "2019-09-01 08:40:33",
    public String nickname           ;// "nickname": "女92",
    public String  avatar          ;// "avatar": "images\/2019_08\/30\/5uFTdphp0NdOCr1567096444190830.jpg",
    public int    gender        ;// "gender": 2


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeInt(this.parent_id);
        dest.writeString(this.text);
        dest.writeString(this.created_at);
        dest.writeString(this.nickname);
        dest.writeString(this.avatar);
        dest.writeInt(this.gender);
    }

    public BroadcaseBean() {
    }

    protected BroadcaseBean(Parcel in) {
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.parent_id = in.readInt();
        this.text = in.readString();
        this.created_at = in.readString();
        this.nickname = in.readString();
        this.avatar = in.readString();
        this.gender = in.readInt();
    }

    public static final Parcelable.Creator<BroadcaseBean> CREATOR = new Parcelable.Creator<BroadcaseBean>() {
        @Override
        public BroadcaseBean createFromParcel(Parcel source) {
            return new BroadcaseBean(source);
        }

        @Override
        public BroadcaseBean[] newArray(int size) {
            return new BroadcaseBean[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }
}
