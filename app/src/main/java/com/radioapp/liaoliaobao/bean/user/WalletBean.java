package com.radioapp.liaoliaobao.bean.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-10-08
 */
public class WalletBean implements Parcelable {

    private  int cash;
    private int withdrawal;
    private int frozen;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.cash);
        dest.writeInt(this.withdrawal);
        dest.writeInt(this.frozen);
    }

    public WalletBean() {
    }

    protected WalletBean(Parcel in) {
        this.cash = in.readInt();
        this.withdrawal = in.readInt();
        this.frozen = in.readInt();
    }

    public static final Parcelable.Creator<WalletBean> CREATOR = new Parcelable.Creator<WalletBean>() {
        @Override
        public WalletBean createFromParcel(Parcel source) {
            return new WalletBean(source);
        }

        @Override
        public WalletBean[] newArray(int size) {
            return new WalletBean[size];
        }
    };

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(int withdrawal) {
        this.withdrawal = withdrawal;
    }

    public int getFrozen() {
        return frozen;
    }

    public void setFrozen(int frozen) {
        this.frozen = frozen;
    }
}
