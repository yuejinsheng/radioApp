package com.radioapp.liaoliaobao.bean.radio;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.jaydenxiao.common.url.BaseConstant;
import com.previewlibrary.enitity.IThumbViewInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能：广播列表
 * 描述
 * Created by yue on 2019-08-28
 */
public class RadioListBean implements Parcelable {

    private int id;  //广播id
    private int user_id; //发布者id
    private String py_user_id; //炮友id 不为null时就是已通过
    private String dating_types; //格式为1,2,3
    private String dating_hopes; //格式为1,2,3
    private int province_id; //省
    private int city_id; //城市
    private String dating_time_str; //约会日期
    private String text; //约会内容
    private int open_comment; //是否开放评价1开放，0不开放
    private String nickname; //用户名
    private String avatar; //头像
    private int gender; //性别
    private String created_at; //发布时间
    @SerializedName("broadcast_resources")
    private List<BroadcaseResources> broadcastResources;//图片资源
    private String is_apply; //不为null时，为已报名
    private String is_like; //不为null时，已点赞
    private int like_count; //点赞数
    public ArrayList<Comments> comments;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPy_user_id() {
        return py_user_id;
    }

    public void setPy_user_id(String py_user_id) {
        this.py_user_id = py_user_id;
    }

    public String getDating_types() {
        return dating_types;
    }

    public void setDating_types(String dating_types) {
        this.dating_types = dating_types;
    }

    public String getDating_hopes() {
        return dating_hopes;
    }

    public void setDating_hopes(String dating_hopes) {
        this.dating_hopes = dating_hopes;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public String getDating_time_str() {
        return dating_time_str;
    }

    public void setDating_time_str(String dating_time_str) {
        this.dating_time_str = dating_time_str;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getOpen_comment() {
        return open_comment;
    }

    public void setOpen_comment(int open_comment) {
        this.open_comment = open_comment;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public List<BroadcaseResources> getBroadcastResources() {
        return broadcastResources;
    }

    public void setBroadcastResources(List<BroadcaseResources> broadcastResources) {
        this.broadcastResources = broadcastResources;
    }

    public String getIs_apply() {
        return is_apply;
    }

    public void setIs_apply(String is_apply) {
        this.is_apply = is_apply;
    }

    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    public int getLike_count() {
        return like_count;
    }

    public void setLike_count(int like_count) {
        this.like_count = like_count;
    }

    @Override
    public String toString() {
        return "RadioListBean{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", py_user_id=" + py_user_id +
                ", dating_types='" + dating_types + '\'' +
                ", dating_hopes='" + dating_hopes + '\'' +
                ", province_id=" + province_id +
                ", city_id=" + city_id +
                ", dating_time_str='" + dating_time_str + '\'' +
                ", text='" + text + '\'' +
                ", open_comment=" + open_comment +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", gender=" + gender +
                ", created_at='" + created_at + '\'' +
                ", broadcastResources=" + broadcastResources +
                ", is_apply='" + is_apply + '\'' +
                ", is_like='" + is_like + '\'' +
                ", like_count=" + like_count +
                '}';
    }


    public static class BroadcaseResources implements IThumbViewInfo {
        /**
         * broadcast_id : 54
         * resource_url : images/2019_08/25/TKR2mphpPlBG8Z1566669929190825.jpeg
         * type : 1
         * mime_type : image/jpeg
         * file_size : 418027
         */

        private int broadcast_id;
        private String resource_url;
        private int type;
        private String mime_type;
        private int file_size;
        private Rect mBounds; // 记录坐标
        private String videoUrl;

        public int getBroadcast_id() {
            return broadcast_id;
        }

        public void setBroadcast_id(int broadcast_id) {
            this.broadcast_id = broadcast_id;
        }

        public String getResource_url() {
            return BaseConstant.IMAGEURL + resource_url;
        }

        public void setResource_url(String resource_url) {
            this.resource_url = resource_url;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getMime_type() {
            return mime_type;
        }

        public void setMime_type(String mime_type) {
            this.mime_type = mime_type;
        }

        public int getFile_size() {
            return file_size;
        }

        public void setFile_size(int file_size) {
            this.file_size = file_size;
        }

        public Rect getmBounds() {
            return mBounds;
        }

        public void setmBounds(Rect mBounds) {
            this.mBounds = mBounds;
        }

        @Override
        public String getUrl() {
            return BaseConstant.IMAGEURL + resource_url;
        }

        @Override
        public Rect getBounds() {
            return mBounds;
        }

        @Nullable
        @Override
        public String getVideoUrl() {
            return videoUrl;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.broadcast_id);
            dest.writeString(this.resource_url);
            dest.writeInt(this.type);
            dest.writeString(this.mime_type);
            dest.writeInt(this.file_size);
            dest.writeParcelable(this.mBounds, flags);
            dest.writeString(this.videoUrl);
        }

        public BroadcaseResources() {
        }

        protected BroadcaseResources(Parcel in) {
            this.broadcast_id = in.readInt();
            this.resource_url = in.readString();
            this.type = in.readInt();
            this.mime_type = in.readString();
            this.file_size = in.readInt();
            this.mBounds = in.readParcelable(Rect.class.getClassLoader());
            this.videoUrl = in.readString();
        }

        public static final Creator<BroadcaseResources> CREATOR = new Creator<BroadcaseResources>() {
            @Override
            public BroadcaseResources createFromParcel(Parcel source) {
                return new BroadcaseResources(source);
            }

            @Override
            public BroadcaseResources[] newArray(int size) {
                return new BroadcaseResources[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeString(this.py_user_id);
        dest.writeString(this.dating_types);
        dest.writeString(this.dating_hopes);
        dest.writeInt(this.province_id);
        dest.writeInt(this.city_id);
        dest.writeString(this.dating_time_str);
        dest.writeString(this.text);
        dest.writeInt(this.open_comment);
        dest.writeString(this.nickname);
        dest.writeString(this.avatar);
        dest.writeInt(this.gender);
        dest.writeString(this.created_at);
        dest.writeList(this.broadcastResources);
        dest.writeString(this.is_apply);
        dest.writeString(this.is_like);
        dest.writeInt(this.like_count);
        dest.writeList(this.comments);
    }

    public RadioListBean() {
    }

    protected RadioListBean(Parcel in) {
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.py_user_id = in.readString();
        this.dating_types = in.readString();
        this.dating_hopes = in.readString();
        this.province_id = in.readInt();
        this.city_id = in.readInt();
        this.dating_time_str = in.readString();
        this.text = in.readString();
        this.open_comment = in.readInt();
        this.nickname = in.readString();
        this.avatar = in.readString();
        this.gender = in.readInt();
        this.created_at = in.readString();
        this.broadcastResources = new ArrayList<BroadcaseResources>();
        in.readList(this.broadcastResources, BroadcaseResources.class.getClassLoader());
        this.is_apply = in.readString();
        this.is_like = in.readString();
        this.like_count = in.readInt();
        in.readList(this.comments,Comments.class.getClassLoader());
    }

    public static final Creator<RadioListBean> CREATOR = new Creator<RadioListBean>() {
        @Override
        public RadioListBean createFromParcel(Parcel source) {
            return new RadioListBean(source);
        }

        @Override
        public RadioListBean[] newArray(int size) {
            return new RadioListBean[size];
        }
    };


    public static class Comments implements Parcelable {
        private  int    id ;             //     "id": 4,
        private int    commentable_id;              //     "commentable_id": 63,
        private int    user_id ;             //     "user_id": 129,
        private String   text;//        "text": "真好看",
        private String   nickname;//    //    "nickname": "女92",
        private String   avatar;     //     "avatar": "images\/2019_08\/30\/5uFTdphp0NdOCr1567096444190830.jpg",
        private int   gender;         //     "gender": 2,
        private String   created_at;    //    "created_at": "2019-09-01 08:40:33"


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.commentable_id);
            dest.writeInt(this.user_id);
            dest.writeString(this.text);
            dest.writeString(this.nickname);
            dest.writeString(this.avatar);
            dest.writeInt(this.gender);
            dest.writeString(this.created_at);
        }

        public Comments() {
        }

        protected Comments(Parcel in) {
            this.id = in.readInt();
            this.commentable_id = in.readInt();
            this.user_id = in.readInt();
            this.text = in.readString();
            this.nickname = in.readString();
            this.avatar = in.readString();
            this.gender = in.readInt();
            this.created_at = in.readString();
        }

        public static final Creator<Comments> CREATOR = new Creator<Comments>() {
            @Override
            public Comments createFromParcel(Parcel source) {
                return new Comments(source);
            }

            @Override
            public Comments[] newArray(int size) {
                return new Comments[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCommentable_id() {
            return commentable_id;
        }

        public void setCommentable_id(int commentable_id) {
            this.commentable_id = commentable_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }


    public ArrayList<Comments> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comments> comments) {
        this.comments = comments;
    }
}
