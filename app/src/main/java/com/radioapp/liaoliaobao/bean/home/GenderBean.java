package com.radioapp.liaoliaobao.bean.home;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-09-06
 */
public class GenderBean implements Parcelable {
    public int favoriteable_id;
    public int id;                      //    用户id
    public int gender;                  //  "gender": 1,
    public String avatar;               //  "avatar": "images\/2019_07\/29\/JifVwphpmvFGCV1564382775190729.jpg",
    public String nickname;              //  "nickname": "熊",
    public int hide_location;           // 是否隐藏地理位置,1隐藏
    public int career_id;
    public String birth_at;
    public String lng;                   //  经度
    public String lat;                   //  纬度
    public String geohash;              //  "geohash": "ws0s47u3c",
    public String verified_at;   //认证时间
    public String vip_ended_at;     //vip截止时间
    public String latest_online_at;      //  最后在线时间
    public String updated_at;         //更新时间
    public int distance;                // 隐私状态 0:公开 1:付费查看相册 2:通过验证后查看用户资料 3:隐身
    public int favorite;
    public List<Regions> regions;
    public int province_id;
    public int city_id;




    public int getFavoriteable_id() {
        return favoriteable_id;
    }

    public void setFavoriteable_id(int favoriteable_id) {
        this.favoriteable_id = favoriteable_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getHide_location() {
        return hide_location;
    }

    public void setHide_location(int hide_location) {
        this.hide_location = hide_location;
    }

    public int getCareer_id() {
        return career_id;
    }

    public void setCareer_id(int career_id) {
        this.career_id = career_id;
    }

    public String getBirth_at() {
        return birth_at;
    }

    public void setBirth_at(String birth_at) {
        this.birth_at = birth_at;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getGeohash() {
        return geohash;
    }

    public void setGeohash(String geohash) {
        this.geohash = geohash;
    }

    public String getVerified_at() {
        return verified_at;
    }

    public void setVerified_at(String verified_at) {
        this.verified_at = verified_at;
    }

    public String getVip_ended_at() {
        return vip_ended_at;
    }

    public void setVip_ended_at(String vip_ended_at) {
        this.vip_ended_at = vip_ended_at;
    }

    public String getLatest_online_at() {
        return latest_online_at;
    }

    public void setLatest_online_at(String latest_online_at) {
        this.latest_online_at = latest_online_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public List<Regions> getRegions() {
        return regions;
    }

    public void setRegions(List<Regions> regions) {
        this.regions = regions;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public static class Regions implements Parcelable {
        public int user_id ; //"user_id": 109,
        public int province_id;//        "province_id": 440000,
        public int  city_id;//


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.user_id);
            dest.writeInt(this.province_id);
            dest.writeInt(this.city_id);
        }

        public Regions() {
        }

        protected Regions(Parcel in) {
            this.user_id = in.readInt();
            this.province_id = in.readInt();
            this.city_id = in.readInt();
        }

        public static final Parcelable.Creator<Regions> CREATOR = new Parcelable.Creator<Regions>() {
            @Override
            public Regions createFromParcel(Parcel source) {
                return new Regions(source);
            }

            @Override
            public Regions[] newArray(int size) {
                return new Regions[size];
            }
        };

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getProvince_id() {
            return province_id;
        }

        public void setProvince_id(int province_id) {
            this.province_id = province_id;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.favoriteable_id);
        dest.writeInt(this.id);
        dest.writeInt(this.gender);
        dest.writeString(this.avatar);
        dest.writeString(this.nickname);
        dest.writeInt(this.hide_location);
        dest.writeInt(this.career_id);
        dest.writeString(this.birth_at);
        dest.writeString(this.lng);
        dest.writeString(this.lat);
        dest.writeString(this.geohash);
        dest.writeString(this.verified_at);
        dest.writeString(this.vip_ended_at);
        dest.writeString(this.latest_online_at);
        dest.writeString(this.updated_at);
        dest.writeInt(this.distance);
        dest.writeInt(this.favorite);
        dest.writeTypedList(this.regions);
        dest.writeInt(this.province_id);
        dest.writeInt(this.city_id);
    }

    public GenderBean() {
    }

    protected GenderBean(Parcel in) {
        this.favoriteable_id=in.readInt();
        this.id = in.readInt();
        this.gender = in.readInt();
        this.avatar = in.readString();
        this.nickname = in.readString();
        this.hide_location = in.readInt();
        this.career_id = in.readInt();
        this.birth_at = in.readString();
        this.lng = in.readString();
        this.lat = in.readString();
        this.geohash = in.readString();
        this.verified_at = in.readString();
        this.vip_ended_at = in.readString();
        this.latest_online_at = in.readString();
        this.updated_at = in.readString();
        this.distance = in.readInt();
        this.favorite = in.readInt();
        this.regions = in.createTypedArrayList(Regions.CREATOR);
        this.province_id = in.readInt();
        this.city_id = in.readInt();
    }

    public static final Parcelable.Creator<GenderBean> CREATOR = new Parcelable.Creator<GenderBean>() {
        @Override
        public GenderBean createFromParcel(Parcel source) {
            return new GenderBean(source);
        }

        @Override
        public GenderBean[] newArray(int size) {
            return new GenderBean[size];
        }
    };
}

