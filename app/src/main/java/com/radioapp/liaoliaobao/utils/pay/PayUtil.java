package com.radioapp.liaoliaobao.utils.pay;


import android.app.Activity;
import android.widget.Toast;

import com.radioapp.liaoliaobao.app.App;
import com.radioapp.liaoliaobao.utils.pay.alipay.Alipay;
import com.radioapp.liaoliaobao.utils.pay.weixin.WXPay;


/**
 * 支付工具类
 */
public class PayUtil {

    private static PayUtil mINSTANCE;
  //  public static final String APPID = "wx578e9e51f68216b1";
  //public static final String APPID = "wx79c74919119828f6";
    public String wxAPPId;

    //接口回调
    public interface PayCallback {
        void onSuccess();

        void onError();

        void onCancel();
    }


    public static PayUtil getInstance() {
        if (mINSTANCE == null) {
            mINSTANCE = new PayUtil();
        }
        return mINSTANCE;
    }


    /**
     * 支付
     *
     * @param type 1，支付宝，2微信
     */
    public void Pay(Activity activity, int type, String  wxAppId,String pay_param, PayCallback payCallBack) {
        this.wxAPPId=wxAppId;
        if (type == 1) {
            doAlipay(activity, pay_param, payCallBack);
        } else if (type == 2) {
            doWXPay(pay_param, payCallBack);
        }
    }


    /**
     * 支付宝支付
     *
     * @param pay_param 支付服务生成的支付参数
     */
    private void doAlipay(Activity activity, String pay_param, PayCallback payCallBack) {
        new Alipay(activity, pay_param, new Alipay.AlipayResultCallBack() {
            @Override
            public void onSuccess() {
                Toast.makeText(App.getInstance(), "支付成功", Toast.LENGTH_SHORT).show();
                if (payCallBack != null) {
                    payCallBack.onSuccess();
                }
            }

            @Override
            public void onDealing() {
                Toast.makeText(App.getInstance(), "支付处理中...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(int error_code) {
                switch (error_code) {
                    case Alipay.ERROR_RESULT:
                        Toast.makeText(App.getInstance(), "支付失败:支付结果解析错误", Toast.LENGTH_SHORT).show();
                        break;

                    case Alipay.ERROR_NETWORK:
                        Toast.makeText(App.getInstance(), "支付失败:网络连接错误", Toast.LENGTH_SHORT).show();
                        break;

                    case Alipay.ERROR_PAY:
                        Toast.makeText(App.getInstance(), "支付错误:支付码支付失败", Toast.LENGTH_SHORT).show();
                        break;

                    default:
                        Toast.makeText(App.getInstance(), "支付错误", Toast.LENGTH_SHORT).show();
                        break;
                }
                if (payCallBack != null) {
                    payCallBack.onError();
                }

            }

            @Override
            public void onCancel() {
                Toast.makeText(App.getInstance(), "支付取消", Toast.LENGTH_SHORT).show();
                if (payCallBack != null) {
                    payCallBack.onCancel();
                }
            }
        }).doPay();
    }

    /**
     * 微信支付
     *
     * @param pay_param 支付服务生成的支付参数
     */
    private void doWXPay(String pay_param, PayCallback payCallBack) {
        WXPay.init(App.getInstance(), wxAPPId);      //要在支付前调用
        WXPay.getInstance().doPay(pay_param, new WXPay.WXPayResultCallBack() {
            @Override
            public void onSuccess() {
                Toast.makeText(App.getInstance(), "支付成功", Toast.LENGTH_SHORT).show();
                if (payCallBack != null) {
                    payCallBack.onSuccess();
                }
            }

            @Override
            public void onError(int error_code) {
                switch (error_code) {
                    case WXPay.NO_OR_LOW_WX:
                        Toast.makeText(App.getInstance(), "未安装微信或微信版本过低", Toast.LENGTH_SHORT).show();
                        break;

                    case WXPay.ERROR_PAY_PARAM:
                        Toast.makeText(App.getInstance(), "参数错误", Toast.LENGTH_SHORT).show();
                        break;

                    case WXPay.ERROR_PAY:
                        Toast.makeText(App.getInstance(), "支付失败", Toast.LENGTH_SHORT).show();
                        break;
                }
                if (payCallBack != null) {
                    payCallBack.onError();
                }
            }

            @Override
            public void onCancel() {
                Toast.makeText(App.getInstance(), "支付取消", Toast.LENGTH_SHORT).show();
                if (payCallBack != null) {
                    payCallBack.onCancel();
                }
            }
        });
    }

}
