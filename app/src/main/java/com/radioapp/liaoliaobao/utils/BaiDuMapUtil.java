package com.radioapp.liaoliaobao.utils;

import android.app.Activity;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.hyphenate.chat.EMClient;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.app.App;
import com.radioapp.liaoliaobao.bean.user.LoginBean;
import com.radioapp.liaoliaobao.bean.user.UserBean;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.Global;
import com.radioapp.liaoliaobao.module.index.MainFragment;
import com.radioapp.liaoliaobao.module.user.login.LoginFragment;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.tbruyelle.rxpermissions2.RxPermissions;

import me.jessyan.autosize.utils.LogUtils;

/**
 * 功能： 百度地图定位的初始化功能
 * 描述: 绑定了activity的生命周期
 * Created by yue on 2019-07-19
 */
public class BaiDuMapUtil   implements LifecycleObserver {
    private static BaiDuMapUtil instance = null;
    private Context context;
    LocationClient locationClient;
    LocationClientOption locationOption;

    private GaoDeAMapLocationListener listener;


    public BaiDuMapUtil setListener(GaoDeAMapLocationListener listener) {
        this.listener = listener;
        return this;
    }

    public interface GaoDeAMapLocationListener {
        void onLocationChanged(BDLocation aMapLocation);
    }

    /**
     * 初始化功能
     *
     * @return
     */
    public synchronized static BaiDuMapUtil getInstance() {
        if (instance == null) {
            instance = new BaiDuMapUtil();
        }
        return instance;
    }


    public BaiDuMapUtil setContext(Context context) {
        this. context= context;
        return this;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void onCreate() {
        Log.e("android,", "onCreate");
        initLocationOption();
    }



    /**
     * 初始化定位参数配置
     */

    public  void initLocationOption() {
//定位服务的客户端。宿主程序在客户端声明此类，并调用，目前只支持在主线程中启动
        locationClient = new LocationClient(App.getInstance());
//声明LocationClient类实例并配置定位参数
        locationOption = new LocationClientOption();
        MyLocationListener myLocationListener = new MyLocationListener();
//注册监听函数
        locationClient.registerLocationListener(myLocationListener);
//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        locationOption.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
//可选，默认gcj02，设置返回的定位结果坐标系，如果配合百度地图使用，建议设置为bd09ll;
        locationOption.setCoorType("gcj02");
//可选，默认0，即仅定位一次，设置发起连续定位请求的间隔需要大于等于1000ms才是有效的
        //   locationOption.setScanSpan(1000);
//可选，设置是否需要地址信息，默认不需要
        locationOption.setIsNeedAddress(true);
//可选，设置是否需要地址描述
        locationOption.setIsNeedLocationDescribe(true);
//可选，设置是否需要设备方向结果
        locationOption.setNeedDeviceDirect(false);
//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        locationOption.setLocationNotify(true);
//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        locationOption.setIgnoreKillProcess(true);
//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        locationOption.setIsNeedLocationDescribe(true);
//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        locationOption.setIsNeedLocationPoiList(true);
//可选，默认false，设置是否收集CRASH信息，默认收集
        locationOption.SetIgnoreCacheException(false);
//可选，默认false，设置是否开启Gps定位
        locationOption.setOpenGps(true);
//可选，默认false，设置定位时是否需要海拔信息，默认不需要，除基础定位版本都可用
        locationOption.setIsNeedAltitude(false);
        locationOption.setScanSpan(180000);//3分钟定位一次
//设置打开自动回调位置模式，该开关打开后，期间只要定位SDK检测到位置变化就会主动回调给开发者，该模式下开发者无需再关心定位间隔是多少，定位SDK本身发现位置变化就会及时回调给开发者
        locationOption.setOpenAutoNotifyMode();
//设置打开自动回调位置模式，该开关打开后，期间只要定位SDK检测到位置变化就会主动回调给开发者
        locationOption.setOpenAutoNotifyMode(180000, 1, LocationClientOption.LOC_SENSITIVITY_HIGHT);
//需将配置好的LocationClientOption对象，通过setLocOption方法传递给LocationClient对象使用
        locationClient.setLocOption(locationOption);
//开始定位
        locationClient.start();
    }

    /**
     * 实现定位回调
     */
    public class MyLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            LogUtils.e("定位");
            SharePref.saveString(Constants.LATITUDE,location.getLatitude()+"");
            SharePref.saveString(Constants.LONGITUDE,location.getLongitude()+"");
            if (listener != null) {
                listener.onLocationChanged(location);
            }
        }

}

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onStop() {
        // locationClient.stop();
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        //  locationClient.stop();
    }

}
