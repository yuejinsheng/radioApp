package com.radioapp.liaoliaobao.utils;

import android.util.Log;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;

/**
 * 功能： 环信的操作类
 * 描述
 * Created by yue on 2019-07-18
 */
public class EaseUIUtils {


    /**
     * 环信登录
     * @param userName
     * @param password
     */
    public static void login(String userName,String password){
        EMClient.getInstance().login(userName,password,new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();
                Log.e("main", "登录聊天服务器成功！");
            }

            @Override
            public void onProgress(int progress, String status) {

            }

            @Override
            public void onError(int code, String message) {
                Log.d("main", "登录聊天服务器失败！");
            }
        });
    }



    /**
     * 退出登录
     */
    public static void loginOut(){
        EMClient.getInstance().logout(true);

    }

}
