package com.radioapp.liaoliaobao.utils.timePicker;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.App;
import com.radioapp.liaoliaobao.bean.appointment.CareerSBean;
import com.radioapp.liaoliaobao.utils.JsonFileReader;

import java.util.ArrayList;
import java.util.List;

/**
 * 职业列表
 */
public class TimePickerCareerUtil {

    //职业集合
    public  static  List<CareerSBean> options1Items = new ArrayList<>();  //省
    public static List<List<CareerSBean.CarBean>> options2Items = new ArrayList<>();  //市


    private static TimePickerCareerUtil timePickerCityUtil;


    /**
     * 城市接口回掉
     */
    public interface CitySelectListener {
        void onSelected(CareerSBean careerSBean, CareerSBean.CarBean carBean);

    }


    public static synchronized TimePickerCareerUtil newInterface() {
        if (timePickerCityUtil == null) {
            timePickerCityUtil = new TimePickerCareerUtil();
        }
        init();
        return timePickerCityUtil;
    }

    private static void init() {
        //读取json
        String jsonData = JsonFileReader.getJson(App.getInstance(), "careers.json");
        //转换城市列表
      //  options1Items = parseData(jsonData);
        options1Items=new Gson().fromJson(jsonData,new TypeToken<List<CareerSBean>>(){}.getType());
        if (options1Items != null) {
            for (CareerSBean addressBean : options1Items) {
                options2Items.add(addressBean.getList());
            }
        }
    }


    /**
     * 显示职业
     *
     * @param activity
     * @param show           1,2
     * @param selectListener
     */
    public void showPickerView(Activity activity, int show, TimePickerCareerUtil.CitySelectListener selectListener) {// 弹出选择器

        OptionsPickerView pvOptions = new OptionsPickerBuilder(activity, (options1, options2, options3, v) -> {
            CareerSBean careerSBean = options1Items.get(options1);
            CareerSBean.CarBean carBean = options2Items.get(options1).get(options2);
            if (selectListener != null) {
                selectListener.onSelected(careerSBean, carBean);
            }

        })
                .setCancelText("取消")//取消按钮文字
                .setSubmitText("确定")//确认按钮文字
                .setContentTextSize(20)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setTitleText("职业选择")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
               // .setBackgroundId(ContextCompat.getColor(activity, R.color.color_66))
                .setTitleColor(Color.WHITE)//标题文字颜色
                .setSubmitColor(Color.WHITE)//确定按钮文字颜色
                .setCancelColor(Color.WHITE)//取消按钮文字颜色
                .setTitleBgColor(ContextCompat.getColor(activity, R.color.color_ff4))//标题背景颜色 Night mode
                .setBgColor(ContextCompat.getColor(activity,R.color.white))//滚轮背景颜色 Night mode
                //.isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
               // .isDialog(true)//是否显示为对话框样式
                .build();
       /* Dialog mDialog = pvOptions.getDialog();
        if (mDialog != null) {

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvOptions.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
                dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }*/
        if (show == 1) {
            pvOptions.setPicker(options1Items);//一级选择器
        }
        if (show == 2) {
            pvOptions.setPicker(options1Items, options2Items);//二级选择器*/
        }
        pvOptions.show();
    }


}
