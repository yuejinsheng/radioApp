package com.radioapp.liaoliaobao.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeFormatExchangeUtils {

    public static long date2TimeStamp(String date) {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(date).getTime() / 1000;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String timeStamp2Date(long timeStamp) {

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");
        return sdf.format(new Date(Long.valueOf(timeStamp)));
    }




    public static String calculateTime(String date){

        long timeStamp = date2TimeStamp(date);
        long nowTimeStamp = System.currentTimeMillis()/1000;
        long cha = nowTimeStamp - timeStamp-900;
        if (cha<60){
            return "在线";
        }else if (cha/60<60){
            return cha/60+"分钟前在线";
        }else if (cha/3600<24){
            return cha/3600+"小时前在线";
        }else if (cha/86400<3){
            return cha/86400+"天前在线";
        }else{
            return "3天前在线";
        }
    }
}
