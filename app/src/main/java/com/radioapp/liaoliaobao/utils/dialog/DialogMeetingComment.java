package com.radioapp.liaoliaobao.utils.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


import com.radioapp.liaoliaobao.R;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class DialogMeetingComment extends Dialog {

    private final EditText mContent;
    private final TextView mSend;
    private Context mContext;
    SendListener mSendListener;

    public interface SendListener{
        void send(String s);
    }
    public DialogMeetingComment(Context context, SendListener sendListener) {
        super(context);

        setContentView(R.layout.dialog_meeting_comment);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        mSendListener = sendListener;
        mContext = context;

        mContent = findViewById(R.id.dialo_meeting_comment_content);
        mSend = findViewById(R.id.dialo_meeting_comment_send);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                // 隐藏软键盘
                imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                if (TextUtils.isEmpty(mContent.getText().toString())){
                    dismiss();
                }else {
                    dismiss();
                    mSendListener.send(mContent.getText().toString());
                }
            }
        });


    }


    @Override
    public void show() {
        super.show();
        /**
         * 设置宽度全屏，要设置在show的后面
         */
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.gravity=Gravity.BOTTOM;
        attributes.width= MATCH_PARENT;
        attributes.height=WRAP_CONTENT;

        getWindow().getDecorView().setPadding(0, 0, 0, 0);

        getWindow().setAttributes(attributes);

    }



}
