package com.radioapp.liaoliaobao.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class FragmentUtil {

    /** 替换Fragment **/
    public static void replaceFragment(FragmentManager manager, int frameLayoutId, Fragment fragment) {
        if (fragment != null) manager.beginTransaction().replace(frameLayoutId, fragment).commit();
    }

}
