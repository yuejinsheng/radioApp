package com.radioapp.liaoliaobao.utils;

public class MoneyExchange {
    public static String exchange(int money){
        String qian;
        int yuan = money / 100;
        int fen = money - money / 100*100;
        if (fen==0){
            qian = yuan+".00";
        } else if (fen>0&&fen<10){
            qian = yuan+".0"+fen;
        }else {
            qian = yuan+"."+fen;
        }
        return qian;
    }
}
