package com.radioapp.liaoliaobao.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-08-26
 */
public class DateUtil {


    /**
     * 2个时间的天数
     * @param startTime
     * @param endTime
     * @return
     */
    public static int caculateTotalTime(String startTime, String endTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        Date date = null;
        int totalTime = 0;
        try {
            date = formatter.parse(startTime);
            long ts = date.getTime();
            date1 = formatter.parse(endTime);
            long ts1 = date1.getTime();
            long ts2 = ts1 - ts;
            totalTime = (int) (ts2 / (24 * 3600 * 1000) + 1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return totalTime;
    }

}
