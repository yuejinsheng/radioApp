package com.radioapp.liaoliaobao.utils.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jaydenxiao.common.commonutils.DisplayUtil;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.uihelper.UiHelper;
import com.radioapp.liaoliaobao.view.span.AndroidSpan;
import com.radioapp.liaoliaobao.view.span.SpanOptions;

/**
 * 功能：通用dialog
 * 描述
 * Created by yue on 2019/4/28
 */
public class AlertDialogutils {
    /**
     * 创建确认Dialog
     **/
    public static Dialog createDialog(Context context, String messageText, String commitText, final View.OnClickListener sureClick) {
        //删除对话框(flag为true是通知范围确认)
        View view = LayoutInflater.from(context).inflate(R.layout.sure_dialog, null);
        TextView off_commit = (TextView) view.findViewById(R.id.off_commit);
        TextView commit = (TextView) view.findViewById(R.id.commit);
        TextView message = (TextView) view.findViewById(R.id.message);
        message.setText(messageText);
        if (!TextUtils.isEmpty(commitText)) commit.setText(commitText);
        final MyDialog builder = new MyDialog(context, DisplayUtil.getScreenWidth(context) * 4 / 5, 0, view, R.style.Dialog_common);
        //取消
        off_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        //确定
        commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sureClick.onClick(v);
                builder.dismiss();
            }
        });
        builder.show();
        return builder;
    }

    /**
     * 创建确认Dialog
     **/
    public static Dialog createThreeDialog(Context context, String messageText, String commitText, String offCommitText,
                                           final View.OnClickListener sureClick, final View.OnClickListener offClick) {
        //删除对话框(flag为true是通知范围确认)
        View view = LayoutInflater.from(context).inflate(R.layout.sure_dialog, null);
        TextView off_commit = (TextView) view.findViewById(R.id.off_commit);
        TextView commit = (TextView) view.findViewById(R.id.commit);
        TextView message = (TextView) view.findViewById(R.id.message);
        off_commit.setText(offCommitText);
        message.setText(messageText);
        if (!TextUtils.isEmpty(commitText)) commit.setText(commitText);
        final MyDialog builder = new MyDialog(context, DisplayUtil.getScreenWidth(context) * 4 / 5, 0, view, R.style.Dialog_common);
        //取消
        off_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offClick.onClick(v);
                builder.dismiss();
            }
        });
        //确定
        commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sureClick.onClick(v);
                builder.dismiss();
            }
        });
        builder.show();
        return builder;
    }

    /**
     * 创建确认Dialog
     **/
    public static Dialog createProtocolDialog(Context context,
                                              final View.OnClickListener sureClick, final View.OnClickListener offClick) {
        //删除对话框(flag为true是通知范围确认)

        View view = LayoutInflater.from(context).inflate(R.layout.sure_dialog, null);
        TextView off_commit = (TextView) view.findViewById(R.id.off_commit);
        TextView tvTitle = view.findViewById(R.id.title);
        TextView commit = (TextView) view.findViewById(R.id.commit);
        TextView message = (TextView) view.findViewById(R.id.message);

        final MyDialog builder = new MyDialog(context, DisplayUtil.getScreenWidth(context) * 4 / 5, 0, view, R.style.Dialog_common);
        AndroidSpan androidSpan = new AndroidSpan()
                .drawCommonSpan("请您务必谨慎阅读，充分理解\"服务协议\"和\"隐私政策\"各条款。\n你可阅读")
                .drawWithOptions("《服务协议》", new SpanOptions()
                        .addForegroundColor(ContextCompat.getColor(context, R.color.color_4b8))
                        .addSpan(new ClickableSpan() {
                            @Override
                            public void onClick(@NonNull View widget) {
                                builder.dismiss();
                                UiHelper.showProtocolFragment((Activity) context, "file:///android_asset/user_agree.html");

                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                /**
                                 * 这里如果设置为false则不带下划线，true带有下划线
                                 */
                                ds.setUnderlineText(false);
                            }
                        }))
                .drawCommonSpan("和")
                .drawWithOptions("《隐私政策》", new SpanOptions()
                        .addForegroundColor(ContextCompat.getColor(context, R.color.color_4b8)).addSpan(new ClickableSpan() {
                            @Override
                            public void onClick(@NonNull View widget) {
                                builder.dismiss();
                                UiHelper.showProtocolFragment((Activity) context, "file:///android_asset/user_agree1.html");
                            }

                            @Override
                            public void updateDrawState(TextPaint ds) {
                                /**
                                 * 这里如果设置为false则不带下划线，true带有下划线
                                 */
                                ds.setUnderlineText(false);
                            }
                        })).drawCommonSpan("了解详细信息，比如同意。请点击\"同意\"开始接受我们的服务。");
        message.setText(androidSpan.getSpanText());
        message.setMovementMethod(LinkMovementMethod.getInstance());
        tvTitle.setText("服务协议和隐私政策");
        commit.setText("同意");
        off_commit.setText("暂不使用");

        //取消
        builder.setCancelable(false);
        builder.setCanceledOnTouchOutside(false);
        off_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offClick.onClick(v);
                builder.dismiss();
            }
        });
        //确定
        commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sureClick.onClick(v);
                builder.dismiss();
            }
        });
        builder.show();
        return builder;
    }


    /**
     * 创建确认Dialog(付费)
     **/
    public static Dialog createPriceDialog(Context context, final DialogPriceListener sureClick) {
        //删除对话框(flag为true是通知范围确认)
        View view = LayoutInflater.from(context).inflate(R.layout.sure_price_dialog, null);
        TextView off_commit = view.findViewById(R.id.off_commit);
        TextView commit = view.findViewById(R.id.commit);
        EditText message = view.findViewById(R.id.et_price);
        final MyDialog builder = new MyDialog(context, DisplayUtil.getScreenWidth(context) * 4 / 5, 0, view, R.style.Dialog_common);
        //取消
        off_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        //确定
        commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(message.getText())) {
                    ToastUitl.showLong("金额不能为空");
                    return;
                }
                Double pri = Double.parseDouble(message.getText().toString()) * 100;
                if (pri < 500 || pri > 10000) {
                    ToastUitl.showLong("金额需要在5-100之间");
                    return;
                }
                if (sureClick != null) {
                    sureClick.setPrice(pri);
                }

                builder.dismiss();
            }
        });
        builder.show();
        return builder;
    }


    public interface DialogPriceListener {
        void setPrice(Double price);
    }
}
