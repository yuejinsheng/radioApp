package com.radioapp.liaoliaobao.utils.timePicker;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.radioapp.liaoliaobao.R;

import java.util.List;

/**
 * 功能：单选的
 * 描述
 * Created by yue on 2019-07-24
 */
public class TimePickerOneUtil<T> {


    /**
     * 回掉
     */
    public interface SelectListener<T> {
        void onSelected(T t);

    }

    /**
     * 显示单选的数据
     *
     * @param activity
     * @param selectListener
     */
    public void showPickerView(Activity activity, String title, List<T> list, TimePickerOneUtil.SelectListener selectListener) {// 弹出选择器

        OptionsPickerView pvOptions = new OptionsPickerBuilder(activity, (options1, options2, options3, v) -> {

            if (selectListener != null) {
                selectListener.onSelected(list.get(options1));
            }

        })
                .setCancelText("取消")//取消按钮文字
                .setSubmitText("确定")//确认按钮文字
                .setContentTextSize(20)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setTitleText(title)//标题文字
                //.setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
               // .setBackgroundId(ContextCompat.getColor(activity, R.color.color_66))
                .setTitleColor(Color.WHITE)//标题文字颜色
                .setSubmitColor(Color.WHITE)//确定按钮文字颜色
                .setCancelColor(Color.WHITE)//取消按钮文字颜色
                .setTitleBgColor(ContextCompat.getColor(activity, R.color.color_ff4))//标题背景颜色 Night mode
                .setBgColor(ContextCompat.getColor(activity, R.color.white))//滚轮背景颜色 Night mode
                // .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                //.isDialog(true)//是否显示为对话框样式
                .build();
      /*  Dialog mDialog = pvOptions.getDialog();
        if (mDialog != null) {

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);
            params.leftMargin = -30;
            params.rightMargin = -30;
            pvOptions.getDialogContainerLayout().setLayoutParams(params);


            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
                dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
            }
        }*/
        pvOptions.setPicker(list);//一级选择器
        pvOptions.show();
    }


}
