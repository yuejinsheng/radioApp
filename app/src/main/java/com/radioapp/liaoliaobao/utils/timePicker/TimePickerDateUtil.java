package com.radioapp.liaoliaobao.utils.timePicker;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.radioapp.liaoliaobao.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-07-26
 */
public class TimePickerDateUtil {
    private static TimePickerDateUtil timePickerDateUtil;

    public static synchronized TimePickerDateUtil newInterface() {
        if (timePickerDateUtil == null) {
            timePickerDateUtil = new TimePickerDateUtil();
        }
        return timePickerDateUtil;
    }


    /**
     * 时间接口回掉
     */

    public interface DateSelectListener {
        void onSelected(String date);


    }

    public  void showTimePicker(Activity activity, String title, final DateSelectListener dateSelectListener){
        Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        //startDate.set(2013,1,1);
        Calendar endDate = Calendar.getInstance();
        //endDate.set(2020,1,1);
        Calendar now = Calendar.getInstance();
        //正确设置方式 原因：注意事项有说明
        startDate.set(1950,1,1);
        endDate.set(now.get(Calendar.YEAR),(now.get(Calendar.MONTH) + 1) ,now.get(Calendar.DAY_OF_MONTH));

        TimePickerView pvTime = new TimePickerBuilder(activity, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                dateSelectListener.onSelected(new SimpleDateFormat("yyyy-MM-dd").format(date));
            }
        })
                .setType(new boolean[]{true, true, true, false, false, false})// 默认全部显示
                .setCancelText("取消")//取消按钮文字
                .setSubmitText("确定")//确认按钮文字
                .setContentTextSize(20)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setTitleText(title)//标题文字
                .isCyclic(true)//是否循环滚动
               // .setBackgroundId(ContextCompat.getColor(activity, R.color.color_66))
                .setTitleColor(Color.WHITE)//标题文字颜色
                .setSubmitColor(Color.WHITE)//确定按钮文字颜色
                .setCancelColor(Color.WHITE)//取消按钮文字颜色
                .setTitleBgColor(ContextCompat.getColor(activity, R.color.color_ff4))//标题背景颜色 Night mode
                .setBgColor(ContextCompat.getColor(activity,R.color.white))//滚轮背景颜色 Night mode
                .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                .setRangDate(startDate,endDate)//起始终止年月日设定
                .setLabel("年","月","日","","","")//默认设置为年月日时分秒
                //.isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                //.isDialog(true)//是否显示为对话框样式
                .build();
//        Dialog mDialog = pvTime.getDialog();
//        if (mDialog != null) {
//
//            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
//                    ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT,
//                    Gravity.BOTTOM);
//
//            params.leftMargin = 0;
//            params.rightMargin = 0;
//            pvTime.getDialogContainerLayout().setLayoutParams(params);
//
//            Window dialogWindow = mDialog.getWindow();
//            if (dialogWindow != null) {
//                dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim);//修改动画样式
//                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
//            }
//        }
        pvTime.show();
    }
}
