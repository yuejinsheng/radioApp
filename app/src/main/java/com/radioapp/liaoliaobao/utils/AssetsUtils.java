package com.radioapp.liaoliaobao.utils;


import android.support.v4.util.ArrayMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.radioapp.liaoliaobao.app.App;
import com.radioapp.liaoliaobao.bean.ExpectBean;
import com.radioapp.liaoliaobao.bean.ProgramBean;
import com.radioapp.liaoliaobao.bean.address.AddressBean;
import com.radioapp.liaoliaobao.bean.appointment.CareerSBean;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * 功能：初始化assets的文件
 * 描述
 * Created by yue on 2019-09-06
 */
public class AssetsUtils {

    private static AssetsUtils assetsUtils;
    //省市区集合
    public List<AddressBean> options1Items = new ArrayList<>();  //省
    public List<List<AddressBean.CitysBean>> options2Items = new ArrayList<>();  //市
    public List<List<List<AddressBean.CitysBean.AreasBean>>> options3Items = new ArrayList<>(); //区
    public List<ExpectBean> expectBeans = new ArrayList<>(); //约会期望
    public List<ProgramBean> programBeans = new ArrayList<>();//约会节目


    //职业集合
    public List<CareerSBean> careerOptions = new ArrayList<>();
    public List<List<CareerSBean.CarBean>> careerOptions2 = new ArrayList<>();


    public static synchronized AssetsUtils newInterface() {
        if (assetsUtils == null) {
            assetsUtils = new AssetsUtils();
        }
        return assetsUtils;
    }


    public void init() {
        initCity();
        initCareer();
        initExpect();
        initProgram();
    }


    private void initCity() {
        //读取json
        String jsonData = JsonFileReader.getJson(App.getInstance(), "address.json");
        //转换城市列表
        options1Items = new Gson().fromJson(jsonData, new TypeToken<List<AddressBean>>() {
        }.getType());
        if (options1Items != null) {
            Observable.fromArray(options1Items)
                    .subscribeOn(Schedulers.io())
                    .map(addressBeans -> {
                        for (AddressBean addressBean : addressBeans) {
                            List<List<AddressBean.CitysBean.AreasBean>> areaList = new ArrayList<>();//该省的所有地区列表（第三极）
                            for (AddressBean.CitysBean city : addressBean.getCitys()) {
                                areaList.add(city.getAreas());
                            }
                            options2Items.add(addressBean.getCitys());
                            options3Items.add(areaList);
                        }
                        return options2Items;
                    }).subscribe();


            /*for (AddressBean addressBean : options1Items) {
                List<List<AddressBean.CitysBean.AreasBean>> areaList = new ArrayList<>();//该省的所有地区列表（第三极）
                for (AddressBean.CitysBean city : addressBean.getCitys()) {
                    areaList.add(city.getAreas());
                }

                options2Items.add(addressBean.getCitys());
                options3Items.add(areaList);
            }*/
        }


    }

    /**
     * 职业
     */
    private void initCareer() {
        //读取json
        String careersJson = JsonFileReader.getJson(App.getInstance(), "careers.json");
        //转换城市列表
        //  options1Items = parseData(jsonData);
        careerOptions = new Gson().fromJson(careersJson, new TypeToken<List<CareerSBean>>() {
        }.getType());
        Observable.fromArray(careerOptions)
                .subscribeOn(Schedulers.io())
                .map(careerSBeans -> {
                    for (CareerSBean addressBean : careerOptions) {
                        careerOptions2.add(addressBean.getList());
                    }
                    return careerOptions2;
                }).subscribe();
    }

    /**
     * 约会期望
     */
    private void initExpect() {
        //读取json
        String expectJson = JsonFileReader.getJson(App.getInstance(), "expect.json");
        expectBeans = new Gson().fromJson(expectJson, new TypeToken<List<ExpectBean>>() {
        }.getType());

    }

    /**
     * 约会节目
     */
    private void initProgram() {
        //读取json
        String programJson = JsonFileReader.getJson(App.getInstance(), "program.json");
        programBeans = new Gson().fromJson(programJson, new TypeToken<List<ProgramBean>>() {
        }.getType());

    }


    /**
     * 通过id获取省市区的名称
     *
     * @param provinceId 省
     * @param cityId     市
     * @param areaId     区
     * @return
     */
    public ArrayMap<String, Object> getAddress(int provinceId, int cityId, int areaId) {
        ArrayMap<String, Object> params = new ArrayMap<>();
        if (options1Items.size() > 0 ) {
            //省，市，区
            for (AddressBean options1Item : options1Items) {
                if(provinceId!=-1) {
                    if (provinceId == options1Item.getId()) {
                        params.put("provinceId", options1Item.getId());
                        params.put("provinceName", options1Item.getAreaName());
                    }
                }
                if (cityId != -1) {
                    for (AddressBean.CitysBean city : options1Item.getCitys()) {
                     if(cityId ==city.getId()){
                         params.put("cityId", city.getId());
                         params.put("cityName", city.getAreaName());
                         break;
                     }
                     if(areaId!=-1){
                         for (AddressBean.CitysBean.AreasBean area : city.getAreas()) {
                             if(areaId ==area.getId()){
                                 params.put("areaId", area.getId());
                                 params.put("areaName", area.getAreaName());
                             }
                         }
                     }
                    }
                }
            }
        }

        return params;
    }


    /**
     * 获取职业的名称
     * @param careerId
     * @return
     */
    public ArrayMap<String,Object> getCareersId(int careerId){
        ArrayMap<String, Object> params = new ArrayMap<>();
        if(careerOptions.size()>0){
            for (CareerSBean option : careerOptions) {
                for (CareerSBean.CarBean bean : option.getList()) {
                    if(bean.getId()==careerId){
                        params.put("careerId",bean.getId());
                        params.put("careerName",bean.getName());
                       break;
                    }
                }
            }
        }
        return params;
    }


    /**
     * 约会期望的id
     * @param id
     * @return
     */
    public ArrayMap<String,Object> getExpectName(int id){
        ArrayMap<String, Object> params = new ArrayMap<>();
        if(expectBeans.size()>0){
            for (ExpectBean bean : expectBeans) {
                if(id ==bean.getId()){
                    params.put("id",bean.getId());
                    params.put("name",bean.getName());
                    break;
                }
            }
        }
        return params;
    }
    /**
     * 约会节目的id
     * @param id
     * @return
     */
    public ArrayMap<String,Object> getProgramBeansName(int id){
        ArrayMap<String, Object> params = new ArrayMap<>();
        if(programBeans.size()>0){
            for (ProgramBean bean : programBeans) {
                if(id ==bean.getId()){
                    params.put("id",bean.getId());
                    params.put("name",bean.getName());
                    break;
                }
            }
        }
        return params;
    }

}


