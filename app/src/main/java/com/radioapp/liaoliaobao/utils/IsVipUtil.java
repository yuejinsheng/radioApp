package com.radioapp.liaoliaobao.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class IsVipUtil {

    public static boolean isVip(String vip_end_at) {

        if (!TextUtils.isEmpty(vip_end_at)) {
            /**
             * 会员
             */
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = sdf.parse(vip_end_at);
                if(date.after(new Date())){
                    return true;
                }else{
                    return false;
                }

            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }

    }
}
