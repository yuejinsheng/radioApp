package com.radioapp.liaoliaobao.receiver;

import android.content.Context;


import com.jaydenxiao.common.sharepref.SharePref;
import com.radioapp.liaoliaobao.constant.Constants;

import cn.jpush.android.api.CustomMessage;
import cn.jpush.android.api.NotificationMessage;
import cn.jpush.android.service.JPushMessageReceiver;

public class JGReceiver extends JPushMessageReceiver {


    @Override
    public void onNotifyMessageArrived(Context context, NotificationMessage notificationMessage) {
        super.onNotifyMessageArrived(context, notificationMessage);

        SharePref.saveInt( Constants.Notice_System,1);
//        LogUtils.d("测试JPush收到极光通知onMessage"+notificationMessage.toString());
//        LogUtils.d("测试系统消息："+PreferenceUtils.getInt(MyApplication.getContext(),Constants.Notice_System));
//        long[] longs = {0, 500, 1000, 1500};
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            String channelId = "XINGDIAN";
//            NotificationManager mNotificationManager = (NotificationManager) MyApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
//
//            NotificationChannel notificationChannel = new NotificationChannel(channelId, "星电消息", NotificationManager.IMPORTANCE_HIGH);
//            notificationChannel.enableLights(true);
//            notificationChannel.enableVibration(true);
//            notificationChannel.setLightColor(Color.RED);
//            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
//            notificationChannel.setShowBadge(true);
//            notificationChannel.setBypassDnd(true);
//            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400});
//            notificationChannel.setDescription("xingdianapp");
//            mNotificationManager.createNotificationChannel(notificationChannel);
//
//            Notification.Builder builder = new Notification.Builder(MyApplication.getContext(), channelId);
//            builder.setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentText(notificationMessage.notificationContent)
//                    .setAutoCancel(true);
//            Intent resultIntent = new Intent(MyApplication.getContext(), MainUi.class);
//            TaskStackBuilder stackBuilder = TaskStackBuilder.create(MyApplication.getContext());
//            stackBuilder.addParentStack(MainActivity.class);
//            stackBuilder.addNextIntent(resultIntent);
//            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//            builder.setContentIntent(resultPendingIntent);
//            mNotificationManager.notify((int) System.currentTimeMillis(), builder.build());
//        } else {
//            NotificationManager mNotificationManager = (NotificationManager) MyApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
//            Notification.Builder builder = new Notification.Builder(MyApplication.getContext());
//            Intent intent = new Intent(MyApplication.getContext(), MainUi.class);
//            PendingIntent pendingIntent = PendingIntent.getActivity(MyApplication.getContext(), 0, intent, 0);
//            builder.setContentIntent(pendingIntent);
//            builder.setSmallIcon(R.drawable.logo);
//            builder.setVibrate(longs);
//            builder.setLargeIcon(BitmapFactory.decodeResource(MyApplication.getContext().getResources(), R.drawable.logo));
//            builder.setAutoCancel(true);
//            builder.setContentText(notificationMessage.notificationContent);
////            builder.setFullScreenIntent(pendingIntent, true);
//            mNotificationManager.notify(1234, builder.build());
//
//
//        }
    }

    /**
     * 自定义消息
     * @param context
     * @param customMessage
     */
    @Override
    public void onMessage(Context context, CustomMessage customMessage) {
        super.onMessage(context, customMessage);


    }
}
