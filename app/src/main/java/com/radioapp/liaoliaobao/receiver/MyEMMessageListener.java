package com.radioapp.liaoliaobao.receiver;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMCmdMessageBody;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.easeui.model.EaseAtMessageHelper;
import com.hyphenate.easeui.model.EaseDingMessageHelper;
import com.hyphenate.exceptions.HyphenateException;
import com.jaydenxiao.common.baseapp.AppManager;
import com.jaydenxiao.common.sharepref.SharePref;
import com.jaydenxiao.common.url.BaseConstant;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.radioapp.liaoliaobao.R;
import com.radioapp.liaoliaobao.app.App;
import com.radioapp.liaoliaobao.bean.EMCmdMessageBean;
import com.radioapp.liaoliaobao.bean.lite.ChatUser;
import com.radioapp.liaoliaobao.constant.Constants;
import com.radioapp.liaoliaobao.constant.IntentConstant;
import com.radioapp.liaoliaobao.module.index.MainActivity;
import com.radioapp.liaoliaobao.module.index.MainFragment;
import com.radioapp.liaoliaobao.module.message.MessageFragment;
import com.radioapp.liaoliaobao.module.message.chat.ChatActivity;
import com.radioapp.liaoliaobao.module.message.chat.ChatMessageFragment;
import com.radioapp.liaoliaobao.module.message.system.list.MessageListAdapter;
import com.radioapp.liaoliaobao.utils.LinNotify;


import org.apache.commons.logging.Log;

import java.util.List;

import me.jessyan.autosize.utils.LogUtils;

import static com.hyphenate.chat.EMMessage.Type.IMAGE;

public class MyEMMessageListener implements EMMessageListener {

    @Override
    public void onMessageReceived(List<EMMessage> messages) {
        LogUtils.d("测试收到消息传消息：" + messages.toString());
        try {

            Activity activity = AppManager.getAppManager().currentActivity();
            LogUtils.e("activity-->"+activity.getClass().getName());
            if (activity instanceof MainActivity) {
                //获取当前fragment的是否在显示中的
                FragmentManager childFragmentManager = ((MainActivity) activity).getSupportFragmentManager();
                List<Fragment> fragments = childFragmentManager.getFragments();
                if (fragments.size() > 0) {
                    for (Fragment fragment : fragments) {
                        LogUtils.e("fragment-->"+fragment.getClass().getName());

                        if (fragment instanceof MessageFragment) {
                            ((MessageFragment) fragment).refreshUIWithMessage();
                            if (fragment.isVisible()) {
                                if (((MessageFragment) fragment).getTabIndex() == 0) {
                                    return;
                                }
                            }
                        }
                    }
                }
            } else if (activity instanceof ChatActivity) {
                return;
            }

            for (EMMessage message : messages) {
                String em_name = "";
                String em_avatar = "";
                if (!TextUtils.isEmpty(message.getStringAttribute("nickName"))) {
                    em_name = message.getStringAttribute("nickName");
                }
                if (!TextUtils.isEmpty(message.getStringAttribute("avatar"))) {
                    em_avatar = message.getStringAttribute("avatar");
                }

                //  EMConversation conversation = EMClient.getInstance().chatManager().getConversation(message.getFrom());

                //保存数据
                if (!TextUtils.isEmpty(em_name) && !TextUtils.isEmpty(em_avatar)) {
                    ChatUser chatUser = new ChatUser(Integer.parseInt(message.getFrom())
                            , em_name, em_avatar);
                }
                //跳转类
                Intent intent = new Intent(App.getInstance(), ChatActivity.class);
                intent.putExtra(IntentConstant.USERID, message.getFrom());
                LinNotify.showMuch(App.getInstance(), em_name, inputText(message, em_name, message.getType()), intent);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onCmdMessageReceived(List<EMMessage> messages) {

        LogUtils.d("测试透传消息：" + messages.toString());
        for (int i = 0; i < messages.size(); i++) {
            EMMessage message = messages.get(i);
            EMCmdMessageBody body = (EMCmdMessageBody) message.getBody();
            String action = body.action();
            EMCmdMessageBean emCmdMessageBean = new Gson().fromJson(action, EMCmdMessageBean.class);
            switch (emCmdMessageBean.flag) {
                case 5:
                    SharePref.saveInt(Constants.REGISTER_MEETING,1);
                    LinNotify.showMuch(App.getInstance(), " 约会报名", emCmdMessageBean.msg, null);
                    break;
                case 3:
                case 4:
                    SharePref.saveInt(Constants.Notice_APPLY_LOOK,1);
                    LinNotify.showMuch(App.getInstance(), " 申请查看", emCmdMessageBean.msg, null);
                    break;
                case 6:
                    SharePref.saveInt(Constants.Notice_RADIO,1);
                    LinNotify.showMuch(App.getInstance(), " 约会广播", emCmdMessageBean.msg, null);
                    break;
                case 1:
                case 2:
                case 7:
                    SharePref.saveInt(Constants.Notice_RETURN,1);
                    LinNotify.showMuch(App.getInstance(), " 收益提醒", emCmdMessageBean.msg, null);
                    break;
            }

        }
        for (EMMessage message : messages) {
            // To handle group-ack msg.
            EaseDingMessageHelper.get().handleAckMessage(message);
        }


    }

    @Override
    public void onMessageRead(List<EMMessage> messages) {
        LogUtils.d("测试aihduhausdhiasd");
    }

    @Override
    public void onMessageDelivered(List<EMMessage> messages) {

    }

    @Override
    public void onMessageRecalled(List<EMMessage> messages) {

    }

    @Override
    public void onMessageChanged(EMMessage message, Object change) {

    }


    public String inputText(EMMessage emMessage, String userName, EMMessage.Type type) {
        String content = "";
        switch (type) {
            case IMAGE:
                content = userName + "发来[" + 1 + "条] " + "[图片]";
                break;
            case LOCATION:
                content = userName + "发来[" + 1 + "条] " + "[定位]";
                break;
            case VOICE:
                content = userName + "发来[" + 1 + "条] " + "[语音]";
                break;
            default:
                EMTextMessageBody body = (EMTextMessageBody) emMessage.getBody();
                content = body.getMessage();
                break;
        }
        return content;
    }


}
