package com.jaydenxiao.common.base;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * des:基类presenter
 * Created by xsf
 * on 2016.07.11:55
 */
public abstract class BasePresenter<V extends BaseView>{
    protected V mView;

    public CompositeDisposable mCompositeDisposable;
    public Context mContext;

    public void onStart(){}
   
    public void attachView(V view) {
        mView=view;
        this.onStart();
    }

    public void detachView() {
        mView=null;
        unSubscribe();
    }

    
    /**
     * 绑定subscriber 并且添加mCompositeDisposable进行内存泄露管理
     */
   /* public  <T> void createRxSubscriber(Observable<T> observable, RxSubscriber<T> subscriber){
           observable.compose(mView.bindToLife())
                  .subscribe(subscriber);
           addSubscribe(subscriber);
    }*/


    protected void addSubscribe(Disposable disposable) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }
        mCompositeDisposable.add(disposable);
    }

    protected void unSubscribe() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();//保证activity结束时取消所有正在执行的订阅
        }
    }

}
