package com.jaydenxiao.common.base;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gyf.barlibrary.ImmersionBar;
import com.jaydenxiao.common.R;
import com.jaydenxiao.common.baseapp.BaseApplication;
import com.jaydenxiao.common.basebean.LoginEvent;
import com.jaydenxiao.common.commonutils.TUtil;
import com.jaydenxiao.common.commonutils.ToastUitl;
import com.jaydenxiao.common.fragmentUtils.FragmentTagConstant;
import com.jkb.fragment.rigger.annotation.Puppet;
import com.jkb.fragment.rigger.rigger.Rigger;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.components.support.RxFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.jessyan.autosize.utils.LogUtils;

/**
 * des:基类fragment
 * Created by xsf
 * on 2016.07.12:38
 */
@Puppet
public abstract class BaseRiggerFragment<V extends BaseView, T extends BasePresenter<V>> extends RxFragment implements BaseView {

    protected View rootView;
    public T mPresenter;
    //ButterKnife
    private Unbinder mUnbinder;
    protected TextView toolbar_header;
    protected Toolbar toolbar;

    protected Activity mActivity;
    protected Context mContext;
    private ImmersionBar mImmersionBar;

    public boolean mIsFirstVisible = true;

    public boolean isViewCreated = false;

    public boolean currentVisibleState = false;


    public static ArrayList<String> mFragmentTags = new ArrayList<>();

    public int[] getPuppetAnimations() {
        return new int[]{
                R.anim.push_left_in_no_alpha,
                R.anim.push_right_out_no_alpha,
                R.anim.push_right_in_no_alpha,
                R.anim.push_left_out_no_alpha
        };
    }


    public void handIntent(Bundle bundle) {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = getActivity();
        mContext = mActivity.getBaseContext();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
        mContext = null;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(getLayoutResource(), container, false);

            mPresenter = TUtil.getT(this, 1);
            if (mPresenter != null) {
                mPresenter.mContext = getActivity();
                mPresenter.attachView((V) this);
            }
            if (rootView.getParent() != null) {
                ((ViewGroup) rootView.getParent()).removeView(rootView);
            }
            if (container != null) {
//                CommonLogger.e("添加父类");
                container.addView(rootView);
            }
            mUnbinder = ButterKnife.bind(this, rootView);
            handIntent(savedInstanceState == null ? getArguments() : savedInstanceState);
            //  mImmersionBar = ImmersionBar.with(this);
           // Rigger.getRigger(mActivity).printStack();
            initView();
        }
        if (rootView.getParent() != null) {
            ((ViewGroup) rootView.getParent()).removeView(rootView);
        }

        return rootView;
    }


    //设置fragment的tag
    public String getFragmentTag() {
        return this.getClass().getName();
    }


    //获取布局文件
    protected abstract int getLayoutResource();


    //初始化view
    protected abstract void initView();

    /**
     * token,重新登录,当前页面需要刷新的刷新数据
     * 只在当前页面数据请求的时候刷新，如果点击就不做刷新
     */
    public abstract void tokenInvalidRefresh();


    @Override
    public <T> LifecycleTransformer<T> bindToLife() {
        return bindToLifecycle();
    }


    /**
     * token失效
     */
    @Override
    public void tokenInvalid() {
        String fragmentCls = "com.radioapp.liaoliaobao.module.user.login.LoginFragment";
        try {
            Fragment fragment = (Fragment) Class.forName(fragmentCls).newInstance();
            //跳登录
            if (fragment != null) {
                Stack<String> strings = Rigger.getRigger(mActivity).getFragmentStack();
                for (String string : strings) {
                    if (string.contains("LoginFragment")) {
                        return;
                    }
                }
                Fragment loginFragment=   Rigger.getRigger(mActivity).findFragmentByTag(fragmentCls);
                if(loginFragment!=null){
                    Rigger.getRigger(mActivity).showFragment(fragmentCls);
                }else{
                    Rigger.getRigger(mActivity).startFragment(fragment);
                }


            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        }
    }


    //初始化toolbar
    protected void initToolbar(int title, boolean showNavigationIcon, boolean menuItemClick,
                               int menuLayout, final OnMenuClickListener onMenuClickListener) {
        initToolbar(this.getResources().getString(title), showNavigationIcon,
                menuItemClick, onMenuClickListener);
        toolbar.inflateMenu(menuLayout);
    }

    //初始化toolbar
    @SuppressWarnings("ConstantConditions")
    protected void initToolbar(String title, boolean showNavigationIcon, boolean menuItemClick,
                               final OnMenuClickListener onMenuClickListener) {
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar_header = (TextView) rootView.findViewById(R.id.toolbar_title_tv);
        toolbar_header.setText(title);
        setHasOptionsMenu(true);
        ((BaseActivity) mActivity).setSupportActionBar(toolbar);
        ActionBar actionBar = ((BaseActivity) mActivity).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        if (showNavigationIcon) {
            toolbar.setNavigationIcon(R.drawable.back);
            // Menu item click 的監聽事件一樣要設定在 setSupportActionBar 才有作用
            // toolbar.setOnMenuItemClickListener(onMenuItemClick);
            //设置toolbar右侧的点击事件
            toolbar.setNavigationOnClickListener(v ->
                    Rigger.getRigger(this).close());
        }
        if (menuItemClick) {
            toolbar.setOnMenuItemClickListener(item -> {
                onMenuClickListener.onItemClick(item);
                return false;
            });
        }
    }

    //点击事件的回调
    public interface OnMenuClickListener {
        void onItemClick(MenuItem item);
    }


    /**
     * 视图真正可见的时候才调用
     */

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // 对于默认 tab 和 间隔 checked tab 需要等到 isViewCreated = true 后才可以通过此通知用户可见
        // 这种情况下第一次可见不是在这里通知 因为 isViewCreated = false 成立,等从别的界面回到这里后会使用 onFragmentResume 通知可见
        // 对于非默认 tab mIsFirstVisible = true 会一直保持到选择则这个 tab 的时候，因为在 onActivityCreated 会返回 false
        if (isViewCreated) {
            if (isVisibleToUser && !currentVisibleState) {
                LogUtils.e("setUserVisibleHint true");
                dispatchUserVisibleHint(true);
            } else if (!isVisibleToUser && currentVisibleState) {
                LogUtils.e("setUserVisibleHint false");
                dispatchUserVisibleHint(false);
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && mImmersionBar != null)
            mImmersionBar.init();
        LogUtils.e(getClass().getSimpleName() + "  onHiddenChanged dispatchChildVisibleState  hidden " + hidden);
        LogUtils.e("onHiddenChanged");
        if (hidden) {
            dispatchUserVisibleHint(false);
        } else {
            dispatchUserVisibleHint(true);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isViewCreated = true;
        // !isHidden() 默认为 true  在调用 hide show 的时候可以使用
        if (!isHidden() && getUserVisibleHint()) {
            dispatchUserVisibleHint(true);
        }
    }


    /**
     * 统一处理 显示隐藏
     *
     * @param visible
     */
    private void dispatchUserVisibleHint(boolean visible) {
        //当前 Fragment 是 child 时候 作为缓存 Fragment 的子 fragment getUserVisibleHint = true
        //但当父 fragment 不可见所以 currentVisibleState = false 直接 return 掉
        // 这里限制则可以限制多层嵌套的时候子 Fragment 的分发
        if (visible && isParentInvisible()) return;

        //此处是对子 Fragment 不可见的限制，因为 子 Fragment 先于父 Fragment回调本方法 currentVisibleState 置位 false
        // 当父 dispatchChildVisibleState 的时候第二次回调本方法 visible = false 所以此处 visible 将直接返回
        if (currentVisibleState == visible || !isViewCreated) {
            return;
        }
        currentVisibleState = visible;
        if (visible) {
            if (mIsFirstVisible) {
                mIsFirstVisible = false;
                onFragmentFirstVisible();
            }
            onFragmentResume();
            dispatchChildVisibleState(true);
        } else {
            dispatchChildVisibleState(false);
            onFragmentPause();
        }
    }


    /**
     * 用于分发可见时间的时候父获取 fragment 是否隐藏
     *
     * @return true fragment 不可见， false 父 fragment 可见
     */
    private boolean isParentInvisible() {
        Fragment parentFragment = getParentFragment();
        if (parentFragment instanceof BaseRiggerFragment) {
            BaseRiggerFragment fragment = (BaseRiggerFragment) parentFragment;
            return !fragment.isSupportVisible();
        } else {
            return false;
        }
    }

    /**
     * 当前 Fragment 是 child 时候 作为缓存 Fragment 的子 fragment 的唯一或者嵌套 VP 的第一 fragment 时 getUserVisibleHint = true
     * 但是由于父 Fragment 还进入可见状态所以自身也是不可见的， 这个方法可以存在是因为庆幸的是 父 fragment 的生命周期回调总是先于子 Fragment
     * 所以在父 fragment 设置完成当前不可见状态后，需要通知子 Fragment 我不可见，你也不可见，
     * <p>
     * 因为 dispatchUserVisibleHint 中判断了 isParentInvisible 所以当 子 fragment 走到了 onActivityCreated 的时候直接 return 掉了
     * <p>
     * 当真正的外部 Fragment 可见的时候，走 setVisibleHint (VP 中)或者 onActivityCreated (hide show) 的时候
     * 从对应的生命周期入口调用 dispatchChildVisibleState 通知子 Fragment 可见状态
     *
     * @param visible
     */
    private void dispatchChildVisibleState(boolean visible) {
        // FragmentManager childFragmentManager = getChildFragmentManager();
        FragmentManager childFragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        List<Fragment> fragments = childFragmentManager.getFragments();
        FragmentManager childFragmentManager1 = getChildFragmentManager();
        if (childFragmentManager1.getFragments().size() > 0) {
            fragments = childFragmentManager1.getFragments();
        }

        if (!fragments.isEmpty()) {
            for (Fragment child : fragments) {
                if (child instanceof BaseRiggerFragment && !child.isHidden() && child.getUserVisibleHint()) {
                    ((BaseRiggerFragment) child).dispatchUserVisibleHint(visible);
                }
            }
        }
    }

    public void onFragmentFirstVisible() {
        LogUtils.e(getClass().getSimpleName() + "  对用户第一次可见");

    }

    public void onFragmentResume() {
        LogUtils.e(getClass().getSimpleName() + "  对用户可见");
       /* FragmentManager childFragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        List<Fragment> fragments = childFragmentManager.getFragments();
        FragmentManager childFragmentManager1 = getChildFragmentManager();
        if (childFragmentManager1.getFragments().size() > 0) {
            fragments = childFragmentManager1.getFragments();
        }
        if (fragments.size() > 0) {
            if (!fragments.isEmpty()) {
                for (Fragment child : fragments) {
                    if (child instanceof BaseRiggerFragment && !child.isHidden() && child.getUserVisibleHint()) {
                        //TODO 当前值改变，页面数据要做刷新
                        if (BaseApplication.isTokenRefresh) {
                            tokenInvalidRefresh();
                        }
                    }
                }
                //TODO 当页面数据刷新后这个值改变掉， 不然下次进来还是原来的值，一直会被刷新
               // BaseApplication.isTokenRefresh=false;
            }
        }
*/

    }


    //首页中点击新手回调
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void newsClick(LoginEvent event) {
        tokenInvalidRefresh();
    }


    public void onFragmentPause() {
        LogUtils.e(getClass().getSimpleName() + "  对用户不可见");
    }


    private boolean isSupportVisible() {
        return currentVisibleState;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) mUnbinder.unbind();
        if (mPresenter != null)
            mPresenter.detachView();


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(  EventBus.getDefault()!=null){
            EventBus.getDefault().unregister(this);
        }

        if (mImmersionBar != null)
            mImmersionBar.destroy();
        Stack<String> fragmentStack = Rigger.getRigger(mActivity).getFragmentStack();
        if (fragmentStack != null && fragmentStack.size() > 0) {
            for (int i = 0; i < fragmentStack.size(); i++) {
                if (fragmentStack.get(i).equals(this.getClass().getName())) {
                    fragmentStack.remove(i);
                }
            }
        }
    }


    /**
     * 如果是fragment自己的跳转，用这个， 第三方的用activity的自定义的方法
     * * @param requestCode
     *
     * @param resultCode
     * @param args
     */
    public void onFragmentResult(int requestCode, int resultCode, Bundle args) {

    }


}
