package com.jaydenxiao.common.baseapp;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.jaydenxiao.common.exception.Cockroach;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.interfaces.BetaPatchListener;
import com.tencent.bugly.beta.upgrade.UpgradeStateListener;


/**
 * APPLICATION
 */
public class BaseApplication extends MultiDexApplication {

    private static BaseApplication baseApplication;


    public static Context getAppContext() {
        return baseApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        baseApplication = this;
        initTinker();
        
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);
        // 安装tinker
//        Beta.installTinker();

    }
     /**
      * 初始化tinker
      */
    private void initTinker(){
        try {
            Beta.canShowUpgradeActs.add((Class<? extends Activity>) Class.forName("com.radioapp.liaoliaobao.module.index.MainActivity"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Log.e("ces","失败");
        }


        long start = System.currentTimeMillis();
        // 设置开发设备，默认为false，上传补丁如果下发范围指定为“开发设备”，需要调用此接口来标识开发设备
        Bugly.setIsDevelopmentDevice(this, true);
        // 多渠道需求塞入
        String channel = getAppMetaData("UMENG_CHANNEL");
        Bugly.setAppChannel(baseApplication, channel);
        // 这里实现SDK初始化，appId替换成你的在Bugly平台申请的appId,调试时将第三个参数设置为true
        Bugly.init(this, "c4351b614e", false);
      //  CrashReport.initCrashReport(getApplicationContext(), "9b80a09069", false);
        long end = System .currentTimeMillis();
           Log.e("init time--->", end - start + "ms");
        // ，正式 5b88e9a08f4a9d206d00007d
      //  MobclickAgent.UMAnalyticsConfig config=new MobclickAgent.UMAnalyticsConfig(this,"5b88e9a08f4a9d206d00007d",channel, MobclickAgent.EScenarioType. E_UM_NORMAL);
       // MobclickAgent. startWithConfigure(config);
    }




    /**
     * 获取application中指定的meta-data
     *
     * @return 如果没有获取成功(没有对应值，或者异常)，则返回值为空
     */
    public static String getAppMetaData(String key) {
        if (TextUtils.isEmpty(key)) return "";
        String resultData = "";
        try {
            Context appContext = BaseApplication.getAppContext();
            PackageManager packageManager = appContext.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(
                    appContext.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null && applicationInfo.metaData != null)
                    resultData = applicationInfo.metaData.get(key) + "";
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            resultData="";
        }
        return resultData;
    }


    /** 异常处理（全局监听） **/
    public void initexception() {
        Cockroach.install(new Cockroach.ExceptionHandler() {
            // handlerException内部建议手动try{  你的异常处理逻辑  }catch(Throwable e){ } ，以防handlerException内部再次抛出异常，导致循环调用handlerException
            @Override
            public void handlerException(final Thread thread, final Throwable throwable) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            throwable.printStackTrace();
                        } catch (Throwable e) {}
                    }
                });
            }
        });
    }

}
