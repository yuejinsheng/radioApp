package com.jaydenxiao.common.url;

import android.text.TextUtils;
import android.util.Log;

import com.jaydenxiao.common.manager.RetrofitManager;
import com.jaydenxiao.common.manager.ServiceManager;

/**
 * 常量
 * 链接修改
 */
public class BaseConstant {

    /**
     * 后台的url
     **/
    public static String BASEURL = "http://apixd.liaoliaobao.cn/api/v1/";   //正式环境
   // public static String BASEURL="http://nightapi.yueyeapp.cn/api/v1/"; //测试环境


   //public static String IMAGEURL = "https://yuenginx.oss-cn-shenzhen.aliyuncs.com/"; //图片前缀 测试
     public static String IMAGEURL ="https://llbao123.oss-cn-shenzhen.aliyuncs.com/";//正式地址




    /**
     * 设置线上地址
     *
     * @param baseurl
     * @param
     */
    public static void setBaseurl(String baseurl) {
        if (!TextUtils.isEmpty(baseurl)) {
            BASEURL = baseurl;
            RetrofitManager.setClearRetrofit();
            ServiceManager.clearServiceMap();
        }
    }

    public static String getBaseUrl() {
        if (TextUtils.isEmpty(BASEURL)) {
            Log.e("base地址：", BASEURL);
            return BASEURL;
        }
        return BASEURL;
    }

}
