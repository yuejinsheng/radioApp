package com.jaydenxiao.common.cache;

import android.util.Log;

import com.jaydenxiao.common.baseapp.BaseApplication;
import com.jaydenxiao.common.commonutils.NetWorkUtils;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * @author
 */

public class CacheInterceptor implements Interceptor {

    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String url = request.url().toString();
        RequestBody requestBody = request.body();
        Charset charset = Charset.forName("UTF-8");
        StringBuilder sb = new StringBuilder();
        sb.append(url);
        Response response;
        if (NetWorkUtils.isNetConnected(BaseApplication.getAppContext())) {
            int maxAge = 60 * 60*24;
            //如果网络正常，执行请求。
            Response originalResponse = chain.proceed(request);
            //获取MediaType，用于重新构建ResponseBody
            MediaType type = originalResponse.body().contentType();

            response = originalResponse.newBuilder()
                .removeHeader("Pragma")
                .removeHeader("Cache-Control")
                .header("Cache-Control", "public, max-age=" + maxAge)
                .build();

                Buffer buffer = new Buffer();
                try {
                    requestBody.writeTo(buffer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            sb.append(buffer.readString(charset));
            buffer.close();
            ResponseBody responseBody = response.body();
            MediaType contentType = responseBody.contentType();

            BufferedSource source = responseBody.source();
            source.request(Long.MAX_VALUE);
            Buffer buffer1 = source.buffer();

            if (type != null) {
                charset = type.charset(Charset.forName("UTF-8"));
            }
            String key = sb.toString();
            String json = buffer1.clone().readString(charset);
            CacheManager.getInstance().putCache(key, json);
           // Log.d(CacheManager.TAG, "put cache-> key:" + key + "-> json:" + json);
        } else {
          //  Log.d("OkHttp", "request:" + url);
          //  Log.d("OkHttp", "request method:" + request.method());
          //  Log.d("OkHttp", "response body:" + "");
            if (request.method().equals("POST")) {
                MediaType contentType = requestBody.contentType();
                if (contentType != null) {
                    charset = contentType.charset(Charset.forName("UTF-8"));
                }
                Buffer buffer = new Buffer();
                try {
                    requestBody.writeTo(buffer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                sb.append(buffer.readString(charset));
               // buffer.close();
            }

            String cache = CacheManager.getInstance().getCache(sb.toString());
            int maxStale = 60 * 60 * 24 * 28;
            //构建一个新的response响应结果
            response = new Response.Builder()
                .removeHeader("Pragma")
                .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                .body(ResponseBody.create(MediaType.parse("application/json"), cache.getBytes()))
                .request(request)
                .protocol(Protocol.HTTP_1_1)
                .message("缓存网络")
                .code(200)
                .build();
        }
        return response;
    }

}
