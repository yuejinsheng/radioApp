package com.jaydenxiao.common.baserx;

/**
 * des:token失效
 * Created by xsf
 * on 2016.09.10:16
 */
public class TokenOffException extends Exception{

    public TokenOffException(String msg){
        super(msg);
    }

}
