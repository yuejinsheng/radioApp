package com.jaydenxiao.common.baserx;

import android.net.ParseException;

import com.google.gson.JsonParseException;

import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * 功能：
 * 描述
 * Created by yue on 2019-11-08
 */
public class CustomException {
    public static final Integer MOBILE_REQUIRED = 10001;  //手机号码必填
    public static final Integer MOBILE_EXIST = 10002;  //手机号码已存在
    public static final Integer MOBILE_INCORRECT_FORMAT = 10003;  //手机号码格式不正确
    public static final Integer MOBILE_SMS_HAS_BEEN_SENT = 10004;  //手机验证码已发送
    public static final Integer MOBILE_SMS_FAILED_TO_SEND = 10005;  //手机验证码发送失败
    public static final Integer MOBILE_SMS_CODE_REQUIRED = 10006;  //手机验证码必填
    public static final Integer MOBILE_SMS_CODE_ERROR = 10007;  //手机验证码不正确
    public static final Integer REGISTER_SOURCE_REQUIRED = 10008;  //注册来源必填
    public static final Integer REGISTER_SOURCE_ERROR = 10009;  //注册来源填写不正确
    public static final Integer PASSWORD_REQUIRED = 10010;  //密码必填
    public static final Integer PASSWORD_LENGTH_TOO_LONG = 10011;  //密码长度不能超过30位
    public static final Integer PASSWORD_LENGTH_TOO_SHORT = 10012;  //密码长度最小6位
    public static final Integer AUTH_CLIENT_NOT_EXISTS = 10013;  //oauth client 不存在
    public static final Integer AUTH_TOKEN_CREATE_FAILED = 10014;  //请求access token失败
    public static final Integer AUTH_REFRESH_TOKEN_REQUIRED = 10015;  //缺少更新令牌
    public static final Integer UPLOAD_AVATAR_FAILED = 10016;  //上传头像失败
    public static final Integer DEVICE_NO_REQUIRED = 10017;  //app设备码必填
    public static final Integer PRIVACY_IS_PUBLIC_REQUIRED = 10018;  //隐私设备是否公开资料
    public static final Integer PRIVACY_HIDE_LOCATION_REQUIRED = 10019;  //隐私设备是否隐藏地理位置
    public static final Integer PRIVACY_IS_VISIBLE_REQUIRED = 10020;  //隐私设备是否可见（隐身）
    public static final Integer PRIVACY_HIDE_SOCIAL_ACCOUNT_REQUIRED = 10021;  //隐私设备隐藏社交帐号
    public static final Integer PRIVACY_VIEW_ALBUM_PRICE_REQUIRED = 10022;  //隐私设备查看相册费用
    public static final Integer PRIVACY_VIEW_ALBUM_PRICE_ERROR = 10023;  //相册费用必须为1至100元
    public static final Integer LNG_REQUIRED = 10024; //经度必填
    public static final Integer LAT_REQUIRED = 10025; //纬度必填
    public static final Integer LNG_MUST_NUMERIC = 10026; //经度必须为整数
    public static final Integer LAT_MUST_NUMERIC = 10027; //纬度必须为整数
    public static final Integer GENDER_ERROR = 10028; //性别填写错误
    public static final Integer NICKNAME_ERROR = 10029;//昵称填写不正确
    public static final Integer CAREER_ERROR = 10030; //职业填写不正确
    public static final Integer BIRTH_AT_ERROR = 10031; //出生年月日填写不正确
    public static final Integer WECHAT_ERROR = 10032;//微信填写错误
    public static final Integer QQ_ERROR = 10033;//qq填写错误
    public static final Integer BWH_STR_ERROR = 10034;//三围填写错误
    public static final Integer DATING_TYPES_REQUIRED = 10035;//约会节目必填
    public static final Integer DATING_HOPES_REQUIRED = 10036;//约会期望必填
    public static final Integer USER_REGIONS_REQUIRED = 10037;//常用地区必填
    public static final Integer DATING_TYPES_FORMAT = 10038;//约会节目格式错误
    public static final Integer DATING_HOPES_FORMAT = 10039;//约会期望格式错误
    public static final Integer USER_REGIONS_FORMAT = 10040;//地区填写格式不正确
    public static final Integer HEIGHT_INTEGER = 10041; //身高必须为整数
    public static final Integer WEIGHT_INTEGER = 10042; //体重必须为整数
    public static final Integer INVITATION_CODE_DOES_NOT_EXIST = 10043; //邀请码不存在
    public static final Integer ORDER_DOES_NOT_EXIST = 10044; //订单不存在
    public static final Integer ORDER_AMOUNT_IS_INCORRECT = 10045; //订单金额不正确
    public static final Integer ORDER_WAIT_DEAL = 10046; //订单等待处理中
    public static final Integer ORDER_TIMEOUT_ERROR = 10047; //订单处理超时 
    public static final Integer PAY_METHOD_REQUIRED = 10048;  //支付方式必填
    public static final Integer PAY_METHOD_INCORRECT = 10049; //支付方式不正确
    public static final Integer VIP_TYPE_REQUIRED = 10050;    //vip类型必填
    public static final Integer VIP_TYPE_INCORRECT = 10051;    //vip类型不正确
    public static final Integer VERIFIED_ERROR = 10052; //认证参数不正确
    public static final Integer NEW_USER_ERROR = 10053; //新用户参数不正确
    public static final Integer INCORRECT_FILE_FORMAT = 10054;//文件格式不正确
    public static final Integer HAS_BEEN_CERTIFIED = 10055; //已认证
    public static final Integer IS_IN_EXAMINE_NOW = 10056; //正在审核中
    public static final Integer ALIPAY_ACCOUNT_REQUIRED = 10057; //支付宝账号必填
    public static final Integer ALIPAY_REALNAME_REQUIRED = 10058; //支付宝姓名必填
    public static final Integer ALIPAY_REALNAME_INCORRECT_FORMAT = 10059; //支付宝姓名不正确
    public static final Integer ALIPAY_ACCOUNT_ERROR = 10060; //支付宝格式不正确
    public static final Integer MEDIAS_COUNT_OVERSTEP = 10061;//媒体数量超出
    public static final Integer THE_PICTURE_FORMAT_IS_INCORRECT = 10062; //图片格式不正确
    public static final Integer CHECK_AFTER_PAYING_RED = 10063; //红包相册需付费后查看
    public static final Integer HAS_BEEN_FOLLOW = 10064; //已经收藏
    public static final Integer HAS_BEEN_BLACKENED = 10065; //已拉黑
    public static final Integer I_WAS_BLACKENED_BY_HIM = 10066; //我被拉黑
    public static final Integer CHECK_AFTER_PAYING = 10067; //付费后才可以查看
    public static final Integer RESOURCES_HAVE_BEEN_VIEWED = 10068; //资源已被查看过
    public static final Integer CANT_VIEW_SAME_SEX_USER_INFORMATION = 10069; //不能查看同性别用户信息
    public static final Integer USERS_ARE_INVISIBLE = 10070; //用户已隐身
    public static final Integer YOU_NEED_TO_APPLY_TO_THE_USER_BEFORE_YOU_CAN_VIEW_THE_ALBUM = 10071;//需向用户申请后才可查看相册
    public static final Integer MORE_THAN_THREE_TIMES_CHECK_AFTER_PAYING = 10072; //已超过三次，每天只有三次免费查看，请付费后再看
    public static final Integer NO_VIP_ONLY_VIEW_TEN_FRIENDS_DAY = 10073; //非vip一天最多只能查看十个好友
    public static final Integer CITY_ERROR = 10074; //城市填写不正确
    public static final Integer PLEASE_GET_IT_THROUGH_PRIVATE_CHAT_WITH_ME = 10075; //请通过私聊与我获取
    public static final Integer THE_BROADCAST_HAS_BEEN_DELETED = 10076;//广播已删除
    public static final Integer YOU_HAVE_APPLIED_FOR_THIS_BROADCASTING_SERVICE = 10077; //你已申请过该条广播
    public static final Integer THE_APPOINTMENT_HAS_BEEN_AUDITED = 10078; //该广播你已审核过
    public static final Integer ALREADY_LIKE = 10079; //已点过赞
    public static final Integer HAVE_SUBMITTED_THE_APPLICATION = 10080; //你已提交了申请，请等待管理员审核后再操作！
    public static final Integer PLEASE_PAY_FOR_THE_BROADCAST = 10081; //请付费后再发布广播
    public static final Integer YOU_HAVE_ALREADY_PAID_THE_FEE = 10082; //你已经付了费
    public static final Integer INCORRECT_PARAMETER_TRANSFER = 60001; //传输参数不正确
    public static final Integer NO_ACTIVATE_USER = 40002;//用户未激活
    public static final Integer LOGIN_FAILED = 40001;    //登录失败:账号或密码错误
    public static final Integer UNAUTHENTICATED = 401;      //未授权
    public static final Integer SQL_ERROR = 20001;  //数据库执行错误
    public static final Integer SERVER_INTERNAL_ERROR = 500;    //服务器内部错误
    public static final Integer TOKENOFF=201;//token失效状态

}
