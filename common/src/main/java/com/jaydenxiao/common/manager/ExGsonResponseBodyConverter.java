/*
 * @version 1.0
 * @date 17-9-11 上午10:04
 * Copyright 杭州优谷数据技术有限公司   All Rights Reserved
 *  未经授权不得进行修改、复制、出售及商业使用
 */

package com.jaydenxiao.common.manager;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaydenxiao.common.baseapp.AppConfig;
import com.jaydenxiao.common.baseapp.BaseApplication;
import com.jaydenxiao.common.basebean.BaseRespose;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class ExGsonResponseBodyConverter<T> implements Converter<ResponseBody,T> {

    private Gson gson;
    private Type type;

    ExGsonResponseBodyConverter(Gson gson, Type type) {
        this.gson = gson;
        this.type = type;
    }

    /** 进行解析预处理操作 **/
    @Override
    public T convert(ResponseBody responseBody) throws IOException {
        String value = responseBody.string();
        try {
            JSONObject response = new JSONObject(value);
            int code = response.getInt("code");
            if(code==200){
                return gson.fromJson(value, type);
                //107,token失效
            }else{
                return gson.fromJson(value, new TypeToken<BaseRespose<String>>(){}.getType());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
            return gson.fromJson(value, type);
    }

}
