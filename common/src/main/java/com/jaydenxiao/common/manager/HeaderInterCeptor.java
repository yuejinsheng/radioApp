package com.jaydenxiao.common.manager;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.jaydenxiao.common.compressorutils.MD5;
import com.jaydenxiao.common.compressorutils.MD5Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * Created by 乐童鞋 on 2018/4/16 16:09
 *   公共请求参数( 以json格式提交参数)
 */

public class HeaderInterCeptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        String method = original.method();
        HttpUrl url = original.url();
        Request request = null;
        Request.Builder builder = original.newBuilder();
        //请求加密
        if (method.equals("GET")) {
            request = builder.addHeader("Connection", "close").url(original.url()).build();
        } else if (method.equals("POST")) {
            if (original.body() instanceof RequestBody) {
                RequestBody requestBody = original.body();
                Buffer buffer = new Buffer();
                requestBody.writeTo(buffer);
                Charset charset = Charset.forName("UTF-8");
                MediaType contentType = requestBody.contentType();
                if (contentType != null) {
                    charset = contentType.charset(Charset.forName("UTF-8"));
                }
                String paramsStr = buffer.readString(charset);
               //添加加密后的参数
               Gson gson = new GsonBuilder().
                   //gson默认把int转化成double的处理
                registerTypeAdapter(Double.class, new JsonSerializer<Double>() {

                    @Override
                    public JsonElement serialize(Double src, Type typeOfSrc, JsonSerializationContext context) {
                        if (src == src.longValue())
                            return new JsonPrimitive(src.longValue());
                        return new JsonPrimitive(src);
                    }
                }).disableHtmlEscaping().create();
               Map<Object, Object> map = gson.fromJson(paramsStr, new TypeToken<Map<Object, Object>>() {
                }.getType());
//                String token = "",version = "";
//                HttpUrl httpUrl = original.url();
//                String str = httpUrl.toString() +";token=" + token + ";version=" + version;
//                String md5_encode = MD5Utils.MD5Encode(str);
//                map.put("token",token);
//                map.put("version",version);
//                map.put("apikey",md5_encode);
//                Log.i("===headerurl===",httpUrl.toString());
                String result = gson.toJson(map);
                RequestBody newsRequestBody = RequestBody.create(MediaType.parse("application/json; charset=UTF-8"), result);
                request = builder.method(original.method(), newsRequestBody).build();
            }
        }
        return chain.proceed(request);
    }
}
