package com.jaydenxiao.common.manager;

import android.support.v4.util.ArrayMap;

/**
 * Created by xdj on 16/3/14.
 * 接口管理
 */
public class ServiceManager {

    private static  ArrayMap<Class, Object> mServiceMap = new ArrayMap<>();

    /**
     * 创建有关加密参数和拦截跳转的retrofit+okHttp的创建，返回关于retrofit接口观察者对象
     * @param serviceClass
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T create(Class<T> serviceClass) {
        Object service = mServiceMap.get(serviceClass);
        if (service == null) {
            service = RetrofitManager.INSTANCE.net().create(serviceClass);
            mServiceMap.put(serviceClass, service);
        }
        return (T) service;
    }

    /**
     * 换beseurl
     */
    public static void clearServiceMap(){
        if(mServiceMap!=null){
            mServiceMap.clear();
        }
    }
}
