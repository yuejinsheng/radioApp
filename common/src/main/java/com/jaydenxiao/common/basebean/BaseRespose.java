/*
 * @version 1.0
 * @date 17-9-11 上午10:04
 * Copyright 杭州优谷数据技术有限公司   All Rights Reserved
 *  未经授权不得进行修改、复制、出售及商业使用
 */

package com.jaydenxiao.common.basebean;

import android.text.TextUtils;

/**
 * des:封装服务器返回数据
 * Created by xsf
 */
public class BaseRespose<T> {
    private Integer code;
    private String msg;
    private T  meta; //数据


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getMeta() {
        return meta;
    }

    public void setMeta(T meta) {
        this.meta = meta;
    }
}
