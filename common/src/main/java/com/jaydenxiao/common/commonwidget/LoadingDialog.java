package com.jaydenxiao.common.commonwidget;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jaydenxiao.common.R;

/**
 * description:弹窗浮动加载进度条
 * Created by xsf
 * on 2016.07.17:22
 */
public class LoadingDialog {

    /** 加载数据对话框 */
    private static Dialog mLoadingDialog;
    private static RotateAnimation refreshAnimation;
    /**
     * 显示加载对话框
     * @param activity 上下文
     * @param msg 对话框显示内容
     * @param cancelable 对话框是否可以取消
     */
    public static Dialog showDialogForLoading(Activity activity, String msg, boolean cancelable) {
        if (mLoadingDialog == null) {
            View view = LayoutInflater.from(activity).inflate(R.layout.dialog_loading, null, false);
             ImageView loading_image=view.findViewById(R.id.loading_image);
            runRefreshAnimal(loading_image);
            mLoadingDialog = new Dialog(activity, R.style.CustomProgressDialog);
            mLoadingDialog.setCancelable(cancelable);
            mLoadingDialog.setCanceledOnTouchOutside(false);
            mLoadingDialog.setContentView(view, new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            mLoadingDialog.show();
        }
        return mLoadingDialog;
    }

    public static Dialog showDialogForLoading(Activity activity) {
        if (mLoadingDialog == null) {
            View view = LayoutInflater.from(activity).inflate(R.layout.dialog_loading, null, false);
            ImageView loading_image=view.findViewById(R.id.loading_image);
            runRefreshAnimal(loading_image);
            mLoadingDialog = new Dialog(activity, R.style.CustomProgressDialog);
            mLoadingDialog.setCancelable(true);
            mLoadingDialog.setCanceledOnTouchOutside(false);
            mLoadingDialog.setContentView(view, new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            mLoadingDialog.show();
        }
        return mLoadingDialog;
    }

    /** 关闭加载对话框 **/
    public static void cancelDialogForLoading() {
        if (mLoadingDialog != null) {
            mLoadingDialog.cancel();
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
            if(refreshAnimation!=null){
                refreshAnimation.cancel();
            }
        }
    }

    // 加载图片
    public static  void runRefreshAnimal(ImageView imageView) {
         refreshAnimation = new RotateAnimation(0, 360,
            Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
            0.5f);
        refreshAnimation.setRepeatCount(-1);
        refreshAnimation.setRepeatMode(Animation.RESTART);
        refreshAnimation.setDuration(1000);
        refreshAnimation.setInterpolator(new LinearInterpolator());
        imageView.startAnimation(refreshAnimation);
        refreshAnimation.startNow();
    }

}
