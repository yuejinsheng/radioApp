package com.jaydenxiao.common.commonwidget.refreshHeader;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.jaydenxiao.common.R;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshKernel;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;

/**
 * Created by solo on 2018/1/31.
 * SmartRefreshLayout 的自定义下拉刷新 Header
 */

public class CustomRefreshHeader extends RelativeLayout implements RefreshHeader {

    private ImageView mImage;
    private AnimationDrawable refreshingAnim;
    private AnimationDrawable pullDownAnim;
    protected SpinnerStyle mSpinnerStyle = SpinnerStyle.Translate;

    private boolean hasSetPullDownAnim = false;
    public CustomRefreshHeader(Context context) {
        super(context);
        initView();
    }

    public CustomRefreshHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CustomRefreshHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    private void initView(){
        View view = View.inflate(getContext(), R.layout.widget_custom_refresh_header, this);
        view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.alpha_20_white));
        mImage = (ImageView) view.findViewById(R.id.iv_refresh_header);
        mImage.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.alpha_20_white));
    }




    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @Override
    public SpinnerStyle getSpinnerStyle() {
        return mSpinnerStyle;
    }



    /**
     * 状态改变时调用。在这里切换第三阶段的动画卖萌小人
     * @param refreshLayout
     * @param oldState
     * @param newState
     */
    @Override
    public void onStateChanged(RefreshLayout refreshLayout, RefreshState oldState, RefreshState newState) {
        switch (newState) {
            case PullDownToRefresh: //下拉刷新开始。正在下拉还没松手时调用
                //每次重新下拉时，将图片资源重置为小人的大脑袋
                  mImage.setScaleX(1f);
                 mImage.setScaleY(1f);
                break;
            case Refreshing: //正在刷新。只调用一次
                //状态切换为正在刷新状态时，设置图片资源为小人卖萌的动画并开始执行
               // mImage.setImageResource(R.drawable.anim_pull_refreshing);
                refreshingAnim = (AnimationDrawable) mImage.getDrawable();
                refreshingAnim.start();
                break;
            case ReleaseToRefresh:
               // mImage.setImageResource(R.drawable.anim_pull_refreshing);
                pullDownAnim = (AnimationDrawable) mImage.getDrawable();
                mImage.setScaleX(1f);
                mImage.setScaleY(1f);
                pullDownAnim.start();
                break;
        }
    }


    /**
     * 动画结束后调用
     */
    @Override
    public int onFinish(RefreshLayout layout, boolean success) {
        // 结束动画
        if (pullDownAnim != null && pullDownAnim.isRunning()) {
            pullDownAnim.stop();
        }
        if (refreshingAnim != null && refreshingAnim.isRunning()) {
            refreshingAnim.stop();
        }
        invalidate();
        //重置状态
        hasSetPullDownAnim = false;
        return 0;
    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {

    }

    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public void setPrimaryColors(int... colors) {
    }

    @Override
    public void onInitialized(@NonNull RefreshKernel kernel, int height, int extendHeight) {

    }

    @Override
    public void onMoving(boolean isDragging, float percent, int offset, int height, int extendHeight) {
                // 下拉的百分比小于100%时，不断调用 setScale 方法改变图片大小
      if (percent < 1) {
        //   mImage.setScaleX(percent);
         // mImage.setScaleY(percent);
            //是否执行过翻跟头动画的标记
            if (hasSetPullDownAnim) {
                hasSetPullDownAnim = false;
            }

        }

        //当下拉的高度达到Header高度100%时，开始加载正在下拉的初始动画，即翻跟头
       /*if (percent >= 1.0) {
            //因为这个方法是不停调用的，防止重复
            if (!hasSetPullDownAnim) {
                mImage.setImageResource(R.drawable.anim_pull_refreshing);
                pullDownAnim = (AnimationDrawable) mImage.getDrawable();
                mImage.setScaleX(1f);
                mImage.setScaleY(1f);
                pullDownAnim.start();
                hasSetPullDownAnim = true;
            }
        }*/
    }

    @Override
    public void onReleased(@NonNull RefreshLayout refreshLayout, int height, int extendHeight) {

    }

    @Override
    public void onStartAnimator(RefreshLayout layout, int height, int extendHeight) {

    }
}
