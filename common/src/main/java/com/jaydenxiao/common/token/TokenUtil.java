package com.jaydenxiao.common.token;

import com.jaydenxiao.common.sharepref.SharePref;

/**
 * 功能： 保存token参数
 * 描述
 * Created by yue on 2019-07-16
 */
public class TokenUtil {
    public static final String TOKEN_TYPE = "token_type";
    public static final String ACCESS_TOKEN = "access_token";



    public static void putTokenType(String token_type) {
        SharePref.saveString(TOKEN_TYPE, token_type);
    }

    public static String getTokenType() {
        return SharePref.getString(TOKEN_TYPE, "");
    }

    /**
     * 保存AccessToken
     * @param access_token
     */
    public static void putAccessToken(String access_token) {
        SharePref.saveString(ACCESS_TOKEN, access_token);
    }

    /**
     * 获取AccessToken
     * @return
     */
    public static String getAccessToken() {
        return SharePref.getString(ACCESS_TOKEN, "");
    }


    /**
     * 设置头部值
     * @return
     */
    public static String getAuthorization() {
        return getTokenType() + " " + getAccessToken();
    }
}
